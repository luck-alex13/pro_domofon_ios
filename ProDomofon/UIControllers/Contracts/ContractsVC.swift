//
//  ContractsVC.swift
//  ProDomofon
//
//  Created by Александр Новиков on 16/06/2019.
//  Copyright © 2019 Александр Новиков. All rights reserved.
//

import UIKit

class ContractsVC: TableVC<Contract>, ContractSelectionProtocol {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        setBar(title: "Договоры", rightTitle: "Добавить", rightSelector: #selector(newConractTapped))
        
        setUpRefreshControl()
        defineVariables(sections: 1, cellIdentifer: "conractItem", placeholderTitle: "Здесь будут отбражаться ваши договоры.", placeholderSubtile: "Добавьте новый договор и он отобразится в списке.")
        tableView.estimatedRowHeight = 170
        tableView.rowHeight = UITableView.automaticDimension
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if let nav = navigationController as? MainNavigationVC {
            nav.tryAuth()
        }
    }
    
    @objc
    func newConractTapped() {
        Utils.shared.openUrl(url: StringConst.ADD_CONTRACT_TUTOTIAL.rawValue)
    }
    
    override func loadDataFromServer(_ refreshControl: UIRefreshControl?) {
        refreshControl?.beginRefreshing()
        RestClient.shared.getUserAndContracts(success: { [weak self] (user) in
            guard let _ = self else { return }
            refreshControl?.endRefreshing()
            DataStorage.shared.contracts = user.contracts.sorted(by: { c1, c2 in
                return c1.number < c2.number
            })
        }) { [weak self]  (err) in
            refreshControl?.endRefreshing()
            Utils.shared.showToastHttp(from: self?.navigationController, with: "Не удалось загрузить договоры.", error: err)
        }
    }
    
    override func readDataFromDatabase() {
        self.dataArray = DataStorage.shared.contracts
        self.updateTableView()
    }
    
    // MARK: - Table view data source
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifer!, for: indexPath) as! ContactViewCell
        cell.setupCell(with: dataArray[indexPath.row])
        cell.delegate = self
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        onSelected(item: dataArray[indexPath.row])
    }
    
    func onSelected(item: Contract) {
        if item.id.count > 0 {
            DataStorage.shared.choosenContractString = item.id
            AppDelegate.shared.openVC(type: .contractInfo)
        }
    }

    func onSelectedCamera(item: Contract) {
        if item.id.count > 0, let devices = item.devices, devices.count > 0 {
            let devicePanels = item.deviceCameraPanels
            if devicePanels.count == 1 {
                if let nvc = Utils.shared.additionalStoryboard().instantiateViewController(withIdentifier: DeviceVC.className) as? DeviceVC {
                    nvc.contract = item
                    nvc.devicePanel = devicePanels[0]
                    self.navigationController?.pushViewController(nvc, animated: true)
                }
            } else {
                if let nvc = Utils.shared.additionalStoryboard().instantiateViewController(withIdentifier: DevicesVC.className) as? DevicesVC {
                    nvc.contracts = [item]
                    self.navigationController?.pushViewController(nvc, animated: true)
                }
            }
        }
    }
}
