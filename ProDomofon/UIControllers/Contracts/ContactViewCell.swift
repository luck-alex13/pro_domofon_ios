//
//  ContactViewCell.swift
//  ProDomofon
//
//  Created by Александр Новиков on 17/06/2019.
//  Copyright © 2019 Александр Новиков. All rights reserved.
//

import UIKit

class ContactViewCell: UITableViewCell {

    @IBOutlet weak var idLabelButton: UIButton!
    @IBOutlet weak var cameraButton: UIButton!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var balanceLabel: UILabel!
    @IBOutlet weak var toPayLabel: UILabel!
    @IBOutlet weak var openDoorBtn: UIButton!
    @IBOutlet weak var itemProgress: UIActivityIndicatorView!
    @IBOutlet weak var moreInfoBtn: UIButton!
    
    var contract: Contract!
    
    fileprivate var opening = false
    weak var delegate: ContractSelectionProtocol?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        openDoorBtn.setCornerRadius(radius: 6)
        openDoorBtn.setTitle("", for: .disabled)
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func setupCell(with contract: Contract) {
        self.contract = contract
        
        idLabelButton.setTitle("Договор #\(contract.number)", for: .normal)
        addressLabel.text = getAddressText(from: contract)
        balanceLabel.text = "Баланс \(contract.balance ?? 0) Р"
        toPayLabel.text = "К оплате \(contract.toPay) Р"
        
        idLabelButton.setTitleColor(contract.contractColor, for: .normal)
        addressLabel.textColor = geAddressColor(from: contract)
        balanceLabel.textColor = getToPayColor(from: contract)
        toPayLabel.textColor = getToPayColor(from: contract)
        
        toPayLabel.isHidden = contract.deviceId == "" || contract.toPay == 0
        
        moreInfoBtn.setCornerRadius(radius: 6)
        moreInfoBtn.layer.borderWidth = 2
        moreInfoBtn.layer.borderColor = Colors.GREEN_PRIMARY.cgColor
        
        if contract.hasCamera {
            cameraButton.showObject(0)
        } else {
            cameraButton.hideObject(0)
        }
    }
    
    func getAddressText(from contact: Contract) -> String? {
        return contact.deviceId == "" ? "Войдите в подъезд используя КЛЮЧ НОВОСЕЛА" : contact.getReadableAddress
    }
    
    func getToPayColor(from contact: Contract) -> UIColor {
        guard let balance = contact.balance else { return Colors.RED_ACCENT }
        return balance <= 0 || contact.toPay > 0 ? Colors.RED_ACCENT : Colors.GREEN
    }
    
    func geAddressColor(from contact: Contract) -> UIColor {
        return contact.deviceId == "" ? Colors.RED_ACCENT : UIColor.gray
    }
    
    @IBAction func moreBtnTapped(_ sender: UIButton) {
        if let contract = self.contract {
            delegate?.onSelected(item: contract)
        }
    }
    
    @IBAction func openBtnPapped(_ sender: UIButton) {
        UIView.animate(withDuration: 0.1,
                       animations: {
                        sender.transform = CGAffineTransform(scaleX: 1.05, y: 1.05)
        },
                       completion: { _ in
                        UIView.animate(withDuration: 0.1, animations: {
                            sender.transform = CGAffineTransform.identity
                        }, completion: { [weak self] _ in
                            guard let self = self else { return }
                            self.requestOpenDoor(contract: self.contract)
                        })
                        
        })
    }
    
    func requestOpenDoor(contract: Contract) {
        guard !opening else {
            return
        }
        
        opening = true
        
        runProgress()
        
        RestClient.shared.openDoor(contractNumber: contract.number, reason: .contracts, success: { [weak self] (resp) in
            self?.stopProgress()
            self?.opening = false
            
            if(resp.isSuccess()) {
                self?.finishWithMessage(title: "Дверь успешно открыта!", message: "Входите в подъезд. У вас есть 3 секунды.")
            }else {
                self?.finishWithMessage(title: "Не удалось открыть дверь", message: resp.message ?? "")
            }
            
            }, errorCall: { [weak self] (err) in
                self?.opening = false
                self?.stopProgress()
                debugPrint(err)
                self?.finishWithMessage(title: "Не удалось открыть дверь", subtitle: nil, message: err.message ?? "")
        })
    }
    
    func finishWithMessage(title: String, subtitle: String? = nil, message: String) {
        PushNotificationManager.shared.showNotification(title: title, subtitle: subtitle, body: message)
    }
    
    func runProgress() {
        UIView.animate(withDuration: 0.2,   animations: { [weak self] in
            self?.itemProgress.isHidden = false
            self?.openDoorBtn.isEnabled = false
            },  completion: { [weak self] finish in
                self?.itemProgress.startAnimating()
        })
    }
    
    func stopProgress() {
        UIView.animate(withDuration: 0.2,   animations: { [weak self] in
            self?.itemProgress.isHidden = true
            self?.openDoorBtn.isEnabled = true
            self?.openDoorBtn.titleLabel?.text = "Открыть дверь"
            },  completion: { [weak self] finish in
                self?.itemProgress.stopAnimating()
        })
    }
    
    @IBAction func contractLabelTapped(_ sender: UIButton) {
        if contract.isOwner {
            if contract.confirmed {
                Utils.shared.showToast(from: self.contentView, with: "Вы владелец договора")
            } else {
                Utils.shared.showToast(from: self.contentView, with: "Договор не заключен со стороны собственника.")
            }
        } else {
            Utils.shared.showToast(from: self.contentView, with: "Вы не владелец договора")
        }
    }
    
    @IBAction func cameraTouch(_ sender: Any) {
        guard let contract = contract, contract.hasCamera else { return }
        delegate?.onSelectedCamera(item: contract)
    }
    
    
}

protocol ContractSelectionProtocol: class {
    func onSelected(item: Contract)
    func onSelectedCamera(item: Contract)
}
