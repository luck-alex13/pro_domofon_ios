//
//  NewContractVC.swift
//  ProDomofon
//
//  Created by Александр Новиков on 23/06/2019.
//  Copyright © 2019 Александр Новиков. All rights reserved.
//

import UIKit

class NewContractVC: InputViewController {

    @IBOutlet weak var constrContentHeight: NSLayoutConstraint!
    @IBOutlet weak var contentScroll: UIScrollView!
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var codeField: UITextField!
    @IBOutlet weak var aptNumberField: UITextField!
    @IBOutlet weak var addButton: UIButton! {
        didSet {
            addButton.setImage(UIImage(named: "ic_plus")?.withRenderingMode(.alwaysTemplate), for: .normal)
            addButton.setCornerRadius(radius: 6)
        }
    }
    var kbHelper: KeyboardHelper = KeyboardHelper()
    var connectionObserver = ConnectionObserver()
    
    override func viewDidLoad() {
        super.viewDidLoad(scrollView: contentScroll, constraintContentHeight: constrContentHeight)
        codeField.delegate = self
        aptNumberField.delegate = self
        
        setTitle(title: "Новый договор")
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        //kbHelper.subscribeOnKeyboardNotifications(observer: self)
    }
    
    
    override func viewWillDisappear(_ animated: Bool) {
        //kbHelper.hideKeyboard(from: self)
        //kbHelper.removeObservers()
    }

    @objc func viewTapped() {
        kbHelper.hideKeyboard(from: self)
    }
    
    @IBAction func addTapped(_ sender: UIButton) {
        handleFormParameters(code: codeField.text, flat: aptNumberField.text)
    }
    
    func handleFormParameters(code: String?, flat: String?) {
        kbHelper.hideKeyboard(from: self)
        guard let code = code, code.count > 0 else {
            AlertManager.shared.showAlert(from: self, title: "Ошибка", message: "Введите код доступа!")
            return
        }
        guard let flat = flat, flat.count > 0 else {
            AlertManager.shared.showAlert(from: self, title: "Ошибка", message: "Введите номер квартиры.")
            return
        }
        
        guard connectionObserver.isConnectedToInternet else {
            AlertManager.shared.showAlertNoInternet(from: self)
            return
        }
        
        createNewConract(code: code, flat: flat)
    }
    
    func createNewConract(code: String, flat: String) {
        AlertManager.shared.showProgressDialog(from: self)
        RestClient.shared.newContract(number: code, flat: flat, success: { [weak self] (resp) in
            AlertManager.shared.hideProgress { [weak self] in
                Utils.shared.showToast(from: self, with: "Договор успешно добавлен")
                self?.navigationController?.popViewController(animated: true)
            }
        }) { [weak self] (err) in
            AlertManager.shared.hideProgress(completion: {
                AlertManager.shared.showAlert(from: self, title: "Ошибка", message: Utils.shared.makeHttpErrorMessage(from: "Не удалось добавить договор.", with: err))
            })
        }
    }
    
    
}
