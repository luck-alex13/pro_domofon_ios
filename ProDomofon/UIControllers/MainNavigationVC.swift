//
//  MainNavigationVC.swift
//  ProDomofon
//
//  Created by Алексей Агильдин on 13.03.2021.
//  Copyright © 2021 Александр Новиков. All rights reserved.
//

import UIKit

class MainNavigationVC: UINavigationController {

    var connectionObserver: ConnectionObserver?
    var errorAlert: UIAlertController?
    var wasAuth = false
    
    fileprivate var locker = NSLock()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        AppDelegate.shared.openVC(type: DataStorage.shared.choosenContract != nil ? .contractInfo : .contracts)
//        DataStorage.shared.selectedIndexPath = DataStorage.shared.choosenContract != nil ? IndexPath(row: 1, section: 1) : IndexPath(row: 0, section: 0)
        
        if let index = DataStorage.shared.choosenContractInfoIndex  {
            if let type = MenuVCType.init(rawValue: index) {
                AppDelegate.shared.openVC(type: type)
            } else {
                AppDelegate.shared.openVC(type: .contractInfo)
            }
        } else {
            AppDelegate.shared.openVC(type: .contracts)
        }
        
        connectionObserver = ConnectionObserver()
        
        NotificationCenter.default.addObserver(self, selector: #selector(showVideoCallVC), name: .showVideoCallVC, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(showBetweenVC), name: .showBetweenVC, object: nil)
        
//        NotificationCenter.default.addObserver(self, selector: #selector(showMainTabBarError), name: .showMainTabBarError, object: nil)
        
        if #available(iOS 13.0, *) {
            let app = UINavigationBarAppearance()
            app.backgroundColor = Colors.GREEN_PRIMARY
            navigationBar.scrollEdgeAppearance = app
            navigationBar.standardAppearance = app
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
            PushNotificationManager.shared.requestNotification(vc: self)
        }
        self.showBetweenVC()
    }

    func showErrorNoInternet() {
        errorAlert = UIAlertController(title: "Ошибка авторизации", message: "Нет поключения к сети Интернет. Включите интернет в настройках и попробуйте авторизоваться снова.", preferredStyle: .alert)
        let retryAction = UIAlertAction(title: "Повторить", style: UIAlertAction.Style.default) { [weak self] (act) in
            self?.tryAuth()
        }
        errorAlert!.addAction(retryAction)
        self.present(errorAlert!, animated: true, completion: nil)
    }
    
    func tryAuth() {
        guard let observer = connectionObserver, observer.isConnectedToInternet else {
            showErrorNoInternet()
            return
        }
        
        if isAuthEnabled() {
            if let _ = errorAlert {
                errorAlert?.dismiss(animated: true, completion: { [weak self] in
                    self?.errorAlert = nil
                    self?.auth()
                })
            }else {
                auth()
            }
        }
    }
    
    func auth() {
        AlertManager.shared.showProgressDialog(from: self)
        RestClient.shared.getUserAndContracts(success: { [weak self] (user) in
            AlertManager.shared.hideProgress()
            self?.finishAuth(user: user)
        }) { [weak self] (err) in
            AlertManager.shared.hideProgress(completion: {
                if (err.status == 401){
                    self?.showLogOutDialog()
                }else {
                    Utils.shared.showToastHttp(from: self?.view, with: "Не удалось авторизоваться.", error: err)
                }
            })
        }
    }
    
    func finishAuth(user: User) {
        DataStorage.shared.lastAuthTime = Utils.shared.TimeInSecond + 30 * 60
        wasAuth = true
        DataStorage.shared.user = user
        DataStorage.shared.contracts = user.contracts
        AppDelegate.shared.checkPushSubcsription()
    }
    
    func isAuthEnabled() -> Bool {
        return !wasAuth || Utils.shared.TimeInSecond > DataStorage.shared.lastAuthTime
    }
    
    func showLogOutDialog() {
        AlertManager.shared.showAlert(from: self, title: "Внимание!", message: "При авторизации произошла ошибка. Необходима повторная авторизация") { (act) in
            DataStorage.shared.removeObjectsBeforeLogginingOut()
            AppDelegate.shared.openLoginVCAsRoot()
        }
    }
    
    @objc
    func showVideoCallVC() {
        NSLog("!!!TIME showVideoCallVC \(DispatchTime.now())")
        guard let _ = PushNotificationManager.shared.videoCall else { return }
        locker.lock()
        if let nextViewController = Utils.shared.additionalStoryboard().instantiateViewController(withIdentifier: VideoCallVC.className) as? VideoCallVC {
            self.pushViewController(nextViewController, animated: true)
        }
        locker.unlock()
        return
    }
    
    @objc
    func showMainTabBarError() {
        if let text = PushNotificationManager.shared.debugString {
            AlertManager.shared.hideProgress()
            Utils.shared.showToast(from: self, with: "\(text)")
        }
        PushNotificationManager.shared.debugString = nil
    }
    
    @objc
    func showBetweenVC() {
        NSLog("!!!TIME showBetweenVC \(DispatchTime.now())")
        guard PushNotificationManager.shared.videoCall == nil else { return }
        guard let classNameLastVC = viewControllers.last?.className, ![BetweenVC.className, VideoCallVC.className].contains(classNameLastVC) else { return }
        locker.lock()
        if let nextViewController = Utils.shared.mainStoryboard().instantiateViewController(withIdentifier: BetweenVC.className) as? BetweenVC {
            self.pushViewController(nextViewController, animated: true)
        }
        locker.unlock()
    }
}
