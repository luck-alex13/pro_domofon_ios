//
//  SelectContractVC.swift
//  ProDomofon
//
//  Created by Александр Новиков on 26/07/2019.
//  Copyright © 2019 Александр Новиков. All rights reserved.
//

import UIKit

protocol SelectContractVCDelegate: AnyObject {
    func selectContract(contractId: String, code: String)
}

class SelectContractVC: VCWithTable<Contract> {
    
    @IBOutlet weak var dataTableView: UITableView! {
        didSet {
            dataTableView.delegate = self
            dataTableView.dataSource = self
        }
    }
    @IBOutlet weak var bottomOffsetDataTable: NSLayoutConstraint!
    @IBOutlet weak var infoLabel: UILabel! {
        didSet {
            let attentionText = "ВНИМАНИЕ!"
            let mainText = " На Вашем телефоне зарегистрировано несколько договоров. Один ключ может быть привязан к нескольким адресам и открывать их подъезды.\n\nПосле отметки нужных адресов нажмите кнопку "
            let chooseText = "Выбрать"
            let text = "\(attentionText)\(mainText)\(chooseText)"
            
            let attributedText = NSMutableAttributedString(string: text)
            let fullRange = NSMakeRange(0, attributedText.length)
            let attentionRange = attributedText.mutableString.range(of: attentionText)
            let chooseRange = attributedText.mutableString.range(of: chooseText)
            
            let style = NSMutableParagraphStyle()
            style.alignment = .left
            
            
            attributedText.addAttribute(NSAttributedString.Key.paragraphStyle, value: style, range: fullRange)
            attributedText.addAttribute(NSAttributedString.Key.foregroundColor, value: Colors.BLACK, range: fullRange)
            attributedText.addAttribute(NSAttributedString.Key.font, value: UIFont.systemFont(ofSize: 15), range: fullRange)
            attributedText.addAttribute(NSAttributedString.Key.font, value: UIFont.systemFont(ofSize: 15, weight: .bold), range: attentionRange)
            attributedText.addAttribute(NSAttributedString.Key.font, value: UIFont.systemFont(ofSize: 15, weight: .bold), range: chooseRange)
            
            infoLabel.attributedText = attributedText
        }
    }
    
    var selectedIDs = [InfoParams]()
    var goal: SelectionGoal?
    
    fileprivate var keyWasAdded: Bool = false
    
    enum SelectionGoal {
        case phones, keys, videoPanel
    }
    
    weak var delegate: SelectContractVCDelegate?
    var code: String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setEmptyBackArrow()
        
        defineVariables(sections: 1, cellIdentifer: SelectContractViewCell.className, placeholderTitle: "Здесь будут отображаться ваши договоры для выбора")
        
        self.tableView = dataTableView
        self.tableView?.estimatedRowHeight = 80
        self.tableView?.rowHeight = UITableView.automaticDimension
        setUpRefreshControl()
        
        let doneButton = UIBarButtonItem(title: "Выбрать", style: .done, target: self, action: #selector(selectionDone))
        self.navigationItem.rightBarButtonItem = doneButton
        setTitleForBackArrow(title: "Назад")
        doneButton.isEnabled = selectedIDs.count > 0
        setCountInTitle()
        
        if goal != .keys {
            bottomOffsetDataTable.constant = 0
            infoLabel.hideObject(0)
        }
    }
    
    override func loadDataFromServer(_ refreshControl: UIRefreshControl?) {
        readDataFromDatabase()
    }
    
    override func readDataFromDatabase() {
        if let goal = goal, goal == .phones, let user = DataStorage.shared.user {
            dataArray = DataStorage.shared.contracts.filter( { $0.userId == user.id } )
        } else {
            dataArray = DataStorage.shared.contracts
        }
        updateTableView()
    }
    
    @objc func selectionDone(sender: UIBarButtonItem) {
        if let goal = goal {
            switch goal {
            case .phones:
                if let nextViewController = Utils.shared.additionalStoryboard().instantiateViewController(withIdentifier: AddPhoneVC.className) as? AddPhoneVC {
                    nextViewController.selectedContracts = selectedIDs
                    self.navigationController?.pushViewController(nextViewController, animated: true)
                }
            case .keys:
                if let nextViewController = Utils.shared.additionalStoryboard().instantiateViewController(withIdentifier: AddKeyVC.className) as? AddKeyVC {
                    nextViewController.delegate = self
                    nextViewController.selectedContracts = selectedIDs
                    self.navigationController?.pushViewController(nextViewController, animated: true)
                }
            case .videoPanel:
                self.navigationController?.popViewController(animated: true)
                delegate?.selectContract(contractId: selectedIDs[0].contractID ?? "", code: code)
            }
        }
    }
    
    func setCountInTitle() {
        self.setTitle(title: "Выбрано: \(selectedIDs.count)")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if keyWasAdded {
            self.navigationController?.popViewController(animated: true)
        }
        setupDataChangeObserver()
        let textAttributes = [NSAttributedString.Key.foregroundColor : UIColor.lightGray]
        navigationController?.navigationBar.titleTextAttributes = textAttributes
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        let textAttributes = [NSAttributedString.Key.foregroundColor : UIColor.white]
        navigationController?.navigationBar.titleTextAttributes = textAttributes
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifer!, for: indexPath) as! SelectContractViewCell
        cell.setupCell(with: dataArray[indexPath.row], checked: selectedIDs.contains(where: { $0.contractID == dataArray[indexPath.row].id && $0.contractNumber == dataArray[indexPath.row].number }))
        cell.checkBox.valueChanged = { [weak self] (value) in
            guard let self = self else { return }
            let contract = self.dataArray[indexPath.row]
            if(value){
                if self.goal == .videoPanel {
                    self.selectedIDs = []
                }
                self.selectedIDs.append(InfoParams(contractID: contract.id, contractNumber: contract.number))
                self.dataTableView.reloadData()
            } else {
                self.selectedIDs.removeAll(where: { params -> Bool in
                   return params.contractID == contract.id
                })
            }
            self.navigationItem.rightBarButtonItem?.isEnabled = self.selectedIDs.count > 0
            self.setCountInTitle()
        }
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifer!, for: indexPath) as! SelectContractViewCell
        //cell.checkBox.valueChanged()
        
        cell.checkBox.isChecked = !cell.checkBox.isChecked
        cell.checkBox.valueChanged?(cell.checkBox.isChecked)
        cell.checkBox.sendActions(for: .valueChanged)
    }
    
}
extension SelectContractVC: AddKeyVCDelegate {
    func keyAdded() {
        self.keyWasAdded = true
    }
}
