//
//  SelectContractViewCell.swift
//  ProDomofon
//
//  Created by Александр Новиков on 26/07/2019.
//  Copyright © 2019 Александр Новиков. All rights reserved.
//

import UIKit

class SelectContractViewCell: UITableViewCell {

    @IBOutlet weak var checkBox: Checkbox!
    @IBOutlet weak var numberLabel: UILabel!
    @IBOutlet weak var addressLabel: UILabel!
    
    func setupCell(with contact: Contract, checked: Bool) {
        checkBox.cornerRadius = 5
        checkBox.checkedBorderColor = Colors.RED_ACCENT
        checkBox.uncheckedBorderColor = .gray
        checkBox.checkmarkColor = Colors.RED_ACCENT
        checkBox.checkmarkStyle = .tick
        checkBox.checkmarkSize = 0.6
        checkBox.isChecked = checked
        
        numberLabel.text = "Договор #\(contact.number)"
        addressLabel.text = contact.getReadableAddress
    }

}
