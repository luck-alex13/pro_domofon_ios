//
//  AppDelegate.swift
//  ProDomofon
//
//  Created by Александр Новиков on 27/05/2019.
//  Copyright © 2019 Александр Новиков. All rights reserved.
//

import UIKit
import Firebase
import UserNotifications
import PushKit
import SwiftyJSON
import AVFoundation

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    var wasHidden = false

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        FirebaseApp.configure()
        
        PushNotificationManager.shared.set(application: application)
        
        let notificationOption = launchOptions?[.remoteNotification]
        if let notification = notificationOption as? [String: AnyObject] {
            if let videoCall = PushNotificationManager.shared.parseVideoCallFrom(info: notification) {
                PushNotificationManager.shared.videoCall = videoCall
                PushNotificationManager.shared.openFromPush = true
                PushNotificationManager.shared.fromAppDelegate = true
            }
        }
        
        let token = DataStorage.shared.accessToken
//        token = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJfaWQiOiI1NTMwZDY5ZTdmMmUzMmQyNzk4ODI4YjIiLCJpYXQiOjE1NjgxOTAxODUsImV4cCI6MzMxMDQxOTAxODV9.bfqobXE7Vn2xl-UTrHDWIEX559eJd97dqOQnvX6fGlU"
        if(token != nil){
            RestClient.shared.updateToken(newToken: token!)
            openMainVCAsRoot()
        }
        
        DataStorage.shared.settingsURLString = UIApplication.openSettingsURLString
        
        return true
    }
    
    func openMainVCAsRoot(){
        let mainVC = Utils.shared.mainStoryboard().instantiateViewController(withIdentifier: MainNavigationVC.className)
        switchViewControllerAsRoot(vc: mainVC)
    }
    
    func openLoginVCAsRoot(){
        DataStorage.shared.selectedType = nil
        let mainVC = Utils.shared.mainStoryboard().instantiateViewController(withIdentifier: "LoginNavigaionVC")
        switchViewControllerAsRoot(vc: mainVC)
    }
    
    func switchViewControllerAsRoot(vc: UIViewController) {
        if self.window?.rootViewController?.className != vc.className {
            self.window?.rootViewController = vc
        }
    }
    
    func openVC(type: MenuVCType, forceUpdate: Bool = false){
        if let navController = self.window?.rootViewController as? UINavigationController {
            var vc: UIViewController? = nil
            switch type {
            case .contracts:
                vc = Utils.shared.mainStoryboard().instantiateViewController(withIdentifier: ContractsVC.className) as? ContractsVC
            case .contractInfo, .costs:
                vc = Utils.shared.additionalStoryboard().instantiateViewController(withIdentifier: ContractInfoMainVC.className) as? ContractInfoMainVC
                if type == .costs {
                    (vc as? ContractInfoMainVC)?.openCosts = true
                }
            case .phones:
                vc = Utils.shared.additionalStoryboard().instantiateViewController(withIdentifier: PhonesVC.className) as? PhonesVC
            case .keys:
                vc = Utils.shared.additionalStoryboard().instantiateViewController(withIdentifier: KeysVC.className) as? KeysVC
            case .headsets, .schedule:
                vc = Utils.shared.additionalStoryboard().instantiateViewController(withIdentifier: HeadSetVC.className) as? HeadSetVC
                if type == .schedule {
                    (vc as? HeadSetVC)?.openSchedule = true
                }
            case .requests:
                vc = Utils.shared.additionalStoryboard().instantiateViewController(withIdentifier: RequestsVC.className) as? RequestsVC
            case .pay:
                vc = Utils.shared.additionalStoryboard().instantiateViewController(withIdentifier: PayVC.className) as? PayVC
            case .history:
                vc = Utils.shared.additionalStoryboard().instantiateViewController(withIdentifier: SipHistoryVC.className) as? SipHistoryVC
//                vc = Utils.shared.additionalStoryboard().instantiateViewController(withIdentifier: ArchiveVC.className) as? ArchiveVC
            case .messages:
                vc = Utils.shared.mainStoryboard().instantiateViewController(withIdentifier: MessagesVC.className) as? MessagesVC
            case .widget:
                vc = Utils.shared.additionalStoryboard().instantiateViewController(withIdentifier: WidgetInfoVC.className) as? WidgetInfoVC
            case .videoPanel:
                vc = Utils.shared.additionalStoryboard().instantiateViewController(withIdentifier: VideoPanelVC.className) as? VideoPanelVC
            case .exit:
                let alert = AlertManager.shared.buildOkCancelAlert(title: "Внимание!", errorMessage: "Вы действительно хотите выйти из аккаунта?", okHandler: { (act) in
                    PushNotificationManager.shared.unsubscribeOnPushes {
                        if let _ = DataStorage.shared.adminToken {
                            DataStorage.shared.backAdminLogginingIn()
                        } else {
                            DataStorage.shared.removeObjectsBeforeLogginingOut()
                        }
                        AppDelegate.shared.openLoginVCAsRoot()
                    }
                }) { (act) in
                    
                }
                navController.present(alert, animated: true, completion: nil)
                return
            case .adminAuth:
                DataStorage.shared.removeAdminObjectsBeforeLogginingOut()
                AppDelegate.shared.openLoginVCAsRoot()
                return
            case .settings:
                Utils.shared.openUrl(url: UIApplication.openSettingsURLString)
                return
            }
            
            if let newVC = vc,
                (forceUpdate ||
                 !forceUpdate && navController.viewControllers.count > 0 && navController.viewControllers[0].className != newVC.className ||
//                newVC.className == ContractInfoMainVC.className ||
                navController.viewControllers.count == 0) {
                navController.setViewControllers([newVC], animated: true)
                DataStorage.shared.selectedType = type
            }
        }
    }
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        let deviceTokenString = deviceToken.hexString
        print("[AppDelegate] APNs token \(deviceTokenString)")
        
//        UIPasteboard.general.string = deviceTokenString
        DataStorage.shared.appleToken = deviceTokenString
        
        PushNotificationManager.shared.subscribeOnPushes(appleToken: deviceTokenString)
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        
        print("didReceiveRemoteNotification \(userInfo)")
        
        let payload = "payload"
        let type = "type"
        
        if let typeValue = userInfo[type], let payloadValue = userInfo[payload] {
            let typeString = String(describing: typeValue)
            let payloadString = String(describing: payloadValue)
            if typeString == "sip" {
                
                DataStorage.shared.timestampGetSipPush = Double(Date().timeIntervalSince1970)
                
                if UIApplication.shared.appConfiguration != .AppStore {
                    let systemSoundID: SystemSoundID = 1016
                    AudioServicesPlaySystemSound(systemSoundID)
                }
                
                let payloadJson = JSON.init(parseJSON: payloadString)
                let videoCall = VideoCall(json: payloadJson)
                
                RestClient.shared.hasCall(baseUrl: videoCall.socketUri, room: videoCall.room, user: videoCall.user) { (resp) in
                    if resp.isSuccess() {
                        PushNotificationManager.shared.showNotification(title: "Входящий вызов с панели", userInfo: userInfo)
                    }
                } errorCall: { (error) in
                    print("didReceiveRemoteNotification hasCall errorCall \(error.message)")
                }
                return
            }
        }
        
        switch application.applicationState {
            
        case .inactive:
            print("Inactive")
            //Show the view with the content of the push
            completionHandler(.newData)
            
        case .background:
            print("Background")
            //Refresh the local model
            completionHandler(.newData)
            
        case .active:
            print("Active")
            //Show an in-app banner
            //
            
            completionHandler(.newData)
        }
    }
    
    func checkPushSubcsription(){
        PushNotificationManager.shared.set(application: UIApplication.shared)
        PushNotificationManager.shared.subscribeIfNot()
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        NSLog("[AppDelegate] applicationWillResignActive()")
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        NSLog("[AppDelegate] applicationDidEnterBackground()")
        wasHidden = true
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        NSLog("[AppDelegate] applicationWillEnterForeground()")
        if let token = DataStorage.shared.accessToken {
            RestClient.shared.updateToken(newToken: token)
            
            let dif = Double(Date().timeIntervalSince1970) - (DataStorage.shared.timestampGetSipPush ?? 0)
            
            DispatchQueue.main.asyncAfter(deadline: .now() + (dif < 20 ? 2 : 0), execute: {
                NotificationCenter.default.post(name: .showBetweenVC, object: nil)
            })
        }
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        NSLog("[AppDelegate] applicationDidBecomeActive()")
        PushNotificationManager.shared.set(application: UIApplication.shared)
//        if let fcmToken = DataStorage.shared.fcmToken, wasHidden {
//            PushNotificationManager.shared.subscribeOnPushes(fcmToken: fcmToken)
//        }
        if let appleToken = DataStorage.shared.appleToken, wasHidden {
            PushNotificationManager.shared.subscribeOnPushes(appleToken: appleToken)
        }
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        UNUserNotificationCenter.current().removeAllDeliveredNotifications()
        UIApplication.shared.applicationIconBadgeNumber = 0
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
    func application(_ application: UIApplication, continue userActivity: NSUserActivity, restorationHandler: @escaping ([UIUserActivityRestoring]?) -> Void) -> Bool {
        guard userActivity.activityType == NSUserActivityTypeBrowsingWeb, let url = userActivity.webpageURL else { return false }
        print(url) // В зависимости от URL Вы можете открывать разные экраны приложения.
        return true
    }

}

extension UIApplicationDelegate {
    
    static var shared: AppDelegate {
        return UIApplication.shared.delegate! as! AppDelegate
    }
}
