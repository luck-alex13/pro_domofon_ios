//
//  FirstLoginVC.swift
//  ProDomofon
//
//  Created by Александр Новиков on 01/06/2019.
//  Copyright © 2019 Александр Новиков. All rights reserved.
//

import UIKit
import RxSwift
import InputMask

class FirstLoginVC: InputViewController, MaskedTextFieldDelegateListener {

    @IBOutlet weak var constrContentHeight: NSLayoutConstraint!
    @IBOutlet weak var contentScroll: UIScrollView!
    @IBOutlet weak var authLabelConstraint: NSLayoutConstraint!
    @IBOutlet weak var topViewConstraint: NSLayoutConstraint!
    @IBOutlet weak var topLabel: UILabel!
    @IBOutlet weak var phoneTextField: UITextField!
    @IBOutlet weak var nextBtn: UIButton!
    @IBOutlet weak var firstFormView: CardView!
    
    var maskedDelegate: MaskedTextFieldDelegate!
    var rawNumber: String? = DataStorage.shared.lastPhone
    
    var connectionObserver = ConnectionObserver()
    
    override func viewDidLoad() {
        super.viewDidLoad(scrollView: contentScroll, constraintContentHeight: constrContentHeight)
        
        setupMask()
        nextBtn.setCornerRadius(radius: 6)
        nextBtn.addTarget(self, action: #selector(touchNext), for: .touchUpInside)
        
        if let phone = DataStorage.shared.lastPhoneWithMask {
            phoneTextField.text = phone
        }
        
        if #available(iOS 13.0, *) {
            let app = UINavigationBarAppearance()
            app.backgroundColor = Colors.GREEN_PRIMARY
            navigationController?.navigationBar.scrollEdgeAppearance = app
            navigationController?.navigationBar.standardAppearance = app
        }
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
            self.phoneTextField.becomeFirstResponder()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if let token = DataStorage.shared.accessToken {
            RestClient.shared.updateToken(newToken: token)
            AppDelegate.shared.openMainVCAsRoot()
        } else if let adminToken = DataStorage.shared.adminToken {
            topLabel.text = "Admin auth"
            RestClient.shared.updateToken(newToken: adminToken)
        }
    }

    @objc
    func touchNext() {
        hideKeyBoard()
        validateAndSend(phone: rawNumber)
    }
    
    func setupMask() {
        let mask = "{+7}([000])[000]-[00]-[00]"
        maskedDelegate = MaskedTextFieldDelegate(primaryFormat: mask)
        maskedDelegate.listener = self
        phoneTextField.delegate = maskedDelegate
    }
    
    open func textField(_ textField: UITextField, didFillMandatoryCharacters complete: Bool, didExtractValue value: String) {
        if complete {
            
            print("value \(value) length = \(value.count)")
        }
        rawNumber = value
        
        DataStorage.shared.lastPhone = value
        DataStorage.shared.lastPhoneWithMask = textField.text
        //print("complete \(complete) value \(value)  ")
    }
    
    func validateAndSend(phone: String?) {
        if let phone = phone {
            if(isPhoneValid(phone: phone)){
                self.hideKeyBoard()
                sendPhoneRequest(for: phone)
            }else {
                Utils.shared.showToast(from: self, with: "Номер телефона слишком короткий!")
            }
        }else {
            phoneTextField.becomeFirstResponder()
            Utils.shared.showToast(from: self, with: "Введите номер телефона!")
        }
    }
    

    func isPhoneValid(phone: String) -> Bool {
        return phone.count == 12
    }
    
    
    func sendPhoneRequest(for phone: String) {
        guard connectionObserver.isConnectedToInternet else {
            AlertManager.shared.showAlertNoInternet(from: self)
            return
        }
        
        AlertManager.shared.showProgressDialog(from: self)
        let index = phone.index(phone.startIndex, offsetBy: 2) // String.Index
        let prefix = phone[index...]
        
        if let _ = DataStorage.shared.adminToken {
            RestClient.shared.authAdmin(phoneNumber: String(prefix)) { tokenResp in
                AlertManager.shared.hideProgress {
                    if let token = tokenResp.token {
                        RestClient.shared.updateToken(newToken: token)
                        DataStorage.shared.accessToken = token
                    }
                    AppDelegate.shared.openMainVCAsRoot()
                }
            } errorCall: { [weak self] error in
                guard let self = self else { return }
                AlertManager.shared.hideProgress {
                    Utils.shared.showToastHttp(from: self.view, with: "При запросе авторизации произошла ошибка. ", error: error)
                }
            }
            return
        }
        
        guard phone != "+72222222222" else {
            RestClient.shared.authRequest(phoneNumber: String(prefix)) { [weak self] resp in
                guard let self = self else { return }
                
                AlertManager.shared.hideProgress {
                    self.openSecondForm(test: true)
                }
            } errorCall: { [weak self] error in
                guard let self = self else { return }
                AlertManager.shared.hideProgress {
                    Utils.shared.showToastHttp(from: self.view, with: "При запросе авторизации произошла ошибка. ", error: error)
                }
            }
            return
        }
        
        RestClient.shared.authRequestWithCall(phoneNumber: String(prefix), success: { [weak self] in
            guard let self = self else { return }
            AlertManager.shared.hideProgress {
                self.openSecondForm()
            }
        }) { [weak self] (error)  in
            guard let self = self else { return }
            AlertManager.shared.hideProgress {
                Utils.shared.showToastHttp(from: self.view, with: "При запросе авторизации произошла ошибка. ", error: error)
            }
        }
    }
    
    func openSecondForm(test: Bool = false) {
        if let nextViewController = Utils.shared.mainStoryboard().instantiateViewController(withIdentifier: SecondLoginVC.className) as? SecondLoginVC, let phone = rawNumber {
            let index = phone.index(phone.startIndex, offsetBy: 2)
            nextViewController.phoneNumber = String(phone[index...])
            nextViewController.type = test ? .sms : .call
            self.navigationController?.pushViewController(nextViewController, animated: true)
        }
    }
}
