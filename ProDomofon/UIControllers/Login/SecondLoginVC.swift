//
//  SecondLoginVC.swift
//  ProDomofon
//
//  Created by Александр Новиков on 03/06/2019.
//  Copyright © 2019 Александр Новиков. All rights reserved.
//

import UIKit

enum SecondLoginType {
    case call, sms
    
    var countCode: Int {
        return self == .call ? 4 : 6
    }
    
    var title: String {
        return self == .call ? "Введите последние 4 цифры входящего звонка" : "Введите код из смс "
    }
    
    var placeholder: String {
        return self == .call ? "Последние 4 цифры" : "Код из смс"
    }
}

class SecondLoginVC: InputViewController {

    @IBOutlet weak var constrContentHeight: NSLayoutConstraint!
    @IBOutlet weak var contentScroll: UIScrollView!
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var smsCodeTF: UITextField!
    @IBOutlet weak var sendBtn: UIButton!
    @IBOutlet weak var sendSms: UIButton!
    
    var phoneNumber: String?
    
    var type: SecondLoginType = .call
    fileprivate var repeatSending = false
    fileprivate var timer: Timer?
    fileprivate var startDate = Date()
    
    var connectionObserver = ConnectionObserver()
    
    override func viewDidLoad() {
        super.viewDidLoad(scrollView: contentScroll, constraintContentHeight: constrContentHeight)
//        topViewConstraint.constant = UIScreen.main.bounds.height * 0.4
//        authLabelConstraint.constant = UIScreen.main.bounds.height * 0.05
        sendBtn.setCornerRadius(radius: 6)
        smsCodeTF.delegate = self
        smsCodeTF.addTarget(self, action: #selector(textFieldEditingChanged), for: .editingChanged)
        
        titleLabel.text = type.title
        
        sendSms.isEnabled = false
        setButtonSendSmsTitle(title: "Отправить код в смс \(repeatSending ? "повторно " : "")(через 30)")
        sendSms.titleLabel?.numberOfLines = 0
        sendSms.titleLabel?.lineBreakMode = .byWordWrapping
        sendSms.titleLabel?.textAlignment = .center
        smsCodeTF.placeholder = type.placeholder
        
        timer?.invalidate()
        timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(timerAction), userInfo: nil, repeats: true)
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
            self.smsCodeTF.becomeFirstResponder()
        }
    }
    
    @objc
    func timerAction() {
        let differenceInSeconds = Int(Date().timeIntervalSince(startDate))
        let timeout = 30
        guard differenceInSeconds <= timeout else {
            sendSms.isEnabled = true
            setButtonSendSmsTitle(title: "Отправить код в смс \(repeatSending ? "повторно" : "")")
            timer?.invalidate()
            return
        }
        let diff = timeout - differenceInSeconds
        setButtonSendSmsTitle(title: "Отправить код в смс \(repeatSending ? "повторно " : "")(через \(diff))")
    }
    
    func setButtonSendSmsTitle(title: String) {
        UIView.performWithoutAnimation {
            sendSms.setTitle(title, for: .normal)
            sendSms.layoutIfNeeded()
        }
    }

    @IBAction func sendClicked(_ sender: UIButton) {
        sendCode(phoneNumber: phoneNumber, code: smsCodeTF.text)
    }
    
    @IBAction func sendSmsTouchUpInside() {
        guard connectionObserver.isConnectedToInternet else {
            AlertManager.shared.showAlertNoInternet(from: self)
            return
        }
        
        guard let phone = phoneNumber else { return }
        
        self.hideKeyBoard()
        AlertManager.shared.showProgressDialog(from: self)
        
        self.type = .sms
        
        RestClient.shared.authRequest(phoneNumber: phone) { [weak self] authResponse in
            AlertManager.shared.hideProgress()
            
            guard let self = self else { return }
//
            self.titleLabel.text = "\(self.type.title) \(authResponse.infoString)"
            self.sendSms.isEnabled = false
            self.setButtonSendSmsTitle(title: "Отправить код в смс повторно (через 30)")
            self.smsCodeTF.placeholder = self.type.placeholder
            
            self.repeatSending = true
            self.startDate = Date()
            self.timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(self.timerAction), userInfo: nil, repeats: true)
            
            self.smsCodeTF.becomeFirstResponder()
        } errorCall: { error in
            AlertManager.shared.hideProgress()
            Utils.shared.showToastHttp(from: self.view, with: "При запросе авторизации произошла ошибка. ", error: error)
        }
    }
    
    
    func sendCode(phoneNumber: String?, code: String?) {
        guard connectionObserver.isConnectedToInternet else {
            AlertManager.shared.showAlertNoInternet(from: self)
            return
        }
        
        guard let code = code, let phone = phoneNumber else { return }
        
        self.hideKeyBoard()
        AlertManager.shared.showProgressDialog(from: self)
        RestClient.shared.confirmAuthBySms(phoneNumber: phone, call: type == .call, code: code) { tokenResponse in
            if let token = tokenResponse.token {
                RestClient.shared.updateToken(newToken: token)
                DataStorage.shared.accessToken = token
            }
            AlertManager.shared.hideProgress()
            self.loadUserAndContracts()
        } errorCall: { error in
            AlertManager.shared.hideProgress()
            Utils.shared.showToastHttp(from: self.view, with: "При отпавке кода произошла ошибка.", error: error)
        }
    }
    
    func loadUserAndContracts() {
        AlertManager.shared.showProgressDialog(from: self)
        RestClient.shared.getUserAndContracts(success: { [weak self] (user) in
            guard let self = self else { return }
            AlertManager.shared.hideProgress()
            DataStorage.shared.user = user
            DataStorage.shared.contracts = user.contracts
            self.openMainVC()
        }) { [weak self] (err) in
            AlertManager.shared.hideProgress(completion: {
                Utils.shared.showToastHttp(from: self?.view, with: "Не удалось авторизоваться.", error: err)
            })
        }
    }
    
    func openMainVC(){
        AppDelegate.shared.openMainVCAsRoot()
    }
    
    @objc
    func textFieldEditingChanged() {
        guard smsCodeTF.text?.count == type.countCode else { return }
        sendCode(phoneNumber: phoneNumber, code: smsCodeTF.text)
    }

}
