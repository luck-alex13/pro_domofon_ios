//
//  VideoCallService.m
//  ProDomofon
//
//  Created by Алексей Агильдин on 31.12.2020.
//  Copyright © 2020 Александр Новиков. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "VideoCallService.h"
#include <pjsua-lib/pjsua.h>

@implementation VideoCallService : NSObject

#pragma mark Singleton Methods

- (id)init {
  if (self = [super init]) {
      status = PJ_FALSE;
  }
  return self;
}

- (void)dealloc {
  // Should never be called, but just here for clarity really.
}

- (void)startPjsipAndRegisterOnServer:(char *)sipDomain withUserName:(char *)sipUser andPassword:(char *)password andPort:(char *)sipPort;
{
    pj_log_set_level(6);
    
    domain = sipDomain;
    port = sipPort;
    
    if(status == PJ_SUCCESS) {
        pjsua_destroy();
    }
    
    status = pjsua_create();
    
    NSLog(@"openSipConnection pjsua_create");
    if (status != PJ_SUCCESS)
    {
        NSLog(@"Error in pjsua_create() %d@", status);
        return;
    }
    
    // Init the config structure
    pjsua_config cfg;
    pjsua_config_default (&cfg);

    //Media
    pjsua_media_config media_cfg;
    pjsua_media_config_default(&media_cfg);

//    cfg.cb.on_incoming_call = &on_incoming_call;
    cfg.cb.on_call_media_state = &on_call_media_state;
    cfg.cb.on_call_state = &on_call_state;
    cfg.cb.on_reg_state = &on_reg_state;

    // Init the logging config structure
    pjsua_logging_config log_cfg;
    pjsua_logging_config_default(&log_cfg);
    log_cfg.console_level = 5;

    // Init the pjsua
    status = pjsua_init(&cfg, &log_cfg, NULL);
    if (status != PJ_SUCCESS) {
        NSLog(@"Error in pjsua_init() %d@", status);
        return;
    }
    
    pjsua_transport_config transport_cfg;
    pjsua_transport_config_default(&transport_cfg);
//    transport_cfg.port = 5060;

    // Add TCP transport.
    status = pjsua_transport_create(PJSIP_TRANSPORT_TCP, &transport_cfg, NULL);
    if (status != PJ_SUCCESS) {
        NSLog(@"Error in pjsua_transport_create() %d@", status);
        return;
    }
    
    // Initialization is done, now start pjsua
    status = pjsua_start();
    if (status != PJ_SUCCESS) {
        NSLog(@"Error in pjsua_start() %d@", status);
    }
    
    pjsua_acc_config acc_cfg;
    pjsua_acc_config_default(&acc_cfg);

    const size_t MAX_SIP_ID_LENGTH = 64;
    const size_t MAX_SIP_REG_URI_LENGTH = 64;
    
    // Account ID
    char sipId[MAX_SIP_ID_LENGTH];
    sprintf(sipId, "sip:%s@%s", sipUser, sipDomain);
    acc_cfg.id = pj_str(sipId);

    // Reg URI
    char regUri[MAX_SIP_REG_URI_LENGTH];
    sprintf(regUri, "sip:%s:%s;transport=tcp", sipDomain, sipPort);
    acc_cfg.reg_uri = pj_str(regUri);
    
    NSLog(@"reg_uri %s", acc_cfg.reg_uri);
    

    // Account cred info
    acc_cfg.cred_count = 1;
    acc_cfg.cred_info[0].scheme = pj_str("digest");
    acc_cfg.cred_info[0].realm = pj_str(sipDomain);
    acc_cfg.cred_info[0].username = pj_str(sipUser);
    acc_cfg.cred_info[0].data_type = PJSIP_CRED_DATA_PLAIN_PASSWD;
    acc_cfg.cred_info[0].data = pj_str(password);
    
    status = pjsua_acc_add(&acc_cfg, PJ_TRUE, &acc_id);
    if (status != PJ_SUCCESS) {
        NSLog(@"Error in pjsua_acc_add() %d@", status);
    }
    
    pjsua_codec_info c[32];
    unsigned k, count = PJ_ARRAY_SIZE(c);
    
    pjsua_enum_codecs(c, &count);
    if (status != PJ_SUCCESS) {
        NSLog(@"Error in pjsua_enum_codecs() %d@", status);
    }
    
    NSLog(@"codec start");
    
    pj_str_t codec;
    for (k=0; k<count; ++k) {
        status = pjsua_codec_set_priority(pj_cstr(&codec, c[k].codec_id.ptr), 0);
        if (status != PJ_SUCCESS) {
            NSLog(@"Error in pjsua_codec_set_priority() %d@", status);
        }
    }
    
    status = pjsua_codec_set_priority(pj_cstr(&codec, "GSM/8000/1"), 200);
    if (status != PJ_SUCCESS) {
        NSLog(@"Error in pjsua_codec_set_priority() %d@", status);
    }
    
    pjsua_enum_codecs(c, &count);
    if (status != PJ_SUCCESS) {
        NSLog(@"Error in pjsua_enum_codecs() %d@", status);
    }
    
    for (k=0; k<count; ++k) {
        printf("  %d\t%.*s\n", c[k].priority, (int)c[k].codec_id.slen,
               c[k].codec_id.ptr);

    }
    NSLog(@"codec finish");
}
- (void)make_call: (char *)callId {
    NSLog(@"Start make a call");
    
    pjsua_call_setting opt;
    pjsua_call_setting_default(&opt);

    opt.aud_cnt = 1;
    opt.vid_cnt = 1;
    
    const size_t MAX_SIP_REG_URI_LENGTH = 64;
    
    char destUri[MAX_SIP_REG_URI_LENGTH];
    sprintf(destUri, "sip:%s@%s:%s;transport=tcp", callId, domain, port);
    NSLog(@"destUri %s", destUri);
    pj_str_t uri = pj_str(destUri);
    
    
    status = pjsua_call_make_call(acc_id, &uri, &opt, NULL, NULL, NULL);
    if (status != PJ_SUCCESS) {
        NSLog(@"Error in pjsua_call_make_call() %d@", status);
    } else {
        [[NSNotificationCenter defaultCenter] postNotificationName: @"sipCallMaked" object: NULL];
    }
}

- (void)change_speaker {
    
}

- (void)turn_off_sound {
    unsigned int tx_level;
    unsigned int rx_level;
    pjsua_conf_get_signal_level(0, &tx_level, &rx_level);
    
    if (status != PJ_SUCCESS) {
        NSLog(@"Error in pjsua_conf_get_signal_level() %d@", status);
    }
    
    NSLog(@"rx_level %d@", rx_level);
    NSLog(@"tx_level %d@", tx_level);
    
    if(rx_level > 0) {
        pjsua_conf_adjust_rx_level(0, 0);
    } else {
        pjsua_conf_adjust_rx_level(0, 1);
    }
    
    if(tx_level > 0) {
        pjsua_conf_adjust_tx_level(0, 0);
    } else {
        pjsua_conf_adjust_tx_level(0, 1);
    }
}

- (void)destroy {
    status = pjsua_destroy();
    
    if (status != PJ_SUCCESS) {
        NSLog(@"Error in pjsua_destroy %d@", status);
    } else {
        [[NSNotificationCenter defaultCenter] postNotificationName: @"sipDestroyed" object: NULL];
    }
}
static void on_reg_state(pjsua_acc_id acc_id) {
    NSLog(@"on_reg_state acc_id: %d", acc_id);
    
    [[NSNotificationCenter defaultCenter] postNotificationName: @"sipRegistered" object: NULL];
}

static void on_call_media_state(pjsua_call_id call_id) {
    NSLog(@"on_call_media_state %d@", call_id);
    pjsua_call_info ci;
    pjsua_call_get_info(call_id, &ci);

    if (ci.media_status == PJSUA_CALL_MEDIA_ACTIVE) {
        // When media is active, connect call to sound device.
        pjsua_conf_connect(ci.conf_slot, 0);
        pjsua_conf_connect(0, ci.conf_slot);

        pjsua_conf_adjust_rx_level(0, 1);
        pjsua_conf_adjust_tx_level(0, 1);
    }
}

static void on_call_state(pjsua_call_id call_id, pjsip_event *e) {
    pjsua_call_info ci;
    pj_status_t status;
    
    PJ_UNUSED_ARG(e);

    status = pjsua_call_get_info(call_id, &ci);
    
    if (status != PJ_SUCCESS) {
        NSLog(@"Error in pjsua_call_get_info %d@", status);
    } else {
        printf("Call %d %ld %s", call_id, ci.state_text.slen, ci.state_text.ptr);
        
        if (ci.state == PJSIP_INV_STATE_DISCONNECTED) {
            [[NSNotificationCenter defaultCenter] postNotificationName: @"sipDisconnected" object: NULL];
        }
    }
}

@end
