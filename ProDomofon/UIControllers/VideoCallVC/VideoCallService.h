//
//  VideoCallService.h
//  ProDomofon
//
//  Created by Алексей Агильдин on 31.12.2020.
//  Copyright © 2020 Александр Новиков. All rights reserved.
//

#import <Foundation/Foundation.h>
#include <pjsua-lib/pjsua.h>

#ifndef VideoCallService_h
#define VideoCallService_h

@class VideoCall;

@interface VideoCallService : NSObject {
    char * domain;
    char * port;
    pj_status_t status;
    pjsua_acc_id acc_id;
}

- (void)startPjsipAndRegisterOnServer:(char *)sipDomain withUserName:(char *)sipUser andPassword:(char *)password andPort:(char *)sipPort;

- (void)make_call: (char *)callId;

- (void)change_speaker;

- (void)turn_off_sound;

- (void)destroy;

@end

#endif /* VideoCallService_h */
