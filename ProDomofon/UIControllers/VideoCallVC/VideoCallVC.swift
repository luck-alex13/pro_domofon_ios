//
//  VideoCallVC.swift
//  ProDomofon
//
//  Created by Алексей Агильдин on 18.12.2020.
//  Copyright © 2020 Александр Новиков. All rights reserved.
//

import UIKit
import AVFoundation
import AVKit
import AlamofireImage
import Alamofire
import WebKit
import SwiftyJSON
import VideoDecoder

@objc protocol VideoCallServiceDelegate {
    func sipRegistered()
    func sipDestroyed()
}

class VideoCallVC: UIViewController {

    fileprivate var backgroundImageView: UIView!
    fileprivate var videoCallBackgroundImageView: UIImageView!
    fileprivate var scale: CGFloat = 1.0
    
    fileprivate var webView: WKWebView!
    
    fileprivate var addressLabel: UILabel!
    
    fileprivate var logoImageView: UIImageView!
    
    fileprivate var endCallButton: UIButton!
    fileprivate var showVideoButton: UIButton!
    fileprivate var changeSpeakerButton: UIButton!
    fileprivate var openButton: UIButton!
    fileprivate var acceptCallButton: UIButton!
    
    fileprivate var speaker = true
    
    fileprivate var service: VideoCallService?
    fileprivate var acceptTouched: Bool = false
    
    fileprivate var player: AVAudioPlayer?
    
    fileprivate var facesRequest: DataRequest?
    
    fileprivate var decoder: VideoDecoder!
    fileprivate var size: CGSize?
    fileprivate var showedVideo: Bool = false {
        didSet {
            guard showVideoButton != nil else { return }
            if showedVideo {
                showVideoButton.setImage(UIImage(named: "show_video_active"), for: .normal)
                
            } else {
                showVideoButton.setImage(UIImage(named: "show_video_unactive"), for: .normal)
                
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        decoder = H264Decoder(delegate: self)
        
        NotificationCenter.default.addObserver(self, selector: #selector(hasCall), name: .hasCall, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(callEnded), name: .hasNotCall, object: nil)
        
        title = "Вызов с подъездной панели"
        
        view.backgroundColor = .black
        
        var posY: CGFloat = CGFloat.topOffset + 44
        let height: CGFloat = view.frame.height - posY - .bottomOffset
        
        backgroundImageView = UIView(frame: CGRect(x: 0, y: posY, width: view.frame.width, height: height))
        backgroundImageView.backgroundColor = .black
        view.addSubview(backgroundImageView)
        
        videoCallBackgroundImageView = UIImageView(frame: backgroundImageView.frame)
        videoCallBackgroundImageView.contentMode = .scaleAspectFill
//        videoCallBackgroundImageView.contentMode = .scaleToFill
//        videoCallBackgroundImageView.contentMode = .scaleAspectFit
        view.addSubview(videoCallBackgroundImageView)
        
        webView = WKWebView(frame: CGRect(x: 0, y: posY, width: view.frame.width, height: height))
        view.addSubview(webView)
        view.bringSubviewToFront(backgroundImageView)
        view.bringSubviewToFront(videoCallBackgroundImageView)
        
        let backAddressView = UIView(frame: CGRect(x: 0, y: posY, width: view.frame.width, height: 46))
        backAddressView.backgroundColor = .black
        backAddressView.alpha = 0.55
        view.addSubview(backAddressView)
        
        addressLabel = UILabel(frame: CGRect(x: 8, y: posY, width: backAddressView.frame.width - 16, height: backAddressView.frame.height))
        addressLabel.textAlignment = .center
        addressLabel.font = UIFont.systemFont(ofSize: 16)
        addressLabel.backgroundColor = .clear
        addressLabel.textColor = .white
        view.addSubview(addressLabel)
        
        let spaceBetween = (view.frame.width - 5 * 60) / 5
        
        let width: CGFloat = view.frame.width - 32
        let logoHeight: CGFloat = width / 5
        posY = posY + height - 76 - logoHeight
        
        let backLogoView = UIView(frame: CGRect(x: 0, y: posY, width: view.frame.width, height: logoHeight))
        backLogoView.backgroundColor = .black
        backLogoView.alpha = 0.55
        view.addSubview(backLogoView)
        
        logoImageView = UIImageView(frame: CGRect(x: 16, y: posY, width: width, height: logoHeight))
        logoImageView.contentMode = .scaleAspectFit
        view.addSubview(logoImageView)
        
        
        posY += logoHeight
        
        endCallButton = UIButton(frame: CGRect(x: spaceBetween / 2, y: posY + 8, width: 60, height: 60))
        endCallButton.setImage(UIImage(named: "end_call"), for: .normal)
        endCallButton.addTarget(self, action: #selector(endCallTouch), for: .touchUpInside)
        view.addSubview(endCallButton)
        
        showVideoButton = UIButton(frame: CGRect(x: endCallButton.frame.maxX + spaceBetween, y: endCallButton.frame.origin.y, width: 60, height: 60))
        showVideoButton.setImage(UIImage(named: "show_video_unactive"), for: .normal)
        showVideoButton.addTarget(self, action: #selector(showVideoTouch), for: .touchUpInside)
        view.addSubview(showVideoButton)
        
        changeSpeakerButton = UIButton(frame: CGRect(x: showVideoButton.frame.maxX + spaceBetween, y: endCallButton.frame.origin.y, width: 60, height: 60))
        changeSpeakerButton.setImage(UIImage(named: "speaker_unactive"), for: .normal)
        changeSpeakerButton.addTarget(self, action: #selector(changeSpeakerTouch), for: .touchUpInside)
        view.addSubview(changeSpeakerButton)
        
        openButton = UIButton(frame: CGRect(x: changeSpeakerButton.frame.maxX + spaceBetween, y: endCallButton.frame.origin.y, width: 60, height: 60))
        openButton.setImage(UIImage(named: "lock_on_unactive"), for: .normal)
        openButton.setImage(UIImage(named: "lock_on_active"), for: .disabled)
        openButton.addTarget(self, action: #selector(openDoorTouch), for: .touchUpInside)
        view.addSubview(openButton)
        
        acceptCallButton = UIButton(frame: CGRect(x: openButton.frame.maxX + spaceBetween, y: endCallButton.frame.origin.y, width: 60, height: 60))
        acceptCallButton.setImage(UIImage(named: "call_on_unactive"), for: .normal)
        acceptCallButton.setImage(UIImage(named: "call_on_active"), for: .disabled)
        acceptCallButton.addTarget(self, action: #selector(acceptTouch), for: .touchUpInside)
        view.addSubview(acceptCallButton)
        
        setData()
    }
    
    fileprivate var accessToCallEnded = true
    @objc
    func callEnded(){
        guard accessToCallEnded else { return }
        self.accessToCallEnded = false
        if service != nil {
            exit(0)
        }
        
        DispatchQueue.main.async {
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    @objc
    func hasCall(){
        guard acceptTouched else { return }
        PushNotificationManager.shared.tryAnswerCall()
        initSip()
        acceptTouched = false
    }
    
    func setData(){
        guard let videoCall = PushNotificationManager.shared.videoCall else { return }
        addressLabel.text = videoCall.address
        
        if let imageUrl = videoCall.images.first {
            Alamofire.request(imageUrl).responseImage { (response) in
                if response.result.value != nil {
                    if let image = UIImage(data: response.data!, scale: 1.0) {
                        let baseHeight: CGFloat = self.view.frame.height - self.videoCallBackgroundImageView.frame.origin.y - .bottomOffset
                        self.scale = self.view.frame.width / image.size.width
                        var newHeight = image.size.height * self.scale
                        var newWidth = self.videoCallBackgroundImageView.frame.width
                        if newHeight > baseHeight {
                            newHeight = baseHeight
                            self.scale = newHeight / image.size.height
                            newWidth = image.size.width * self.scale
                        }
                        self.videoCallBackgroundImageView.frame.size.height = newHeight
                        self.videoCallBackgroundImageView.frame.size.width = newWidth
                        self.videoCallBackgroundImageView.frame.origin.x = self.view.frame.width / 2 - newWidth / 2
                        
                        if let imgScale = UIImage(data: response.data!, scale: self.scale) {
                            self.videoCallBackgroundImageView.image = imgScale
                        } else {
                            self.videoCallBackgroundImageView.image = image
                        }
                        self.getFaces()
                    } else {
                        print("error parse image from data")
                    }
                }
            }
        }
        
        if let url = videoCall.bannerImageURL {
            print("bannerImageURL: \(url.absoluteURL)")
            logoImageView.af_setImage(withURL: url)
        }
        
//        if let url = videoCall.videoStreamUrl {
//            webView.load(URLRequest(url: url))
//        }
//        if let url = URL(string: "\(videoCall.socketUri)/video.html?device=\(videoCall.room)&user=\(videoCall.user)") {
//            webView.load(URLRequest(url: url))
//        }
        
        if PushNotificationManager.shared.openFromPush {
            stopSound()
        } else {
            playSound()
        }
    }
    
    func playSound(){
        guard let url = Bundle.main.url(forResource: "sip_call_melody", withExtension: "mp3") else { return }
        
        do {
            try AVAudioSession.sharedInstance().overrideOutputAudioPort(.speaker)
            
            player = try AVAudioPlayer(contentsOf: url)
            player?.delegate = self
            player?.volume = 1
            player?.play()
            changeSpeakerButton.setImage(UIImage(named: "speaker_active"), for: .normal)
        } catch let error {
            print(error.localizedDescription)
        }
    }
    
    func stopSound(){
        player?.stop()
        changeSpeakerButton.setImage(UIImage(named: "speaker_unactive"), for: .normal)
    }
    
    func getFaces(){
        guard let videoCall = PushNotificationManager.shared.videoCall else { return }
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
            self.facesRequest = RestClient.shared.getFacesCall(baseUrl: videoCall.socketUri, room: videoCall.room, user: videoCall.user) { (data) in
                do {
                    let json = try JSON(data: data)
                    print("getFacesCall json \(json)")
                    if let faces = json["faces"].array {
                        for face in faces {
                            let x: CGFloat = CGFloat(face["x"].intValue) * self.scale
                            let y: CGFloat = CGFloat(face["y"].intValue) * self.scale
                            let w: CGFloat = CGFloat(face["w"].intValue) * self.scale
                            let h: CGFloat  = CGFloat(face["h"].intValue) * self.scale
                            
                            let lineLayer = CAShapeLayer()
                            let linePath = UIBezierPath()
                            linePath.move(to: CGPoint(x: x , y: y))
                            linePath.addLine(to: CGPoint(x: x + w, y: y))
                            linePath.addLine(to: CGPoint(x: x + w, y: y + h))
                            linePath.addLine(to: CGPoint(x: x, y: y + h))
                            linePath.addLine(to: CGPoint(x: x, y: y))
                            
                            lineLayer.path = linePath.cgPath
                            lineLayer.fillColor = nil
                            lineLayer.opacity = 1
                            lineLayer.strokeColor = UIColor.red.cgColor
                            
                            self.videoCallBackgroundImageView.layer.addSublayer(lineLayer)
                        }
                    }
                } catch {
                    print("error getFacesCall parse json from data")
                }
            } errorCall: { (error) in
                print("error getFacesCall \(String(describing: error.message))")
            }
        }
    }
    
    @objc
    func acceptTouch(){
        stopSound()
        guard service == nil else { return }
        acceptTouched = true
        acceptCallButton.isEnabled = false
    }
    
    @objc
    func initSip(){
        guard let videoCall = PushNotificationManager.shared.videoCall else { return }
        print("initSip")
        
        do {
            try AVAudioSession.sharedInstance().overrideOutputAudioPort(.none)
        } catch (let error) {
            print("Error overrideOutputAudioPort \(error.localizedDescription)")
            return
        }
        speaker = false
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.sipDestroyed), name: NSNotification.Name("sipDestroyed"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.sipRegistered), name: NSNotification.Name("sipRegistered") , object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.sipCallMaked), name: NSNotification.Name("sipCallMaked") , object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.sipDisconnected), name: NSNotification.Name("sipDisconnected") , object: nil)
        
        let host = strdup(videoCall.host.removingPercentEncoding!)
        let userName = strdup(videoCall.user)
        let password = strdup(videoCall.password)
        let port = strdup("\(videoCall.port)")
        
        self.service = VideoCallService.init()
        self.service?.startPjsipAndRegister(onServer: host, withUserName: userName, andPassword: password, andPort: port)
    }
    
    @objc
    func sipDestroyed(){
        
    }
    
    @objc
    func sipRegistered(){
        guard let videoCall = PushNotificationManager.shared.videoCall else { return }
        let callId = strdup(videoCall.callId)
        service?.make_call(callId)
    }
    
    @objc
    func sipCallMaked(){
        guard let videoCall = PushNotificationManager.shared.videoCall else { return }
        RestClient.shared.acceptCall(baseUrl: videoCall.socketUri, room: videoCall.room, user: videoCall.user, callId: videoCall.callId) { [weak self] (resp) in
            guard let self = self else { return }
            if resp.isSuccess() {
                PushNotificationManager.shared.callAnswered()
                self.changeSpeakerButton.hideObject()
                UIDevice.current.isProximityMonitoringEnabled = true
//                self.changeSpeakerButton.setImage(UIImage(named: "speaker_active"), for: .normal)
            }
        } errorCall: { [weak self] (error) in
//            AlertManager.shared.hideProgress()
//            Utils.shared.showToastHttp(from: self?.view, with: "При запросе подтверждения звонка произошла ошибка. ", error: error)
        }
    }
    
    @objc
    func sipDisconnected(){
        guard service == nil else { return }
        callEnded()
    }
    
    deinit {
        print("deinit")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        PushNotificationManager.shared.openedCall = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        UIDevice.current.isProximityMonitoringEnabled = false
        facesRequest?.cancel()
        PushNotificationManager.shared.openFromPush = false
        service?.destroy()
        stopSound()
        PushNotificationManager.shared.videoCall = nil
        NotificationCenter.default.removeObserver(self)
        PushNotificationManager.shared.endCall()
        stopShowVideo()
    }
    
    @objc
    func endCallTouch(){
        stopSound()
        if service == nil {
            let alertController = UIAlertController(title: "ПРО Домофон", message: "Дополнительные действия", preferredStyle: .actionSheet)
            alertController.addAction(UIAlertAction(title: "Прервать у меня", style: .default, handler: { (_) in
                self.callEnded()
            }))

            alertController.addAction(UIAlertAction(title: "Прервать у всех", style: .default, handler: { (_) in
                self.endCall()
            }))

            alertController.addAction(UIAlertAction(title: "Сообщить \"Не звонить\"", style: .default, handler: { (_) in
                
            }))

            alertController.addAction(UIAlertAction(title: "Отмена", style: .cancel, handler: nil))

            if let popoverController = alertController.popoverPresentationController, UIDevice.current.userInterfaceIdiom == .pad {
                popoverController.sourceView = self.view
                popoverController.sourceRect = CGRect(x: self.view.bounds.maxX, y: self.view.bounds.maxY - 200, width: 0, height: 0)
                popoverController.permittedArrowDirections = []
            }
            
            self.present(alertController, animated: true, completion: nil)
        } else {
            endCall()
        }
    }
    
    func endCall(){
        guard let videoCall = PushNotificationManager.shared.videoCall else { return }
        RestClient.shared.endCall(baseUrl: videoCall.socketUri, room: videoCall.room, user: videoCall.user, callId: videoCall.callId) { (resp) in
            if resp.isSuccess() {
                
            }
            self.callEnded()
        } errorCall: { (error) in
            self.callEnded()
//            AlertManager.shared.hideProgress()
//            Utils.shared.showToastHttp(from: self?.view, with: "При запросе завершения звонка произошла ошибка. ", error: error)
        }
    }
    
    @objc
    func showVideoTouch(){
        guard let videoCall = PushNotificationManager.shared.videoCall else { return }
        if !showedVideo {
            showedVideo = true
            if #available(iOS 13.0, *), let video = videoCall.video {
                WSManager.shared.setConnection(server: video.server, user: video.user, device: video.device, panel: video.panel)
                WSManager.shared.delegate = self
                WSManager.shared.connect(type: .stream)
            } else if let url = videoCall.videoStreamUrl {
                webView.load(URLRequest(url: url))
                backgroundImageView.hideObject()
                videoCallBackgroundImageView.hideObject()
            }
            
        } else {
            showedVideo = false
            stopShowVideo()
        }
    }
    
    func stopShowVideo() {
        if #available(iOS 13.0, *) {
            WSManager.shared.stopStream()
        } else {
            webView.stopLoading()
            backgroundImageView.showObject()
            videoCallBackgroundImageView.showObject()
        }
    }
    
    @objc
    func changeSpeakerTouch(){
        if service == nil {
            if let pl = player {
                if pl.isPlaying {
                    stopSound()
                } else {
                    playSound()
                }
            } else {
                playSound()
            }
        } else {
            let audioSession = AVAudioSession.sharedInstance()
            do {
                if speaker {
                    try audioSession.overrideOutputAudioPort(.none)
                    speaker = false
                    changeSpeakerButton.setImage(UIImage(named: "speaker_unactive"), for: .normal)
                } else {
                    try audioSession.overrideOutputAudioPort(.speaker)
                    speaker = true
                    changeSpeakerButton.setImage(UIImage(named: "speaker_active"), for: .normal)
                }
            } catch {
                print("Speaker error : \(error.localizedDescription)")
            }
        }
    }
    
    @objc
    func turnOffSoundTouch(){
        service?.turn_off_sound()
    }
    
    @objc
    func openDoorTouch(){
        openButton.isEnabled = false
        stopSound()
        guard let videoCall = PushNotificationManager.shared.videoCall else { return }
        RestClient.shared.openDoorCall(baseUrl: videoCall.socketUri, room: videoCall.room, user: videoCall.user, callId: videoCall.callId, success: { [weak self] (resp) in
            guard let self = self else { return }
            DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
                self.openButton.isEnabled = true
            }
            if resp.isSuccess() {
                self.finishWithMessage(title: "Дверь успешно открыта!", message: "Входите в подъезд. У вас есть 3 секунды.")
                if self.service == nil {
                    DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
                        self.callEnded()
                    }
                }
//                AlertManager.shared.hideProgress()
//                Utils.shared.showToast(from: self.view, with: resp.messageText())
                
//                self.navigationController?.popViewController(animated: true)
            } else {
                self.finishWithMessage(title: "Не удалось открыть дверь", message: resp.messageText())
            }
        }, errorCall: { (error) in
            self.openButton.isEnabled = true
            self.callEnded()
//            AlertManager.shared.hideProgress()
//            Utils.shared.showToastHttp(from: self?.view, with: "При запросе открытия двери произошла ошибка. ", error: error)
        })
    }
    
    func finishWithMessage(title: String, subtitle: String? = nil, message: String) {
        PushNotificationManager.shared.showNotification(title: title, subtitle: subtitle, body: message)
    }
}
extension VideoCallVC: AVAudioPlayerDelegate {
    func audioPlayerDidFinishPlaying(_ player: AVAudioPlayer, successfully flag: Bool) {
        if service == nil {
            stopSound()
        }
    }
}
extension VideoCallVC: WSManagerDelegate {
    func receiveInit(width: Int, height: Int) {
        self.size = CGSize(width: width, height: height)
    }
    
    func receiveData(data: Data) {
        guard let size = size else { return }
        let videoPacket = VideoPacket(data, fps: 30, type: .h264, videoSize: size)
        decoder.decodeOnePacket(videoPacket)
    }
    
    func error() {
        
    }
}
extension VideoCallVC: VideoDecoderDelegate {
    func decodeOutput(video: CMSampleBuffer) {
        if let image = video.image {
            DispatchQueue.main.async {
                self.videoCallBackgroundImageView.image = image
            }
        }
    }
    
    func decodeOutput(error: DecodeError) {
        print("decodeOutput error: \(error)")
    }
}
