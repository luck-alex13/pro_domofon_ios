//
//  MenuVC.swift
//  ProDomofon
//
//  Created by Алексей Агильдин on 12.03.2021.
//  Copyright © 2021 Александр Новиков. All rights reserved.
//

import UIKit

enum MenuVCType: Int {
    case contracts = -1
    case contractInfo = 1
    case phones = 2
    case keys = 3
    case headsets = 4
    case requests = 5
    case pay = 6
    case messages = 7
    case widget = 8
    case history = 9
    case videoPanel = 10
    case exit = 12
    case adminAuth = 20
    case costs = 30
    case schedule = 31
    case settings = 32
    
    var getHelpConst: HelpConst? {
        switch self {
        case .contracts: return .contracts
        case .phones: return .phones
        case .keys: return .keys
        case .videoPanel: return .apartment_intercoms
        case .requests: return .requests
        case .messages: return .messages
        case .widget: return .widget
        default: return nil
        }
    }
}

class MenuVC: UIViewController {

    @IBOutlet weak var tableView: UITableView! {
        didSet {
            tableView.delegate = self
            tableView.dataSource = self
            tableView.separatorStyle = .none
        }
    }
    
    @IBOutlet weak var phoneLabel: UILabel! {
        didSet {
            let user = DataStorage.shared.user
            var text = ""
            if let user = user, user.phone.count > 0 {
                text = "8\(user.phone)"
            }
            phoneLabel.text = text
        }
    }
    
    fileprivate var contracts = DataStorage.shared.contracts
    fileprivate var choosenContractIndex: Int {
        guard let strId = DataStorage.shared.choosenContractString, let index = contracts.firstIndex(where: { $0.id == strId }) else { return 0 }
        return index
    }
    fileprivate var sectionCount: Int {
        return 3 + contracts.count
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    class MenuLine {
        var text: String
        var imageName: String
        
        init(text: String, imageName: String){
            self.text = text
            self.imageName = imageName
        }
    }
    
    func getMenuLine(indexPath: IndexPath) -> MenuLine {
        var text = ""
        var imageName = ""
        if indexPath.section == 0 {
            text = "Договоры"
            imageName = "marked_list"
        } else if indexPath.section == sectionCount - 2 {
            switch indexPath.row {
            case 0:
                text = "Сообщения"
                imageName = "ic_message"
                break
            case 1:
                text = "Виджет"
                imageName = "ic_lock_basic"
                break
            case 2:
                text = "Архив"
                imageName = "ic_archive"
                break
            case 3:
                text = "Справка"
                imageName = "ic_help"
                break
            case 4:
                text = "Контакты"
                imageName = "ic_message"
                break
            case 5:
                text = "Выйти из аккаунта"
                imageName = "logout"
                break
            default: break
            }
        } else if indexPath.row > 0 && indexPath.section > 0 && indexPath.section < sectionCount - 2 {
            switch indexPath.row {
            case 1:
                text = "Начисления"
                imageName = "info_bar"
                break
            case 2:
                text = "Телефоны"
                imageName = "phone-log"
                break
            case 3:
                text = "Ключи"
                imageName = "ic_key_common"
                break
            case 4:
                text = "Видео-панель"
                imageName = "ic_tablet"
                break
            case 5:
                text = "Квартирные трубки"
                imageName = "deskphone"
                break
            case 6:
                text = "Заявки"
                imageName = "wrench"
                break
            case 7:
                text = "Оплата"
                imageName = "ruble"
                break
            default: break
            }
        }
        return MenuLine(text: text, imageName: imageName)
    }
}
extension MenuVC: UITableViewDataSource, UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return sectionCount
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return 1
        } else if section == sectionCount - 1 {
            return DataStorage.shared.user?.isAdmin ?? false ? 4 : 3
        } else if section == sectionCount - 2 {
            return 6
        } else {
            if section - 1 == choosenContractIndex {
                return 8
            } else {
                return 1
            }
        }
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        if section == 0 || section == sectionCount - 2 || section == sectionCount - 1 {
            return 0
        } else {
            if section == sectionCount - 3 {
                return 20
            } else {
                return 2
            }
        }
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        if section == 0 || section == sectionCount - 2 || section == sectionCount - 1 {
            return nil
        } else {
            let view = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.width, height: section == sectionCount - 3 ? 20 : 2))
            view.backgroundColor = .clear
            return view
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0 ||
            indexPath.section == sectionCount - 2 ||
            indexPath.row > 0 && indexPath.section > 0 && indexPath.section < sectionCount - 2 {
            return 40
        } else if indexPath.section == sectionCount - 1 {
            return 30
        } else if indexPath.row == 0 {
            if indexPath.section - 1 < contracts.count {
                let width = view.frame.width - 40
                let contract = contracts[indexPath.section - 1]
                let height = UILabel.estimatedHeight(width: width, text: contract.menuContractName, font: UIFont.systemFont(ofSize: 16))
                return height > 40 ? height : 40
            } else {
                return 40
            }
        } else {
            return 40
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell()
        cell.selectionStyle = .none
        
        var isSelected = false
        if let selectedType = DataStorage.shared.selectedType {
            if indexPath.section == 0 && selectedType == .contracts {
                isSelected = true
            }
            if indexPath.section == sectionCount - 2 &&
                (indexPath.row == 0 && selectedType == .messages ||
                 indexPath.row == 1 && selectedType == .widget ||
                 indexPath.row == 2 && selectedType == .history) {
                isSelected = true
            }
            if indexPath.section - 1 == choosenContractIndex {
                if (indexPath.row == 1 && (selectedType == .contractInfo || selectedType == .costs) ||
                 indexPath.row == 2 && selectedType == .phones ||
                 indexPath.row == 3 && selectedType == .keys ||
                 indexPath.row == 4 && selectedType == .videoPanel ||
                 indexPath.row == 5 && (selectedType == .headsets || selectedType == .schedule) ||
                 indexPath.row == 6 && selectedType == .requests ||
                    indexPath.row == 7 && selectedType == .pay) {
                    isSelected = true
                }
            }
        }
        
        cell.contentView.backgroundColor = isSelected ? .lightGray : .clear
        
        cell.contentView.subviews.forEach { (u) in
            u.removeFromSuperview()
        }
        
        let stockWidth = cell.contentView.frame.width - 40
        
        var leftOffsetImage: CGFloat = 0
        var leftOffsetLabel: CGFloat = 0
        var font = UIFont.systemFont(ofSize: 16)
        var textColor: UIColor = isSelected ? .white : Colors.BLACK
        let imageColor: UIColor = isSelected ? .white : Colors.GREEN_DARK
        var numberOfLines: Int = 1
        
        let menuLine = getMenuLine(indexPath: indexPath)
        var underline = false
        var contractName = false
        
        if indexPath.section == sectionCount - 1 {
            switch indexPath.row {
            case 0:
                menuLine.text = "Настройки"
                leftOffsetLabel = cell.contentView.frame.height
                font = UIFont.systemFont(ofSize: 12)
                break
            case 1:
                menuLine.text = "Политика конфиденциальности"
                leftOffsetLabel = cell.contentView.frame.height
                font = UIFont.systemFont(ofSize: 12)
                underline = true
                break
            case 2:
                let appVersion = Bundle.main.infoDictionary!["CFBundleShortVersionString"] as? String ?? "1.0"
                let appBuild = Bundle.main.infoDictionary!["CFBundleVersion"] as? String ?? "1"
                
                menuLine.text = "Version: \(appVersion) build: \(appBuild)"
                
                leftOffsetLabel = cell.contentView.frame.height
                font = UIFont.systemFont(ofSize: 12)
                break
            case 3:
                menuLine.text = "Авторизация под пользователем"
                leftOffsetLabel = cell.contentView.frame.height
                font = UIFont.systemFont(ofSize: 12)
                break
            default: break
            }
        } else if indexPath.row == 0 && indexPath.section > 0 && indexPath.section < sectionCount - 2 {
            if indexPath.section - 1 < contracts.count {
                let contract = contracts[indexPath.section - 1]
                menuLine.text = contract.menuContractName
                contractName = true
            } else {
                menuLine.text = "undefined"
            }
            leftOffsetLabel = 12
            if indexPath.section - 1 == choosenContractIndex {
                cell.contentView.backgroundColor = Colors.GREEN_DARK
                textColor = .white
            } else {
                cell.contentView.backgroundColor = .clear
                textColor = .black
                
                if indexPath.section - 1 == 0 || indexPath.section - 2 == choosenContractIndex {
                    let topLine = UIView(frame: CGRect(x: 0, y: 0, width: cell.contentView.frame.width, height: 1))
                    topLine.backgroundColor = Colors.GREEN_DARK
                    cell.contentView.addSubview(topLine)
                }
                
                let bottomLine = UIView(frame: CGRect(x: 0, y: cell.contentView.frame.height - 1, width: cell.contentView.frame.width, height: 1))
                bottomLine.backgroundColor = Colors.GREEN_DARK
                cell.contentView.addSubview(bottomLine)
            }
            numberOfLines = 0
        }
        
        if indexPath.section > 0 && indexPath.section < sectionCount - 2 && indexPath.row > 0 {
            leftOffsetImage = cell.contentView.frame.height
            leftOffsetLabel = leftOffsetImage + cell.contentView.frame.height
        } else if !contractName {
            leftOffsetImage = cell.contentView.frame.height / 2 - 24 / 2
            leftOffsetLabel = cell.contentView.frame.height
        }
        
        if menuLine.imageName != "" {
            let imageView = UIImageView(frame: CGRect(x: leftOffsetImage, y: cell.contentView.frame.height / 2 - 24 / 2, width: 24, height: 24))
            imageView.image = UIImage(named: menuLine.imageName)?.withRenderingMode(.alwaysTemplate)
            imageView.tintColor = imageColor
            cell.contentView.addSubview(imageView)
        }
        
        let label = UILabel(frame: CGRect(x: leftOffsetLabel, y: 0, width: stockWidth - leftOffsetLabel, height: cell.contentView.frame.height))
        label.font = font
        label.textColor = textColor
        label.textAlignment = .left
        label.lineBreakMode = .byWordWrapping
        label.numberOfLines = numberOfLines
        if underline {
            let underlineAttribute = [NSAttributedString.Key.underlineStyle: NSUnderlineStyle.thick.rawValue]
            let underlineAttributedString = NSAttributedString(string: menuLine.text, attributes: underlineAttribute)
            label.attributedText = underlineAttributedString
        } else {
            label.text = menuLine.text
        }
        cell.contentView.addSubview(label)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        tableView.deselectRow(at: indexPath, animated: true)
        var forceUpdate = false
        var type: MenuVCType? = nil
        if indexPath.section == 0 {
            type = .contracts
        } else if indexPath.section == sectionCount - 1 {
            switch indexPath.row {
            case 0: Utils.shared.openUrl(url: UIApplication.openSettingsURLString)
            case 1: Utils.shared.openUrl(url: StringConst.POLICY.rawValue)
            case 3: type = .adminAuth
            default: break
            }
        } else if indexPath.section == sectionCount - 2 {
            switch indexPath.row {
            case 0: type = .messages
            case 1: type = .widget
            case 2: type = .history
            case 3:
                if let type = DataStorage.shared.selectedType, let helpType = type.getHelpConst {
                    Utils.shared.openUrl(url: "\(StringConst.HELP.rawValue)#\(helpType.rawValue)")
                } else {
                    Utils.shared.openUrl(url: StringConst.HELP.rawValue)
                }
            case 4: Utils.shared.openUrl(url: StringConst.CONTACTS.rawValue)
            case 5: type = .exit
            default: break
            }
        } else {
            switch indexPath.row {
            case 0: type = DataStorage.shared.selectedType
            case 1: type = .contractInfo
            case 2: type = .phones
            case 3: type = .keys
            case 4: type = .videoPanel
            case 5: type = .headsets
            case 6: type = .requests
            case 7: type = .pay
            default: break
            }
            DataStorage.shared.choosenContractInfoIndex = type?.rawValue
            
            if indexPath.section - 1 != choosenContractIndex {
                if indexPath.section - 1 < contracts.count {
                    DataStorage.shared.choosenContractString = contracts[indexPath.section - 1].id
                    tableView.reloadData()
                    forceUpdate = true
                }
            }
        }
        
        guard let type = type else { return }
        if type != .exit && type != .adminAuth {
            AppDelegate.shared.openVC(type: type, forceUpdate: forceUpdate)
        }
        if !forceUpdate {
            dismiss(animated: true, completion: {
                if type == .exit || type == .adminAuth {
                    AppDelegate.shared.openVC(type: type)
                }
            })
        }
    }
}
