//
//  ViewControllerWithData.swift
//  ProDomofon
//
//  Created by Александр Новиков on 16/06/2019.
//  Copyright © 2019 Александр Новиков. All rights reserved.
//
import UIKit

protocol ViewControllerWithData {
    
    func defineVariables()
    
    func readDataFromDatabase()
    
    func setUpRefreshControl()
    
    func loadDataFromServer(_ refreshControl: UIRefreshControl?)
    
    func updateTableView()
}

extension ViewControllerWithData {
    
    
}
