//
//  SimpleVCWithData.swift
//  ProDomofon
//
//  Created by Александр Новиков on 19/10/2019.
//  Copyright © 2019 Александр Новиков. All rights reserved.
//

import UIKit

class SimpleVCWithData: TabbedVC, ViewControllerWithData {
    
    func defineVariables() {
        
    }
    
    func readDataFromDatabase() {
        
    }
    
    func setUpRefreshControl() {
        
    }
    
    func loadDataFromServer(_ refreshControl: UIRefreshControl?) {
        
    }
    
    func updateTableView() {
        
    }
    

}
