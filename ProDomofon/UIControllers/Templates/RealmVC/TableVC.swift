//
//  TableVC.swift
//  ProDomofon
//
//  Created by Алексей Агильдин on 30.03.2021.
//  Copyright © 2021 Алексей Агильдин. All rights reserved.
//

import UIKit

class TableVC<T : Codable>: UITableViewController, ViewControllerWithData {
    
    var dataArray : Array<T> = []
    
    var сonnectionObserver: ConnectionObserver?
    
    var cellIdentifer: String?
    var sections: Int?
    var placeholderTitle, placeholderSubtile: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        defineVariables()
        readDataFromDatabase()
        tableView?.tableFooterView = UIView(frame: CGRect.zero)
    }

    
    func setUpRefreshControl() {
        refreshControl = UIRefreshControl()
        refreshControl!.addTarget(self, action: #selector(refreshControlTraget), for: UIControl.Event.valueChanged)
        refreshControl!.tintColor = Colors.RED_ACCENT
        
        tableView?.addSubview(self.refreshControl!)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        NotificationCenter.default.addObserver(self, selector: #selector(updateData), name: .updateData, object: nil)
        setupDataChangeObserver()
        setUpNetworkObserving()
    }
    
    @objc
    func updateData(){
        readDataFromDatabase()
    }
    
    func setUpNetworkObserving(){
        сonnectionObserver = ConnectionObserver()
        сonnectionObserver!.observeOnConnection(reachable: { [weak self] in
            self?.loadDataFromServer(self?.refreshControl)
            }, notReachable: { [weak self] in
                
        })
    }
    
    func setupDataChangeObserver() {
//        notification = dataList?._observe({ [weak self] (changes) in
//            self?.updateTableView()
//        })
    }
    
    func setUpNetworkCkecking(){
        сonnectionObserver = ConnectionObserver()
    }
    
    func isConnected() -> Bool{
       return сonnectionObserver?.isConnectedToInternet ?? false
    }
    
    override func viewWillDisappear(_ animated: Bool) {
//        notification?.invalidate()
        NotificationCenter.default.removeObserver(self, name: .updateData, object: nil)
        сonnectionObserver?.endListening()
    }
    
    // MARK: ViewControllerWithData protocol metgods
    
    func updateTableView() {
        self.tableView?.reloadData()
    }
    
    func defineVariables() {
        sections = 1
    }
    
    func defineVariables(sections: Int? = 1, cellIdentifer: String? = nil, placeholderTitle: String? = nil, placeholderSubtile: String? = nil) {
        self.sections = sections
        self.cellIdentifer = cellIdentifer
        self.placeholderTitle = placeholderTitle
        self.placeholderSubtile = placeholderSubtile
    }
    
    @objc
    func readDataFromDatabase() {
        
    }
    
    @objc func refreshControlTraget(){
        loadDataFromServer(refreshControl)
    }
    
    func loadDataFromServer(_ refreshControl: UIRefreshControl?) {
        
    }
    
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return sections!
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        triggerBackgroundView(countOfRows: dataArray.count)
        return dataArray.count
    }
    
    func triggerBackgroundView(countOfRows: Int) {
        if countOfRows == 0 {
            tableView?.setBackgroundView(title: placeholderTitle, message: placeholderSubtile)
        } else {
            tableView?.restoreBackgroundView()
        }
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cellId = cellIdentifer {
            let cell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath)
            return cell
        } else {
            return UITableViewCell()
        }
    }
}

