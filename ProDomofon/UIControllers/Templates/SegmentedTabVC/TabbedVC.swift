//
//  TabbedVC.swift
//  ProDomofon
//
//  Created by Lexus on 10.11.2022.
//  Copyright © 2022 Алексей Агильдин. All rights reserved.
//

import UIKit

class TabbedVC: UIViewController {

    var tabIndex: Int?
    
    func getTabIndex() -> Int? {
        return tabIndex
    }
    
    func setTabIndex(index: Int) {
        tabIndex = index
    }
}
