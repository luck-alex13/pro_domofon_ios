//
//  SegmentedTabVC.swift
//  ProDomofon
//
//  Created by Александр Новиков on 16/10/2019.
//  Copyright © 2019 Александр Новиков. All rights reserved.
//

import UIKit
class SegmentedTabVC: UIViewController, TabsProtocol {
    
    var tabsVCList: [UIViewController]?
    var pageViewController : UIPageViewController?
    
    var lastPendingViewControllerIndex = 0
    var currentPageIndex = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initTabTitles()
        
    }
    
    func initPages() {
        
    }
    
    func initTabTitles() {
        
    }
    
    func instantinatePage(atIndex index: Int) -> UIViewController? {
        return nil
    }
    
    
    func getPage(atIndex index: Int) -> UIViewController? {
        guard let tabsVCList = tabsVCList, index >= 0 && index < tabsVCList.count else { return nil }
        return tabsVCList[index]
    }
    
    func setUpPageViewController(contentView: UIView, storyboard: UIStoryboard) {
        initPages()
        
        /* Getting the page View controller */
        pageViewController = UIPageViewController(transitionStyle: .scroll, navigationOrientation: .horizontal, options: nil)
        self.pageViewController!.dataSource = self as? UIPageViewControllerDataSource
        self.pageViewController!.delegate = self as? UIPageViewControllerDelegate
        
        self.pageViewController!.setViewControllers([getPage(atIndex: 0)!], direction: .forward, animated: true, completion: nil)
        
        /* width and height have to be from container contentView */
        self.pageViewController!.view.frame = CGRect(x:0, y:0, width: contentView.frame.width, height: contentView.frame.height)
        self.addChild(pageViewController!)
        contentView.addSubview(pageViewController!.view)
        self.pageViewController!.didMove(toParent: self)
    }
    
    func tabSelected(_ sender: UISegmentedControl) {
        pageSelected(index: sender.selectedSegmentIndex)
    }
    
    func pageSelected(index: Int) {
        if let vc = self.getPage(atIndex: index) {
            if self.currentPageIndex < index {
                self.pageViewController?.setViewControllers([vc], direction: .forward, animated: true, completion: nil)
            }else {
                self.pageViewController?.setViewControllers([vc], direction: .reverse, animated: true, completion: nil)
            }
        }
    }
    
    func changeSelectedSegmentIndex(index: Int) {
        print("changeSelectedSegmentIndex: \(index)")
    }
}
extension SegmentedTabVC: UIPageViewControllerDataSource {
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        if let vc = viewController as? TabbedVC, let index = vc.getTabIndex() {
            return getPage(atIndex: index - 1)
        }
        return nil
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        if let vc = viewController as? TabbedVC, let index = vc.getTabIndex() {
            return getPage(atIndex: index + 1)
        }
        return nil
    }
}
extension SegmentedTabVC: UIPageViewControllerDelegate {
    
    func pageViewController(_ pageViewController: UIPageViewController, willTransitionTo pendingViewControllers: [UIViewController]){
        if let vc = pendingViewControllers[0] as? TabbedVC, let index = vc.getTabIndex() {
            self.lastPendingViewControllerIndex = index
        }
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        if completed {
            self.currentPageIndex = self.lastPendingViewControllerIndex
            self.changeSelectedSegmentIndex(index: currentPageIndex)
        }
    }
}

protocol TabsProtocol {
    
    func initPages()
    
    func initTabTitles()
    
    func instantinatePage(atIndex index: Int) -> UIViewController?
    
    func changeSelectedSegmentIndex(index: Int)
    
}

