//
//  YourTextMessageTableViewCell.swift
//  EasyMoney
//
//  Created by hackintosh-3 on 31.10.2018.
//  Copyright © 2018 io.games. All rights reserved.
//

import UIKit

class YourTextMessageTableViewCell: BaseChatCell, SpikaBaseCell {
    
    @IBOutlet weak var yourTextMessage: UITextView! {
        didSet {
            textMessage = yourTextMessage
        }
    }
    @IBOutlet weak var yourBackground: UIView!
    @IBOutlet weak var avatarImage: UIImageView!
    @IBOutlet weak var parentView: UIView!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var dateConstraint: NSLayoutConstraint!
    @IBOutlet weak var itemProgressView: UIActivityIndicatorView!
    @IBOutlet weak var dateTopConstraint: NSLayoutConstraint!
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        setupProgress(isAnimating: false)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    override func setMessages(current message: SupportMessage, before messageBefore: SupportMessage, after messageAfter: SupportMessage) {
        super.setMessages(current: message, before: messageBefore, after: messageAfter)
        
        setupBackground()
        setupMessage(with: message)
        setupAvatar()
        //setupName(with: message.user?.name)
        
//        let date = Date(timeIntervalSince1970: TimeInterval(message.createdAt / 1000))
//        let dateCreated = timeFormatter.string(from: date)
//
//        setupDate(with: date)
//        setupTime(wite: dateCreated, status: message.status)
    }
    
    override func setupProgress(isAnimating:Bool) {
        itemProgressView.isHidden = !isAnimating
        dateTopConstraint.constant = isAnimating ? 30 : 0
        if isAnimating {
            itemProgressView.startAnimating()
        }
    }
    
    func setupBackground() {
        yourBackground.layer.cornerRadius = 6
        yourBackground.layer.masksToBounds = true
        yourBackground.backgroundColor = messViewColor
    }
    
    override func setupTime(with time: String?) {
        timeLabel.text = time
//        if status == SpikaConst.MessageStatus.SENT {
//            timeLabel.text = "sending".localized
//        }else {
//            timeLabel.text = time
//        }
//        timeLabel.textColor = SpikaConst.Colors.greyText
    }
    
    override func setupDate(with date: String?) {
        dateLabel.text = date
        if shouldShowDate {
            
            dateConstraint.constant = 20.0
        } else {
            dateLabel.text = ""
            dateConstraint.constant = 0.0
        }
    }
    
    func setupAvatar() {
        avatarImage.setRounded()
        if shouldShowAvatar {
            avatarImage.isHidden = false
            
            
//            let singleTap = UITapGestureRecognizer(target: self, action: #selector(self.openProfile(_:)))
            avatarImage.isUserInteractionEnabled = true
//            avatarImage.addGestureRecognizer(singleTap)
            
        } else {
            avatarImage.isHidden = true
        }
    }
    
    override func handleLongPressGestures(_ sender: UILongPressGestureRecognizer?) {
//        if sender?.state == .began, let user = activeUser {
//            sender?.view?.becomeFirstResponder()
//            let menuController = UIMenuController.shared
//            menuController.menuItems = createMenuForTextMessage(user: user)
//
//            menuController.setTargetRect(yourTextMessage.frame, in: yourTextMessage)
//            menuController.setMenuVisible(true, animated: true)
//        }
    }

    
    override func canPerformAction(_ action: Selector, withSender sender: Any?) -> Bool {
        /*
        if action == #selector(self.handleAnswer(_:)) {
            return true
        } else if action == #selector(self.handleCopy(_:)) {
            return true
        } else if action == #selector(self.handleDelete(_:)) {
            return true
        } else if action == #selector(self.handleBan(_:)) {
            return true
        } else if action == #selector(self.handleDeleteLastHour(_:)) {
            return true
        } else if action == #selector(self.handleDeleteAndBan(_:)) {
            return true
        } else if action == #selector(self.handleComplain(_:)) {
            return true
        }
         */
        return false
    }


}
