//
//  TextMessageTableViewCell.swift
//  EasyMoney
//
//  Created by hackintosh-3 on 19.10.2018.
//  Copyright © 2018 io.games. All rights reserved.
//

import UIKit

class MyTextMessageTableViewCell: BaseChatCell, SpikaBaseCell {
    
    @IBOutlet weak var myTextMessage: UITextView! {
        didSet {
            textMessage = myTextMessage
        }
    }
    @IBOutlet weak var myBackgroundView: UIView!
    @IBOutlet weak var parentView: UIView!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var dateConstraint: NSLayoutConstraint!
    @IBOutlet weak var itemProgressView: UIActivityIndicatorView!
    @IBOutlet weak var dateTopConstraint: NSLayoutConstraint!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setupProgress(isAnimating: false)
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    override func setMessages(current message: SupportMessage, before messageBefore: SupportMessage, after messageAfter: SupportMessage) {
        super.setMessages(current: message, before: messageBefore, after: messageAfter)
        
        setupBackground()
        setupMessage(with: message)
//
//        let date = Date(timeIntervalSince1970: TimeInterval(message.createdAt / 1000))
//        let dateCreated = timeFormatter.string(from: date)
//
//        setupDate(with: date)
    }
    
    func setupBackground(){
        myBackgroundView.layer.cornerRadius = 6
        myBackgroundView.layer.masksToBounds = true
        myBackgroundView.backgroundColor = messViewColor
    }
    
    override func setupTime(with time: String?) {
        timeLabel.text = time
//        if status == SpikaConst.MessageStatus.SENT {
//            timeLabel.text = "sending".localized
//        }else {
//            timeLabel.text = time
//        }
//        timeLabel.textColor = SpikaConst.Colors.greyText
    }
    
    
    func setupAvatar() {
    }
    
    override func setupDate(with date: String?) {
        dateLabel.text = date
        if shouldShowDate {
            dateConstraint.constant = 20.0
        } else {
            dateLabel.text = ""
            dateConstraint.constant = 0.0
        }
    }
    
    override func setupProgress(isAnimating:Bool) {
        itemProgressView.isHidden = !isAnimating
        dateTopConstraint.constant = isAnimating ? 30 : 0
        if isAnimating {
            itemProgressView.startAnimating()
        }
    }
    
    override func handleLongPressGestures(_ sender: UILongPressGestureRecognizer?) {
        /*
        if sender?.state == .began {
            sender?.view?.becomeFirstResponder()
            let menuController = UIMenuController.shared
            menuController.menuItems = [UIMenuItem(title: "copy".localized, action: #selector(self.handleCopy(_:)))]
            menuController.setTargetRect(myTextMessage.frame, in: myTextMessage)
            menuController.setMenuVisible(true, animated: true)
        }*/
        
    }
    override func canPerformAction(_ action: Selector, withSender sender: Any?) -> Bool {
//        if action == #selector(self.handleCopy(_:)) {
//            return true
//        } 
        return false
    }
}
