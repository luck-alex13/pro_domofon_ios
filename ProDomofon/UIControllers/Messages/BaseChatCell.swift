//
//  BaseChatCell.swift
//  EasyMoney
//
//  Created by hackintosh-3 on 19.10.2018.
//  Copyright © 2018 io.games. All rights reserved.
//

import UIKit

protocol BaseChatCellDelegate: class {
    func urlTouch(url: URL)
}

class BaseChatCell: UITableViewCell {
    
    var message, messageAfter, messageBefore: SupportMessage?
    weak var delegate: BaseChatCellDelegate?
    var shouldShowAvatar = true
    var shouldShowName = false
    var shouldShowDate = true
    var htmlSupporting = false
    var messViewColor, messTextColor: UIColor?
    var maxMessageLength: Int = 0
    var lpgr: UILongPressGestureRecognizer?
    weak var activeUser: User?
    
    weak var tapHandler: TapHandler?
    
    var textMessage: UITextView?
    fileprivate var textAlign: NSTextAlignment = .left
    
    override func awakeFromNib() {
        // Initialization code
        selectionStyle = UITableViewCell.SelectionStyle.none
        
        layer.shouldRasterize = true
        layer.rasterizationScale = UIScreen.main.scale
        
        lpgr = UILongPressGestureRecognizer(target: self, action: #selector(handleLongPressGestures(_:)))
        
        addGestureRecognizer(lpgr!)
        
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    @objc func handleLongPressGestures(_ sender: UILongPressGestureRecognizer?) {
        if sender?.state == .began {
            sender?.view?.becomeFirstResponder()
            let menuController = UIMenuController.shared
            let it = UIMenuItem(title: "Details", action: #selector(handleDetails(_:)))
            menuController.menuItems = [it]
            menuController.setMenuVisible(true, animated: true)
        }
    }
    
    @objc func handleDetails(_ sender: Any?) {
        print("Action handleDetails triggered, however need some way to refer the tapped")
        //delegate?.responds(to: #selector())
//        if (delegate as? NSObject)?.responds(to: #selector(NSObject.onInfoClicked(_:))) ?? false {
//            delegate?.onInfoClicked(message)
//        }
    }
    
    @objc func handleImageTap(_ sender: UITapGestureRecognizer? = nil) {
        
    }
    
    func setMessage(_ message: SupportMessage) {
        self.message = message
    }
    
    func setMessages(current message: SupportMessage, before messageBefore: SupportMessage, after messageAfter: SupportMessage) {
        self.message = message
        self.messageBefore = message
        self.messageAfter = message
                
        self.messViewColor = !message.isFromUser() ? Colors.GREEN_PRIMARY : #colorLiteral(red: 0.8980392157, green: 0.8980392157, blue: 0.8980392157, alpha: 1)
        self.messTextColor = !message.isFromUser() ? UIColor.white : UIColor.black
        self.textAlign = !message.isFromUser() ? .left : .right
    }
    
    func setupTime(with time: String?) {}
    
    func setupDate(with date: String?) {}
    
    func setupProgress(isAnimating:Bool) {}
    
    func attributedText(withString string: String, boldString: String, font: UIFont) -> NSAttributedString {
        let attributedString = NSMutableAttributedString(string: string,
                                                         attributes: [NSAttributedString.Key.font: font])
        let boldFontAttribute: [NSAttributedString.Key: Any] = [NSAttributedString.Key.font: UIFont.boldSystemFont(ofSize: font.pointSize)]
        let range = (string as NSString).range(of: boldString)
        attributedString.addAttributes(boldFontAttribute, range: range)
        return attributedString
    }
    
    override var canBecomeFirstResponder: Bool {
        return true
    }
    
    override func canPerformAction(_ action: Selector, withSender sender: Any?) -> Bool {
        if message != nil {
            return false
        }
        if action == #selector(handleDetails(_:)) {
            return true
        }
        return false
    }
    
    /*
    func createMenuForTextMessage(user: User) -> [UIMenuItem] {
        var menu: [UIMenuItem] = []
        
        menu.append(UIMenuItem(title: "copy".localized, action: #selector(self.handleCopy(_:))))
        menu.append(UIMenuItem(title: "answer_to".localized, action: #selector(self.handleAnswer(_:))))
        
        
        return menu
    }
     */
    
    func setupMessage(with message: SupportMessage) {
        let style = NSMutableParagraphStyle()
        style.alignment = textAlign
        
        let attributedOriginalText = NSMutableAttributedString(string: message.text.replacingByURL)
        let textRange = attributedOriginalText.mutableString.range(of: message.text.replacingByURL)
        
        attributedOriginalText.addAttribute(NSAttributedString.Key.paragraphStyle, value: style, range: textRange)
        attributedOriginalText.addAttribute(NSAttributedString.Key.foregroundColor, value: messTextColor, range: textRange)
        attributedOriginalText.addAttribute(NSAttributedString.Key.font, value: UIFont(name: "Helvetica Neue", size: 16)!, range: textRange)

        for url in message.text.foundUrlArray() {
            let linkRange = attributedOriginalText.mutableString.range(of: url.replacingByURL)
            attributedOriginalText.addAttribute(NSAttributedString.Key.link, value: link, range: linkRange)
            attributedOriginalText.setAttributes([.link: url.replacingByURL], range: linkRange)
            attributedOriginalText.addAttribute(NSAttributedString.Key.foregroundColor, value: messTextColor, range: linkRange)
            attributedOriginalText.addAttribute(NSAttributedString.Key.font, value: UIFont(name: "Helvetica Neue", size: 16)!, range: linkRange)
        }
        
        textMessage?.delegate = self
        textMessage?.isEditable = false
        textMessage?.attributedText = attributedOriginalText
        textMessage?.linkTextAttributes = [
            kCTForegroundColorAttributeName: messTextColor,
            kCTUnderlineStyleAttributeName: NSUnderlineStyle.single.rawValue,
        ] as [NSAttributedString.Key : Any]
    }
    
    /*
    @objc func openProfile(_ sender: Any?) {
        delegate?.openProfile(for: message)
    }
    
    @objc func handleCopy(_ sender: Any?) {
        delegate?.handleCopy(for: message)
    }
    
    @objc func handleDelete(_ sender: Any?) {
        delegate?.handleDelete(for: message)
    }
    
    @objc func handleAnswer(_ sender: Any?) {
        delegate?.handleAnswer(for: message)
    }
    
    @objc func handleComplain(_ sender: Any?) {
        delegate?.handleComplain(for: message)
    }
    
    @objc func handleBan(_ sender: Any?) {
        delegate?.handleBan(for: message)
    }
    
    @objc func handleDeleteLastHour(_ sender: Any?) {
        delegate?.handleDeleteLastHour(for: message)
    }
    @objc func handleDeleteAndBan(_ sender: Any?) {
        delegate?.handleDeleteAndBan(for: message)
    }
    
    @objc func showImagePreview(_ sender: Any?) {
        
        
    }
     */
}
extension BaseChatCell: UITextViewDelegate {
    func textView(_ textView: UITextView, shouldInteractWith URL: URL, in characterRange: NSRange, interaction: UITextItemInteraction) -> Bool {
        delegate?.urlTouch(url: URL)
        return false
    }
}

protocol SpikaBaseCell {
    func setupBackground()
    func setupAvatar()
}

protocol CellClickedDelegate: NSObjectProtocol {
    func handleCopy(for message: SupportMessage?)
    func handleAnswer(for message: SupportMessage?)
    func handleBan(for message: SupportMessage?)
    func handleComplain(for message: SupportMessage?)
    func handleDelete(for message: SupportMessage?)
    func handleDeleteLastHour(for message: SupportMessage?)
    func handleDeleteAndBan(for message: SupportMessage?)
    func openProfile(for message: SupportMessage?)
    func showImagePreview(for messageUrl: String)
}

protocol CSCellClickedDelegate: NSObjectProtocol {
    func onInfoClicked(_ message: SupportMessage?)
}

protocol TapHandler: AnyObject {
    
    func onViewTapped(view: UIView)
    
    func onImagePreviewTapped(imgUrl: String)
}
