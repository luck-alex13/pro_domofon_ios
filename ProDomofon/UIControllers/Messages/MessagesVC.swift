//
//  MessagesVC.swift
//  ProDomofon
//
//  Created by Александр Новиков on 05/09/2019.
//  Copyright © 2019 Александр Новиков. All rights reserved.
//

import UIKit
import ReverseExtension

class MessagesVC: VCWithTable<SupportMessage> {
    
    @IBOutlet weak var chatTableView: UITableView!
    @IBOutlet weak var messageField: GrowingTextView!
    //@IBOutlet weak var messageField: UITextView!
    @IBOutlet weak var sendBtn: UIButton!
    @IBOutlet weak var sendProgressView: UIActivityIndicatorView!
    @IBOutlet weak var bottomView: UIView!
    
    var errorAlert: UIAlertController?
    var kbHelper: KeyboardHelper = KeyboardHelper()
    var dateHelper = DateHelper(locale: Locale.current)
    let messageDateFormatter = DateFormatter()
    let dateFormatter = DateFormatter()
    let timeFormatter = DateFormatter()
    var total: Int = 0
    var currentOffset: Int = 0
    let offsetStep = 50
    var loading = false
    var tabBarHeight: CGFloat? = nil {
        didSet {
            kbHelper.tabBarHeight = tabBarHeight
        }
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setBar(title: "Чат")
        
        setUpRefreshControl()
        
        setupTableView()
        setupTextView()
        setReadyState()
        setupLocalFormatters()
        
        chatTableView.addGestureRecognizer(kbHelper.tapGestureRecognizerThatHideKeyboard)
        self.view.addGestureRecognizer(kbHelper.swipeGestureRecognizerThatHideKeyboard)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        kbHelper.subscribeOnKeyboardNotifications(observer: self)
    }
    
    override func setUpNetworkObserving() {
        сonnectionObserver = ConnectionObserver()
        сonnectionObserver!.observeOnConnection(reachable: { [weak self] in
            self?.loadDataFromServer(self?.refreshControl)
            }, notReachable: { [weak self] in
                self?.showErrorNoInternet { [weak self] (act) in
                    self?.loadDataFromServer(nil)
                }
        })
    }
    
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        kbHelper.hideKeyboard(from: self)
        kbHelper.removeObservers()
    }
    
    func setupTableView() {
        chatTableView.estimatedRowHeight = 40
        chatTableView.rowHeight = UITableView.automaticDimension
        
        chatTableView.register(MyTextMessageTableViewCell.nib, forCellReuseIdentifier: MyTextMessageTableViewCell.className)
        chatTableView.register(YourTextMessageTableViewCell.nib, forCellReuseIdentifier: YourTextMessageTableViewCell.className)

        
        self.chatTableView.delegate = self
        self.chatTableView.dataSource = self
        
        // REVERSE DIRECTION
        self.chatTableView.re.delegate = self
        self.chatTableView.re.scrollViewDidReachTop = { [weak self] scrollView in
            guard let self = self else { return }
            print("scrollViewDidReachTop")
            if !self.loading, self.currentOffset < self.total {
                self.currentOffset += self.offsetStep
                self.getSupportMessages(offset: self.currentOffset, limit: self.offsetStep)
            }
            //self?.loadMoreMesages()
        }
//        self.chatTableView.re.scrollViewDidReachBottom = { [weak self] scrollView in
//            print("scrollViewDidReachBottom")
//        }
        tableView?.tableFooterView = UIView(frame: CGRect.zero)
        self.tableView = chatTableView
        
        chatTableView.addGestureRecognizer(kbHelper.tapGestureRecognizerThatHideKeyboard)
        self.view.addGestureRecognizer(kbHelper.swipeGestureRecognizerThatHideKeyboard)
    }
    
    func setupTextView() {
        messageField.layer.cornerRadius = 4.0
        messageField.layer.borderWidth = 0.8
        messageField.layer.borderColor = #colorLiteral(red: 0.6666666865, green: 0.6666666865, blue: 0.6666666865, alpha: 0.9)
        messageField.placeholder = "Напишите сообщение..."
        messageField.dataDetectorTypes = .link
//        if let config = chatConfig {
//            maxMessageLength = config.type == ChatConfig.TYPE_SUPPORT ? SpikaConst.Numbers.supportMessageLength : SpikaConst.Numbers.commonMessageLength
//
//            if config.isPrivateChat() && !config.canWrite {
//                messageField.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(showWarnAddToFriendMessage)))
//            }else {
//                messageField.delegate = self
//            }
//        }
    }
    
    func setLoadingState() {
        self.sendBtn.isHidden = true
        self.sendBtn.alpha = 0.0
        self.sendProgressView.startAnimating()
    }
    
    func setReadyState() {
        self.sendBtn.isHidden = false
        self.sendProgressView.stopAnimating()
        
        UIView.animate(withDuration: 1.0) {
            self.sendBtn.alpha = 1.0
        }
    }
    
    func setupLocalFormatters(){
        messageDateFormatter.dateFormat = SupportMessage.SERVER_DATE_FORMAT
        messageDateFormatter.locale = Locale.current
        
        dateFormatter.dateStyle = .medium
        dateFormatter.timeStyle = .none
        
        timeFormatter.dateStyle = .none
        timeFormatter.timeStyle = .short
    }
    
    @IBAction func sendBtnTapped(_ sender: UIButton) {
        UIButton.animate(withDuration: 0.1,
                         animations: {
                            sender.transform = CGAffineTransform(scaleX: 1.1, y: 1.1)
        },
                         completion: { finish in
                            UIButton.animate(withDuration: 0.2, animations: {
                                sender.transform = CGAffineTransform.identity
                            },  completion: { finish in
                                self.onSendTapped()
                            })
        })
    }
    
    func onSendTapped() {
        self.sendMessage(textField: self.messageField)
    }
    
    func sendMessage(textField: UITextView) {
        
        if let message = textField.text, message.count > 0 {
            kbHelper.hideKeyboard(from: self)
            setLoadingState()
            textField.text = String()
            let newMessage = NewMessage(text: message, dtime: messageDateFormatter.string(from: Date()))
            
            RestClient.shared.postNewMessage(newMessage, success: { [weak self] (messResp) in
                guard let self = self else { return }
                self.setReadyState()
                
                DataStorage.shared.updateMessage(message: messResp)
                self.dataArray.insert(messResp, at: 0)
                self.updateTableView()
            }) { [weak self] (error) in
                Utils.shared.showToastHttp(from: self?.view, with: "Не удалось отправить сообщение", error: error)
            }
//            let messObj = MessageForSocket.init(userID: self.activeUser!.userId, type: SpikaConst.MessageType.TYPE_TEXT, text: message, roomID: self.chatConfig!.roomID, localID: Utils.shared.generateRandomStringWith(length: 32))
//
//            if let replyId = self.replyMessageID, let replyName = self.replyName, message.hasPrefix(replyName) {
//                messObj.replyTo = replyId
//                let indexStartOfText = message.index(message.startIndex, offsetBy: replyName.count)
//                messObj.text = String(message[indexStartOfText...])
//
//                self.replyMessageID = nil
//                self.replyName = nil
//            }
//
//            let tempMessage = ChatMessage(localId: messObj.localID, user: self.activeUser!, roomID: messObj.roomID, messageText: messObj.text, typeMessage: messObj.type, currentTime: Utils.shared.MilliSeconds)
//            self.saveDataToDatabase(object: tempMessage)
//
//            self.tempSentMessagesLocalId.append(messObj.localID)
//            self.socketController?.emitMessage(messObj)
//
//            textField.text = String()
//            self.setDefaultState()
        }else {
            Utils.shared.showToast(from: self, with: "Вы не ввели сообщение!", customPosition: true)
        }
        
    }
    
    override func updateData() {
        
    }
    
    override func readDataFromDatabase() {
        dataArray = DataStorage.shared.messages
        self.updateTableView()
    }
    
    override func loadDataFromServer(_ refreshControl: UIRefreshControl?) {
        guard let observer = сonnectionObserver, observer.isConnectedToInternet else {
            showErrorNoInternet { [weak self] (act) in
                self?.loadDataFromServer(nil)
            }
            return
        }
        if currentOffset == 0 {
            AlertManager.shared.showProgressDialog(from: self)
        }
        getSupportMessages(offset: 0, limit: offsetStep)
    }
    
    func getSupportMessages(offset: Int, limit: Int) {
        self.loading = true
        currentOffset = offset
        RestClient.shared.getSupportMessages(offset: offset, limit: limit, success: { [weak self] (response) in
            guard let self = self else { return }
            self.loading = false
            AlertManager.shared.hideProgress()
            let mess = response.rows.sorted(by: { $0.dtime > $1.dtime })
            if offset > 0 {
                DataStorage.shared.updateMessages(mess: mess)
                self.dataArray = self.dataArray + mess
                self.updateTableView()
//                self.tableView.inserrow
            } else {
                DataStorage.shared.messages = mess
                self.dataArray = mess
                self.updateTableView()
            }
            self.total = response.total
            self.tableView?.tableHeaderView = nil
        }) { [weak self] (error) in
            self?.loading = false
            AlertManager.shared.hideProgress()
            Utils.shared.showToastHttp(from: self?.view, with: "Не удалось загрузить сообщения", error: error)
            self?.tableView?.tableHeaderView = nil
        }
    }
    
    func showErrorNoInternet(handler: ((UIAlertAction) -> Void)? = nil) {
        errorAlert = UIAlertController(title: "Нет поключения к сети Интернет", message: "Включите интернет в настройках и попробуйте авторизоваться снова.", preferredStyle: .alert)
//        let retryAction = UIAlertAction(title: "Повторить", style: UIAlertAction.Style.default) { [weak self] (act) in
//            self?.tryAuth()
//        }
        let retryAction = UIAlertAction(title: "Повторить", style: UIAlertAction.Style.default, handler: handler)
        errorAlert!.addAction(retryAction)
        self.present(errorAlert!, animated: true, completion: nil)
    }

    // MARK: - Table view data source
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var cell: BaseChatCell?
        
        let message = dataArray[indexPath.row]
        let messageAfter = dataArray[max(indexPath.row - 1, 0)] // ниже текущего $message
        let messageBefore = dataArray[max(min(indexPath.row + 1, dataArray.count - 1), 0)] //выше текущего $message
        
        if (message.isFromUser()) {
            cell = tableView.dequeueReusableCell(withIdentifier: MyTextMessageTableViewCell.className, for: indexPath) as? BaseChatCell
        } else {
            cell = tableView.dequeueReusableCell(withIdentifier: YourTextMessageTableViewCell.className, for: indexPath) as? BaseChatCell
        }
        
        cell?.setupProgress(isAnimating: indexPath.row == dataArray.count - 1 && currentOffset < total)
        
        cell?.setMessages(current: message, before: messageBefore, after: messageAfter)
        
        if message.dtime != "" {
            let dateHelper = DateHelper(locale: Locale.current)
            if let date = dateHelper.parseDate(from: message.dtime) {
                cell?.setupDate(with: dateHelper.getOnlyDate(from: date.timeIntervalSince1970))
            } else {
                cell?.setupDate(with: nil)
            }
        }
        
        if let curDate = DateHelper(locale: Locale.current).parseDate(from: message.dtime), let beforeDate = DateHelper(locale: Locale.current).parseDate(from: messageBefore.dtime) {
            cell?.shouldShowDate = (!Calendar.current.isDate(curDate, inSameDayAs: beforeDate) ||
                indexPath.row == dataArray.count - 1)
            
            cell?.setupTime(with: timeFormatter.string(from: curDate))
//            cell?.setupDate(with: dateFormatter.string(from: curDate))
        }
//
//        cell?.htmlSupporting = chatConfig!.type == ChatConfig.TYPE_SUPPORT
//        cell?.maxMessageLength = chatConfig!.type == ChatConfig.TYPE_SUPPORT ? SpikaConst.Numbers.supportMessageLength : SpikaConst.Numbers.commonMessageLength
//
//
        cell?.delegate = self
        return cell!
    }
    
    deinit {
        print("deinit MessagesVC")
        //RealmDb.shared.deleteFromDb(allOf: SupportMessage.self)
    }
    
    override func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        if !loading, scrollView.contentOffset.y < 40 {
            let view = UIView(frame: CGRect(x: 0, y: 0, width: view.frame.width, height: 40))
            view.backgroundColor = .clear
            
            let activityIndicator = UIActivityIndicatorView(frame: CGRect(x: view.frame.width / 2 - 10, y: 10, width: 20, height: 20))
            activityIndicator.color = Colors.RED_ACCENT
            activityIndicator.startAnimating()
            view.addSubview(activityIndicator)
            
            tableView?.tableHeaderView = view
            
            getSupportMessages(offset: 0, limit: offsetStep)
        }
    }
    

}
extension MessagesVC: BaseChatCellDelegate {
    func urlTouch(url: URL) {
        if url.absoluteString.contains(StringConst.APP_LINK.rawValue) {
            let arr = url.absoluteString.components(separatedBy: StringConst.APP_LINK.rawValue)
            if arr.count == 2 {
                LinkManager.shared.parseAndOpen(link: arr[1])
            }
        } else if UIApplication.shared.canOpenURL(url) {
            UIApplication.shared.open(url)
        }
    }
}
