//
//  SecondViewController.swift
//  ProDomofon
//
//  Created by Александр Новиков on 27/05/2019.
//  Copyright © 2019 Александр Новиков. All rights reserved.
//

import UIKit
import WebKit

class WidgetInfoVC: UIViewController {

    var webView: WKWebView!
    
    let targetUrl = StringConst.WIDGET_INFO.rawValue
    
    private var isInjected: Bool = false
    
    override func loadView() {
        setBar(title: "Виджет")
        
        webView = WKWebView()
        webView.navigationDelegate = self
        webView.uiDelegate = self
        view = webView
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setTitle(title: "Описание виджета")
        let url = URL(string: targetUrl)!
        webView.load(URLRequest(url: url))
        
        let refresh = UIBarButtonItem(barButtonSystemItem: .refresh, target: webView, action: #selector(webView.reload))
        self.navigationItem.rightBarButtonItem = refresh
    }
}
extension WidgetInfoVC: WKNavigationDelegate {
    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
        if let url = webView.url {
            print("opens url=\(url)")
            if !url.absoluteString.elementsEqual(targetUrl) && !url.absoluteString.elementsEqual("about:blank") {
                Utils.shared.openUrl(url: url.absoluteString)
            }
        }
    }
    
    func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
        if navigationAction.navigationType == .linkActivated, let url = navigationAction.request.url {
            print(url)
            if url.absoluteString.contains(StringConst.APP_LINK.rawValue) {
                let arr = url.absoluteString.components(separatedBy: StringConst.APP_LINK.rawValue)
                if arr.count == 2 {
                    LinkManager.shared.parseAndOpen(link: arr[1])
                }
                decisionHandler(.allow)
                return
            } else if UIApplication.shared.canOpenURL(url) {
                UIApplication.shared.open(url)
                decisionHandler(.cancel)
                return
            }
        } else {
            print("not a user click")
            decisionHandler(.allow)
            return
        }
    }
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        if isInjected == true {
            return
        }
        self.isInjected = true
        // get HTML text
        let js = "document.body.outerHTML"
        
        webView.evaluateJavaScript(js) { (html, error) in
            let scale = "0.75"
            let headerString = "<head><meta name='viewport' content='width=device-width, initial-scale=\(scale), maximum-scale=\(scale), minimum-scale=\(scale), user-scalable=no'></head>"
            webView.loadHTMLString(headerString + (html as! String), baseURL: nil)
        }
        
    }
}
extension WidgetInfoVC: WKUIDelegate {
    func webView(_ webView: WKWebView, createWebViewWith configuration: WKWebViewConfiguration, for navigationAction: WKNavigationAction, windowFeatures: WKWindowFeatures) -> WKWebView? {
        if let url = navigationAction.request.url, navigationAction.targetFrame == nil {
            Utils.shared.openUrl(url: url.absoluteString)
        }
        return nil
    }
}
