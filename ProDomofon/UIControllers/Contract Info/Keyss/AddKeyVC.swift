//
//  AddKeyVC.swift
//  ProDomofon
//
//  Created by Александр Новиков on 18/08/2019.
//  Copyright © 2019 Александр Новиков. All rights reserved.
//

import UIKit

protocol AddKeyVCDelegate: class {
    func keyAdded()
}

class AddKeyVC: InputViewController, UICollectionViewDataSource {
    
    @IBOutlet weak var constrContentHeight: NSLayoutConstraint!
    @IBOutlet weak var contentScroll: UIScrollView!
    @IBOutlet weak var contactsList: UICollectionView!
    @IBOutlet weak var keyTextField: UITextField!
    @IBOutlet weak var addButton: UIButton! {
        didSet {
            addButton.setCornerRadius(radius: 6)
        }
    }
    
    weak var delegate: AddKeyVCDelegate?
    
    var selectedContracts = [InfoParams]()
    
    var connectionObserver = ConnectionObserver()
    var rawNumber: String?
    
    let keyCodeLength = 8

    override func viewDidLoad() {
        super.viewDidLoad(scrollView: contentScroll, constraintContentHeight: constrContentHeight)
        setTitle(title: "Добавление ключа")
        
        setEmptyBackArrow()
        
        self.contactsList.register(UINib(nibName: ContractIDCollectionViewCell.className, bundle: nil), forCellWithReuseIdentifier: ContractIDCollectionViewCell.className)
        
        let keyboardView = KeyKeyboardView(frame: CGRect(x: 0, y: 0, width: view.frame.width, height: 200 + .bottomOffset))
        keyboardView.delegate = self
        
        keyTextField.delegate = self
        keyTextField.inputView = keyboardView
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        keyTextField.becomeFirstResponder()
    }

    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let currentString: NSString = textField.text! as NSString
        let newString: NSString =
            currentString.replacingCharacters(in: range, with: string) as NSString
        return newString.length <= keyCodeLength
    }
    

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return selectedContracts.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: ContractIDCollectionViewCell.className, for: indexPath) as! ContractIDCollectionViewCell
        
        cell.setupView(with: selectedContracts[indexPath.row])
        cell.setTapFinish {
            self.remove(contractId: self.selectedContracts[indexPath.row].contractID)
        }
        return cell
    }
    
    func remove(contractId: String?) {
        self.selectedContracts.removeAll(where: { params -> Bool in
           return params.contractID == contractId
        })
        
        contactsList.reloadData()
        if(self.selectedContracts.count == 0) {
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    @IBAction func addTapped(_ sender: UIButton) {
        sender.hideKeyboard()
        self.validateAndSend(number: keyTextField.text)
    }
    
    func validateAndSend(number: String?) {
        if let number = number {
            if(isKeyValid(number: number)){
                keyTextField.hideKeyboard()
                sendAddRequest(for: number)
            }else {
                Utils.shared.showToast(from: self, with: "Код ключа слишком короткий!")
            }
        }else {
            keyTextField.becomeFirstResponder()
            Utils.shared.showToast(from: self, with: "Введите код ключа!")
        }
    }
    
    func isKeyValid(number: String) -> Bool {
        return number.count == keyCodeLength
    }
    
    func sendAddRequest(for number: String) {
        guard connectionObserver.isConnectedToInternet else {
            AlertManager.shared.showAlertNoInternet(from: self)
            return
        }
        
        var selectedIDs = [String]()
        
        selectedContracts.forEach { item in
            selectedIDs.append(item.contractID!)
        }
        
        AlertManager.shared.showProgressDialog(from: self)
        
        RestClient.shared.addNewKey(number: number, contacts: selectedIDs, success: { [weak self] () in
            AlertManager.shared.hideProgress(completion: {
                guard let self = self else { return }
                self.delegate?.keyAdded()
                Utils.shared.showToast(from: self.view, with: "Ключ успешно добавлен.")
                self.navigationController?.popViewController(animated: true)
            })
            
        }) { [weak self] (error)  in
            AlertManager.shared.hideProgress()
            Utils.shared.showToastHttp(from: self?.view, with: "При добавлении ключа произошла ошибка. ", error: error)
        }
    }

}
extension AddKeyVC: KeyKeyboardCellDelegate {
    
    func keyKeyboardTouch(type: KeyButtonType, character: Character) {
        switch type {
        case .delete:
            var newText = keyTextField.text ?? ""
            guard newText.count > 0 else { return }
            if let startIndexInt = keyTextField.selectedRangeStart, let finishIndexInt = keyTextField.selectedRangeEnd, startIndexInt > 0 {
                let leftIndex = newText.index(newText.startIndex, offsetBy: startIndexInt - (startIndexInt == finishIndexInt ? 1 : 0))
                let rightIndex = newText.index(newText.startIndex, offsetBy: finishIndexInt)
                let leftStr = newText[..<leftIndex]
                let rightStr = newText[rightIndex...]
                newText = String(leftStr + rightStr)
            }
            keyTextField.text = newText
            break
        case .ok:
            keyTextField.resignFirstResponder()
            break
        case .text:
            var newText = keyTextField.text ?? ""
            if let indexInt = keyTextField.selectedRangeStart {
                let leftIndex = newText.index(newText.startIndex, offsetBy: indexInt)
                let leftStr = newText[..<leftIndex]
                let rightStr = newText[leftIndex...]
                newText = leftStr + "\(character)" + rightStr
            } else {
                newText += "\(character)"
            }
            keyTextField.text = newText
            break
        }
    }
    
}
