//
//  EditCommentVC.swift
//  ProDomofon
//
//  Created by Александр Новиков on 01/12/2019.
//  Copyright © 2019 Александр Новиков. All rights reserved.
//

import UIKit

class EditCommentVC: InputViewController {
    
    @IBOutlet weak var closeBtn: UIButton!
    @IBOutlet weak var changeBtn: UIButton!
    @IBOutlet weak var commentField: UITextField!
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var mainScroll: UIScrollView!
    @IBOutlet weak var contentHeightConstraint: NSLayoutConstraint!
    
    var selectedKey: Key?
    
    override func viewDidLoad() {
        super.viewDidLoad(scrollView: mainScroll, constraintContentHeight: contentHeightConstraint)
        contentView.setCornerRadius(radius: 10)
        
        commentField.text = selectedKey?.comment
        commentField.delegate = self
        
        //TODO доработать обновление ключа
    }

    
    @IBAction func closeTapped(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func changeTapped(_ sender: UIButton) {
        commentField.hideKeyboard()
        guard let text = commentField.text else {
            AlertManager.shared.showAlert(from: self, title: "Заполните поле для коментария или нажмите «Отмена»", message: nil)
            return
        }
        
        if let key = selectedKey {
            editKeyComment(compositeId: key.contractId ?? "", keyId: key.id, comment: text)
        }
    }
    
    func editKeyComment(compositeId: String, keyId: String, comment: String) {
        AlertManager.shared.showProgressDialog(from: self)
        RestClient.shared.editKeyComment(keyId: keyId, comment: comment, success: { [weak self]  (newKey) in
//            RealmDb.shared.updateKey(newKey: newKey, compositeId: compositeId)
            AlertManager.shared.hideProgress(completion: {
                self?.dismiss(animated: true, completion: nil)
            })
        }) { [weak self]  (error) in
            AlertManager.shared.hideProgress()
            Utils.shared.showToastHttp(from: self?.view, with: "При добавлении ключа произошла ошибка. ", error: error)
        }
    }

}
