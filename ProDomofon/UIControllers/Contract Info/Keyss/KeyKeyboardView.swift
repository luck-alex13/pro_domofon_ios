//
//  KeyKeyboardView.swift
//  ProDomofon
//
//  Created by Алексей Агильдин on 24.03.2021.
//  Copyright © 2021 Александр Новиков. All rights reserved.
//

import UIKit

class KeyKeyboardView: UIView {

    fileprivate var keyboardCollectionView: UICollectionView!
    fileprivate var width: CGFloat = 0
    fileprivate var height: CGFloat = 0
    
    weak var delegate: KeyKeyboardCellDelegate?
    
    override func draw(_ rect: CGRect) {
        guard keyboardCollectionView == nil else { return }
        
        width = (rect.width - 8) / 5
        height = (rect.height - .bottomOffset) / 4
        
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        layout.minimumLineSpacing = 0
        layout.minimumInteritemSpacing = 0
        
        keyboardCollectionView = UICollectionView(frame: CGRect(x: 0, y: 0, width: rect.width, height: rect.height), collectionViewLayout: layout)
        keyboardCollectionView.register(KeyKeyboardCell.self, forCellWithReuseIdentifier: "KeyKeyboardCell")
        keyboardCollectionView.backgroundColor = .clear
        keyboardCollectionView.isScrollEnabled = false
        keyboardCollectionView.allowsSelection = false
        keyboardCollectionView.delegate = self
        keyboardCollectionView.dataSource = self
        addSubview(keyboardCollectionView)
    }
    
}
extension KeyKeyboardView: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 18
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "KeyKeyboardCell", for: indexPath) as! KeyKeyboardCell
        cell.contentView.subviews.forEach({ $0.removeFromSuperview() })
        cell.delegate = self.delegate
        
        switch indexPath.row {
        case 0...2: cell.setupCell(character: Array("\(indexPath.row + 7)")[0])
        case 5...7: cell.setupCell(character: Array("\(indexPath.row - 1)")[0])
        case 10...12: cell.setupCell(character: Array("\(indexPath.row - 9)")[0])
        case 3: cell.setupCell(character: "A")
        case 4: cell.setupCell(character: "B")
        case 8: cell.setupCell(character: "C")
        case 9: cell.setupCell(character: "D")
        case 13: cell.setupCell(character: "E")
        case 14: cell.setupCell(character: "F")
        case 15: cell.setupCell(character: "0")
        case 16: cell.setupCell(type: .delete)
        case 17: cell.setupCell(type: .ok)
        default: break
        }
        
        return cell
    }
    
}
extension KeyKeyboardView: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if indexPath.row == 15 {
            return CGSize(width: width * 3 + 4, height: height)
        } else if indexPath.row == 16 {
            return CGSize(width: width + 3, height: height)
        } else if indexPath.row == 17 {
            return CGSize(width: width, height: height)
        } else {
            return CGSize(width: width, height: height)
        }
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let cell = collectionView.cellForItem(at: indexPath)
        cell?.layer.borderWidth = 2.0
        cell?.layer.borderColor = UIColor.gray.cgColor
    }
}
