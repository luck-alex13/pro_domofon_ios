//
//  SelectKeyVC.swift
//  ProDomofon
//
//  Created by Александр Новиков on 01/12/2019.
//  Copyright © 2019 Александр Новиков. All rights reserved.
//

import UIKit

class SelectKeyVC: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource {
    
    @IBOutlet weak var closeBtn: UIButton!
    @IBOutlet weak var changeBtn: UIButton!
    @IBOutlet weak var numberPickerView: UIPickerView!
    @IBOutlet weak var contentView: UIView!
    
    var delegate: SelectKeyVCDelegate?
    
    var keys: [Key] = []
    var selectedValue: Key?

    override func viewDidLoad() {
        super.viewDidLoad()
        contentView.setCornerRadius(radius: 10)
        
        setEmptyBackArrow()
        
        // Connect data:
        self.numberPickerView.delegate = self
        self.numberPickerView.dataSource = self
        // Do any additional setup after loading the view.
    }
    
    @IBAction func closeTapped(_ sender: UIButton) {
        delegate?.onCancelled()
        delegate = nil
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func changeTapped(_ sender: UIButton) {
        if let value = selectedValue {
            delegate?.onKeySelected(key: value)
            delegate = nil
        } else {
            delegate?.onKeySelected(key: keys[numberPickerView.selectedRow(inComponent: 0)])
            delegate = nil
        }
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return keys.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return keys[row].number
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        selectedValue = keys[row]
    }

}

protocol SelectKeyVCDelegate: class {
    func onKeySelected(key: Key)
    
    func onCancelled()
}
