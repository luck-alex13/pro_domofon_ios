//
//  KeyKeyboardCell.swift
//  ProDomofon
//
//  Created by Алексей Агильдин on 24.03.2021.
//  Copyright © 2021 Александр Новиков. All rights reserved.
//

import UIKit

protocol KeyKeyboardCellDelegate: class {
    func keyKeyboardTouch(type: KeyButtonType, character: Character)
}

enum KeyButtonType {
    case text, delete, ok
}

class KeyKeyboardCell: UICollectionViewCell {
    
    fileprivate var button: UIButton!
    
    fileprivate var character: Character = "_"
    fileprivate var type: KeyButtonType = .text
    
    weak var delegate: KeyKeyboardCellDelegate?
    
    func setupCell(character: Character) {
        setupCell(type: .text, character: character)
    }
    
    func setupCell(type: KeyButtonType) {
        setupCell(type: type, character: "_")
    }
    
    fileprivate func setupCell(type: KeyButtonType, character: Character) {
        contentView.backgroundColor = .clear
        button = UIButton(frame: CGRect(x: 4, y: 4, width: contentView.frame.width - 8, height: contentView.frame.height - 8))
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 16)
        button.titleLabel?.textAlignment = .center
        button.setTitleColor(Colors.GREEN_PRIMARY, for: .normal)
        button.backgroundColor = .white
        button.setRoundedBorder(color: Colors.GREEN_DARK, borderWidth: 1, radius: 5)
        let heightImage = button.frame.height - 16
//        let widthImage = heightImage * 50 / 29
        let sideOffset = (button.frame.width - heightImage) / 2
        button.imageEdgeInsets = UIEdgeInsets(top: 8, left: sideOffset, bottom: 8, right: sideOffset)
        button.tintColor = Colors.GREEN_PRIMARY
        button.addTarget(self, action: #selector(buttonTouch), for: .touchUpInside)
        contentView.addSubview(button)
        
        self.type = type
        switch type {
        case .text:
            self.character = character
            button.setTitle("\(character)", for: .normal)
            break
        case .ok: button.setTitle("ОК", for: .normal)
        case .delete: button.setImage(UIImage(named: "backspace")?.withRenderingMode(.alwaysTemplate), for: .normal)
        }
    }
    
    @objc
    func buttonTouch(){
        delegate?.keyKeyboardTouch(type: type, character: character)
    }
}
