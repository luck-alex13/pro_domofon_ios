//
//  KeysViewCell.swift
//  ProDomofon
//
//  Created by Александр Новиков on 16/08/2019.
//  Copyright © 2019 Александр Новиков. All rights reserved.
//

import UIKit

protocol KeySelectionProcessDelegate: class {
    
    func onSelected(key: Key)
    
    func onDeselected(key: Key)
    
    func onLongPressed()
    
    func onEditTapped(item: ContractsKeys)
    
    func onShopsTapped(conractId: String)
}

class KeysViewCell: UITableViewCell {
    
    @IBOutlet weak var keyLabel: UILabel!
    @IBOutlet weak var keyCheckbox: Checkbox! {
        didSet {
            keyCheckbox.valueChanged = { (value) in
                if(value){
                    self.delegate?.onSelected(key: self.key)
                }else {
                    self.delegate?.onDeselected(key: self.key)
                }
            }
            keyCheckbox.cornerRadius = 5
            keyCheckbox.checkedBorderColor = Colors.RED_ACCENT
            keyCheckbox.uncheckedBorderColor = .gray
            keyCheckbox.checkmarkColor = Colors.RED_ACCENT
            keyCheckbox.checkmarkStyle = .tick
            keyCheckbox.checkmarkSize = 0.6
        }
    }
    @IBOutlet weak var keyIconConstraint: NSLayoutConstraint!
    @IBOutlet weak var keyImageView: UIImageView!
    @IBOutlet weak var commentLabel: UILabel!
    
    weak var delegate: KeySelectionProcessDelegate?
    var key: Key!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        NotificationCenter.default.addObserver(self, selector: #selector(longPressPhones), name: .longPressPhones, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(cancelLongPressPhones), name: .cancelLongPressPhones, object: nil)
    }

    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    @objc
    func longPressPhones(){
        keyCheckbox.isHidden = false
        keyIconConstraint.constant = CGFloat(41)
    }
    
    @objc
    func cancelLongPressPhones(){
        keyCheckbox.isHidden = true
        keyIconConstraint.constant = CGFloat(8)
    }

}
