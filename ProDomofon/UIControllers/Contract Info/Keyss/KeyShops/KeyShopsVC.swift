//
//  KeyShopsVC.swift
//  ProDomofon
//
//  Created by Александр Новиков on 04/12/2019.
//  Copyright © 2019 Александр Новиков. All rights reserved.
//

import UIKit

class KeyShopsVC: VCWithTable<KeyShop> {

    @IBOutlet weak var shopsTableView: UITableView!
    
    var contractId: String?
    var houseID: Int?
    
    override func viewDidLoad() {
        if let id = contractId, let contract = DataStorage.shared.contracts.first(where: { $0.id == id }) {
            houseID = contract.houseId
        }
        super.viewDidLoad()
        setTitle(title: "Магазины")
        setUpRefreshControl()
        defineVariables(sections: 1, cellIdentifer: ShopViewCell.className, placeholderTitle: "Здесь будут отображаться ближайшие точки продажи ключей.")
        
        shopsTableView.estimatedRowHeight = 130
        shopsTableView.rowHeight = UITableView.automaticDimension
        self.tableView = shopsTableView
       
    }
    
    override func readDataFromDatabase() {
//        if let houseID = houseID {
//            dataArray = DataStorage.shared.keyShops.filter({ $0.houseID == houseID })
//        } else {
            dataArray = DataStorage.shared.keyShops
//        }
        updateTableView()
    }
    
    override func loadDataFromServer(_ refreshControl: UIRefreshControl?) {
        guard let houseID = houseID else { return }
        refreshControl?.beginRefreshing()
        RestClient.shared.getKeyShops(houseId: houseID, success: { [weak self] (shops) in
            guard let self = self else { return }
            self.refreshControl?.endRefreshing()
//            for shop in shops {
//                shop.houseID = self.houseID ?? 0
//            }
            DataStorage.shared.keyShops = shops
        }) { [weak self] (error) in
            guard let self = self else { return }
            self.refreshControl?.endRefreshing()
            Utils.shared.showToastHttp(from: self.navigationController, with: "Не удалось загрузить магазины.", error: error)
        }
    }
    

    @IBAction func moreTapped(_ sender: Any) {
        Utils.shared.openUrl(url: StringConst.ALL_KEY_SHOPS.rawValue)
    }
    
    // MARK: - Table view data source
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifer!, for: indexPath) as! ShopViewCell
        cell.setupCell(with: dataArray[indexPath.row])
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        Utils.shared.openUrl(url: StringConst.KEY_SHOP_PAGE.rawValue + dataArray[indexPath.row].point.id)
    }
    
}
