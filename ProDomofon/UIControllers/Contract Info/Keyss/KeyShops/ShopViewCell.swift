//
//  ShopViewCell.swift
//  ProDomofon
//
//  Created by Александр Новиков on 04/12/2019.
//  Copyright © 2019 Александр Новиков. All rights reserved.
//

import UIKit

class ShopViewCell: UITableViewCell {

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setupCell(with itemData: KeyShop) {
        nameLabel.text = itemData.name
        addressLabel.text = itemData.address
        timeLabel.text = itemData.worktime
    }

}
