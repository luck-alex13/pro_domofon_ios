//
//  ContractKeysViewCell.swift
//  ProDomofon
//
//  Created by Александр Новиков on 15/08/2019.
//  Copyright © 2019 Александр Новиков. All rights reserved.
//

import UIKit


class ContractKeysViewCell: UITableViewCell, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet weak var contactNumberLabel: UILabel!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var keysTableView: UITableView! {
        didSet {
            keysTableView.dataSource = self
            keysTableView.delegate = self
        }
    }
    
    @IBOutlet weak var tvHeigthConstraint: NSLayoutConstraint!
    @IBOutlet weak var editButton: UIButton!
    
    weak var selectionDelegate: KeySelectionProcessDelegate?

    var itemData: ContractsKeys?
    fileprivate var isActivated = false
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        NotificationCenter.default.addObserver(self, selector: #selector(longPressPhones), name: .longPressPhones, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(cancelLongPressPhones), name: .cancelLongPressPhones, object: nil)
    }

    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    @objc
    func longPressPhones(){
        isActivated = true
    }
    
    @objc
    func cancelLongPressPhones(){
        isActivated = false
    }
    
    func setupCell(with itemData: ContractsKeys) {
        self.itemData = itemData
        keysTableView.reloadData()
        contactNumberLabel.text = "Договор #\(itemData.contractNumber)"
        addressLabel.text = itemData.address
        
        editButton.isHidden = itemData.keys.count == 0
        
        let heigth = itemData.keys.count * 50
        tvHeigthConstraint.constant = CGFloat(heigth)
        
        let longPressGestureRecognizer = UILongPressGestureRecognizer(target: self, action: #selector(longPressed))
        keysTableView.addGestureRecognizer(longPressGestureRecognizer)
        
    }
    @IBAction func editTapped(_ sender: UIButton) {
        if let itemData = itemData {
            selectionDelegate?.onEditTapped(item: itemData)
        }
        
    }
    @IBAction func shopsTapped(_ sender: Any) {
        if let itemData = itemData {
            selectionDelegate?.onShopsTapped(conractId: itemData.contractId)
        }
    }
    
    @objc func longPressed(sender: UILongPressGestureRecognizer) {
        selectionDelegate?.onLongPressed()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return itemData?.keys.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: KeysViewCell.className, for: indexPath) as! KeysViewCell
        let key = itemData?.keys[indexPath.row]
        key?.contractId = itemData?.contractId ?? ""
        cell.key = key
        cell.delegate = self.selectionDelegate
        
        cell.keyLabel.text = itemData?.keys[indexPath.row].number
        cell.commentLabel.text = itemData?.keys[indexPath.row].comment
        
        cell.keyCheckbox.isHidden = !isActivated
        cell.keyCheckbox.isChecked = false
        
        cell.keyImageView.image = itemData!.keys[indexPath.row].removed == true ? #imageLiteral(resourceName: "ic_lock_basic").imageWithColor(color1: UIColor.darkGray) : #imageLiteral(resourceName: "ic_key_common").imageWithColor(color1: Colors.GREEN_DARK)
        cell.keyIconConstraint.constant = isActivated ? CGFloat(41) : CGFloat(8)
        return cell
    }
    
}
