//
//  KeysVC.swift
//  ProDomofon
//
//  Created by Александр Новиков on 15/08/2019.
//  Copyright © 2019 Александр Новиков. All rights reserved.
//

import UIKit
import RxSwift

class KeysVC: TableVC<ContractsKeys> {
    
    var selecting: Bool = false
    var selectedKeys = [Key]()
    let disposeBag = DisposeBag()
    var selectVC: SelectKeyVC?
    var contractID: String? {
        if let c =  DataStorage.shared.choosenContract {
            return c.id
        } else {
            return nil
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        setDefaultBar()
        
        defineVariables(sections: 1, cellIdentifer: ContractKeysViewCell.className, placeholderTitle: "Здесь будут отбражаться ключи привязанные к договорам.", placeholderSubtile: nil)
        tableView.estimatedRowHeight = 160
        tableView.rowHeight = UITableView.automaticDimension
        
        setUpRefreshControl()
    }
    
    func setDefaultBar(){
        setBar(title: "Ключи", rightTitle: "Действия", rightSelector: #selector(showKeysActionsMenu))
    }
    
    @objc
    func showKeysActionsMenu() {
        let alert = UIAlertController(title: nil, message: "Выберите действие", preferredStyle: .actionSheet)
        
        
        alert.addAction(UIAlertAction(title: "Добавить ключ", style: .default, handler: { (_) in
            self.selectContractForKey()
        }))
        
        let unlockAction = UIAlertAction(title: "Разблокировать ключ", style: .default) { (_) in
            self.onLongPressed()
        }
        if dataArray.filter({ $0.keys.filter({ $0.removed }).count > 0 }).count > 0 {
            alert.addAction(unlockAction)
        }
        
        let deleteAction = UIAlertAction(title: "Удалить ключ", style: .default) { (_) in
            self.onLongPressed()
        }
        if dataArray.count > 0 {
            deleteAction.isEnabled = dataArray[0].keys.count > 0
        }
        alert.addAction(deleteAction)
        
//        alert.addAction(UIAlertAction(title: "Объединить ключи", style: .default, handler: { (_) in
//            self.combineKeys()
//        }))
        
        alert.addAction(UIAlertAction(title: "Закрыть", style: .cancel, handler: nil))
        
        self.present(alert, animated: true, completion: {
            print("completion block")
        })
    }
    
    @objc func selectContractForKey() {
        let contracts = DataStorage.shared.contracts
        let c = DataStorage.shared.choosenContract
        if contracts.count == 1 || !(c?.isOwner ?? true) {
            if let nextViewController = Utils.shared.additionalStoryboard().instantiateViewController(withIdentifier: AddKeyVC.className) as? AddKeyVC {
                nextViewController.selectedContracts = [InfoParams(contractID: c?.id ?? "", contractNumber: c?.number ?? 0)]
                self.navigationController?.pushViewController(nextViewController, animated: true)
            }
        } else {
            if let nextViewController = Utils.shared.additionalStoryboard().instantiateViewController(withIdentifier: SelectContractVC.className) as? SelectContractVC {
                if let curContract = contracts.first(where: { $0.id == contractID }) {
                    nextViewController.selectedIDs = [InfoParams(contractID: curContract.id, contractNumber: curContract.number)]
                }
                nextViewController.goal = .keys
                self.navigationController?.pushViewController(nextViewController, animated: true)
            }
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        cancelTapped()
        super.viewWillDisappear(animated)
    }
    
    
    override func loadDataFromServer(_ refreshControl: UIRefreshControl?) {
        refreshControl?.beginRefreshing()
        RestClient.shared.getKeys(success: { [weak self] (keys) in
            guard let self = self else { return }
            refreshControl?.endRefreshing()
            DataStorage.shared.contractKeys = keys
            self.readDataFromDatabase()
        }) { [weak self] (err) in
            refreshControl?.endRefreshing()
            Utils.shared.showToastHttp(from: self?.navigationController, with: "Не удалось загрузить ключи.", error: err)
        }
    }
    
    override func readDataFromDatabase() {
        if let cID = contractID {
            dataArray = DataStorage.shared.contractKeys.filter({ $0.contractId == cID })
        } else {
            dataArray = DataStorage.shared.contractKeys
        }
        self.updateTableView()
    }

    // MARK: - Table view data source

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifer!, for: indexPath) as! ContractKeysViewCell
        
        cell.setupCell(with: dataArray[indexPath.row])
        cell.selectionDelegate = self
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    func updateDeleteButton() {
        navigationItem.rightBarButtonItem?.isEnabled = selectedKeys.count > 0
    }
    
    @objc func cancelTapped(){
        selecting = false
        setDefaultBar()
        NotificationCenter.default.post(name: .cancelLongPressPhones, object: nil)
        selectedKeys = []
        updateTableView()
    }
    
    @objc
    func unlockTapped() {
        guard let c = DataStorage.shared.choosenContract else { return }
        let lockedKeys = selectedKeys.filter({ $0.removed })
        guard lockedKeys.count > 0 else {
            AlertManager.shared.showAlert(from: self, title: "Ошибка", message: "Не выбраны заблокированные ключи")
            return
        }
        unlockKeys(number: c.number, selected: lockedKeys)
    }
    
    
    @objc
    func deleteTapped() {
        let end = selectedKeys.count > 1 ? "выбранные ключи" : "выбранный ключ"
        let alert = AlertManager.shared.buildOkCancelAlert(title: "Внимание!", errorMessage: "Вы действительно хотите удалить \(end)?", okHandler: { (act) in
            self.deleteKeys(selected: self.selectedKeys)
        }) { (act) in
            
        }
        
        self.present(alert, animated: true, completion: nil)
    }
    
    func unlockKeys(number: Int, selected: [Key]) {
        AlertManager.shared.showProgressDialog(from: self)
        RestClient.shared.unlockKeys(number: number, keys: selected.map({ $0.number })) { [weak self] in
            guard let self = self else { return }
            AlertManager.shared.hideProgress {
                self.cancelTapped()
                self.loadDataFromServer(self.refreshControl)
            }
        } errorCall: { error in
            AlertManager.shared.hideProgress {
                Utils.shared.showToastHttp(from: self.view, with: "При запросе разблокировки ключей произошла ошибка. ", error: error)
            }
        }

    }
    
    func deleteKeys(selected: [Key]) {
        guard let observer = super.сonnectionObserver, observer.isConnectedToInternet else {
            AlertManager.shared.showAlertNoInternet(from: self)
            return
        }
        AlertManager.shared.showProgressDialog(from: self)
        
        Observable.from(selected)
            .concatMap { (key) -> Observable<Key> in
                return RestClient.shared.removeKey(keyId: key.id, contactID: key.contractId ?? "")
            }
            .do(onNext: { (resp) in
                //self.savePhones(response: resp)
            })
            .toArray()
            .subscribe(onNext: { (resp) in
                self.selectedKeys = []
                AlertManager.shared.hideProgress()
                self.cancelTapped()
                self.loadDataFromServer(self.refreshControl)
            }, onError: { (error) in
                AlertManager.shared.hideProgress()
                Utils.shared.showToast(from: self.navigationController, with: "Произошла ошибка. Не удалось удалить ключ.")
            })
            .disposed(by: disposeBag)
    }
    
    func combineKeys()  {
        guard let observer = super.сonnectionObserver, observer.isConnectedToInternet else {
            AlertManager.shared.showAlertNoInternet(from: self)
            return
        }
        
       let alert = AlertManager.shared.buildOkCancelAlert(title: "Внимание!", errorMessage: "Ключи всех договоров будут объединены. Продолжить?", okHandler: { [weak self] (act) in
            AlertManager.shared.showProgressDialog(from: self)
            RestClient.shared.combineKeys(success: { [weak self] in
                AlertManager.shared.hideProgress()
                Utils.shared.showToast(from: self?.navigationController, with: "Ключи успешно объединены.")
                self?.loadDataFromServer(self?.refreshControl)
            }) { [weak self] (error) in
                AlertManager.shared.hideProgress()
                Utils.shared.showToastHttp(from: self?.navigationController, with: "Произошла ошибка. Не удалось объединить ключи.", error: error)
            }
        })
        present(alert, animated: true, completion: nil)
    }
}
extension KeysVC: KeySelectionProcessDelegate {
    func onSelected(key: Key) {
        selectedKeys.append(key)
        updateDeleteButton()
    }
    
    func onDeselected(key: Key) {
        if let ind = selectedKeys.firstIndex(where: { $0.id == key.id }) {
            selectedKeys.remove(at: ind)
            updateDeleteButton()
        }
        updateDeleteButton()
    }
    
    @objc
    func onSelectActionTouch() {
        let alert = UIAlertController(title: nil, message: "Выберите действие", preferredStyle: .actionSheet)
        
        let unlockAction = UIAlertAction(title: "Разблокировать ключ", style: .default) { (_) in
            self.unlockTapped()
        }
        alert.addAction(unlockAction)
        
        let deleteAction = UIAlertAction(title: "Удалить ключ", style: .default) { (_) in
            self.deleteTapped()
        }
        alert.addAction(deleteAction)
        
//        alert.addAction(UIAlertAction(title: "Объединить ключи", style: .default, handler: { (_) in
//            self.combineKeys()
//        }))
        
        alert.addAction(UIAlertAction(title: "Закрыть", style: .cancel, handler: nil))
        
        self.present(alert, animated: true, completion: {
            print("completion block")
        })
    }
    
    func onLongPressed() {
        if(!selecting) {
            navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Действия", style: .done, target: self, action: #selector(onSelectActionTouch))
            navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Отмена", style: .done, target: self, action: #selector(cancelTapped))
            selecting = true
            updateDeleteButton()
            NotificationCenter.default.post(name: .longPressPhones, object: nil)
        }
    }
    
    func onEditTapped(item: ContractsKeys) {
        if (item.keys.count > 1) {
            if let myAlert = Utils.shared.additionalStoryboard().instantiateViewController(withIdentifier: SelectKeyVC.className) as? SelectKeyVC {
                self.selectVC = myAlert
                myAlert.keys = item.keys
                myAlert.delegate = self
                myAlert.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
                myAlert.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
                self.present(myAlert, animated: true, completion: nil)
            }
        } else {
            if let myAlert = Utils.shared.additionalStoryboard().instantiateViewController(withIdentifier: EditCommentVC.className) as? EditCommentVC {
                myAlert.selectedKey = item.keys[0]
                myAlert.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
                myAlert.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
                self.present(myAlert, animated: true, completion: nil)
            }
        }
    }
    
    func onShopsTapped(conractId: String) {
        if let nextViewController = Utils.shared.additionalStoryboard().instantiateViewController(withIdentifier: KeyShopsVC.className) as? KeyShopsVC {
            nextViewController.contractId = conractId
            self.navigationController?.pushViewController(nextViewController, animated: true)
        }
    }
}

extension KeysVC: SelectKeyVCDelegate {
    
    func onKeySelected(key: Key) {
        self.selectVC?.dismiss(animated: false, completion: {
            if let myAlert = Utils.shared.additionalStoryboard().instantiateViewController(withIdentifier: EditCommentVC.className) as? EditCommentVC {
                myAlert.selectedKey = key
                myAlert.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
                myAlert.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
                self.present(myAlert, animated: true, completion: nil)
            }
        })
        
    }
    
    func onCancelled() {
    }
}

