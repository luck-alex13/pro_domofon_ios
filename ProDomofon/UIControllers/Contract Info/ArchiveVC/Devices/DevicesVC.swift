//
//  DevicesVC.swift
//  ProDomofon
//
//  Created by Lexus on 26.07.2022.
//  Copyright © 2022 Алексей Агильдин. All rights reserved.
//

import UIKit
import AlignedCollectionViewFlowLayout

class DevicesVC: TabbedVC {
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    var contracts: [Contract]?
    
    private let itemsPerRow: CGFloat = 2
    private let sectionInsets = UIEdgeInsets(
      top: 8.0,
      left: 16.0,
      bottom: 8.0,
      right: 16.0)
    private var widthPerItem: CGFloat = 100
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let flowLayout = AlignedCollectionViewFlowLayout(horizontalAlignment: .left, verticalAlignment: .top)
        
        let paddingSpace = sectionInsets.left * (itemsPerRow + 1)
        let availableWidth = view.frame.width - paddingSpace
        widthPerItem = availableWidth / itemsPerRow
        flowLayout.minimumLineSpacing = sectionInsets.left
        flowLayout.sectionInset = sectionInsets
        flowLayout.estimatedItemSize = .init(width: widthPerItem, height: 2 * widthPerItem)
        
        collectionView.collectionViewLayout = flowLayout
        
        collectionView.register(DevicesCollectionReusableView.self, forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: DevicesCollectionReusableView.reuseIdentifierHeader)
    }
    
    @objc
    func updateData() {
        collectionView.reloadData()
    }
    
    func changeTitles(contract: Contract, devicePanel: DevicePanel, name: String) {
        let newEl = ContractPanelName(deviceId: devicePanel.deviceId, panel: devicePanel.panel.field, name: name)
        if let index = contract.customNamesOfPanel.firstIndex(where: { $0.deviceId == newEl.deviceId && $0.panel == newEl.panel }) {
            contract.customNamesOfPanel[index] = newEl
        } else {
            contract.customNamesOfPanel.append(newEl)
        }
        
        if let data = contract.customNamesOfPanelData {
            AlertManager.shared.showProgressDialog(from: self)
            RestClient.shared.editContract(contractId: contract.id, data: data) { [weak self] resp in
                AlertManager.shared.hideProgress {
                    self?.updateData()
                }
            } errorCall: { [weak self] (error) in
                AlertManager.shared.hideProgress {
                    Utils.shared.showToastHttp(from: self?.view, with: "Не удалось сохранить название камеры.", error: error)
                }
            }
        } else {
            Utils.shared.showToast(from: self.view, with: "Ошибка при преобразовании объекта customNamesOfPanel в json")
        }
    }
    
}

extension DevicesVC: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        return CGSize(width: collectionView.frame.width, height: 30)
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        let header = collectionView.dequeueReusableSupplementaryView(ofKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: DevicesCollectionReusableView.reuseIdentifierHeader, for: indexPath) as! DevicesCollectionReusableView
        header.setupView(contract: contracts?[indexPath.section], width: view.frame.width)
        
        return header
    }
}

extension DevicesVC: UICollectionViewDataSource {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return contracts?.count ?? 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return contracts?[section].deviceCameraPanels.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "DeviceCell", for: indexPath)
        
        if let dcell = cell as? DeviceCollectionViewCell {
            dcell.widthConstraint.constant = widthPerItem
            dcell.heightConstraint.constant = 2 * widthPerItem
            dcell.setupView(contract: contracts?[indexPath.section], devicePanel: contracts?[indexPath.section].deviceCameraPanels[indexPath.row])
            dcell.delegate = self
        } else {
            cell.backgroundColor = .random
        }
        
        return cell
    }
}
extension DevicesVC: DeviceCollectionViewCellDelegate {
    func changeTitle(contract: Contract, devicePanel: DevicePanel) {
        
        let alertVC = UIAlertController(title: "Введите новое название камеры", message: nil, preferredStyle: .alert)
        
        alertVC.addTextField { (textField) in
            textField.placeholder = "Название"
            textField.text = devicePanel.name
        }
        
        let cancelAction = UIAlertAction(title: "Отмена", style: .cancel)
        alertVC.addAction(cancelAction)
        
        let okAction = UIAlertAction(title: "Ок", style: .default) { [weak self] _ in
            guard let self = self else { return }
            guard let textFields = alertVC.textFields else { return }
            guard let nameText = textFields[0].text else { return }
            self.changeTitles(contract: contract, devicePanel: devicePanel, name: nameText)
        }
        alertVC.addAction(okAction)
        
        self.navigationController?.present(alertVC, animated: true)
    }
    
    func tapDevice(contract: Contract, devicePanel: DevicePanel) {
        if let nvc = Utils.shared.additionalStoryboard().instantiateViewController(withIdentifier: DeviceVC.className) as? DeviceVC {
            nvc.contract = contract
            nvc.devicePanel = devicePanel
            self.navigationController?.pushViewController(nvc, animated: true)
        }
    }
}

final class DevicesCollectionReusableView: UICollectionReusableView {

    static let reuseIdentifierHeader = "devicesHeaderView"
    fileprivate var titleLabel: UILabel!
    
    func setupView(contract: Contract?, width: CGFloat) {
        if titleLabel == nil {
            titleLabel = UILabel(frame: CGRect(x: 16, y: 0, width: width - 32, height: 30))
            titleLabel.font = UIFont.systemFont(ofSize: 17)
            titleLabel.textColor = .gray
            titleLabel.textAlignment = .left
            addSubview(titleLabel)
        }
        
        titleLabel.text = contract?.getReadableAddress ?? "undefined address"
    }

}
