//
//  DeviceCollectionViewCell.swift
//  ProDomofon
//
//  Created by Lexus on 28.07.2022.
//  Copyright © 2022 Алексей Агильдин. All rights reserved.
//

import UIKit

protocol DeviceCollectionViewCellDelegate: class {
    func changeTitle(contract: Contract, devicePanel: DevicePanel)
    func tapDevice(contract: Contract, devicePanel: DevicePanel)
}

class DeviceCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var imageView: UIImageView! {
        didSet {
            imageView.contentMode = .scaleAspectFill
            let tapPressImageView = UITapGestureRecognizer(target: self, action: #selector(tapPressImageView))
            tapPressImageView.cancelsTouchesInView = true
            imageView.addGestureRecognizer(tapPressImageView)
            imageView.isUserInteractionEnabled = true
            imageView.setCornerRadius(radius: 5)
        }
    }
    @IBOutlet weak var nameLabel: UILabel! {
        didSet {
            let tapPressLabel = UITapGestureRecognizer(target: self, action: #selector(tapPressLabel))
            tapPressLabel.cancelsTouchesInView = true
            nameLabel.addGestureRecognizer(tapPressLabel)
            nameLabel.isUserInteractionEnabled = true
        }
    }
    @IBOutlet weak var heightConstraint: NSLayoutConstraint!
    @IBOutlet weak var widthConstraint: NSLayoutConstraint!
    
    var delegate: DeviceCollectionViewCellDelegate?
    
    fileprivate var contract: Contract?
    fileprivate var devicePanel: DevicePanel?
    
    func setupView(contract: Contract?, devicePanel: DevicePanel?){
        self.contract = contract
        self.devicePanel = devicePanel
        
        nameLabel.text = " \(devicePanel?.name ?? "undefined") "
        nameLabel.sizeToFit()
        nameLabel.setCornerRadius(radius: 5)
        
        guard let devicePanel = devicePanel else { return }
        RestClient.shared.getCamLastPhoto(idContract: contract?.id ?? "undefined", devicePanel: devicePanel) { [weak self] (data) in
            guard let self = self else { return }
            guard let image = UIImage(data: data) else { return }
            self.imageView.image = image
        } errorCall: { [weak self] (err) in
            //guard let self = self else { return }
            print("Не удалось загрузить изображение.")
        }
    }
    
    @objc func tapPressLabel(){
        guard let contract = contract, let devicePanel = devicePanel else { return }
        delegate?.changeTitle(contract: contract, devicePanel: devicePanel)
    }
    
    @objc func tapPressImageView(){
        guard let contract = contract, let devicePanel = devicePanel else { return }
        delegate?.tapDevice(contract: contract, devicePanel: devicePanel)
    }
    
}
