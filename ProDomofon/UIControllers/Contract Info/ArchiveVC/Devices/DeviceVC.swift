//
//  DeviceVC.swift
//  ProDomofon
//
//  Created by Lexus on 26.07.2022.
//  Copyright © 2022 Алексей Агильдин. All rights reserved.
//

import UIKit
import VideoDecoder
import VideoToolbox
import WebKit

class DeviceVC: TabbedVC {
    
    @IBOutlet weak var scrollView: UIScrollView! {
        didSet {
            scrollView.delegate = self
        }
    }
    @IBOutlet weak var imageView: UIImageView! {
        didSet {
            imageView.contentMode = .scaleToFill
        }
    }
    @IBOutlet weak var webView: WKWebView! {
        didSet {
            webView.navigationDelegate = self
            webView.hideObject(0)
        }
    }
    
    fileprivate var controlVideoFlowView: ControlVideoFlowView!
    fileprivate var datePickerView: DatePickerView!
    
    var contract: Contract!
    var devicePanel: DevicePanel!
    var showingDate: Date? = Date()
    
    fileprivate var size: CGSize?
    
    fileprivate var decoder: VideoDecoder!
    
    fileprivate var videoHistories: [VideoHistory] = []
 
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = .black
        
        setEmptyBackArrow()
        setTitle(title: contract.getReadableAddress, fontSize: 14)
        
        decoder = H264Decoder(delegate: self)
        
        if #available(iOS 13.0, *) {
            controlVideoFlowView = ControlVideoFlowView(frame: CGRect(x: 0, y: view.frame.height - 150 - .bottomOffset, width: view.frame.width, height: 150 + .bottomOffset))
            controlVideoFlowView.delegate = self
            view.addSubview(controlVideoFlowView)
        }
        
        datePickerView = DatePickerView(frame: CGRect(x: 0, y: 0, width: view.frame.width, height: view.frame.height))
        datePickerView.delegate = self
        datePickerView.hideObject(0)
        view.addSubview(datePickerView)
        
        AlertManager.shared.showProgressDialog(from: self)
        RestClient.shared.getCamLastPhoto(idContract: contract.id, devicePanel: devicePanel) { [weak self] (data) in
            AlertManager.shared.hideProgress {
                guard let self = self else { return }
                guard let image = UIImage(data: data) else { return }
                guard self.imageView.image == nil else { return }
                self.imageView.image = image
            }
        } errorCall: { [weak self] (err) in
            print("Не удалось загрузить изображение.")
            AlertManager.shared.hideProgress()
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        stopVideo()
        UIApplication.shared.isIdleTimerDisabled = false
        controlVideoFlowView.hideObject(0)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        UIApplication.shared.isIdleTimerDisabled = true
        
        camRaiseMotion()
        
        RestClient.shared.videoArchiveRange(contractId: contract.id, deviceId: devicePanel.deviceId, panel: devicePanel.panel.field) { videoHistories in
            self.videoHistories = videoHistories
            self.controlVideoFlowView.setVideoHistories(videoHistories: videoHistories)
            self.controlVideoFlowView.showObject()
        } errorCall: { error in
            Utils.shared.showToastHttp(from: self.view, with: "Не удалось получить диапазон дат вызовов. Попробуйте позже.", error: error)
        }
    }
    
    func showVideo() {
        if #available(iOS 13.0, *) {
            WSManager.shared.setConnection(device: devicePanel.sip, panel: devicePanel.panel.field)
            WSManager.shared.delegate = self
            
            if let date = showingDate {
                controlVideoFlowView.setDate(date: date, accuracy: true)
                showingDate = nil
            } else {
                play(timeInterval: self.controlVideoFlowView.getTimeInterval())
            }
        } else {
            guard let url = URL(string: "https://video.domofon.dom38.ru/video4.html?device=\(devicePanel.sip)&user=\(DataStorage.shared.user?.id ?? "")&panel=\(devicePanel.panel.field)") else { return }
            print("webView url: \(url.absoluteString)")
            webView.load(URLRequest(url: url))
        }
    }
    
    func stopVideo() {
        if #available(iOS 13.0, *) {
            WSManager.shared.stopStream()
            controlVideoFlowView.stopPlaying()
        } else {
            webView.stopLoading()
        }
    }
    
    func camRaiseMotion(){
        RestClient.shared.camRaiseMotion(idContract: contract.id, deviceId: devicePanel.deviceId, panel: devicePanel.panel.field, success: {  [weak self] in
            guard let self = self else { return }
            self.showVideo()
        }, errorCall: { [weak self] (error)  in
            guard let self = self else { return }
            Utils.shared.showToastHttp(from: self.view, with: "При запросе cam raise motion произошла ошибка. ", error: error)
//            DispatchQueue.main.asyncAfter(deadline: .now() + 10) {
//                self.camRaiseMotion()
//            }
        })
    }
    
    func finishWithMessage(title: String, subtitle: String? = nil, message: String) {
        PushNotificationManager.shared.showNotification(title: title, subtitle: subtitle, body: message)
    }
    
}
extension DeviceVC: WSManagerDelegate {
    func receiveInit(width: Int, height: Int) {
        self.size = CGSize(width: width, height: height)
    }
    
    func receiveData(data: Data) {
        guard let size = size else { return }
        let videoPacket = VideoPacket(data, fps: 30, type: .h264, videoSize: size)
        decoder.decodeOnePacket(videoPacket)
    }
    
    func receiveTs(ts: Double) {
        controlVideoFlowView.setTimestamp(ts: ts)
    }
}
extension DeviceVC: VideoDecoderDelegate {
    func decodeOutput(video: CMSampleBuffer) {
        AlertManager.shared.hideProgress()
        if let image = video.image {
            DispatchQueue.main.async {
                self.controlVideoFlowView.checkPlayed()
                
                self.imageView.image = image
                self.imageView.showObject()
            }
        }
    }
    
    func decodeOutput(error: DecodeError) {
        print("decodeOutput error: \(error)")
    }
    
    func error() {
        AlertManager.shared.hideProgress()
    }
}
extension DeviceVC: WKNavigationDelegate {
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        self.webView.showObject()
        AlertManager.shared.hideProgress()
    }
}
extension DeviceVC: UIScrollViewDelegate {
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return webView.isHidden ? imageView : webView
    }
}
@available(iOS 13.0, *)
extension DeviceVC: ControlVideoFlowViewDelegate {
    func speedChanged(speedValue: Int) {
        WSManager.shared.requestSpeed(withValue: speedValue)
    }
    
    func play(timeInterval: Double) {
        AlertManager.shared.showProgressDialog(from: self)
        let nowDate = Date()
        
        /*
        print("nowDate: \(nowDate.timeIntervalSince1970)")
        print("timeInterval: \(timeInterval)")
        print("delta: \(timeInterval-nowDate.timeIntervalSince1970)")
        */
        
        if(abs(timeInterval-nowDate.timeIntervalSince1970) < 100){
            WSManager.shared.connect(type: .stream)
        } else {
            WSManager.shared.connect(type: .archive, withTimestart: timeInterval)
        }
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 5, execute: {
            AlertManager.shared.hideProgress()
        })
    }
    func pause() {
        WSManager.shared.stopStream()
    }
    
    func showSpeedAlert(speedFactor: Double) {
        let alert = UIAlertController(title: "Камера", message: "Текущая скорость воспроизведения: \(speedFactor)х. Выберите скорость воспроизведения", preferredStyle: .actionSheet)
        
        if speedFactor != 0.5 {
            let action = UIAlertAction(title: "0.5x", style: .default , handler:{ (alertAction)in
                self.controlVideoFlowView.setSpeed(value: 0.5)
            })
            alert.addAction(action)
        }
        
        if speedFactor != 1 {
            let action = UIAlertAction(title: "1x", style: .default , handler:{ (alertAction)in
                self.controlVideoFlowView.setSpeed(value: 1)
            })
            alert.addAction(action)
        }
        
        if speedFactor != 2 {
            let action = UIAlertAction(title: "2x", style: .default , handler:{ (alertAction)in
                self.controlVideoFlowView.setSpeed(value: 2)
            })
            alert.addAction(action)
        }
        
        if speedFactor != 3 {
            let action = UIAlertAction(title: "3x", style: .default , handler:{ (alertAction)in
                self.controlVideoFlowView.setSpeed(value: 3)
            })
            alert.addAction(action)
        }
        
        alert.addAction(UIAlertAction(title: "Отмена", style: .cancel, handler: nil))
        
        
        alert.popoverPresentationController?.sourceView = self.view

        self.present(alert, animated: true, completion: nil)
    }
    
    func showDateAlert(date: Date) {
        guard videoHistories.count > 0 else { return }
        
        datePickerView.setDate(date: date)
        datePickerView.setEnabledDate(videoHistories: videoHistories)
        datePickerView.showObject()
    }
    
    func openDoor() {
        RestClient.shared.openDoor(contractNumber: contract.number, reason: .device, device: devicePanel.deviceId, panel: devicePanel.panel.field, success: { [weak self] (resp) in
            guard let self = self else { return }
            self.controlVideoFlowView.stopProgress(success: true)
            
            if(resp.isSuccess()) {
                self.finishWithMessage(title: "Дверь успешно открыта!", message: "Входите в подъезд. У вас есть 3 секунды.")
            } else {
                self.finishWithMessage(title: "Не удалось открыть дверь", message: resp.message ?? "")
            }
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 3.5, execute: { [weak self] in
                guard let self = self else { return }
                self.controlVideoFlowView.stopProgress(success: false)
            })
            
            }, errorCall: { [weak self] (err) in
                guard let self = self else { return }
                self.controlVideoFlowView.stopProgress(success: false)
                debugPrint(err)
                self.finishWithMessage(title: "Не удалось открыть дверь", subtitle: nil, message: err.message ?? "")
        })
    }
}
extension DeviceVC: DatePickerViewDelegate {
    func dateSelected(date: Date) {
        self.controlVideoFlowView.setDate(date: date)
    }
}
