//
//  ArchiveVC.swift
//  ProDomofon
//
//  Created by Lexus on 09.11.2022.
//  Copyright © 2022 Алексей Агильдин. All rights reserved.
//

import UIKit

class ArchiveVC: SegmentedTabVC {

    @IBOutlet weak var tabControl: UISegmentedControl!
    @IBOutlet weak var contentView: UIView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setBar(title: "Архивы")
        
        setUpPageViewController(contentView: contentView, storyboard: Utils.shared.additionalStoryboard())
    }
    
    override func initPages() {
        tabsVCList = [UIViewController]()
        tabsVCList?.append(self.instantinatePage(atIndex: 0)!)
        tabsVCList?.append(self.instantinatePage(atIndex: 1)!)
    }
    
    override func initTabTitles() {
        tabControl.setTitle("Вызовы", forSegmentAt: 0)
        tabControl.setTitle("Камеры", forSegmentAt: 1)
    }
    
    override func instantinatePage(atIndex index: Int) -> UIViewController? {
        guard index >= 0 && index < 2 else { return nil}
        if index == 0 {
            let sipHistoryVC = Utils.shared.additionalStoryboard().instantiateViewController(withIdentifier: SipHistoryVC.className) as! SipHistoryVC
            sipHistoryVC.setTabIndex(index: index)
            return sipHistoryVC
        } else if index == 1 {
            var contracts: [Contract] = []
            var devicePanels: [DevicePanel] = []
            for contract in DataStorage.shared.contracts {
                if contract.deviceCameraPanels.count > 0 {
                    contract.deviceCameraPanels.forEach({ devicePanels.append($0) })
                    contracts.append(contract)
                }
            }
//            if devicePanels.count > 1 || devicePanels.count == 0 {
                let devicesVC = Utils.shared.additionalStoryboard().instantiateViewController(withIdentifier: DevicesVC.className) as! DevicesVC
                devicesVC.contracts = DataStorage.shared.contracts
                devicesVC.setTabIndex(index: index)
                return devicesVC
//            } else {
//                let deviceVC = Utils.shared.additionalStoryboard().instantiateViewController(withIdentifier: DeviceVC.className) as! DeviceVC
//                deviceVC.contract = contracts[0]
//                deviceVC.devicePanel = devicePanels[0]
//                deviceVC.setTabIndex(index: index)
//                return deviceVC
//            }
        } else {
            return nil
        }
    }
    
    @IBAction func segmentSelected(_ sender: UISegmentedControl) {
        super.tabSelected(sender)
    }

    override func changeSelectedSegmentIndex(index: Int) {
        tabControl.selectedSegmentIndex = index
    }
}
