//
//  PlayVideoSipVC.swift
//  ProDomofon
//
//  Created by Lexus on 23.10.2022.
//  Copyright © 2022 Алексей Агильдин. All rights reserved.
//

import UIKit
import VideoDecoder
import VideoToolbox
import WebKit

@available(iOS 13.0, *)
class PlayVideoSipVC: UIViewController {

    @IBOutlet weak var backAddressView: UIView! {
        didSet {
            backAddressView.alpha = 0.5
        }
    }
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var scrollView: UIScrollView! {
        didSet {
            scrollView.delegate = self
        }
    }
    @IBOutlet weak var imageView: UIImageView! {
        didSet {
            imageView.contentMode = .scaleToFill
        }
    }
    @IBOutlet weak var mainActivityIndicatorView: UIActivityIndicatorView!
    
    @IBOutlet weak var nameLabel: UILabel!
    
    var sipHistory: SipHistory!
    
    fileprivate var size: CGSize?
    
    fileprivate var decoder: VideoDecoder!
    
    fileprivate var opening = false
    
    override func viewDidLoad() {
        super.viewDidLoad()

        view.backgroundColor = .black
        
        addressLabel.text = sipHistory.address
        addressLabel.sizeToFit()
        
        nameLabel.text = " \(sipHistory.panel) "
        nameLabel.sizeToFit()
        nameLabel.setCornerRadius(radius: 5)
        
        decoder = H264Decoder(delegate: self)
        
        mainActivityIndicatorView.startAnimating()
        
//        RestClient.shared.getCamLastPhoto(idContract: contract.id, devicePanel: devicePanel) { [weak self] (data) in
//            guard let self = self else { return }
//            guard let image = UIImage(data: data) else { return }
//            guard self.imageView.image == nil else { return }
//            self.imageView.image = image
//        } errorCall: { [weak self] (err) in
//            //guard let self = self else { return }
//            print("Не удалось загрузить изображение.")
//        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        stopVideo()
        UIApplication.shared.isIdleTimerDisabled = false
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        showVideo()
        UIApplication.shared.isIdleTimerDisabled = true
    }
    
    func showVideo() {
        WSManager.shared.setConnection(device: sipHistory.sip, panel: sipHistory.panel, type: .archive)
        WSManager.shared.delegate = self
        WSManager.shared.connect(type: .archive, withTimestart: sipHistory.created / 1000)
    }
    
    func stopVideo() {
        WSManager.shared.stopStream()
    }
    
    func finishWithMessage(title: String, subtitle: String? = nil, message: String) {
        PushNotificationManager.shared.showNotification(title: title, subtitle: subtitle, body: message)
    }

}
@available(iOS 13.0, *)
extension PlayVideoSipVC: WSManagerDelegate {
    func receiveInit(width: Int, height: Int) {
        self.size = CGSize(width: width, height: height)
    }
    
    func receiveData(data: Data) {
        guard let size = size else { return }
        let videoPacket = VideoPacket(data, fps: 30, type: .h264, videoSize: size)
        decoder.decodeOnePacket(videoPacket)
    }
    
    func error() {
        
    }
}
@available(iOS 13.0, *)
extension PlayVideoSipVC: VideoDecoderDelegate {
    func decodeOutput(video: CMSampleBuffer) {
        if let image = video.image {
            DispatchQueue.main.async {
                self.imageView.image = image
                self.mainActivityIndicatorView.hideObject()
                self.imageView.showObject()
            }
        }
    }
    
    func decodeOutput(error: DecodeError) {
        print("decodeOutput error: \(error)")
    }
}
@available(iOS 13.0, *)
extension PlayVideoSipVC: UIScrollViewDelegate {
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return imageView
    }
}
