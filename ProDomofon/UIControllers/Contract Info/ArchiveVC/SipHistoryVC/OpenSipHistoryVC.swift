//
//  OpenSipHistoryVC.swift
//  ProDomofon
//
//  Created by Алексей Агильдин on 17.06.2021.
//  Copyright © 2021 Алексей Агильдин. All rights reserved.
//

import UIKit
import ImageSlideshow
import AVKit

class OpenSipHistoryVC: UIViewController {

    fileprivate var imageSlideshow: ImageSlideshow!
    fileprivate var addressLabel: UILabel!
    fileprivate var closeButton: UIButton!
    fileprivate var infoLabel: UILabel!
    fileprivate var dateLabel: UILabel!
    fileprivate var timeLabel: UILabel!
    fileprivate var loadButton: UIButton!
    fileprivate var firstButton: UIButton!
    fileprivate var secondButton: UIButton!
    
    var dataArray: [SipHistory] = []
    var index: Int? = nil
    
    func showMedia(by url: URL) {
        let player = AVPlayer(url: url)
        
        let playerViewController = AVPlayerViewController()
        playerViewController.player = player

        present(playerViewController, animated: true) {
            player.play()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        view.backgroundColor = .black
        
        imageSlideshow = ImageSlideshow(frame: CGRect(x: 0, y: .topOffset + 50, width: view.frame.width, height: view.frame.height - .topOffset - .bottomOffset - 50 - 50))
        imageSlideshow.circular = false
        imageSlideshow.delegate = self
        imageSlideshow.pageIndicator = nil
        view.addSubview(imageSlideshow)
        
        let topView = UIView(frame: CGRect(x: 0, y: .topOffset, width: view.frame.width, height: 50))
        topView.backgroundColor = .black
        view.addSubview(topView)
        
        let item = dataArray[imageSlideshow.currentPage]
        
        addressLabel = UILabel(frame: CGRect(x: 12, y: 0, width: topView.frame.width - 24, height: topView.frame.height))
        addressLabel.lineBreakMode = .byWordWrapping
        addressLabel.numberOfLines = 0
        addressLabel.font = UIFont.systemFont(ofSize: 16, weight: .medium)
        addressLabel.textColor = .white
        addressLabel.textAlignment = .left
        addressLabel.text = item.address
        topView.addSubview(addressLabel)
        
        closeButton = UIButton(frame: CGRect(x: topView.frame.width - 50, y: 0, width: 50, height: 50))
        closeButton.setImage(UIImage(named: "ic_cross")?.withRenderingMode(.alwaysTemplate), for: .normal)
        closeButton.tintColor = .white
        closeButton.addTarget(self, action: #selector(closeTouch), for: .touchUpInside)
        topView.addSubview(closeButton)
        
        let bottomView = UIView(frame: CGRect(x: 0, y: view.frame.height - .bottomOffset - 50, width: view.frame.width, height: 50))
        bottomView.backgroundColor = .black
        view.addSubview(bottomView)
        
        dateLabel = UILabel(frame: CGRect(x: bottomView.frame.width - 200 - 12, y: 0, width: 200, height: 30))
        dateLabel.font = UIFont.systemFont(ofSize: 16, weight: .medium)
        dateLabel.textColor = .white
        dateLabel.textAlignment = .right
        dateLabel.text = item.dateString
        bottomView.addSubview(dateLabel)
        
        timeLabel = UILabel(frame: CGRect(x: bottomView.frame.width - 200 - 12, y: dateLabel.frame.maxY, width: 200, height: 20))
        timeLabel.font = UIFont.systemFont(ofSize: 10, weight: .medium)
        timeLabel.textColor = .white
        timeLabel.textAlignment = .right
        timeLabel.text = item.timeString
        bottomView.addSubview(timeLabel)
        
        infoLabel = UILabel(frame: CGRect(x: 12, y: dateLabel.frame.maxY, width: 150, height: 20))
        infoLabel.font = UIFont.systemFont(ofSize: 10, weight: .medium)
        infoLabel.textColor = .white
        infoLabel.textAlignment = .left
        infoLabel.text = "\(imageSlideshow.currentPage + 1) / \(dataArray.count)"
        bottomView.addSubview(infoLabel)
        
        firstButton = UIButton(frame: CGRect(x: view.frame.width / 3 - 60 / 2, y: bottomView.frame.origin.y - 10 - 60, width: 60, height: 60))
        firstButton.setImage(UIImage(named: "videoPlay"), for: .normal)
        firstButton.addTarget(self, action: #selector(firstTouch), for: .touchUpInside)
        view.addSubview(firstButton)
//        if let video = item.video, let _ = video.url { } else {
            firstButton.hideObject(0)
//        }
        
        secondButton = UIButton(frame: CGRect(x: view.frame.width / 2 - 60 / 2, y: firstButton.frame.origin.y, width: 60, height: 60))
        secondButton.setImage(UIImage(named: "audioPlay"), for: .normal)
        secondButton.addTarget(self, action: #selector(secondTouch), for: .touchUpInside)
        view.addSubview(secondButton)
        if let audio = item.audio, let _ = audio.url { } else {
            secondButton.hideObject(0)
        }
        
        loadButton = UIButton(frame: CGRect(x: view.frame.width * 2 / 3 - 60 / 2, y: firstButton.frame.origin.y, width: 60, height: 60))
        loadButton.setImage(UIImage(named: "download"), for: .normal)
        loadButton.addTarget(self, action: #selector(loadTouch), for: .touchUpInside)
        view.addSubview(loadButton)
        
        
        setInputs()
        if let ind = index {
            imageSlideshow.setCurrentPage(ind, animated: true)
        }
    }
    
    func setInputs() {
        let savedIndex = imageSlideshow.currentPage
        let inputs = dataArray.map { (sipHistory) -> ImageSource in
            if let photo = sipHistory.photo, let _ = photo.url {
                if let image = MediaManager.shared.getSavedImageFromCache(named: photo.originalName) {
                    return ImageSource(image: image)
                } else if let image = MediaManager.shared.getSavedImageFromCache(named: photo.thumbName) {
                    return ImageSource(image: image)
                } else {
                    return ImageSource(image: UIImage.getImageWithColor(color: .darkGray, size: CGSize(width: 10, height: 10)))
                }
            } else {
                return ImageSource(image: UIImage.getImageWithColor(color: .black, size: CGSize(width: 10, height: 10)))
            }
        }
        imageSlideshow.setImageInputs(inputs)
        imageSlideshow.setCurrentPage(savedIndex, animated: false)
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.setNavigationBarHidden(false, animated: true)
    }

    @objc
    func closeTouch() {
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc
    func loadTouch() {
        guard imageSlideshow.currentPage < dataArray.count else { return }
        guard let photo = dataArray[imageSlideshow.currentPage].photo, let url = photo.url else { return }
        
        if let im = MediaManager.shared.getSavedImageFromCache(named: photo.originalName) {
            MediaManager.shared.saveImageToGalery(image: im) { [weak self] (success, errorText) in
                guard let self = self else { return }
                self.callbackSaveMedia(success: success, mediaText: "Изображение", errorText: errorText)
            }
        } else {
            RestClient.shared.getMediaSip(url: url) { [weak self] (data) in
                guard let image = UIImage(data: data) else { return }
                MediaManager.shared.saveImageToGalery(image: image) { [weak self] (success, errorText) in
                    guard let self = self else { return }
                    self.callbackSaveMedia(success: success, mediaText: "Изображение", errorText: errorText)
                }
            } errorCall: { (err) in
                print("Не удалось загрузить изображение.")
            }
        }
    }
    
    func callbackSaveMedia(success: Bool, mediaText: String, errorText: String) {
        DispatchQueue.main.async {
            if success {
                Utils.shared.showToast(from: self, with: "\(mediaText) успешно сохранено")
            } else {
                Utils.shared.showToast(from: self, with: "Не удалось сохранить \(mediaText.lowercased()). \(errorText)")
            }
        }
    }
    
    
    @objc
    func firstTouch() {
        let alertController = UIAlertController(title: "ПРО Домофон", message: "Дополнительные действия", preferredStyle: .actionSheet)
        alertController.addAction(UIAlertAction(title: "Скачать", style: .default, handler: { [weak self] (_) in
            guard let self = self else { return }
            guard self.imageSlideshow.currentPage < self.dataArray.count else { return }
            guard let video = self.dataArray[self.imageSlideshow.currentPage].video, let url = video.url else { return }
            
            if let cacheUrl = MediaManager.shared.getSavedURLFromCache(named: video.originalName) {
                MediaManager.shared.saveVideoByURL(url: cacheUrl) { [weak self] (success, errorText) in
                    guard let self = self else { return }
                    self.callbackSaveMedia(success: success, mediaText: "Видео", errorText: errorText)
                }
            } else {
                RestClient.shared.getMediaSip(url: url) { (data) in
                    guard MediaManager.shared.saveMediaToCache(data: data, withName: video.originalName) else { return }
                    guard let cacheUrl = MediaManager.shared.getSavedURLFromCache(named: video.originalName) else { return }
                    MediaManager.shared.saveVideoByURL(url: cacheUrl, callback: { [weak self] (success, errorText) in
                        guard let self = self else { return }
                        self.callbackSaveMedia(success: success, mediaText: "Видео", errorText: errorText)
                    })
                } errorCall: { (err) in
                    Utils.shared.showToast(from: self, with: "Не удалось загрузить видео")
                }
            }
        }))

        alertController.addAction(UIAlertAction(title: "Просмотр", style: .default, handler: { [weak self] (_) in
            guard let self = self else { return }
            guard self.imageSlideshow.currentPage < self.dataArray.count else { return }
            guard let video = self.dataArray[self.imageSlideshow.currentPage].video, let url = video.url else { return }
            
            if let cacheUrl = MediaManager.shared.getSavedURLFromCache(named: video.originalName) {
                self.showMedia(by: cacheUrl)
            } else {
                RestClient.shared.getMediaSip(url: url) { [weak self] (data) in
                    guard let self = self else { return }
                    guard MediaManager.shared.saveMediaToCache(data: data, withName: video.originalName) else { return }
                    guard let cacheUrl = MediaManager.shared.getSavedURLFromCache(named: video.originalName) else { return }
                    self.showMedia(by: cacheUrl)
                } errorCall: { (err) in
                    print("Не удалось загрузить видео.")
                }
            }
        }))

        alertController.addAction(UIAlertAction(title: "Отмена", style: .cancel, handler: nil))

        if let popoverController = alertController.popoverPresentationController, UIDevice.current.userInterfaceIdiom == .pad {
            popoverController.sourceView = self.view
            popoverController.sourceRect = CGRect(x: self.view.bounds.maxX, y: self.view.bounds.maxY - 200, width: 0, height: 0)
            popoverController.permittedArrowDirections = []
        }
        
        self.present(alertController, animated: true, completion: nil)
    }
    
    @objc
    func secondTouch() {
        
    }
    
    func loadingImage(named: String, url: URL, with completion: (() -> Void)? = nil) {
        RestClient.shared.getMediaSip(url: url, success: { [weak self] (data) in
            guard let self = self else { return }
            guard let image = UIImage(data: data) else { return }
            guard MediaManager.shared.saveImageToCache(image: image, withName: named) else {
                print("OpenSipHistoryVC loading image \(named) error")
                return
            }
            print("OpenSipHistoryVC loading image \(named) success")
            completion?()
        }, errorCall: { (err) in
            print("OpenSipHistoryVC Не удалось загрузить изображение.")
        })
    }
}
extension OpenSipHistoryVC: ImageSlideshowDelegate {
    func imageSlideshow(_ imageSlideshow: ImageSlideshow, didChangeCurrentPageTo page: Int) {
        print("page: \(page)")
        infoLabel.text = "\(imageSlideshow.currentPage + 1) / \(dataArray.count)"
        if page < dataArray.count {
            addressLabel.text = dataArray[page].address
            let dateHelper = DateHelper(locale: Locale.current)
            dateLabel.text = dateHelper.getOnlyDate(from: dataArray[page].created / 1000)
            timeLabel.text = dateHelper.getOnlyTime(from: dataArray[page].created / 1000)
            
            if let photo = dataArray[page].photo, let url = photo.url {
                if MediaManager.shared.getSavedImageFromCache(named: photo.originalName) == nil {
                    loadingImage(named: photo.originalName, url: url) {
                        self.setInputs()
                    }
                }
                loadButton.showObject()
            } else {
                loadButton.hideObject(0)
            }
            /*
            if let video = dataArray[page].video, let _ = video.url {
                firstButton.showObject()
            } else {
                firstButton.hideObject(0)
            }
             */
            if let audio = dataArray[page].video, let _ = audio.url {
//                    secondButton.showObject()
            } else {
//                    secondButton.hideObject(0)
            }
        }
    }
}
