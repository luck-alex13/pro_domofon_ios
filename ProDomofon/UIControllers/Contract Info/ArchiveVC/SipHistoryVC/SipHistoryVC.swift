//
//  SipHistoryVC.swift
//  ProDomofon
//
//  Created by Алексей Агильдин on 01.06.2021.
//  Copyright © 2021 Алексей Агильдин. All rights reserved.
//

import UIKit
import ImageSlideshow
import Photos

class SipHistoryVC: VCWithTable<SipHistory> {
    
    @IBOutlet weak var dataTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setBar(title: "Архивы")
        
        defineVariables(sections: 1, cellIdentifer: SipHistoryCell.className, placeholderTitle: "Здесь будет отбражаться архив вызовов.", placeholderSubtile: nil)

        self.tableView = dataTableView
        self.tableView?.rowHeight = 180
//        self.tableView?.estimatedRowHeight = 180
//        self.tableView?.rowHeight = UITableView.automaticDimension
        
        setUpRefreshControl()
    }
    
    override func loadDataFromServer(_ refreshControl: UIRefreshControl?) {
        refreshControl?.beginRefreshing()
        RestClient.shared.sipHistory { [weak self] (sipHistories) in
            guard let self = self else { return }
            refreshControl?.endRefreshing()
            self.dataArray = sipHistories
            self.updateTableView()
            //DataStorage.shared.sipHistories = sipHistories
            self.indexLoadingImage = 0
            self.lazyLoadOriginalImages()
        } errorCall: { [weak self] (err) in
            refreshControl?.endRefreshing()
            Utils.shared.showToastHttp(from: self?.navigationController, with: "Не удалось загрузить историю sip звонков.", error: err)
        }
    }
    
    override func readDataFromDatabase() {
//        dataArray = DataStorage.shared.sipHistories
        dataArray = []
        updateTableView()
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifer!, for: indexPath) as! SipHistoryCell
        cell.setupCell(with: dataArray[indexPath.row])
        cell.delegate = self
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
    }

    
    fileprivate var loadingOriginalImages = false
    fileprivate var indexLoadingImage = 0
    func lazyLoadOriginalImages(next: Bool = false) {
        if next {
            indexLoadingImage += 1
        }
        guard !loadingOriginalImages || next else { return }
        loadingOriginalImages = true
        guard indexLoadingImage < dataArray.count else {
            loadingOriginalImages = false
            return
        }
        
        guard let photo = dataArray[indexLoadingImage].photo, let url = photo.url else {
            lazyLoadOriginalImages(next: true)
            return
        }
        
        if MediaManager.shared.getSavedImageFromCache(named: photo.originalName) == nil {
            RestClient.shared.getMediaSip(url: url, success: { [weak self] (data) in
                guard let self = self else { return }
                guard let image = UIImage(data: data) else { return }
                guard MediaManager.shared.saveImageToCache(image: image, withName: photo.originalName) else {
                    print("loading image \(photo.originalName) error")
                    return
                }
                print("loading image \(photo.originalName) success")
                self.lazyLoadOriginalImages(next: true)
            }, errorCall: { [weak self] (err) in
                guard let self = self else { return }
                print("Не удалось загрузить изображение.")
                self.lazyLoadOriginalImages(next: true)
            })
        } else {
            lazyLoadOriginalImages(next: true)
        }
    }
}
extension SipHistoryVC: SipHistoryCellDelegate {
    func imageTap(itemData: SipHistory) {
        guard let vc = Utils.shared.additionalStoryboard().instantiateViewController(withIdentifier: OpenSipHistoryVC.className) as? OpenSipHistoryVC else { return }
        vc.dataArray = dataArray
        if let index = dataArray.firstIndex(where: { $0.callId == itemData.callId }) {
            vc.index = index
        }
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func playVideoTap(itemData: SipHistory) {
        if #available(iOS 13.0, *) {
            let date = Date(timeIntervalSince1970: itemData.created / 1000)
            let devicePanel = DevicePanel(sip: itemData.sip, deviceId: itemData.device, panel: Panel(number: 1, camera: true, bleUuid: "", field: itemData.panel, type: nil), name: "", bleUuid: nil, contractNumber: nil)
            
            if let contract = DataStorage.shared.contracts.first(where: { $0.number == itemData.contract }), let nvc = Utils.shared.additionalStoryboard().instantiateViewController(withIdentifier: DeviceVC.className) as? DeviceVC {
                nvc.contract = contract
                nvc.devicePanel = devicePanel
                nvc.showingDate = date
                self.navigationController?.pushViewController(nvc, animated: true)
            }
        } else {
            
        }
    }
}
