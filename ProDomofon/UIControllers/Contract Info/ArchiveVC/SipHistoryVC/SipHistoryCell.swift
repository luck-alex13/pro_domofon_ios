//
//  SipHistoryCell.swift
//  ProDomofon
//
//  Created by Алексей Агильдин on 01.06.2021.
//  Copyright © 2021 Алексей Агильдин. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireImage

protocol SipHistoryCellDelegate: class {
    func imageTap(itemData: SipHistory)
    func playVideoTap(itemData: SipHistory)
}

class SipHistoryCell: UITableViewCell {

    @IBOutlet weak var cardView: CardView!
    fileprivate var myImageView: UIImageView?
    fileprivate var activityIndicatorView: UIActivityIndicatorView?
    @IBOutlet weak var previewImageView: UIImageView! {
        didSet {
            previewImageView.hideObject(0)
        }
    }
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    
    @IBOutlet weak var photoButton: UIButton! {
        didSet {
//            photoButton.setImage(UIImage(named: "photo")?.withRenderingMode(.alwaysTemplate), for: .normal)
//            photoButton.tintColor = .clear
            photoButton.setImage(UIImage(named: "photo"), for: .normal)
            photoButton.addTarget(self, action: #selector(photoTouch), for: .touchUpInside)
            photoButton.hideObject(0)
        }
    }
    @IBOutlet weak var videoButton: UIButton! {
        didSet {
            videoButton.setImage(UIImage(named: "video"), for: .normal)
//            videoButton.tintColor = .clear
//            videoButton.setImage(UIImage(named: "video")?.withRenderingMode(.alwaysTemplate), for: .normal)
            videoButton.addTarget(self, action: #selector(videoTouch), for: .touchUpInside)
        }
    }
    @IBOutlet weak var audioButton: UIButton! {
        didSet {
//            audioButton.setImage(UIImage(named: "phone")?.withRenderingMode(.alwaysTemplate), for: .normal)
            audioButton.tintColor = .clear
            audioButton.setImage(UIImage(named: "phone")?.withRenderingMode(.alwaysTemplate), for: .normal)
            audioButton.addTarget(self, action: #selector(audioTouch), for: .touchUpInside)
        }
    }
    
    var itemData: SipHistory?
    var delegate: SipHistoryCellDelegate?
    
    func setupCell(with itemData: SipHistory) {
        self.itemData = itemData
        
        if myImageView == nil {
            myImageView = UIImageView(frame: CGRect(x: 12, y: 12, width: 70, height: 140))
            if let mImageView = myImageView {
                mImageView.contentMode = .scaleToFill
                mImageView.setCornerRadius(radius: 4)
                cardView.addSubview(mImageView)
                
                let tapRecognizer = UITapGestureRecognizer(target: self, action: #selector(imageTouchDown))
                
                mImageView.isUserInteractionEnabled = true
                mImageView.addGestureRecognizer(tapRecognizer)
                
                
                activityIndicatorView = UIActivityIndicatorView(frame: CGRect(x: mImageView.frame.size.width / 2 - 20 / 2, y: mImageView.frame.size.height / 2 - 20 / 2, width: 20, height: 20))
                if let aIndicatorView = activityIndicatorView {
                    aIndicatorView.style = .white
                    aIndicatorView.hidesWhenStopped = true
                    aIndicatorView.hideObject(0)
                    mImageView.addSubview(aIndicatorView)
                }
            }
        }
        
        guard let mImageView = myImageView, let aIndicatorView = activityIndicatorView else { return }
        
        addressLabel.text = itemData.address
        
        mImageView.image = UIImage.getImageWithColor(color: .black, size: mImageView.frame.size)
        
        if let photo = itemData.photo, let thumb = photo.thumb {
            
            mImageView.image = UIImage.getImageWithColor(color: .darkGray, size: mImageView.frame.size)
            
            if let im = MediaManager.shared.getSavedImageFromCache(named: photo.thumbName) {
                myImageView?.image = im
                photoButton.setImage(UIImage(named: "photo"), for: .normal)
            } else {
                aIndicatorView.startAnimating()
                aIndicatorView.showObject()
                RestClient.shared.getMediaSip(url: thumb) { [weak self] (data) in
                    guard let self = self else { return }
                    guard let image = UIImage(data: data) else { return }
                    guard itemData.photo?.thumb == thumb else { return }
                    mImageView.image = image
                    self.photoButton.setImage(UIImage(named: "photo"), for: .normal)
                    aIndicatorView.stopAnimating()
                    MediaManager.shared.saveImageToCache(image: image, withName: photo.thumbName)
                } errorCall: { [weak self] (err) in
                    guard let self = self else { return }
                    self.photoButton.setImage(UIImage(named: "photo")?.withRenderingMode(.alwaysTemplate), for: .normal)
                    self.photoButton.tintColor = .clear
                    print("Не удалось загрузить изображение.")
                }
            }
            photoButton.tintColor = .black
        } else {
            photoButton.setImage(UIImage(named: "photo")?.withRenderingMode(.alwaysTemplate), for: .normal)
            photoButton.tintColor = .clear
        }
        
//        if let video = itemData.video, let _ = video.url {
            videoButton.setImage(UIImage(named: "video"), for: .normal)
            videoButton.tintColor = .black
//        } else {
//            videoButton.setImage(UIImage(named: "video")?.withRenderingMode(.alwaysTemplate), for: .normal)
//            videoButton.tintColor = .clear
//        }
        
        let dateHelper = DateHelper(locale: Locale.current)
        dateLabel.text = dateHelper.getOnlyDate(from: itemData.created / 1000)
        timeLabel.text = dateHelper.getOnlyTime(from: itemData.created / 1000)
    }

    @objc
    func photoTouch() {
        guard let data = itemData, let photo = data.photo, let url = photo.url else { return }
        
        if let im = MediaManager.shared.getSavedImageFromCache(named: photo.originalName) {
            MediaManager.shared.saveImageToGalery(image: im) { [weak self] (success, errorText) in
                guard let self = self else { return }
                self.callbackSaveMedia(success: success, mediaText: "Изображение", errorText: errorText)
            }
        } else {
            RestClient.shared.getMediaSip(url: url) { [weak self] (data) in
                guard let image = UIImage(data: data) else { return }
                MediaManager.shared.saveImageToGalery(image: image) { [weak self] (success, errorText) in
                    guard let self = self else { return }
                    
                    self.callbackSaveMedia(success: success, mediaText: "Изображение", errorText: errorText)
                }
            } errorCall: { (err) in
                print("Не удалось загрузить изображение.")
            }
        }
    }
    
    func callbackSaveMedia(success: Bool, mediaText: String, errorText: String) {
        DispatchQueue.main.async {
            if success {
                Utils.shared.showToast(from: self.cardView, with: "\(mediaText) успешно сохранено")
            } else {
                Utils.shared.showToast(from: self.cardView, with: "Не удалось сохранить \(mediaText.lowercased()). \(errorText)")
            }
            
        }
    }
    
    @objc
    func videoTouch() {
        guard let itemD = itemData else { return }
        delegate?.playVideoTap(itemData: itemD)
        
        
//        guard let data = itemData, let video = data.video, let url = video.url else { return }
//
//        if let cacheUrl = MediaManager.shared.getSavedURLFromCache(named: video.originalName) {
//            MediaManager.shared.saveVideoByURL(url: cacheUrl) { [weak self] (success, errorText) in
//                guard let self = self else { return }
//                self.callbackSaveMedia(success: success, mediaText: "Видео", errorText: errorText)
//            }
//        } else {
//            RestClient.shared.getMediaSip(url: url) { (data) in
//                guard MediaManager.shared.saveMediaToCache(data: data, withName: video.originalName) else { return }
//                guard let cacheUrl = MediaManager.shared.getSavedURLFromCache(named: video.originalName) else { return }
//                MediaManager.shared.saveVideoByURL(url: cacheUrl, callback: { [weak self] (success, errorText) in
//                    guard let self = self else { return }
//                    self.callbackSaveMedia(success: success, mediaText: "Видео", errorText: errorText)
//                })
//            } errorCall: { (err) in
//                print("Не удалось загрузить видео.")
//            }
//        }
    }
    
    @objc
    func audioTouch() {
        
    }
    
    @objc
    func imageTouchDown() {
        guard let data = itemData else { return }
        delegate?.imageTap(itemData: data)
    }
}
