//
//  InfoParams.swift
//  ProDomofon
//
//  Created by Александр Новиков on 25.10.2020.
//  Copyright © 2020 Александр Новиков. All rights reserved.
//

import Foundation

class InfoParams {
    
    var contractID: String?
    var contractNumber: Int?
    var flatNumber: String?
    var linkToPay: String?
    var address: String?
    
    init(contractID: String?, contractNumber: Int?, flatNumber: String?, linkToPay: String?, address: String?) {
        self.contractID = contractID
        self.contractNumber = contractNumber
        self.flatNumber = flatNumber
        self.linkToPay = linkToPay
        self.address = address
    }
    
    convenience init(contractID: String?, contractNumber: Int?) {
        self.init(contractID: contractID, contractNumber: contractNumber, flatNumber: nil, linkToPay: nil, address: nil)
    }

}
