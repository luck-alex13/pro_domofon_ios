//
//  RequestInfoVC.swift
//  ProDomofon
//
//  Created by Александр Новиков on 26/08/2019.
//  Copyright © 2019 Александр Новиков. All rights reserved.
//

import UIKit

protocol RequestInfoVCDelegate {
    func updateRequests()
}

class RequestInfoVC: UIViewController, UITextViewDelegate {
    
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var requestIDLabel: UILabel!
    //@IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var commentTV: UITextView!
    
    @IBOutlet weak var closeBtn: UIButton!
    @IBOutlet weak var remakeBtn: UIButton!
    @IBOutlet weak var helpLabel: UILabel!
    @IBOutlet weak var heightConstraint: NSLayoutConstraint!
    
    var requestData: ServiceRequest?
    var kbHelper: KeyboardHelper = KeyboardHelper()
    var connectionObserver = ConnectionObserver()
    let placeHolderText = "Опишите вашу пробелему"
    
    var delegate: RequestInfoVCDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        contentView.setCornerRadius(radius: 10)
        heightConstraint.constant = 350
        if let request = requestData {
            requestIDLabel.text = "Заявка \(request.callId)"
            descriptionLabel.text = request.desc
            statusLabel.text = request.statusName
            statusLabel.textColor = getStatusColor(status: request.status)
            
            if request.created != "" {
                let dateHelper = DateHelper(locale: Locale.current)
                if let date = dateHelper.parseDate(from: request.created) {
                    dateLabel.text = dateHelper.getOnlyDate(from: date.timeIntervalSince1970)
                } else {
                    dateLabel.text = ""
                }
            }
            
            if let stat = ServiceRequest.Status(rawValue: request.status), stat == .WORK_DONE {
                remakeBtn.isHidden = false
                commentTV.isHidden = false
                helpLabel.isHidden = false
                commentTV.setCornerRadius(radius: 6)
                commentTV.layer.borderWidth = 1
                commentTV.layer.borderColor = UIColor.init(rgb: 0xBBBBBB).cgColor
                commentTV.delegate = self
                commentTV.text = placeHolderText
                commentTV.textColor = UIColor.lightGray
                
                contentView.addGestureRecognizer(kbHelper.tapGestureRecognizerThatHideKeyboard)
                heightConstraint.constant = 450
            }
            
            
            
        }
        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        kbHelper.subscribeOnKeyboardNotifications(observer: self)
    }
    
    
    override func viewWillDisappear(_ animated: Bool) {
        kbHelper.hideKeyboard(from: self)
        kbHelper.removeObservers()
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == UIColor.lightGray {
            textView.text = nil
            textView.textColor = UIColor.black
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = placeHolderText
            textView.textColor = UIColor.lightGray
        }
    }
    
    func getStatusColor(status: Int) -> UIColor {
        if let stat = ServiceRequest.Status(rawValue: status){
            switch stat {
            case .BAD_WORK, .INCORRECT_REQUEST, .WRONG_PHONE, .NOT_PAID, .NOT_COMPLETED:
                return Colors.RED_ACCENT
                
            case .WORK_DONE, .ACT_DONE, .CLOSED_TO_REPAIR, .COMPLETED:
                return Colors.GREEN
                
            case .CANCELLED, .EDITING, .WAITING_FOR_DEVICE_SELECTION, .CALL_BACK:
                return Colors.YELLOW
            }
        }else {
            return UIColor.darkGray
        }
    }
    

    @IBAction func closeTapped(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func remakeTapped(_ sender: UIButton) {
        if let comment = validateComment() {
            if let request = requestData, request.id != "" {
                remakeRequest(requestId: request.id, comment: comment)
            }
        } else {
            Utils.shared.showToast(from: self, with: "Необходимо заполнить комментарий, чтобы отправить заявку на доработку!")
        }
    }
    
    func validateComment() -> String? {
        if let comment = commentTV.text, !comment.elementsEqual(placeHolderText) {
            return comment
        }
        return nil
    }
    
    func remakeRequest(requestId: String, comment: String) {
        guard connectionObserver.isConnectedToInternet else {
            AlertManager.shared.showAlertNoInternet(from: self)
            return
        }
        AlertManager.shared.showProgressDialog(from: self)
        RestClient.shared.restoreRequest(requestId: requestId, comment: comment, success: { [weak self] (resp) in
            guard let self = self else { return }
            AlertManager.shared.hideProgress {
                DataStorage.shared.updateRequest(request: resp)
                self.dismiss(animated: true, completion: {
                    self.delegate?.updateRequests()
                })
            }
        }) { [weak self] (error) in
            AlertManager.shared.hideProgress()
            Utils.shared.showToastHttp(from: self?.view, with: "При отправке запроса произошла ошибка.", error: error)
        }
    }
    
}
