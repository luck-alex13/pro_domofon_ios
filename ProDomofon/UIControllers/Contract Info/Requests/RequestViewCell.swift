//
//  RequestViewCell.swift
//  ProDomofon
//
//  Created by Александр Новиков on 22/08/2019.
//  Copyright © 2019 Александр Новиков. All rights reserved.
//

import UIKit

protocol RequestTapperDelegate: class {
    func requestTapped(_ request: ServiceRequest?)
}

class RequestViewCell: UITableViewCell {
    
    
    @IBOutlet weak var requestIDLabel: UILabel!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UITextView!
    @IBOutlet weak var dateLabel: UILabel!
    
    weak var delegate: RequestTapperDelegate?
    var itemData: ServiceRequest?
    
    func setupCell(with request: ServiceRequest) {
        self.itemData = request
        
        requestIDLabel.text = "Заявка \(request.callId)"
        var address = ""
        if let addr = request.contractAddress {
            address += addr
        }
        if let flat = request.contractFlat {
            address += "кв. \(flat)"
        }
        addressLabel.text = address
        descriptionLabel.text = request.desc
        statusLabel.text = request.statusName
        statusLabel.textColor = getStatusColor(status: request.status)
        
        if request.created != "" {
            let dateHelper = DateHelper(locale: Locale.current)
            if let date = dateHelper.parseDate(from: request.created) {
                dateLabel.text = dateHelper.getOnlyDate(from: date.timeIntervalSince1970)
            } else {
                dateLabel.text = ""
            }
        }
        
        let recognizer = UITapGestureRecognizer()
        recognizer.addTarget(self, action: #selector(cellTapped))
        addGestureRecognizer(recognizer)
    }
    
    @objc func cellTapped() {
        delegate?.requestTapped(itemData)
    }
    
    func getStatusColor(status: Int) -> UIColor {
        if let stat = ServiceRequest.Status(rawValue: status){
            switch stat {
            case .BAD_WORK, .INCORRECT_REQUEST, .WRONG_PHONE, .NOT_PAID, .NOT_COMPLETED:
                return Colors.RED_ACCENT
                
            case .WORK_DONE, .ACT_DONE, .CLOSED_TO_REPAIR, .COMPLETED:
                return Colors.GREEN
                
            case .CANCELLED, .EDITING, .WAITING_FOR_DEVICE_SELECTION, .CALL_BACK:
                return Colors.YELLOW
            }
        } else {
            return UIColor.darkGray
        }
    }

}
