//
//  CallsVC.swift
//  ProDomofon
//
//  Created by Александр Новиков on 22/08/2019.
//  Copyright © 2019 Александр Новиков. All rights reserved.
//

import UIKit

class RequestsVC: TableVC<ServiceRequest> {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setBar(title: "Заявки", rightTitle: "Создать", rightSelector: #selector(newRequestTapped))
        
        defineVariables(sections: 1, cellIdentifer: RequestViewCell.className, placeholderTitle: "Здесь будут отбражаться ваши заявки.", placeholderSubtile: nil)
        setUpRefreshControl()

        tableView.estimatedRowHeight = 170
        tableView.rowHeight = UITableView.automaticDimension
    }
    
    override func readDataFromDatabase() {
        var data: [ServiceRequest] = []
        if let contract = DataStorage.shared.choosenContract {
            data = DataStorage.shared.requests.filter({ $0.contract == contract.id })
        } else {
            data = DataStorage.shared.requests
        }
        dataArray = data.sorted(by: { s1, s2 in
            if !s1.created.isEmpty && !s2.created.isEmpty {
                let dateHelper = DateHelper(locale: Locale.current)
                if let d1 = dateHelper.parseDate(from: s1.created), let d2 = dateHelper.parseDate(from: s2.created) {
                    return d1 > d2
                }
            }
            return false
        })
        
        tableView.reloadData()
    }
    
    override func loadDataFromServer(_ refreshControl: UIRefreshControl?) {
        guard let user = DataStorage.shared.user else { return }
        
        refreshControl?.beginRefreshing()
        RestClient.shared.getRequests(userId: user.id, success: {  [weak self] (resp) in
            guard let self = self else { return }
            self.refreshControl?.endRefreshing()
            DataStorage.shared.requests = resp
        }) { [weak self] (error) in
            refreshControl?.endRefreshing()
            Utils.shared.showToastHttp(from: self?.navigationController, with: "Не удалось загрузить заявки.", error: error)
        }
    }

    @objc
    func newRequestTapped() {
        if let nextViewController = Utils.shared.additionalStoryboard().instantiateViewController(withIdentifier: NewRequestVC.className) as? NewRequestVC {
            self.navigationController?.pushViewController(nextViewController, animated: true)
        }
    }

    // MARK: - Table view data source
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifer!, for: indexPath) as! RequestViewCell
        cell.setupCell(with: dataArray[indexPath.row])
        cell.delegate = self
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
}
extension RequestsVC: RequestTapperDelegate {
    func requestTapped(_ request: ServiceRequest?) {
        if let request = request, let myAlert =  Utils.shared.additionalStoryboard().instantiateViewController(withIdentifier: RequestInfoVC.className) as? RequestInfoVC {
            myAlert.delegate = self
            myAlert.requestData = request
            myAlert.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
            myAlert.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
            self.navigationController?.present(myAlert, animated: true, completion: nil)
        }
    }
}
extension RequestsVC: RequestInfoVCDelegate {
    func updateRequests() {
        refreshControl?.beginRefreshing()
        readDataFromDatabase()
        refreshControl?.endRefreshing()
    }
}
