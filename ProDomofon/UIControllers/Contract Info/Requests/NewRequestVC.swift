//
//  NewRequestVC.swift
//  ProDomofon
//
//  Created by Александр Новиков on 24/08/2019.
//  Copyright © 2019 Александр Новиков. All rights reserved.
//

import UIKit
import DropDown

class NewRequestVC: InputViewController, UITextViewDelegate {
    
    @IBOutlet weak var constrContentHeight: NSLayoutConstraint!
    @IBOutlet weak var contentScroll: UIScrollView!
    @IBOutlet weak var addButton: UIButton! {
        didSet {
            addButton.setCornerRadius(radius: 6)
        }
    }
    @IBOutlet weak var problemTV: UITextView!
    @IBOutlet weak var addressPicker: SelectButton!
    
    let optionDropDown = DropDown()
    
    var connectionObserver = ConnectionObserver()
    var contracts: [Contract] = DataStorage.shared.contracts
    
    var selectedContract: Contract? = DataStorage.shared.choosenContract
    
    let placeHolderText = "Опишите вашу пробелему"

    override func viewDidLoad() {
        super.viewDidLoad(scrollView: contentScroll, constraintContentHeight: constrContentHeight, recognizer: UITapGestureRecognizer(target: self, action: #selector(viewTapped)) )

        setTitle(title: "Новая заявка")
        setEmptyBackArrow()
        
        problemTV.setCornerRadius(radius: 6)
        problemTV.layer.borderWidth = 1
        problemTV.layer.borderColor = UIColor.init(rgb: 0xBBBBBB).cgColor
        problemTV.delegate = self
        problemTV.text = placeHolderText
        problemTV.textColor = UIColor.lightGray
        
        setActiveField(problemTV)
        lastOffset = self.contentScroll.contentOffset
        
        setupDropDown(with: contracts)
    }
    
    @objc func viewTapped() {
        problemTV.hideKeyboard()
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        //super.textViewDidBeginEditing(textView)
        lastOffset = self.contentScroll.contentOffset
        if textView.textColor == UIColor.lightGray {
            textView.text = nil
            textView.textColor = UIColor.black
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        //super.textViewDidEndEditing(textView)
        if textView.text.isEmpty {
            textView.text = placeHolderText
            textView.textColor = UIColor.lightGray
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        connectionObserver.endListening()
    }
    
    func setupDropDown(with contacts: [Contract]) {
        var optionsList = [String]()
        
        contacts.forEach { (contract) in
            optionsList.append(contract.getReadableAddress)
            if contract.id == selectedContract?.id {
                addressPicker.setTitle(contract.getReadableAddress, for: .normal)
            }
        }
        
        optionDropDown.width = addressPicker.bounds.width
        optionDropDown.bottomOffset = CGPoint(x: 0, y: addressPicker.bounds.height)
        optionDropDown.anchorView = addressPicker
        optionDropDown.direction = .bottom
        optionDropDown.dismissMode = .onTap
        optionDropDown.cellNib = UINib(nibName: "DropDownCell", bundle: Bundle(for: DropDownCell.self))
        optionDropDown.customCellConfiguration = nil
        optionDropDown.dataSource = optionsList
        optionDropDown.selectionAction = { [weak self] (index, item) in
            guard let self = self else { return }
            self.addressPicker.setTitle(item, for: .normal)
            self.selectedContract = self.contracts[index]
        }
    }
    
    @IBAction func optionTapped(_ sender: SelectButton) {
        optionDropDown.show()
    }
    
    @IBAction func createTapped(_ sender: UIButton) {
        super.hideKeyBoard()
        if let contact = selectedContract {
            if let comment = validateComment() {
                createRequest(with: contact, and: comment)
            } else {
                Utils.shared.showToast(from: self, with: "Необходимо описать вашу проблему, чтобы создать заявку!")
            }
        }else {
            Utils.shared.showToast(from: self, with: "Выберите адрес!")
        }
    }
    
    func validateComment() -> String? {
        if let comment = problemTV.text, !comment.elementsEqual(placeHolderText) {
            return comment
        }
        return nil
    }
    
    func createRequest(with contract: Contract, and comment: String) {
        guard connectionObserver.isConnectedToInternet else {
            AlertManager.shared.showAlertNoInternet(from: self)
            return
        }
        
        AlertManager.shared.showProgressDialog(from: self)
        RestClient.shared.createRequest(contractId: contract.id, comment: comment, success: { [weak self] (resp) in
            AlertManager.shared.hideProgress()
//            RealmDb.shared.saveToDb(object: ServiceRequestCache(id: resp.id, callID: resp.callId, type: resp.type, typeName: resp.typeName, status: resp.status, statusName: resp.statusName, workComplete: resp.workComplete, desc: resp.desc, created: resp.created, amount: resp.amount, contract: resp.contract, contractFlat: resp.contractFlat, contractAddress: resp.contractAddress, contractNumber: resp.contractNumber, phone: resp.phone), update: true)
            self?.navigationController?.popViewController(animated: true)
        }) { [weak self] (error) in
            AlertManager.shared.hideProgress()
            Utils.shared.showToastHttp(from: self?.view, with: "Не удалось создать заявку.", error: error)
        }
        self.navigationController?.popViewController(animated: true)
    }
    

}
