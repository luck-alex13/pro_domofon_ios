//
//  AliasesVM.swift
//  ProDomofon
//
//  Created by Александр Новиков on 07/11/2019.
//  Copyright © 2019 Александр Новиков. All rights reserved.
//

import Foundation
import RxSwift

class AliasesVM {
    
    
    let rangesList: PublishSubject<[Range]> = PublishSubject()
    let rangesError: PublishSubject<ErrorBody> = PublishSubject()
    
    let contractSubject: PublishSubject<Contract> = PublishSubject()
    
    func requestAvailableRanges(contractId: String) {
        guard ConnectionObserver().isConnectedToInternet else {
            rangesList.onError(NoInternetError())
            return
        }
        
        RestClient.shared.requestAvailableRanges(for: contractId, success: { [weak self] (resp) in
            self?.rangesList.onNext(resp.ranges!)
        }) { [weak self] (error) in
            self?.rangesList.onError(ServerError(error: error))
        }
    }
    
    func changeAlias(contractId: String, with newValue: Int) {
        guard ConnectionObserver().isConnectedToInternet else {
            rangesList.onError(NoInternetError())
            return
        }
        
        RestClient.shared.newAliasForContract(contractId: contractId, newAlias: newValue, success: { [weak self] (resp) in
            self?.contractSubject.onNext(resp)
        }) { [weak self] (error) in
            self?.rangesList.onError(ServerError(error: error))
        }
        
    }
    
}
