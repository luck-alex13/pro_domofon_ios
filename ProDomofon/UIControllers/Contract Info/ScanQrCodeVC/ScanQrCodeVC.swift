//
//  ScanQrCodeVC.swift
//  ProDomofon
//
//  Created by Lexus on 25.11.2022.
//  Copyright © 2022 Алексей Агильдин. All rights reserved.
//

import UIKit
import AVFoundation

protocol ScanQrCodeVCDelegate: AnyObject {
    func found(link: String)
}

class ScanQrCodeVC: UIViewController {

    fileprivate var closeButton: UIButton!
    fileprivate var titleLabel: UILabel!
    
    fileprivate var captureSession: AVCaptureSession!
    fileprivate var previewLayer: AVCaptureVideoPreviewLayer!
    
    weak var scanDelegate:  ScanQrCodeVCDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        view.backgroundColor = UIColor.black
        captureSession = AVCaptureSession()

        guard let videoCaptureDevice = AVCaptureDevice.default(for: .video) else { return }
        let videoInput: AVCaptureDeviceInput

        do {
            videoInput = try AVCaptureDeviceInput(device: videoCaptureDevice)
        } catch {
            return
        }

        if (captureSession.canAddInput(videoInput)) {
            captureSession.addInput(videoInput)
        } else {
            failed()
            return
        }

        let metadataOutput = AVCaptureMetadataOutput()

        if (captureSession.canAddOutput(metadataOutput)) {
            captureSession.addOutput(metadataOutput)

            metadataOutput.setMetadataObjectsDelegate(self, queue: DispatchQueue.main)
            metadataOutput.metadataObjectTypes = [.qr]
        } else {
            failed()
            return
        }

        previewLayer = AVCaptureVideoPreviewLayer(session: captureSession)
        previewLayer.frame = view.layer.bounds
        previewLayer.videoGravity = .resizeAspectFill
        view.layer.addSublayer(previewLayer)
        
        closeButton = UIButton(frame: CGRect(x: 16, y: .topOffset + 16, width: 50, height: 50))
        closeButton.setImage(UIImage(named: "exit"), for: .normal)
        closeButton.addTarget(self, action: #selector(closeTouch), for: .touchUpInside)
        view.addSubview(closeButton)
        
        titleLabel = UILabel(frame: CGRect(x: 16, y: view.frame.height - .bottomOffset - 16 - 30, width: view.frame.width - 32, height: 30))
        titleLabel.font = .systemFont(ofSize: 14)
        titleLabel.textAlignment = .center
        titleLabel.textColor = .white
        titleLabel.text = "Наведите камеру на QR код"
        view.addSubview(titleLabel)
        
        DispatchQueue.global(qos: .userInitiated).async {
            self.captureSession.startRunning()
            
//            DispatchQueue.main.async {
//                print("This is run on the main queue, after the previous code in outer block")
//            }
        }
        
    }
    
    func failed() {
        let ac = UIAlertController(title: "Scanning not supported", message: "Your device does not support scanning a code from an item. Please use a device with a camera.", preferredStyle: .alert)
        ac.addAction(UIAlertAction(title: "OK", style: .default))
        present(ac, animated: true)
        captureSession = nil
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        DispatchQueue.global(qos: .userInitiated).async {
            if (self.captureSession?.isRunning == false) {
                self.captureSession.startRunning()
            }
//            DispatchQueue.main.async {
//                print("This is run on the main queue, after the previous code in outer block")
//            }
        }
        
        
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)

        if (captureSession?.isRunning == true) {
            captureSession.stopRunning()
        }
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }

    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .portrait
    }
    
    @objc
    func closeTouch() {
        self.navigationController?.popViewController(animated: true)
    }


}
extension ScanQrCodeVC: AVCaptureMetadataOutputObjectsDelegate {
    
    func metadataOutput(_ output: AVCaptureMetadataOutput, didOutput metadataObjects: [AVMetadataObject], from connection: AVCaptureConnection) {
        captureSession.stopRunning()

        if let metadataObject = metadataObjects.first {
            guard let readableObject = metadataObject as? AVMetadataMachineReadableCodeObject else { return }
            guard let stringValue = readableObject.stringValue else { return }
            AudioServicesPlaySystemSound(SystemSoundID(kSystemSoundID_Vibrate))
            closeTouch()
            scanDelegate?.found(link: stringValue)
        }
    }
    
}
