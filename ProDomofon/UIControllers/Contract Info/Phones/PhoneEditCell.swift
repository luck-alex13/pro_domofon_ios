//
//  PhoneEditCell.swift
//  ProDomofon
//
//  Created by Алексей Агильдин on 27.04.2021.
//  Copyright © 2021 Алексей Агильдин. All rights reserved.
//

import UIKit

class PhoneEditCell: UITableViewCell {

    @IBOutlet weak var contactNumberLabel: UILabel!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var homeIntercomView: HomeIntercomView! {
        didSet {
            homeIntercomView.textAlign = .left
        }
    }
    @IBOutlet weak var scheduleView: UIView!
    @IBOutlet weak var alwaysSwitch: UISwitch! {
        didSet {
            alwaysSwitch.addTarget(self, action: #selector(alwaysChanged), for: .valueChanged)
        }
    }
    @IBOutlet weak var linesTableView: UITableView! {
        didSet {
            linesTableView.register(UINib(nibName: "ScheduleLineViewCell", bundle: nil), forCellReuseIdentifier: ScheduleLineViewCell.identifier)
            linesTableView.dataSource = self
            linesTableView.delegate = self
        }
    }
    @IBOutlet weak var heightLinesTableView: NSLayoutConstraint!
    @IBOutlet weak var addButton: UIButton! {
        didSet {
            addButton.setCornerRadius(radius: 6)
            let imageView = UIImageView(frame: CGRect(x: addButton.frame.width - 5 - 20, y: 5, width: 20, height: 20))
            imageView.image = UIImage(named: "ic_plus")?.withRenderingMode(.alwaysTemplate)
            imageView.tintColor = .white
            addButton.addSubview(imageView)
        }
    }
    
    weak var delegate: ScheduleViewCellDelegate?
    
    var phone: Phone!
    var itemData: ContractsPhones?
    
    func setupCell(with itemData: ContractsPhones, phone: Phone) {
        self.itemData = itemData
        self.phone = phone
        contactNumberLabel.text = "Договор #\(itemData.contractNumber)"
        if itemData.flat != "" {
            addressLabel.text = "\(itemData.address ?? "") кв. \(itemData.flat)"
        } else {
            addressLabel.text = itemData.address ?? ""
        }
        alwaysSwitch.setOn(phone.always, animated: false)
        addButton.isEnabled = !phone.always
        addButton.backgroundColor = phone.always ? .lightGray : Colors.RED_ACCENT 
        
        heightLinesTableView.constant = CGFloat(70 * phone.schedules.count)
        linesTableView.reloadData()
    }
    
    
    @IBAction func addTouch() {
        delegate?.addScheduleTouch()
    }
    
    @objc
    func alwaysChanged(){
        phone.always = alwaysSwitch.isOn
        addButton.isEnabled = !phone.always
        addButton.backgroundColor = phone.always ? .lightGray : Colors.RED_ACCENT 
    }
}
extension PhoneEditCell: UITableViewDataSource {
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: ScheduleLineViewCell.identifier, for: indexPath) as! ScheduleLineViewCell
        cell.setupCell(with: phone.schedules[indexPath.row])
        cell.delegate = self
        return cell
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return phone.schedules.count
        
    }
}
extension PhoneEditCell: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        if phone.schedules[indexPath.row].enabled {
            delegate?.didSelectSchedule(data: phone.schedules[indexPath.row])
        }
    }
}
extension PhoneEditCell: ScheduleLineViewCellDelegate {
    func scheduleChanged(id: String, state: Bool) {
        if let index = phone.schedules.firstIndex(where: { $0.id == id }) {
            phone.schedules[index].enableChanged(state: state)
            delegate?.phoneChanged(phone: phone)
        }
    }
}
