//
//  ContractPhonesInfoViewCell.swift
//  ProDomofon
//
//  Created by Lexus on 16.05.2023.
//  Copyright © 2023 Алексей Агильдин. All rights reserved.
//

import UIKit

class ContractPhonesInfoViewCell: UITableViewCell {

    @IBOutlet weak var infoLabel: UILabel!
    @IBOutlet weak var infoTextView: UITextView!
}
