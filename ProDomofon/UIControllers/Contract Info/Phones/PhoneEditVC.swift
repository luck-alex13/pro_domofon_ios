//
//  PhoneEditVC.swift
//  ProDomofon
//
//  Created by Алексей Агильдин on 27.04.2021.
//  Copyright © 2021 Алексей Агильдин. All rights reserved.
//

import UIKit

class PhoneEditVC: VCWithTable<ContractsPhones> {

    @IBOutlet weak var infoLabel: UILabel! {
        didSet {
            infoLabel.text = "Для сохранения изменений нажмите кнопку Сохранить"
        }
    }
    @IBOutlet weak var dataTableView: UITableView!
    fileprivate var scheduleEditView: ScheduleEditView!
    
    var phone: Phone!
    
    var params: InfoParams? {
        if let c = DataStorage.shared.choosenContract {
            return InfoParams(
                contractID: c.id,
                contractNumber: c.number,
                flatNumber: c.flat,
                linkToPay: c.getLinkToPay(baseUrl: ApiRouter.getUser.getBaseUrl()),
                address: c.getReadableAddress
            )
        } else {
            return nil
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setDefaultBar()
    
        defineVariables(sections: 1, cellIdentifer: PhoneEditCell.className, placeholderTitle: "Здесь будет отображаться расписание по телефону \(phone.number.getPhoneWithMask)", placeholderSubtile: nil)

//        ContactManager.shared.tryingGetContact(phone: phone.number, delegate: self)
        
        self.tableView = dataTableView
        self.tableView?.estimatedRowHeight = 250
        self.tableView?.rowHeight = UITableView.automaticDimension
        
        setUpRefreshControl()
        
        scheduleEditView = ScheduleEditView(frame: CGRect(x: 0, y: 0, width: view.frame.width, height: view.frame.height))
        scheduleEditView.delegate = self
        view.addSubview(scheduleEditView)
        scheduleEditView.hideObject(0)
    }
    
    func setDefaultBar(){
        setBarWithBack(title: phone.number.getPhoneWithMask, rightTitle: "Сохранить", rightSelector: #selector(barSaveTouch))
        setEmptyBackArrow()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        let textAttributes = [NSAttributedString.Key.foregroundColor : UIColor.lightGray]
        navigationController?.navigationBar.titleTextAttributes = textAttributes
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        let textAttributes = [NSAttributedString.Key.foregroundColor : UIColor.white]
        navigationController?.navigationBar.titleTextAttributes = textAttributes
    }
    
    @objc
    func barSaveTouch() {
        AlertManager.shared.showProgressDialog(from: self)
        RestClient.shared.editSipConfiguration(phone: phone) { [weak self] (resp) in
            guard let self = self else { return }
            self.phone = resp
            AlertManager.shared.hideProgress {
                self.updateTableView()
            }
        } errorCall: { [weak self] (error) in
            guard let self = self else { return }
            AlertManager.shared.hideProgress {
                Utils.shared.showToastHttp(from: self.view, with: "Не удалось сохранить расписание.", error: error)
                self.loadDataFromServer(self.refreshControl)
            }
        }
    }
    
    override func loadDataFromServer(_ refreshControl: UIRefreshControl?) {
        readDataFromDatabase()
    }
    
    override func readDataFromDatabase() {
        if let cID = params?.contractID {
            dataArray = DataStorage.shared.contractPhones.filter({ $0.contractId == cID })
        } else {
            dataArray = DataStorage.shared.contractPhones
        }
        self.updateTableView()
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifer!, for: indexPath) as! PhoneEditCell
        cell.setupCell(with: dataArray[indexPath.row], phone: phone)
        cell.delegate = self
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
    }
    
    func newSchedule(schedule: Schedule) {
        phone.schedules.append(schedule)
        updateTableView()
    }
    
    func editSchedule(schedule: Schedule) {
        if let index = phone.schedules.firstIndex(where: { $0.id == schedule.id }) {
            phone.schedules[index] = schedule
            updateTableView()
        }
    }

}
extension PhoneEditVC: ContactManagerDelegate {
    func getName(text: String) {
        placeholderTitle = "Здесь будет отображаться расписание по телефону \(text)"
        title = text
    }
}
extension PhoneEditVC: ScheduleViewCellDelegate {
    func addScheduleTouch(){
        scheduleEditView.set(data: Schedule())
        scheduleEditView.showObject()
    }
    
    func headsetChanged(contractId: String, headset: HeadSet) {
        
    }
    
    func phoneChanged(phone: Phone) {
        
    }
    
    func didSelectSchedule(data: Schedule) {
        scheduleEditView.set(data: data)
        scheduleEditView.showObject()
    }
}
extension PhoneEditVC: ScheduleEditViewDelegate {
    func saveTouch(data: Schedule){
        if data.dow.count > 0 {
            if data.id == nil {
                newSchedule(schedule: data)
            } else {
                editSchedule(schedule: data)
            }
            scheduleEditView.hideObject()
        } else {
            AlertManager.shared.showAlert(from: self, title: "Ошибка", message: "Выберите дни недели прежде, чем сохранить расписание", handler: nil)
        }
    }
}
