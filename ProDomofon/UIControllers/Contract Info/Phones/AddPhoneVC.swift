//
//  AddPhoneVC.swift
//  ProDomofon
//
//  Created by Александр Новиков on 27/07/2019.
//  Copyright © 2019 Александр Новиков. All rights reserved.
//

import UIKit
import InputMask
import ContactsUI

class AddPhoneVC: InputViewController, UICollectionViewDataSource, MaskedTextFieldDelegateListener {
    
    @IBOutlet weak var constrContentHeight: NSLayoutConstraint!
    @IBOutlet weak var contentScroll: UIScrollView!
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var contactsList: UICollectionView!
    @IBOutlet weak var phoneTextField: UITextField! {
        didSet {
            let rightView = UIView(frame: CGRect(x: 0, y: 0, width: phoneTextField.frame.height, height: phoneTextField.frame.height))
            phoneTextField.rightView = rightView
        }
    }
    @IBOutlet weak var addButton: UIButton! {
        didSet {
            addButton.setCornerRadius(radius: 6)
        }
    }
    @IBOutlet weak var contactsButton: UIButton! {
        didSet {
            contactsButton.setImage(UIImage(named: "contacts")?.withRenderingMode(.alwaysTemplate), for: .normal)
            contactsButton.imageEdgeInsets = UIEdgeInsets(top: 4, left: 4, bottom: 4, right: 4)
            contactsButton.tintColor = Colors.GREEN_PRIMARY
            contactsButton.addTarget(self, action: #selector(chooseFromContacts), for: .touchUpInside)
        }
    }
    
    var selectedContracts = [InfoParams]()
    
    var connectionObserver = ConnectionObserver()
    var maskedDelegate: MaskedTextFieldDelegate!
    var rawNumber: String?
    
    fileprivate var peoplePicker: CNContactPickerViewController!
    
    override func viewDidLoad() {
        super.viewDidLoad(scrollView: contentScroll, constraintContentHeight: constrContentHeight)
        setTitle(title: "Добавление телефона")
        
        self.contactsList.register(UINib(nibName: ContractIDCollectionViewCell.className, bundle: nil), forCellWithReuseIdentifier: ContractIDCollectionViewCell.className)
        setupMask()
    }
    
    @objc
    func chooseFromContacts() {
        peoplePicker = CNContactPickerViewController()
        peoplePicker.delegate = self
        peoplePicker.displayedPropertyKeys = [CNContactPhoneNumbersKey]
        present(peoplePicker, animated: true, completion: nil)
    }
    
    func setupMask() {
        let mask = "{+7}([000])[000]-[00]-[00]"
        maskedDelegate = MaskedTextFieldDelegate(primaryFormat: mask)
        maskedDelegate.listener = self
        phoneTextField.delegate = maskedDelegate
    }
    
    func setPhone(phone: String){
        let str = phone.onlyNumbers
        let index = str.index(str.startIndex, offsetBy: 1)
        let newPhone = String(str[index...])
        phoneTextField.text = newPhone.getPhoneWithMask
        rawNumber = phoneTextField.text
    }
    
    open func textField(_ textField: UITextField, didFillMandatoryCharacters complete: Bool, didExtractValue value: String) {
        if complete {
            
            print("value \(value) length = \(value.count)")
        }
        rawNumber = value
        //print("complete \(complete) value \(value)  ")
    }
    
    func validateAndSend(phone: String?) {
        if let phone = phone {
            if(isPhoneValid(phone: phone)){
                phoneTextField.hideKeyboard()
                sendAddRequest(for: phone)
            }else {
                Utils.shared.showToast(from: self, with: "Номер телефона слишком короткий!")
            }
        }else {
            phoneTextField.becomeFirstResponder()
            Utils.shared.showToast(from: self, with: "Введите номер телефона!")
        }
    }
    
    
    func isPhoneValid(phone: String) -> Bool {
        return phone.onlyNumbers.count == 11
    }
    
    @IBAction func addTapped(_ sender: UIButton) {
        self.phoneTextField.hideKeyboard()
        self.validateAndSend(phone: rawNumber)
    }
    
    func sendAddRequest(for phone: String) {
        guard connectionObserver.isConnectedToInternet else {
            AlertManager.shared.showAlertNoInternet(from: self)
            return
        }
        
        var selectedIDs = [String]()
        
        selectedContracts.forEach { item in
            selectedIDs.append(item.contractID!)
        }
        
        AlertManager.shared.showProgressDialog(from: self)
        let index = phone.index(phone.startIndex, offsetBy: 2) // String.Index
        let prefix = phone[index...]
        RestClient.shared.addNewPhone(phoneNumber: String(prefix), contacts: selectedIDs, success: { [weak self] (response) in
            guard let self = self else { return }
            self.savePhones(response: response)
            AlertManager.shared.hideProgress(completion: {
                Utils.shared.showToast(from: self.view, with: "Номер успешно добавлен.")
                self.navigationController?.popViewController(animated: true)
            })
            
        }) { [weak self] (error)  in
            AlertManager.shared.hideProgress()
            Utils.shared.showToastHttp(from: self?.view, with: "При добавлении телефона произошла ошибка. ", error: error)
        }
    }
    
    func savePhones(response: PhonesResponse) {
        if let contracts = response.contracts {
//             RealmDb.shared.saveToDb(contracts, update: true)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return selectedContracts.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: ContractIDCollectionViewCell.className, for: indexPath) as! ContractIDCollectionViewCell
        
        cell.setupView(with: selectedContracts[indexPath.row])
        cell.setTapFinish {
            self.remove(contractId: self.selectedContracts[indexPath.row].contractID)
        }
        return cell
    }
    
    func remove(contractId: String?) {
        
        self.selectedContracts.removeAll(where: { params -> Bool in
           return params.contractID == contractId
        })
        
        contactsList.reloadData()
        if(self.selectedContracts.count == 0) {
            self.navigationController?.popViewController(animated: true)
        }
       
    }

}
extension AddPhoneVC: CNContactPickerDelegate {
    
    func contactPicker(_ picker: CNContactPickerViewController, didSelect contact: CNContact) {
        picker.dismiss(animated: true, completion: nil)
        if contact.phoneNumbers.count > 1 {
            let multiplePhoneNumbersAlert = UIAlertController(title: "Внимание", message: "Контакт имеет несколько номеров, выберите один их них", preferredStyle: .actionSheet)
            for number in contact.phoneNumbers {
                let numberAction = UIAlertAction(title: number.value.stringValue, style: .default, handler: { (theAction) -> Void in
                    self.setPhone(phone: number.value.stringValue)
                })
                multiplePhoneNumbersAlert.addAction(numberAction)
            }
            multiplePhoneNumbersAlert.addAction(UIAlertAction(title: "Отмена", style: .cancel, handler: nil))
            present(multiplePhoneNumbersAlert, animated: true, completion: nil)
        } else {
            if contact.phoneNumbers.count > 0 {
                self.setPhone(phone: contact.phoneNumbers[0].value.stringValue)
            } else {
                AlertManager.shared.showAlert(from: self, title: "Внимание", message: "Выбранный контакт не содержит номера телефона")
            }
        }
    }
}
