//
//  ContractsPhonesViewCell.swift
//  ProDomofon
//
//  Created by Александр Новиков on 01/07/2019.
//  Copyright © 2019 Александр Новиков. All rights reserved.
//

import UIKit

protocol ContractsPhonesViewCellDelegate: PhonesViewCellDelegate {
    func onLongPressed()
}

class ContractsPhonesViewCell: UITableViewCell, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet weak var contactNumberLabel: UILabel!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var phonesTableView: UITableView! {
        didSet {
            phonesTableView.dataSource = self
            phonesTableView.delegate = self
        }
    }
    
    @IBOutlet weak var tvHeigthConstraint: NSLayoutConstraint!
    var itemData: ContractsPhones?
    
    weak var delegate: ContractsPhonesViewCellDelegate?
    
    fileprivate var isActivated = false
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        NotificationCenter.default.addObserver(self, selector: #selector(longPressPhones), name: .longPressPhones, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(cancelLongPressPhones), name: .cancelLongPressPhones, object: nil)
    }

    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    @objc
    func longPressPhones(){
        isActivated = true
    }
    
    @objc
    func cancelLongPressPhones(){
        isActivated = false
    }
    
    func setupCell(with itemData: ContractsPhones) {
        self.itemData = itemData
        phonesTableView.reloadData()
        contactNumberLabel.text = "Договор #\(itemData.contractNumber)"
        if itemData.flat != "" {
            addressLabel.text = "\(itemData.address ?? "") кв. \(itemData.flat)"
        } else {
            addressLabel.text = itemData.address ?? ""
        }
        
        let heigth = itemData.phones.map({ $0.carNumber != nil ? 75 : 50 }).reduce(0, +)
        tvHeigthConstraint.constant = CGFloat(heigth)
        
        let longPressGestureRecognizer = UILongPressGestureRecognizer(target: self, action: #selector(longPressed))
        phonesTableView.addGestureRecognizer(longPressGestureRecognizer)
        
    }
    
    @objc func longPressed(sender: UILongPressGestureRecognizer) {
        delegate?.onLongPressed()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return itemData?.phones.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        guard let itemData = itemData else { return 0 }
        guard let _ = itemData.phones[indexPath.row].carNumber else { return 50 }
        return 75
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: PhonesViewCell.className, for: indexPath) as! PhonesViewCell
        cell.delegate = self.delegate
        cell.phone = itemData!.phones[indexPath.row]
        
        cell.phoneLabel.text = ""
        if let idata = itemData, idata.phones.count > indexPath.row {
            let phone = idata.phones[indexPath.row]
            if phone.number.count > 0 {
                cell.phoneLabel.text = phone.number.getPhoneWithMask
            } else {
                cell.phoneLabel.text = ""
            }
            if let carNumber = phone.carNumber {
                cell.autoLabel.text = "авто: \(carNumber)"
            } else {
                cell.autoLabel.text = ""
            }
            ContactManager.shared.tryingGetContact(phone: phone.number, delegate: cell)
            
            cell.mySwitch.setOn(phone.sipEnabled, animated: true)
        }
        
        cell.phoneCheckbox.isHidden = !isActivated
        cell.phoneIconConstraint.constant = isActivated ? CGFloat(41) : CGFloat(8)
        
        return cell
    }
}
