//
//  ContractIDCollectionViewCell.swift
//  ProDomofon
//
//  Created by Александр Новиков on 29/07/2019.
//  Copyright © 2019 Александр Новиков. All rights reserved.
//

import UIKit

class ContractIDCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var idLabel: UILabel!
    @IBOutlet weak var itemView: UIView!
    @IBOutlet weak var crossImage: UIImageView!
    
    var tapFinish: (() -> Void)?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        // Initialization code
    }
    
    func setupView(with params: InfoParams){
        itemView.setCornerRadius(radius: 18)
        idLabel.text = String(params.contractNumber!)
        
        crossImage.setRounded()
        crossImage.isUserInteractionEnabled = true
        let tapRecognizer = UITapGestureRecognizer()
        tapRecognizer.addTarget(self, action: #selector(crossTapped(_: )))
        crossImage.addGestureRecognizer(tapRecognizer)
    }
    
    func setTapFinish(tapFinish: @escaping () -> Void) {
        self.tapFinish = tapFinish
    }
    
    @objc func crossTapped(_ sender: UIView){
        UIView.animate(withDuration: 0.2, animations: {
            self.crossImage.alpha = 0.1
        }) { (val) in
            UIView.animate(withDuration: 0.1, animations: {
                self.crossImage.alpha = 1
            }, completion: { (val) in
                self.tapFinish?()
            })
        }
    }

}
