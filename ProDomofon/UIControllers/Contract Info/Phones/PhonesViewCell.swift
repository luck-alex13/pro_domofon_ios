//
//  PhonesViewCell.swift
//  ProDomofon
//
//  Created by Александр Новиков on 05/07/2019.
//  Copyright © 2019 Александр Новиков. All rights reserved.
//

import UIKit

protocol PhonesViewCellDelegate: class {
    func onSettingsTouch(phone: Phone)
    func onSelected(phone: Phone)
    func onDeselected(phone: Phone)
    func mySwitchChanged(phone: Phone, state: Bool)
}

class PhonesViewCell: UITableViewCell {

    @IBOutlet weak var phoneLabel: UILabel!
    @IBOutlet weak var autoLabel: UILabel!
    @IBOutlet weak var phoneCheckbox: Checkbox! {
        didSet {
            phoneCheckbox.valueChanged = { (value) in
                if(value){
                    self.delegate?.onSelected(phone: self.phone)
                }else {
                    self.delegate?.onDeselected(phone: self.phone)
                }
            }
            phoneCheckbox.cornerRadius = 5
            phoneCheckbox.checkedBorderColor = Colors.RED_ACCENT
            phoneCheckbox.uncheckedBorderColor = .gray
            phoneCheckbox.checkmarkColor = Colors.RED_ACCENT
            phoneCheckbox.checkmarkStyle = .tick
            phoneCheckbox.checkmarkSize = 0.6
        }
    }
    @IBOutlet weak var phoneIconConstraint: NSLayoutConstraint!
    @IBOutlet weak var settingsButton: UIButton! {
        didSet {
            settingsButton.setImage(UIImage(named: "ic_gear")?.withRenderingMode(.alwaysTemplate), for: .normal)
            settingsButton.addTarget(self, action: #selector(settingsTouch), for: .touchUpInside)
        }
    }
    @IBOutlet weak var mySwitch: UISwitch! {
        didSet {
            mySwitch.addTarget(self, action: #selector(mySwitchChanged), for: .valueChanged)
        }
    }
    
    
    weak var delegate: PhonesViewCellDelegate?
    var phone: Phone!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        NotificationCenter.default.addObserver(self, selector: #selector(longPressPhones), name: .longPressPhones, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(cancelLongPressPhones), name: .cancelLongPressPhones, object: nil)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    @objc
    func longPressPhones(){
        phoneCheckbox.isHidden = false
        phoneIconConstraint.constant = CGFloat(41)
    }
    
    @objc
    func cancelLongPressPhones(){
        phoneCheckbox.isHidden = true
        phoneIconConstraint.constant = CGFloat(8)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    @objc
    func settingsTouch() {
        delegate?.onSettingsTouch(phone: phone)
    }
    
    @objc
    func mySwitchChanged(){
        delegate?.mySwitchChanged(phone: phone, state: mySwitch.isOn)
    }
}
extension PhonesViewCell: ContactManagerDelegate {
    func getName(text: String) {
        phoneLabel.text = text
    }
}
