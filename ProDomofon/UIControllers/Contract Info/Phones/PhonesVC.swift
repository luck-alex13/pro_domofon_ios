//
//  PhonesVC.swift
//  ProDomofon
//
//  Created by Александр Новиков on 30/06/2019.
//  Copyright © 2019 Александр Новиков. All rights reserved.
//

import UIKit
import RxSwift

class PhonesVC: VCWithTable<ContractsPhones> {
    
    @IBOutlet weak var dataTableView: UITableView!
    @IBOutlet weak var homeIntercomView: HomeIntercomView!
    
    var selecting: Bool = false
    var selectedPhones = [Phone]()
    let disposeBag = DisposeBag()
    
    var params: InfoParams? {
        if let c = DataStorage.shared.choosenContract {
            return InfoParams(
                contractID: c.id,
                contractNumber: c.number,
                flatNumber: c.flat,
                linkToPay: c.getLinkToPay(baseUrl: ApiRouter.getUser.getBaseUrl()),
                address: c.getReadableAddress
            )
        } else {
            return nil
        }
    }
    
    var isOwner: Bool {
        guard let contract = DataStorage.shared.choosenContract else { return false }
        return contract.isOwner
    }
    
    var hasBarrier: Bool {
        guard let contract = DataStorage.shared.choosenContract else { return false }
        return contract.hasBarrier
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setDefaultBar()
        
        defineVariables(sections: 2, cellIdentifer: ContractsPhonesViewCell.className, placeholderTitle: "Здесь будут отбражаться телефоны привязанные к договорам.", placeholderSubtile: nil)

        self.tableView = dataTableView
        self.tableView?.estimatedRowHeight = 160
        self.tableView?.rowHeight = UITableView.automaticDimension
        
        setUpRefreshControl()
    }
    
    func setDefaultBar(){
        if let c = DataStorage.shared.choosenContract, c.isOwner {
            setBar(title: "Телефоны", rightTitle: "Добавить", rightSelector: #selector(showAddPhoneVC))
        } else {
            setBar(title: "Телефоны")
        }
    }
    
    @objc
    func selectContractForPhone() {
        if let nextViewController = Utils.shared.additionalStoryboard().instantiateViewController(withIdentifier: SelectContractVC.className) as? SelectContractVC {
            nextViewController.goal = .phones
            self.navigationController?.pushViewController(nextViewController, animated: true)
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        cancelTapped()
        super.viewWillDisappear(animated)
    }
    
    @objc func showAddPhoneVC() {
        if let c = DataStorage.shared.choosenContract, c.isOwner {
            if let nextViewController = Utils.shared.additionalStoryboard().instantiateViewController(withIdentifier: AddPhoneVC.className) as? AddPhoneVC {
                var data = [InfoParams]()
                data.append(self.params!)
                nextViewController.selectedContracts = data
                self.navigationController?.pushViewController(nextViewController, animated: true)
            }
        } else {
            Utils.shared.showToast(from: self, with: "Изменять список телефонов можно с приложения ТОЛЬКО основного телефона")
        }
        
    }
    
    override func loadDataFromServer(_ refreshControl: UIRefreshControl?) {
        refreshControl?.beginRefreshing()        
        RestClient.shared.getPhones(success: { [weak self] (phonesResp) in
            refreshControl?.endRefreshing()
            self?.savePhones(response: phonesResp)
        }) { [weak self] (err) in
            refreshControl?.endRefreshing()
            Utils.shared.showToastHttp(from: self?.navigationController, with: "Не удалось загрузить телефоны.", error: err)
        }
    }
    
    override func readDataFromDatabase() {
        if let cID = params?.contractID {
            dataArray = DataStorage.shared.contractPhones.filter({ $0.contractId == cID })
        }else {
            dataArray = DataStorage.shared.contractPhones
        }
        self.updateTableView()
    }
    
    func savePhones(response: PhonesResponse) {
        if let contracts = response.contracts {
            DataStorage.shared.contractPhones = contracts
        } else {
            DataStorage.shared.contractPhones = []
        }
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return super.tableView(tableView, numberOfRowsInSection: section)
        } else {
            return 2 + (hasBarrier ? 1 : 0)
        }
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifer!, for: indexPath) as! ContractsPhonesViewCell
            cell.setupCell(with: dataArray[indexPath.row])
            cell.delegate = self
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: ContractPhonesInfoViewCell.className, for: indexPath) as! ContractPhonesInfoViewCell
            
            if isOwner {
                cell.infoLabel.text = "Вы владелец договора"
            } else {
                cell.infoLabel.text = "Вы не владелец договора"
            }
            
            switch indexPath.row {
            case 0:
                cell.infoLabel.text = "Добавление телефонов членов семьи доступно с приложения только главного телефона этого договора."
            case 1:
                cell.infoLabel.text = "Для удаления телефона из списка по данному договору:\n1. Нажмите на номер телефона и удерживайте 1 сек.;\n2. Отметьте телефон;\n3. Нажмите кнопку \"Удалить\" в правом верхнем углу приложения."
            case 2:
                let text = "Добавить/Изменить номер автомобиля для автоматического открывания ворот перейдите в раздел \"Сообщения\" и отправьте номер без пробелов.(Пример Н126ВР138).\nКаждый член семьи добавляет свой авто самостоятельно.\n\n"
                let link = "Перейти в раздел \"Сообщения\""
                let url = URL(string: StringConst.MAIN_LINK.rawValue)!
                
                let style = NSMutableParagraphStyle()
                style.alignment = .left

                let attributedOriginalText = NSMutableAttributedString(string: text + link)
                let textRange = attributedOriginalText.mutableString.range(of: text)
                let linkRange = attributedOriginalText.mutableString.range(of: link)
                let fullRange = NSMakeRange(0, attributedOriginalText.length)
                
                attributedOriginalText.addAttribute(NSAttributedString.Key.link, value: link, range: linkRange)
                attributedOriginalText.setAttributes([.link: url], range: linkRange)
                attributedOriginalText.addAttribute(NSAttributedString.Key.paragraphStyle, value: style, range: fullRange)
                attributedOriginalText.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.darkGray, range: textRange)
                attributedOriginalText.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor(rgb: 0x734dff), range: linkRange)
                attributedOriginalText.addAttribute(NSAttributedString.Key.font, value: UIFont.systemFont(ofSize: 14), range: fullRange)

                cell.infoLabel.attributedText = attributedOriginalText
                
                cell.infoTextView.delegate = self
                cell.infoTextView.attributedText = attributedOriginalText
                cell.infoTextView.linkTextAttributes = [
                    kCTForegroundColorAttributeName: UIColor(rgb: 0x734dff),
                    kCTUnderlineStyleAttributeName: NSUnderlineStyle.single.rawValue,
                ] as [NSAttributedString.Key : Any]
                cell.infoTextView.textContainer.lineFragmentPadding = 0
                cell.infoTextView.textContainerInset = .zero
                cell.infoTextView.isScrollEnabled = false
                
                cell.infoLabel.isHidden = true
                cell.infoTextView.isHidden = false
            default: break
            }
            
            cell.infoLabel.sizeToFit()
            return cell
        }
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard indexPath.section == 0 else { return }
        tableView.deselectRow(at: indexPath, animated: false)
    }
    
    @objc func deleteTapped() {
        /*
        guard let c = DataStorage.shared.choosenContract, c.isOwner else {
            Utils.shared.showToast(from: self, with: "Изменять список телефонов можно с приложения ТОЛЬКО основного телефона")
            return
        }
         */
        
        let end = selectedPhones.count > 1 ? "выбранные телефоны" : "выбранный телефон"
        let alert = AlertManager.shared.buildOkCancelAlert(title: "Внимание!", errorMessage: "Вы действительно хотите удалить \(end)?", okHandler: { (act) in
            self.deletePhones(selected: self.selectedPhones)
        }) { (act) in
            
        }
        self.present(alert, animated: true, completion: nil)
    }
    
    func deletePhones(selected: [Phone]) {
        guard let observer = super.сonnectionObserver, observer.isConnectedToInternet else {
            AlertManager.shared.showAlertNoInternet(from: self)
            return
        }
        AlertManager.shared.showProgressDialog(from: self)
        
        Observable.from(selected)
            .concatMap { (phone) -> Observable<PhonesResponse> in
                return RestClient.shared.removePhone(phoneNumber: phone.number, contactID: phone.contractId)
            }
            .do(onNext: { (resp) in
                self.savePhones(response: resp)
            })
            .toArray()
            .subscribe(onNext: { (resp) in
                AlertManager.shared.hideProgress {
                    if let user = DataStorage.shared.user, selected.contains(where: { phone in
                        return phone.number == user.phone
                    }) {
                        DataStorage.shared.choosenContractString = nil
                        AppDelegate.shared.openVC(type: .contracts)
                    }
                    self.updateTableView()
                    self.cancelTapped()
                }
            }, onError: { (error) in
                Utils.shared.showToast(from: self.navigationController, with: "Произошла ошибка. Не удалось удалить номер телефона.")
                self.cancelTapped()
            })
            .disposed(by: disposeBag)
    }
    
    func updateDeleteButton() {
        navigationItem.rightBarButtonItem?.isEnabled = selectedPhones.count > 0
    }
    
    @objc func cancelTapped(){
        selecting = false
        setDefaultBar()
        NotificationCenter.default.post(name: .cancelLongPressPhones, object: nil)
    }

}
extension PhonesVC: ContractsPhonesViewCellDelegate {
    func onSelected(phone: Phone) {
        selectedPhones.append(phone)
        updateDeleteButton()
    }
    
    func onDeselected(phone: Phone) {
        if let ind = selectedPhones.firstIndex(where: { $0.id == phone.id }) {
            selectedPhones.remove(at: ind)
            updateDeleteButton()
        }
    }
    
    func onLongPressed() {
        if(!selecting) {
            navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Удалить", style: .done, target: self, action: #selector(deleteTapped))
            navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Отмена", style: .done, target: self, action: #selector(cancelTapped))
            selecting = true
            updateDeleteButton()
            NotificationCenter.default.post(name: .longPressPhones, object: nil)
        }
    }
    
    func onSettingsTouch(phone: Phone){
        if let nextViewController = Utils.shared.additionalStoryboard().instantiateViewController(withIdentifier: PhoneEditVC.className) as? PhoneEditVC {
            nextViewController.phone = phone
            self.navigationController?.pushViewController(nextViewController, animated: true)
        }
    }
    
    func mySwitchChanged(phone: Phone, state: Bool) {
        guard dataArray.count > 0 else { return }
        let contract = dataArray[0]
        
        if let index = contract.phones.firstIndex(where: { $0.id == phone.id }) {
            contract.phones[index].sipEnabled = state
        }
        
        if let data = contract.dataForEditPhoneSip {
            AlertManager.shared.showProgressDialog(from: self)
            RestClient.shared.editPhoneSip(data: data) {
                AlertManager.shared.hideProgress()
            } errorCall: { (error) in
                AlertManager.shared.hideProgress()
                Utils.shared.showToastHttp(from: self, with: "Не удалось изменить доступность телефона.", error: error)
                self.loadDataFromServer(self.refreshControl)
            }
        }
    }
}
extension PhonesVC: UITextViewDelegate {
    func textView(_ textView: UITextView, shouldInteractWith URL: URL, in characterRange: NSRange, interaction: UITextItemInteraction) -> Bool {
        AppDelegate.shared.openVC(type: .messages)
        return false
    }
}
