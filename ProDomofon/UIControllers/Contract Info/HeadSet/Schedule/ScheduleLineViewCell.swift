//
//  ScheduleLineViewCell.swift
//  ProDomofon
//
//  Created by Алексей Агильдин on 24.02.2021.
//  Copyright © 2021 Александр Новиков. All rights reserved.
//

import UIKit

protocol ScheduleLineViewCellDelegate: class {
    func scheduleChanged(id: String, state: Bool)
}

class ScheduleLineViewCell: UITableViewCell {

    @IBOutlet weak var daysLabel: UILabel!
    @IBOutlet weak var tumbler: UISwitch! {
        didSet {
            tumbler.addTarget(self, action: #selector(tumblerChanged), for: .valueChanged)
        }
    }
    @IBOutlet weak var timeLabel: UILabel!
    
    weak var delegate: ScheduleLineViewCellDelegate?
    
    fileprivate var schedule: Schedule!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    func setupCell(with itemData: Schedule) {
        self.schedule = itemData
        tumbler.setOn(itemData.enabled, animated: false)
        daysLabel.attributedText = itemData.daysAttributedText
        timeLabel.text = itemData.timeText
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

    @objc
    func tumblerChanged() {
        delegate?.scheduleChanged(id: schedule.id!, state: tumbler.isOn)
    }
}
