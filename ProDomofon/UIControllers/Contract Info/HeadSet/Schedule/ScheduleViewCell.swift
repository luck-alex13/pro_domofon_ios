//
//  ScheduleViewCell.swift
//  ProDomofon
//
//  Created by Алексей Агильдин on 18.02.2021.
//  Copyright © 2021 Александр Новиков. All rights reserved.
//

import UIKit

protocol ScheduleViewCellDelegate: class {
    func addScheduleTouch()
    func headsetChanged(contractId: String, headset: HeadSet)
    func phoneChanged(phone: Phone)
    func didSelectSchedule(data: Schedule)
}

class ScheduleViewCell: UITableViewCell {
    
    @IBOutlet weak var cardView: UIView!
    @IBOutlet weak var alwaysSwitch: UISwitch! {
        didSet {
            alwaysSwitch.addTarget(self, action: #selector(alwaysChanged), for: .valueChanged)
        }
    }
    @IBOutlet weak var linesTableView: UITableView! {
        didSet {
            linesTableView.register(UINib(nibName: "ScheduleLineViewCell", bundle: nil), forCellReuseIdentifier: ScheduleLineViewCell.identifier)
            linesTableView.dataSource = self
            linesTableView.delegate = self
        }
    }
    @IBOutlet weak var heightLinesTableView: NSLayoutConstraint!
    @IBOutlet weak var addButton: UIButton! {
        didSet {
            addButton.setCornerRadius(radius: 6)
            let imageView = UIImageView(frame: CGRect(x: addButton.frame.width - 5 - 20, y: 5, width: 20, height: 20))
            imageView.image = UIImage(named: "ic_plus")?.withRenderingMode(.alwaysTemplate)
            imageView.tintColor = .white
            addButton.addSubview(imageView)
        }
    }
    fileprivate var headset: HeadSet?
    fileprivate var contractId: String!
    
    weak var delegate: ScheduleViewCellDelegate?

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func setupCell(with itemData: Contract) {
        contractId = itemData.id
        if let h = itemData.headset {
            self.headset = h
            alwaysSwitch.setOn(h.always, animated: false)
            addButton.isEnabled = !h.always
            addButton.backgroundColor = h.always ? .lightGray : Colors.RED_ACCENT
            heightLinesTableView.constant = CGFloat(70 * h.schedules.count)
        }
        linesTableView.reloadData()
    }

    @IBAction func addTouch() {
        delegate?.addScheduleTouch()
    }
    
    @objc
    func alwaysChanged(){
        if let h = headset {
            h.always = alwaysSwitch.isOn
            addButton.isEnabled = !h.always
            addButton.backgroundColor = h.always ? .lightGray : Colors.RED_ACCENT
            delegate?.headsetChanged(contractId: contractId, headset: h)
        }
    }
}
extension ScheduleViewCell: UITableViewDataSource {
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: ScheduleLineViewCell.identifier, for: indexPath) as! ScheduleLineViewCell
        if let h = headset {
            cell.setupCell(with: h.schedules[indexPath.row])
        }
        cell.delegate = self
        return cell
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return headset?.schedules.count ?? 0
    }
}
extension ScheduleViewCell: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        if let h = headset, h.schedules[indexPath.row].enabled {
            delegate?.didSelectSchedule(data: h.schedules[indexPath.row])
        }
    }
}
extension ScheduleViewCell: ScheduleLineViewCellDelegate {
    func scheduleChanged(id: String, state: Bool) {
        if let h = headset {
            if let index = h.schedules.firstIndex(where: { $0.id == id }) {
                h.schedules[index].enableChanged(state: state)
                delegate?.headsetChanged(contractId: contractId, headset: h)
            }
        }
    }
}
