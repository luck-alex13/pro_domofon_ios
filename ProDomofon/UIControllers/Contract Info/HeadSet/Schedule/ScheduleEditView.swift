//
//  ScheduleEditView.swift
//  ProDomofon
//
//  Created by Алексей Агильдин on 16.03.2021.
//  Copyright © 2021 Александр Новиков. All rights reserved.
//

import UIKit

protocol ScheduleEditViewDelegate: class {
    func saveTouch(data: Schedule)
}

class ScheduleEditView: UIView {

    fileprivate var startDateTextField: UITextField!
    fileprivate var endDateTextField: UITextField!
    fileprivate var collectionView: UICollectionView!
    fileprivate var timeInputView: TimeInputView!
    
    fileprivate var data = Schedule()
    var delegate: ScheduleEditViewDelegate?
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        
        guard startDateTextField == nil else  {
            return
        }
        
        backgroundColor = .clear
        
        let control = UIControl(frame: rect)
        control.backgroundColor = UIColor.black.withAlphaComponent(0.75)
        control.addTarget(self, action: #selector(bgTouch), for: .touchDown)
        addSubview(control)
        
        let mainControl = UIControl()
        
        var posY: CGFloat = 0
        let width = rect.width - 32
        
        let backTitleView = UIView(frame: CGRect(x: 0, y: posY, width: width, height: 40))
        backTitleView.backgroundColor = Colors.GREEN_PRIMARY
        mainControl.addSubview(backTitleView)
        
        let titleLabel = UILabel(frame: CGRect(x: 24, y: 0, width: width - 48, height: backTitleView.frame.height))
        titleLabel.textAlignment = .left
        titleLabel.textColor = .white
        titleLabel.font = UIFont.systemFont(ofSize: 16, weight: .semibold)
        titleLabel.text = "Настройка расписания"
        backTitleView.addSubview(titleLabel)
        
        let lineView = UIView(frame: CGRect(x: 0, y: backTitleView.frame.height - 1, width: backTitleView.frame.width, height: 1))
        lineView.backgroundColor = .darkGray
        backTitleView.addSubview(lineView)
        
        posY += backTitleView.frame.height
        
        let beginLabel = UILabel(frame: CGRect(x: 0, y: posY, width: width / 2, height: 40))
        beginLabel.textAlignment = .center
        beginLabel.textColor = .black
        beginLabel.font = UIFont.systemFont(ofSize: 16, weight: .semibold)
        beginLabel.text = "начало"
        mainControl.addSubview(beginLabel)
        
        let endLabel = UILabel(frame: CGRect(x: width / 2, y: posY, width: width / 2, height: 40))
        endLabel.textAlignment = .center
        endLabel.textColor = .black
        endLabel.font = UIFont.systemFont(ofSize: 16, weight: .semibold)
        endLabel.text = "конец"
        mainControl.addSubview(endLabel)
        
        posY += beginLabel.frame.height + 16
        
        timeInputView = TimeInputView(frame: CGRect(x: 0, y: 0, width: rect.width, height: 200))
        timeInputView.delegate = self
        
        startDateTextField = UITextField(frame: CGRect(x: width / 4 - 100 / 2, y: posY, width: 100, height: 30))
        startDateTextField.textAlignment = .center
        startDateTextField.font = UIFont.systemFont(ofSize: 16)
        startDateTextField.textColor = .black
        startDateTextField.inputView = timeInputView
        startDateTextField.delegate = self
        startDateTextField.setRoundedBorder(color: .darkGray, borderWidth: 1, radius: 4)
        startDateTextField.text = data.startText
        mainControl.addSubview(startDateTextField)
        
        endDateTextField = UITextField(frame: CGRect(x: width * 3 / 4 - 100 / 2, y: posY, width: 100, height: 30))
        endDateTextField.textAlignment = .center
        endDateTextField.font = UIFont.systemFont(ofSize: 16)
        endDateTextField.textColor = .black
        endDateTextField.inputView = timeInputView
        endDateTextField.delegate = self
        endDateTextField.setRoundedBorder(color: .darkGray, borderWidth: 1, radius: 4)
        endDateTextField.text = data.endText
        mainControl.addSubview(endDateTextField)
        
        posY += startDateTextField.frame.height + 16
        
        let daysLabel = UILabel(frame: CGRect(x: 24, y: posY, width: width - 48, height: 40))
        daysLabel.textAlignment = .left
        daysLabel.textColor = .black
        daysLabel.font = UIFont.systemFont(ofSize: 16, weight: .semibold)
        daysLabel.text = "дни недели"
        mainControl.addSubview(daysLabel)
        
        posY += daysLabel.frame.height
        
        
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        layout.minimumLineSpacing = 0
        layout.minimumInteritemSpacing = 0
        
        layout.estimatedItemSize = CGSize(width: (width - 32) / 7, height: 50)
        
        collectionView = UICollectionView(frame: CGRect(x: 16, y: posY, width: width - 32, height: 50), collectionViewLayout: layout)
        collectionView.register(UICollectionViewCell.self, forCellWithReuseIdentifier: "cell")
        collectionView.backgroundColor = .clear
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.isScrollEnabled = false
        collectionView.setRoundedBorder(color: .darkGray, borderWidth: 1, radius: 5)
        mainControl.addSubview(collectionView)
        
        posY += collectionView.frame.height + 16
        
        let bottomLine = UIView(frame: CGRect(x: 0, y: posY, width: width, height: 1))
        bottomLine.backgroundColor = .darkGray
        mainControl.addSubview(bottomLine)
        posY += bottomLine.frame.height
        
        let cancelButton = UIButton(frame: CGRect(x: 0, y: posY, width: width / 2, height: 40))
        cancelButton.backgroundColor = .clear
        cancelButton.titleLabel?.font = UIFont.systemFont(ofSize: 16)
        cancelButton.setTitle("ОТМЕНА", for: .normal)
        cancelButton.setTitleColor(Colors.RED_ACCENT, for: .normal)
        cancelButton.addTarget(self, action: #selector(bgTouch), for: .touchUpInside)
        mainControl.addSubview(cancelButton)
        
        let saveButton = UIButton(frame: CGRect(x: width / 2, y: posY, width: width / 2, height: 40))
        saveButton.backgroundColor = .clear
        saveButton.titleLabel?.font = UIFont.systemFont(ofSize: 16)
        saveButton.setTitle("СОХРАНИТЬ", for: .normal)
        saveButton.setTitleColor(Colors.GREEN_PRIMARY, for: .normal)
        saveButton.addTarget(self, action: #selector(saveTouch), for: .touchUpInside)
        mainControl.addSubview(saveButton)
        
        posY += cancelButton.frame.height
        
        mainControl.frame = CGRect(x: 16, y: rect.height / 2 - posY / 2, width: width, height: posY)
        mainControl.backgroundColor = .white
        mainControl.setCornerRadius(radius: 10)
        mainControl.addTarget(self, action: #selector(mainControlTouch), for: .touchDown)
        addSubview(mainControl)
    }

    func set(data: Schedule) {
        self.data = data
        if startDateTextField != nil {
            startDateTextField.text = data.startText
            endDateTextField.text = data.endText
            collectionView.reloadData()
        }
    }
    
    @objc
    func bgTouch(){
        mainControlTouch()
        hideObject()
    }
    
    @objc
    func mainControlTouch(){
        startDateTextField.resignFirstResponder()
        endDateTextField.resignFirstResponder()
    }
    
    @objc
    func saveTouch(){
        delegate?.saveTouch(data: data)
    }
}
extension ScheduleEditView: UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 7
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath)
        cell.contentView.subviews.forEach { (s) in
            s.removeFromSuperview()
        }
        
        let titleLabel = UILabel(frame: CGRect(x: 0, y: 0, width: cell.contentView.frame.width, height: cell.contentView.frame.height))
        let choosen = data.dow.contains(indexPath.row)
        titleLabel.backgroundColor = .clear
        titleLabel.font = choosen ? UIFont.systemFont(ofSize: 16, weight: .heavy) : UIFont.systemFont(ofSize: 16)
        titleLabel.textColor = choosen ? Colors.GREEN_DARK : .darkGray
        titleLabel.text = Schedule.arrayDays[indexPath.row]
        titleLabel.textAlignment = .center
        cell.contentView.addSubview(titleLabel)
        
        if indexPath.row != 6 {
            let vertLine = UIView(frame: CGRect(x: cell.contentView.frame.width - 1, y: 3, width: 1, height: cell.contentView.frame.height - 6))
            vertLine.backgroundColor = .darkGray
            cell.contentView.addSubview(vertLine)
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        data.editDow(value: indexPath.row)
        collectionView.reloadItems(at: [indexPath])
    }
}
extension ScheduleEditView: UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        switch textField {
        case startDateTextField:
            timeInputView.setParentDate(textField: startDateTextField, date: data.startDate)
            timeInputView.setMinMaxDate(minimumDate: nil, maximumDate: data.maximumStartDate)
            break
        case endDateTextField:
            timeInputView.setParentDate(textField: endDateTextField, date: data.endDate)
            timeInputView.setMinMaxDate(minimumDate: data.start > 60 ? data.startDate : data.minimumEndDate, maximumDate: nil)
            break
        default: break
        }
    }
}
extension ScheduleEditView: TimeInputViewDelegate {
    
    func timeChange(parent: UITextField, seconds: Int) {
        switch parent {
        case startDateTextField:
            data.editStart(value: seconds)
            startDateTextField.text = data.startText
            if data.start > data.end {
                data.editEnd(value: data.start + 1)
                endDateTextField.text = data.endText
            }
            break
        case endDateTextField:
            data.editEnd(value: seconds)
            endDateTextField.text = data.endText
            break
        default: break
        }
    }
    
    func doneTouch(parent: UITextField) {
        parent.resignFirstResponder()
    }
}
