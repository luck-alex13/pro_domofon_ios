//
//  ScheduleVC.swift
//  ProDomofon
//
//  Created by Алексей Агильдин on 18.02.2021.
//  Copyright © 2021 Александр Новиков. All rights reserved.
//

import UIKit

class ScheduleVC: VCWithTable<Contract> {

    @IBOutlet weak var dataTableView: UITableView!
    
    weak var delegate: ScheduleVCDelegate?
    
    var params: InfoParams?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        defineVariables(sections: 1, cellIdentifer: ScheduleViewCell.className, placeholderTitle: "Здесь будут отображаться расписания")
        
        self.tableView = dataTableView
        self.tableView?.estimatedRowHeight = 300
        self.tableView?.rowHeight = UITableView.automaticDimension
        setUpRefreshControl()
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifer!, for: indexPath) as! ScheduleViewCell
        cell.setupCell(with: dataArray[indexPath.row])
        cell.delegate = self
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    override func loadDataFromServer(_ refreshControl: UIRefreshControl?) {
        refreshControl?.beginRefreshing()
        RestClient.shared.getUserAndContracts(success: { [weak self] (user) in
            guard let self = self else { return }
            refreshControl?.endRefreshing()
            DataStorage.shared.scheduleContracts = user.contracts
//            self.dataArray = user.contracts
//            self?.saveDataToDatabase(user.contracts ?? [], update: true)
//            self.updateTableView()
        }) { [weak self] (err) in
            refreshControl?.endRefreshing()
            Utils.shared.showToastHttp(from: self?.view, with: "Не удалось загрузить расписание.", error: err)
        }
    }
    
    override func readDataFromDatabase() {
        if let p = params, let cID = p.contractID {
            dataArray = DataStorage.shared.scheduleContracts.filter({ $0.id == cID })
        } else {
            dataArray = DataStorage.shared.scheduleContracts
        }
        updateTableView()
    }
    
    func newSchedule(schedule: Schedule) {
        if dataArray.count > 0 {
            let contract = dataArray[0]
            if let headset = contract.headset {
                headset.schedules.append(schedule)
                sendHeadSet(contractId: contract.id, headset: headset)
            }
        }
    }
    
    func editSchedule(schedule: Schedule) {
        if dataArray.count > 0 {
            let contract = dataArray[0]
            if let headset = contract.headset, let index = headset.schedules.firstIndex(where: { $0.id == schedule.id }) {
                headset.schedules[index] = schedule
                sendHeadSet(contractId: contract.id, headset: headset)
            }
        }
    }
    
    func sendHeadSet(contractId: String, headset: HeadSet){
        if let data = headset.data {
            AlertManager.shared.showProgressDialog(from: self)
            RestClient.shared.editContract(contractId: contractId, data: data) { resp in
                AlertManager.shared.hideProgress {
                    self.updateTableView()
                }
            } errorCall: { [weak self] (error) in
                AlertManager.shared.hideProgress {
                    Utils.shared.showToastHttp(from: self?.view, with: "Не удалось сохранить расписание.", error: error)
                    self?.loadDataFromServer(self?.refreshControl)
                }
            }

            
        } else {
            Utils.shared.showToast(from: self.view, with: "Ошибка при преобразовании объекта HeadSet в json")
            self.loadDataFromServer(self.refreshControl)
        }
    }
}
extension ScheduleVC: ScheduleViewCellDelegate {
    func addScheduleTouch(){
        delegate?.showScheduleEditView(data: Schedule())
    }
    
    func headsetChanged(contractId: String, headset: HeadSet) {
        sendHeadSet(contractId: contractId, headset: headset)
    }
    
    func phoneChanged(phone: Phone) {
        
    }
    
    func didSelectSchedule(data: Schedule) {
        delegate?.showScheduleEditView(data: data)
    }
}
