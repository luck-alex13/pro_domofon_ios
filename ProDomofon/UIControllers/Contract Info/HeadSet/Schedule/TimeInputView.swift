//
//  TimeInputView.swift
//  ProDomofon
//
//  Created by Алексей Агильдин on 22.03.2021.
//  Copyright © 2021 Александр Новиков. All rights reserved.
//

import UIKit

protocol TimeInputViewDelegate: class {
    func timeChange(parent: UITextField, seconds: Int)
    func doneTouch(parent: UITextField)
}

class TimeInputView: UIView {

    fileprivate var doneButton: UIButton!
    fileprivate var datePicker: UIDatePicker!
    
    fileprivate var parent: UITextField?
    fileprivate var date: Date!
    fileprivate var minimumDate: Date?
    fileprivate var maximumDate: Date?
    
    var delegate: TimeInputViewDelegate?
    
    override func draw(_ rect: CGRect) {
        guard datePicker == nil else { return }
        
        doneButton = UIButton()
        doneButton.setTitleColor(Colors.GREEN_DARK, for: .normal)
        doneButton.titleEdgeInsets = UIEdgeInsets(top: 0, left: 8, bottom: 0, right: 8)
        doneButton.setTitle("Готово", for: .normal)
        doneButton.sizeToFit()
        doneButton.frame = CGRect(x: rect.width - doneButton.frame.width - 20, y: 0, width: doneButton.frame.width + 20, height: 40)
        doneButton.addTarget(self, action: #selector(doneTouch), for: .touchUpInside)
        addSubview(doneButton)
        
        datePicker = UIDatePicker(frame: CGRect(x: 0, y: doneButton.frame.height, width: rect.width, height: rect.height - doneButton.frame.height))
        datePicker.datePickerMode = .time
        datePicker.addTarget(self, action: #selector(dateChanged), for: .valueChanged)
        if #available(iOS 13.4, *) {
            datePicker.preferredDatePickerStyle = .wheels
        } else {
            
        }
        if date != nil {
            datePicker.setDate(date, animated: true)
        }
        datePicker.minimumDate = minimumDate
        datePicker.maximumDate = maximumDate
        
        addSubview(datePicker)
    }
    
    func setParentDate(textField: UITextField, date: Date){
        parent = textField
        self.date = date
        if datePicker != nil {
            datePicker.setDate(date, animated: true)
        }
    }
    
    func setMinMaxDate(minimumDate: Date?, maximumDate: Date?) {
        self.minimumDate = minimumDate
        self.maximumDate = maximumDate
        if datePicker != nil {
            datePicker.minimumDate = minimumDate
            datePicker.maximumDate = maximumDate
        }
    }
    
    @objc
    func doneTouch(){
        guard let p = parent else { return }
        delegate?.doneTouch(parent: p)
    }
    
    @objc
    func dateChanged(){
        guard let p = parent else { return }
        let calendar = Calendar.current
        let hours = calendar.component(.hour, from: datePicker.date)
        let minutes = calendar.component(.minute, from: datePicker.date)
        let seconds = hours * 60 * 60 + minutes * 60
        delegate?.timeChange(parent: p, seconds: seconds)
    }
    
}
