//
//  HeadSetVC.swift
//  ProDomofon
//
//  Created by Алексей Агильдин on 18.02.2021.
//  Copyright © 2021 Александр Новиков. All rights reserved.
//

import UIKit

protocol ScheduleVCDelegate: class {
    func showScheduleEditView(data: Schedule)
}

class HeadSetVC: SegmentedTabVC {

    @IBOutlet weak var tabControl: UISegmentedControl!
    @IBOutlet weak var contentView: UIView!
    
    fileprivate var scheduleEditView: ScheduleEditView!
    
    fileprivate var scheduleVC: ScheduleVC!
    
    var openSchedule: Bool?
    
    var params: InfoParams? {
        if let c = DataStorage.shared.choosenContract {
            return InfoParams(
                contractID: c.id,
                contractNumber: c.number,
                flatNumber: c.flat,
                linkToPay: c.getLinkToPay(baseUrl: ApiRouter.getUser.getBaseUrl()),
                address: c.getReadableAddress
            )
        } else {
            return nil
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setBar(title: "Квартирные трубки")
        
        setUpPageViewController(contentView: contentView, storyboard: Utils.shared.additionalStoryboard())
        
        scheduleEditView = ScheduleEditView(frame: CGRect(x: 0, y: 0, width: view.frame.width, height: view.frame.height))
        scheduleEditView.delegate = self
        view.addSubview(scheduleEditView)
        scheduleEditView.hideObject(0)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if let _ = openSchedule {
            changeSelectedSegmentIndex(index: 1)
            pageSelected(index: 1)
            openSchedule = nil
        }
    }
    
    override func initPages() {
        tabsVCList = [UIViewController]()
        tabsVCList?.append(self.instantinatePage(atIndex: 0)!)
        tabsVCList?.append(self.instantinatePage(atIndex: 1)!)
    }
    
    override func initTabTitles() {
        tabControl.setTitle("Установка", forSegmentAt: 0)
        tabControl.setTitle("Расписание", forSegmentAt: 1)
    }
    
    override func instantinatePage(atIndex index: Int) -> UIViewController? {
        guard index >= 0 && index < 2 else { return nil}
        if index == 0 {
            let listVC = Utils.shared.additionalStoryboard().instantiateViewController(withIdentifier: InstallVC.className) as! InstallVC
            listVC.setTabIndex(index: index)
            listVC.params = self.params
            return listVC
        }else if index == 1 {
            scheduleVC = Utils.shared.additionalStoryboard().instantiateViewController(withIdentifier: ScheduleVC.className) as? ScheduleVC
            scheduleVC.setTabIndex(index: index)
            scheduleVC.delegate = self
            scheduleVC.params = self.params
            return scheduleVC
        } else {
            return nil
        }
    }
    
    @IBAction func segmentSelected(_ sender: UISegmentedControl) {
        super.tabSelected(sender)
    }
    
    override func changeSelectedSegmentIndex(index: Int) {
        tabControl.selectedSegmentIndex = index
    }
}
extension HeadSetVC: ScheduleVCDelegate {
    func showScheduleEditView(data: Schedule) {
        scheduleEditView.set(data: data)
        scheduleEditView.showObject()
    }
}
extension HeadSetVC: ScheduleEditViewDelegate {
    func saveTouch(data: Schedule){
        if data.dow.count > 0 {
            if data.id == nil {
                scheduleVC.newSchedule(schedule: data)
            } else {
                scheduleVC.editSchedule(schedule: data)
            }
            scheduleEditView.hideObject()
        } else {
            AlertManager.shared.showAlert(from: self, title: "Ошибка", message: "Выберите дни недели прежде, чем сохранить расписание", handler: nil)
        }
    }
}
