//
//  InstallPayViewCell.swift
//  ProDomofon
//
//  Created by Алексей Агильдин on 10.03.2021.
//  Copyright © 2021 Александр Новиков. All rights reserved.
//

import UIKit

class InstallPayViewCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel! {
        didSet {
            titleLabel.text = "Второй шаг:"
        }
    }
    @IBOutlet weak var mainTextLabel: UILabel! {
        didSet {
            let text = "Внесите оплату любым удобным для Вас способом, например нажав кнопку Оплатить - оплата пройдет в теч. 1 минуты (в том числе карты Сберьбанка). "
            let redText = "ВНИМАНИЕ! При оплате через продукты Сбербанка (в т.ч. Сбербанк онлайн), средства поступают в теч. 3х рабочих дней"
            let attributedText = NSMutableAttributedString(string: text + redText)
            let textRange = attributedText.mutableString.range(of: text)
            let redTextRange = attributedText.mutableString.range(of: redText)
//            let fullRange = NSMakeRange(0, attributedText.length)
            
            attributedText.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.black, range: textRange)
            attributedText.addAttribute(NSAttributedString.Key.foregroundColor, value: Colors.RED_ACCENT, range: redTextRange)

            mainTextLabel.attributedText = attributedText            
        }
    }
    

}
