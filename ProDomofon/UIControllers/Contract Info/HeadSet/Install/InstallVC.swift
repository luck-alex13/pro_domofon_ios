//
//  InstallVC.swift
//  ProDomofon
//
//  Created by Алексей Агильдин on 18.02.2021.
//  Copyright © 2021 Александр Новиков. All rights reserved.
//

import UIKit

class InstallVC: VCWithTable<Install> {

    @IBOutlet weak var dataTableView: UITableView!
    
    var params: InfoParams?
    
    var dataCall: InstallCall? {
        let d = dataArray.filter { (i) -> Bool in
            return i.contracts.first(where: { (ic) -> Bool in
                return ic.contractId == params?.contractID && ic.contractId != "" && ic.call != nil
            }) != nil
        }
        if d.count > 0 {
            if let index = d[0].contracts.firstIndex(where: { ic in
                return ic.contractId == params?.contractID && ic.call != nil
            }) {
                return d[0].contracts[index].call ?? nil
            }
            return nil
        } else {
            return nil
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        defineVariables(sections: 1, cellIdentifer: InstallViewCell.className, placeholderTitle: "Здесь будут отображаться квартирные трубки")
        
        self.tableView = dataTableView
        self.tableView?.rowHeight = UITableView.automaticDimension
        setUpRefreshControl()
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return dataCall != nil ? 2 : 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return section == 0 && dataCall == nil ? super.tableView(tableView, numberOfRowsInSection: section) : 1
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0 {
            return 226
        } else {
            return 120
        }
    }
    
    

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let call = dataCall {
            if indexPath.section > 0 {
                if call.paid > 0 {
                    let cell = tableView.dequeueReusableCell(withIdentifier: InstallWaitViewCell.className, for: indexPath) as! InstallWaitViewCell
                    return cell
                } else {
                    let cell = tableView.dequeueReusableCell(withIdentifier: InstallPayViewCell.className, for: indexPath) as! InstallPayViewCell
                    return cell
                }
            } else {
                let cell = tableView.dequeueReusableCell(withIdentifier: InstallViewCell.className, for: indexPath) as! InstallViewCell
                cell.delegate = self
                cell.contractID = self.params?.contractID
                
                let dataInstall = dataArray as [Install]
                if let inst = dataInstall.first(where: { (i) -> Bool in
                    return i.id == call.deviceId
                }) {
                    cell.setupCell(with: inst)
                }
                return cell
            }
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: InstallViewCell.className, for: indexPath) as! InstallViewCell
            cell.delegate = self
            cell.contractID = self.params?.contractID
            cell.setupCell(with: dataArray[indexPath.row])
            return cell
        }
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        if indexPath.section == 0 {
            if let call = dataCall {
                let dataInstall = dataArray as [Install]
                if let inst = dataInstall.first(where: { (i) -> Bool in
                    return i.id == call.deviceId
                }) {
                    Utils.shared.openUrl(url: inst.url)
                }
            } else {
                Utils.shared.openUrl(url: dataArray[indexPath.row].url)
            }
        }
    }
    
    override func loadDataFromServer(_ refreshControl: UIRefreshControl?) {
        refreshControl?.beginRefreshing()
        RestClient.shared.getHeadsetDevices { [weak self] (installs) in
            guard let self = self else { return }
            refreshControl?.endRefreshing()
            DataStorage.shared.installs = installs
            self.updateTableView()
        } errorCall: { (err) in
            refreshControl?.endRefreshing()
            Utils.shared.showToastHttp(from: self.view, with: "Не удалось загрузить квартирные трубки.", error: err)
        }
    }
    
    override func readDataFromDatabase() {
        dataArray = DataStorage.shared.installs
        updateTableView()
    }
    
    
}
extension InstallVC: InstallViewCellDelegate {
    func createRequest(contractId: String, deviceId: Int, name: String) {
        let alert = AlertManager.shared.buildOkCancelAlert(title: "Внимание", errorMessage: "Вы действительно хотите сделать установку переговорного устройства \(name) по адресу \(params?.address ?? "")", okText: "Продолжить", cancelText: "Отмена", okHandler: { (_) in
            AlertManager.shared.showProgressDialog(from: self)
            RestClient.shared.createRequest(contractId: contractId, deviceId: deviceId) { (resp) in
                AlertManager.shared.hideProgress()
                self.loadDataFromServer(self.refreshControl)
            } errorCall: { (error) in
                AlertManager.shared.hideProgress()
                Utils.shared.showToastHttp(from: self.view, with: "Не удалось создать заявку.", error: error)
                self.updateTableView()
            }
        }, cancelHandler: nil)
        self.present(alert, animated: true, completion: nil)
    }
    func cancelRequest(callId: Int) {
        AlertManager.shared.showProgressDialog(from: self)
        RestClient.shared.cancelRequest(callId: callId) { (resp) in
            AlertManager.shared.hideProgress()
            if let call = self.dataCall, call.paid > 0 {
                AlertManager.shared.showAlert(from: self, title: "Внимание", message: "Заявка успешно отменена. В течение 60 минут средства будут возвращены на баланс Вашего лицевого счета. Ожидайте уведомления.") { (_) in
                    self.loadDataFromServer(self.refreshControl)
                }
            } else {
                self.loadDataFromServer(self.refreshControl)
            }
        } errorCall: { (error) in
            AlertManager.shared.hideProgress()
            Utils.shared.showToastHttp(from: self.view, with: "Не удалось отменить заявку.", error: error)
            self.updateTableView()
        }
    }
    
    func payTouch() {
        let res = Utils.shared.openUrl(url: self.params!.linkToPay!)
        if(!res) {
            Utils.shared.showToast(from: self, with: "Невалидная ссылка для оплаты")
        }
    }
}
