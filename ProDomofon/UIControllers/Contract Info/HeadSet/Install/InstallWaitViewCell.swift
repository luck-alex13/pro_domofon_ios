//
//  InstallWaitViewCell.swift
//  ProDomofon
//
//  Created by Алексей Агильдин on 10.03.2021.
//  Copyright © 2021 Александр Новиков. All rights reserved.
//

import UIKit

class InstallWaitViewCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel! {
        didSet {
            titleLabel.text = "Заявка оплачена."
        }
    }
    
    @IBOutlet weak var mainTextLabel: UILabel! {
        didSet {
            mainTextLabel.text = "В течение 5ти РАБОЧИХ дней Вам перезвонит мастер для согласования даты/времени установки (подключения ранее установленной) трубки."
        }
    }
    
    @IBOutlet weak var bottomTitleLabel: UILabel! {
        didSet {
            bottomTitleLabel.text = "Ожидайте звонка."
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
