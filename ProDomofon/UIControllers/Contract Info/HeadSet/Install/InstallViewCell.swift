//
//  InstallViewCell.swift
//  ProDomofon
//
//  Created by Алексей Агильдин on 18.02.2021.
//  Copyright © 2021 Александр Новиков. All rights reserved.
//

import UIKit

protocol InstallViewCellDelegate: class {
    func createRequest(contractId: String, deviceId: Int, name: String)
    func cancelRequest(callId: Int)
    func payTouch()
}

class InstallViewCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var installImage: UIImageView!
    @IBOutlet weak var infoLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var orderButton: UIButton!
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var payButton: UIButton!
    
    weak var delegate: InstallViewCellDelegate?
    
    var contractID: String?
    
    fileprivate var data: Install!
    fileprivate var item: InstallContract? {
        if let it = data.contracts.first(where: { (ic) -> Bool in
            return ic.contractId == contractID && ic.contractId != nil
        }) {
            return it
        } else {
            return nil
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        orderButton.setCornerRadius(radius: 6)
        cancelButton.setCornerRadius(radius: 6)
        payButton.setCornerRadius(radius: 6)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setupCell(with itemData: Install) {
        data = itemData
        
        titleLabel.text = itemData.name
        if let allowedUrlString = itemData.image.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed), let url = URL(string: allowedUrlString) {
            installImage.af_setImage(withURL: url)
        }
        infoLabel.text = itemData.desc
        priceLabel.text = "\(itemData.amount) ₽"
        
        orderButton.showObject(0)
        cancelButton.hideObject(0)
        payButton.hideObject(0)
        
        if let it = item {
            orderButton.isEnabled = it.enabled
            if it.enabled {
                orderButton.backgroundColor = Colors.RED_ACCENT
                var titleButton = "ЗАКАЗАТЬ"
                if let call = it.call, itemData.id == call.deviceId {
                    if call.paid > 0 {
                        titleButton = "ОТМЕНИТЬ"
                        
                        if !acceessToCancel(call: call) {
                            titleButton += " (доступно через 10 минут)"
                            orderButton.isEnabled = false
                        } else {
                            orderButton.isEnabled = true
                        }
                    } else {
                        orderButton.hideObject(0)
                        cancelButton.showObject(0)
                        payButton.showObject(0)
                    }
                }
                orderButton.setTitle(titleButton, for: .normal)
            } else {
                orderButton.backgroundColor = .gray
            }
        }
    }
    
    fileprivate func acceessToCancel(call: InstallCall) -> Bool {
        if let d = DateHelper(locale: Locale.current).parseDate(from: call.created) {
            
            let minuteUnit = Set<Calendar.Component>([.minute])
            let difference = NSCalendar.current.dateComponents(minuteUnit, from: d, to: Date())
            
            return difference.minute != nil ? difference.minute! > 10 : true
        } else {
            return true
        }
    }

    
    @IBAction func orderTouchUpInside() {
        if let it = item, let call = it.call {
            if it.enabled && data.id == call.deviceId && call.callId > 0 {
                if acceessToCancel(call: call) {
                    cancelCall(callId: call.callId)
                }
            }
        } else {
            guard let cId = contractID else { return }
            delegate?.createRequest(contractId: cId, deviceId: data.id, name: data.name ?? "")
            orderButton.isEnabled = false
            orderButton.backgroundColor = .gray
            DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                self.orderButton.backgroundColor = Colors.RED_ACCENT
                self.orderButton.isEnabled = true
            }
        }
    }
    
    @IBAction func cancelTouchUpInside() {
        if let it = item, let call = it.call {
            if it.enabled && data.id == call.deviceId && call.callId > 0 {
                cancelCall(callId: call.callId)
            }
        }
    }
    
    @IBAction func payTouchUpInside() {
        delegate?.payTouch()
        payButton.isEnabled = false
        payButton.backgroundColor = .gray
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
            self.payButton.backgroundColor = Colors.GREEN_PRIMARY
            self.payButton.isEnabled = true
        }
    }
    
    fileprivate func cancelCall(callId: Int){
        delegate?.cancelRequest(callId: callId)
        orderButton.isEnabled = false
        orderButton.backgroundColor = .gray
        cancelButton.isEnabled = false
        cancelButton.backgroundColor = .gray
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
            self.orderButton.backgroundColor = Colors.RED_ACCENT
            self.orderButton.isEnabled = true
            self.cancelButton.backgroundColor = Colors.RED_ACCENT
            self.cancelButton.isEnabled = true
        }
    }
}
