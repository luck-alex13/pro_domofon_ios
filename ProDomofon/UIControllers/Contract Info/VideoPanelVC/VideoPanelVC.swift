//
//  VideoPanelVC.swift
//  ProDomofon
//
//  Created by Lexus on 09.11.2022.
//  Copyright © 2022 Алексей Агильдин. All rights reserved.
//

import UIKit

class VideoPanelVC: SegmentedTabVC {

    @IBOutlet weak var tabControl: UISegmentedControl!
    @IBOutlet weak var contentView: UIView!
    
    fileprivate var scheduleEditView: ScheduleEditView!
    
    fileprivate var scheduleVC: ScheduleVC!
    
    var params: InfoParams? {
        if let c = DataStorage.shared.choosenContract {
            return InfoParams(
                contractID: c.id,
                contractNumber: c.number,
                flatNumber: c.flat,
                linkToPay: c.getLinkToPay(baseUrl: ApiRouter.getUser.getBaseUrl()),
                address: c.getReadableAddress
            )
        } else {
            return nil
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setBar(title: "Видео-панель")
        
        setUpPageViewController(contentView: contentView, storyboard: Utils.shared.additionalStoryboard())
        
        scheduleEditView = ScheduleEditView(frame: CGRect(x: 0, y: 0, width: view.frame.width, height: view.frame.height))
        scheduleEditView.delegate = self
        view.addSubview(scheduleEditView)
        scheduleEditView.hideObject(0)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(false, animated: false)
    }
    
    override func initPages() {
        tabsVCList = [UIViewController]()
        tabsVCList?.append(self.instantinatePage(atIndex: 0)!)
        tabsVCList?.append(self.instantinatePage(atIndex: 1)!)
    }
    
    override func initTabTitles() {
        tabControl.setTitle("Информация", forSegmentAt: 0)
        tabControl.setTitle("Расписание", forSegmentAt: 1)
    }
    
    override func instantinatePage(atIndex index: Int) -> UIViewController? {
        guard index >= 0 && index < 2 else { return nil}
        if index == 0 {
            let infoVC = Utils.shared.additionalStoryboard().instantiateViewController(withIdentifier: InfoVideoPanelVC.className) as! InfoVideoPanelVC
            infoVC.setTabIndex(index: index)
            infoVC.delegate = self
            return infoVC
        }else if index == 1 {
            scheduleVC = Utils.shared.additionalStoryboard().instantiateViewController(withIdentifier: ScheduleVC.className) as? ScheduleVC
            scheduleVC.setTabIndex(index: index)
            scheduleVC.delegate = self
            scheduleVC.params = self.params
            return scheduleVC
        } else {
            return nil
        }
    }
    
    @IBAction func segmentSelected(_ sender: UISegmentedControl) {
        super.tabSelected(sender)
    }
    
    override func changeSelectedSegmentIndex(index: Int) {
        tabControl.selectedSegmentIndex = index
    }

    func setVideoPanel(contract: String, code: String) {
        guard contract != "", code != "" else { return }
        
        RestClient.shared.contractConfirm(contractId: contract, code: code) {
            Utils.shared.showToast(from: self.view, with: "Видео-панель успешно подключена к договору")
        } errorCall: { error in
            Utils.shared.showToastHttp(from: self.view, with: "Не удалось подключить видео-панель к договору. Попробуйте позже.", error: error)
        }
    }
    
    fileprivate func parseCode(link: String) -> String {
        let ar = link.split(separator: "/")
        if let str = ar.last {
            return String(str)
        }
        return ""
    }
}
extension VideoPanelVC: ScheduleVCDelegate {
    func showScheduleEditView(data: Schedule) {
        scheduleEditView.set(data: data)
        scheduleEditView.showObject()
    }
}
extension VideoPanelVC: ScheduleEditViewDelegate {
    func saveTouch(data: Schedule){
        if data.dow.count > 0 {
            if data.id == nil {
                scheduleVC.newSchedule(schedule: data)
            } else {
                scheduleVC.editSchedule(schedule: data)
            }
            scheduleEditView.hideObject()
        } else {
            AlertManager.shared.showAlert(from: self, title: "Ошибка", message: "Выберите дни недели прежде, чем сохранить расписание", handler: nil)
        }
    }
}
extension VideoPanelVC: InfoVideoPanelVCDelegate {
    func connectTouched() {
        if let nvc = Utils.shared.additionalStoryboard().instantiateViewController(withIdentifier: ScanQrCodeVC.className) as? ScanQrCodeVC {
            nvc.scanDelegate = self
            self.navigationController?.setNavigationBarHidden(true, animated: false)
            self.navigationController?.pushViewController(nvc, animated: true)
        }
    }
}
extension VideoPanelVC: ScanQrCodeVCDelegate {
    func found(link: String) {
        let code = parseCode(link: link)
        
        let contracts = DataStorage.shared.contracts
        let c = DataStorage.shared.choosenContract
//        if contracts.count == 1 || !(c?.isOwner ?? true) {
        if contracts.count == 1 {
            setVideoPanel(contract: c?.id ?? "", code: code)
        } else {
            if let nextViewController = Utils.shared.additionalStoryboard().instantiateViewController(withIdentifier: SelectContractVC.className) as? SelectContractVC {
                if let curContract = contracts.first(where: { $0.id == params?.contractID }) {
                    nextViewController.selectedIDs = [InfoParams(contractID: curContract.id, contractNumber: curContract.number)]
                }
                nextViewController.goal = .videoPanel
                nextViewController.code = code
                nextViewController.delegate = self
                self.navigationController?.setNavigationBarHidden(false, animated: false)
                self.navigationController?.pushViewController(nextViewController, animated: true)
            }
        }
    }
}
extension VideoPanelVC: SelectContractVCDelegate {
    func selectContract(contractId: String, code: String) {
        setVideoPanel(contract: contractId, code: code)
    }
}
