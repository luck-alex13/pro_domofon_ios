//
//  InfoVideoPanelVC.swift
//  ProDomofon
//
//  Created by Lexus on 09.11.2022.
//  Copyright © 2022 Алексей Агильдин. All rights reserved.
//

import UIKit
import WebKit

protocol InfoVideoPanelVCDelegate: class {
    func connectTouched()
}

class InfoVideoPanelVC: TabbedVC {

    @IBOutlet weak var webView: WKWebView!
    @IBOutlet weak var connectButton: UIButton!
    
    let targetUrl = StringConst.VIDEO_PL.rawValue
    
    weak var delegate: InfoVideoPanelVCDelegate?
    
    private var isInjected: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        connectButton.setCornerRadius(radius: 5)
        
//        webView.navigationDelegate = self
//        webView.uiDelegate = self
        
        let url = URL(string: targetUrl)!
        webView.navigationDelegate = self
        webView.load(URLRequest(url: url))
    }
    
    @IBAction func connectButtonTouchUpInside() {
        delegate?.connectTouched()
    }
}
extension InfoVideoPanelVC: WKNavigationDelegate {
    func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
        if navigationAction.navigationType == .linkActivated, let url = navigationAction.request.url {
            print(url)
            if url.absoluteString.contains(StringConst.APP_LINK.rawValue) {
                let arr = url.absoluteString.components(separatedBy: StringConst.APP_LINK.rawValue)
                if arr.count == 2 {
                    LinkManager.shared.parseAndOpen(link: arr[1])
                }
                decisionHandler(.allow)
                return
            } else if UIApplication.shared.canOpenURL(url) {
                UIApplication.shared.open(url)
                decisionHandler(.cancel)
                return
            }
        } else {
            print("not a user click")
            decisionHandler(.allow)
            return
        }
    }
}
