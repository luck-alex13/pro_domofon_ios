//
//  ContractInfoMainVC.swift
//  ProDomofon
//
//  Created by Александр Новиков on 16/10/2019.
//  Copyright © 2019 Александр Новиков. All rights reserved.
//

import UIKit
import RxSwift

class ContractInfoMainVC: SegmentedTabVC {

    @IBOutlet weak var tabControl: UISegmentedControl!
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var payButton: UIButton!
    
    var params: InfoParams? {
        if let c = DataStorage.shared.choosenContract {
            return InfoParams(
                contractID: c.id,
                contractNumber: c.number,
                flatNumber: c.flat,
                linkToPay: c.getLinkToPay(baseUrl: ApiRouter.getUser.getBaseUrl()),
                address: c.getReadableAddress
            )
        } else {
            return nil
        }
    }
    
    var aliasesVm = AliasesVM()
    let disposeBag = DisposeBag()
    
    var openCosts: Bool?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setBar(title: "Начисления", rightTitle: "Действия", rightSelector: #selector(settingsTapped))
        
        setUpPageViewController(contentView: contentView, storyboard: Utils.shared.additionalStoryboard())
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if let nav = navigationController as? MainNavigationVC {
            nav.tryAuth()
        }
        if let _ = openCosts {
            changeSelectedSegmentIndex(index: 1)
            pageSelected(index: 1)
            openCosts = nil
        }
    }
    
    override func initPages() {
        tabsVCList = [UIViewController]()
        tabsVCList?.append(self.instantinatePage(atIndex: 0)!)
        tabsVCList?.append(self.instantinatePage(atIndex: 1)!)
    }
    
    override func initTabTitles() {
        tabControl.setTitle("Начисления", forSegmentAt: 0)
        tabControl.setTitle("Условия", forSegmentAt: 1)
    }
    
    override func instantinatePage(atIndex index: Int) -> UIViewController? {
        guard index >= 0 && index < 2 else { return nil}
        if index == 0 {
            let listVC = Utils.shared.additionalStoryboard().instantiateViewController(withIdentifier: CalculationsVC.className) as! CalculationsVC
            listVC.setTabIndex(index: index)
            listVC.contractID = params?.contractID
            listVC.linkToPay = params?.linkToPay
            return listVC
        }else if index == 1 {
            let listVC = Utils.shared.additionalStoryboard().instantiateViewController(withIdentifier: CostsVC.className) as! CostsVC
            listVC.setTabIndex(index: index)
            listVC.contractID = params?.contractID
            return listVC
        } else {
            return nil
        }
    }
    
    
    @IBAction func segmentSelected(_ sender: UISegmentedControl) {
        super.tabSelected(sender)
    }
    
    override func changeSelectedSegmentIndex(index: Int) {
        super.changeSelectedSegmentIndex(index: index)
        tabControl.selectedSegmentIndex = index
    }
    
    @IBAction func payTouch() {
        if let p = params, let url = p.linkToPay {
            let res = Utils.shared.openUrl(url: url)
            if(!res) {
                Utils.shared.showToast(from: self, with: "Невалидная ссылка для оплаты")
            }
        }
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
    }
    
    @objc
    func settingsTapped() {
        let alert = UIAlertController(title: nil, message: "Выберите действие", preferredStyle: .actionSheet)
        
        
//        alert.addAction(UIAlertAction(title: "Ключи", style: .default, handler: { [weak self] (_) in
//            if let nextViewController = Utils.shared.mainStoryboard().instantiateViewController(withIdentifier: KeysVC.className) as? KeysVC {
//                nextViewController.contractID = self?.params?.contractID
//                self?.navigationController?.pushViewController(nextViewController, animated: true)
//            }
//        }))
//
//        alert.addAction(UIAlertAction(title: "Телефоны", style: .default, handler: { [weak self] (_) in
//            if let nextViewController = Utils.shared.mainStoryboard().instantiateViewController(withIdentifier: PhonesVC.className) as? PhonesVC {
//                nextViewController.contractID = self?.params?.contractID
//                self?.navigationController?.pushViewController(nextViewController, animated: true)
//            }
//        }))
        
        alert.addAction(UIAlertAction(title: "Скачать договор", style: .default, handler: { (_) in
            self.downloadTapped()
        }))
        
        alert.addAction(UIAlertAction(title: "Сменить номер вызова", style: .default, handler: { (_) in
            self.changeCallNumber()
        }))
        
        alert.addAction(UIAlertAction(title: "Вернуть исходный номер вызова", style: .destructive, handler: { (_) in
            self.resetCallNumber()
        }))
        
        alert.addAction(UIAlertAction(title: "Закрыть", style: .cancel, handler: nil))
        
        self.present(alert, animated: true, completion: {
            print("completion block")
        })
    }
    
    func changeCallNumber() {
        guard let contractId = params?.contractID else {
            AlertManager.shared.showAlert(from: self, title: "Ошибка", message: "Не указан ID договора")
            return
        }
        
        aliasesVm.rangesList
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: { [weak self] (ranges) in
                AlertManager.shared.hideProgress(completion: {
                    if let myAlert =  Utils.shared.additionalStoryboard().instantiateViewController(withIdentifier: AliasSelectionVC.className) as? AliasSelectionVC {
                        myAlert.contractId = self?.params?.contractID
                        myAlert.ranges = ranges
                        myAlert.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
                        myAlert.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
                        self?.navigationController?.present(myAlert, animated: true, completion: nil)
                    }
                })
            }, onError: { [weak self] (error) in
                    if let serverErr = error as? ServerError {
                        AlertManager.shared.hideProgress()
                        Utils.shared.showToastHttp(from: self?.view, with: "Не удалось получить алиасы для договора.", error: serverErr.errorResponse)
                    }
                    if error is NoInternetError {
                        AlertManager.shared.showAlertNoInternet(from: self)
                    }
                    
                    
            })
            .disposed(by: disposeBag)
        
        AlertManager.shared.showProgressDialog(from: self)
        aliasesVm.requestAvailableRanges(contractId: contractId)        
    }
    
    func resetCallNumber() {
        guard let contractId = params?.contractID else {
            AlertManager.shared.showAlert(from: self, title: "Ошибка", message: "Не указан ID договора")
            return
        }
        guard let flat = params?.flatNumber, let strFlat = Int(flat) else {
            AlertManager.shared.showAlert(from: self, title: "Ошибка", message: "Не указан номер квартиры")
            return
        }
        
        aliasesVm.contractSubject
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: { [weak self] (resp) in
                AlertManager.shared.hideProgress(completion: {
                    AlertManager.shared.showAlert(from: self, title: "Номера вызова изменен", message: "Вы успешно сбросили номер вызова квартиры. Вызов по номеру квартиры будет работать в обычном режиме. Изменения вступят в силу в течение 40 минут!") { (act) in
                    }
                })
            }, onError: { [weak self] (error) in
                    if let serverErr = error as? ServerError {
                        AlertManager.shared.hideProgress()
                        Utils.shared.showToastHttp(from: self?.view, with: "Не удалось сбросить номер вызова.", error: serverErr.errorResponse)
                    }
                    if error is NoInternetError {
                        AlertManager.shared.showAlertNoInternet(from: self)
                    }
                    
            })
            .disposed(by: disposeBag)
        
        AlertManager.shared.showProgressDialog(from: self)
        aliasesVm.changeAlias(contractId: contractId, with: strFlat)
    }
    
    func downloadTapped() {
        if let id = params?.contractID {
            AlertManager.shared.showProgressDialog(from: self)
            RestClient.shared.requestTemporaryToken(for: id, success: { [weak self] (tokenObj) in
                AlertManager.shared.hideProgress()
                if let uid = tokenObj.uid {
                    var link = ApiRouter.getUser.baseApiUrl()
                    link += "/contracts/export-by-token/\(uid)"
                    if (!Utils.shared.openUrl(url: link)) {
                        Utils.shared.showToast(from: self, with: "Невалидная ссылка для доступа к договору.")
                    }
                }else {
                    Utils.shared.showToast(from: self, with: "Ошибка! Временный токен не получен.")
                }
                
            }) { [weak self] (err) in
                AlertManager.shared.hideProgress()
                Utils.shared.showToastHttp(from: self?.view, with: "Не удалось получить доступ к договору.", error: err)
            }
            
        }
    }
    
}


