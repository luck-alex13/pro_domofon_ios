//
//  CalculationsVC.swift
//  ProDomofon
//
//  Created by Александр Новиков on 16/10/2019.
//  Copyright © 2019 Александр Новиков. All rights reserved.
//

import UIKit

class CalculationsVC: VCWithTable<Calculation> {
    
    @IBOutlet weak var dataTableView: UITableView!
    
    var contractID: String?
    var linkToPay: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()        
        defineVariables(sections: 1, cellIdentifer: CalculationViewCell.className, placeholderTitle: "Здесь будут отображаться ваши начисления")
        
        self.tableView = dataTableView
        self.tableView?.estimatedRowHeight = 80
        self.tableView?.rowHeight = UITableView.automaticDimension
        setUpRefreshControl()
    }
    
    override func readDataFromDatabase() {
        if let contractID = contractID {
            dataArray = DataStorage.shared.calculations.filter({ $0.contractId == contractID })
        } else {
            dataArray = DataStorage.shared.calculations
        }
        self.updateTableView()
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifer!, for: indexPath) as! CalculationViewCell
        cell.setupCell(with: dataArray[indexPath.row])
        return cell
    }

    override func loadDataFromServer(_ refreshControl: UIRefreshControl?) {
        refreshControl?.beginRefreshing()
        if let contractID = contractID {
            RestClient.shared.getUserAndContracts(success: { [weak self] (user) in
                guard let _ = self else { return }
                DataStorage.shared.contracts = user.contracts
                
                RestClient.shared.getCalculations(for: contractID, success: { [weak self] (respData) in
                    guard let _ = self else { return }
                    refreshControl?.endRefreshing()
                    DataStorage.shared.calculations = respData.calculations
                }) { [weak self]  (err) in
                    refreshControl?.endRefreshing()
                    Utils.shared.showToastHttp(from: self?.view, with: "Не удалось загрузить начисления.", error: err)
                }
                
            }) { [weak self]  (err) in
                refreshControl?.endRefreshing()
                Utils.shared.showToastHttp(from: self?.navigationController, with: "Не удалось загрузить договоры.", error: err)
            }
        }
    }
    @IBAction func footerButtonTapped(_ sender: Any) {
        if let link = linkToPay {
            if (!Utils.shared.openUrl(url: link)) {
                Utils.shared.showToast(from: self, with: "Невалидная ссылка для оплаты")
            }
        }else {
            NSLog("linkToPay is nil")
        }
        
    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40
    }
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.width, height: 40))
        view.backgroundColor = .white
        
        let label = UILabel(frame: CGRect(x: 16, y: 0, width: view.frame.width - 32, height: view.frame.height))
        label.backgroundColor = .clear
        label.font = UIFont.systemFont(ofSize: 20, weight: .medium)
        label.textColor = UIColor.init(rgb: 0x25A679)
        label.textAlignment = .right
        if let contract = DataStorage.shared.choosenContract {
            label.text = "Баланс \(contract.balance ?? 0) Р"
        }
        view.addSubview(label)
        
        return view
    }
}
