//
//  CalculationViewCell.swift
//  ProDomofon
//
//  Created by Александр Новиков on 16/10/2019.
//  Copyright © 2019 Александр Новиков. All rights reserved.
//

import UIKit

class CalculationViewCell: UITableViewCell {

    @IBOutlet weak var infoLabel: UILabel!
    @IBOutlet weak var amountLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setupCell(with itemData: Calculation) {
        infoLabel.text = itemData.comment
        
        var amount = String(Int(itemData.amount))
        if(itemData.amount > 0) {
            amount = "+\(amount)"
        }
        amountLabel.text = "\(amount) ₽"
        amountLabel.textColor = getAmountColor(from: itemData)
        
        let dateHelper = DateHelper(locale: Locale.current)
        if let date = dateHelper.parseDate(from: itemData.dtime) {
            dateLabel.text = "\(dateHelper.getOnlyDate(from: date.timeIntervalSince1970)) \(dateHelper.getOnlyTime(from: date.timeIntervalSince1970))"
        }
    }
    
    func getAmountColor(from itemData: Calculation) -> UIColor {
        return itemData.amount < 0 ? Colors.RED_ACCENT : Colors.GREEN
    }

}
