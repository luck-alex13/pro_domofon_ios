//
//  PayVC.swift
//  ProDomofon
//
//  Created by Александр Новиков on 25.10.2020.
//  Copyright © 2020 Александр Новиков. All rights reserved.
//

import UIKit
import WebKit

class PayVC: UIViewController {
    
    var targetUrl: String {
        if let c = DataStorage.shared.choosenContract {
            return c.getLinkToPay(baseUrl: ApiRouter.getUser.getBaseUrl())
        } else {
            return ""
        }
    }
    
    @IBOutlet weak var paymentButton: UIButton!
    @IBOutlet weak var equiringLabel: UILabel!
    @IBOutlet weak var equiringSpeed: UILabel!
    @IBOutlet weak var sberLable: UILabel!
    @IBOutlet weak var sberSpeed: UILabel!
    @IBOutlet weak var intervalLable: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setBar(title: "Оплата")
        
        paymentButton.setCornerRadius(radius: 8)
        
        equiringLabel.makeTextAttributed(targetWords: "Интернет эквайринг", color: Colors.GREEN_PRIMARY)
        equiringSpeed.makeTextAttributed(targetWords: "в течение 1 минуты", color: Colors.GREEN_PRIMARY)
        sberLable.makeTextAttributed(targetWords: "СбербанкОнлайн", color: Colors.GREEN_PRIMARY)
        sberSpeed.makeTextAttributed(targetWords: "3 РАБОЧИХ дня", color: Colors.RED_ACCENT)
        intervalLable.makeTextAttributed(targetWords: "в течение 30 минут", color: Colors.GREEN_PRIMARY)
        //eqMutableStr.addAttributes([NSAttributedString.Key.font: equiringLabel.font], range: NSRange(range!, in: text))
    
    }
    @IBAction func toPaymentTapped(_ sender: Any) {
        let res = Utils.shared.openUrl(url: targetUrl)
        if(!res) {
            Utils.shared.showToast(from: self, with: "Невалидная ссылка для оплаты")
        }
        
    }
    
}
extension UILabel {
    
    func makeTextAttributed(targetWords: String, color: UIColor? = nil) {
        guard let text = self.text else {
            return
        }
        var attrStr: NSMutableAttributedString
        
        if let attrs = attributedText {
            attrStr = NSMutableAttributedString(attributedString: attrs)
        }else {
            attrStr = NSMutableAttributedString(string: text, attributes: [NSAttributedString.Key.font: font])
        }
        
        guard let range = text.range(of: targetWords, options: .caseInsensitive) else {
            return
        }
        
        let nsRange = NSRange(range, in: text)
        
        if let color = color {
            attrStr.addAttribute(NSAttributedString.Key.foregroundColor, value: color, range: nsRange)
        }
        
        attrStr.addAttribute(NSAttributedString.Key.font, value: UIFont.systemFont(ofSize: 15, weight: .bold), range: nsRange)
        // возможно перезапишет атрибуты если использовать более одного раза
        attributedText = attrStr
               
    }
    
}
