//
//  CostViewCell.swift
//  ProDomofon
//
//  Created by Александр Новиков on 19/10/2019.
//  Copyright © 2019 Александр Новиков. All rights reserved.
//

import UIKit

class CostViewCell: UITableViewCell {
    
    @IBOutlet weak var infoLabel: UILabel!
    @IBOutlet weak var amountLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func setupCell(with itemData: SingleService) {
        infoLabel.text = itemData.typeName
        amountLabel.text = "\(itemData.amount ?? 0) ₽"
    }
    
    func setupCell(with itemData: RegularService) {
        infoLabel.text = itemData.name
        amountLabel.text = "\(itemData.amount ?? 0) ₽"
    }

}
