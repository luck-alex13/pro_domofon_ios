//
//  OptionalServiceViewCell.swift
//  ProDomofon
//
//  Created by Александр Новиков on 21/10/2019.
//  Copyright © 2019 Александр Новиков. All rights reserved.
//

import UIKit

class OptionalServiceViewCell: UITableViewCell {

    @IBOutlet weak var infoLabel: UILabel!
    @IBOutlet weak var amountLabel: UILabel!
    @IBOutlet weak var serviceSwitch: UISwitch!
    
    
    weak var switchListener: SwithChangedListener?
    var itemData: RegularService?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setupCell(with itemData: RegularService) {
        self.itemData = itemData
        infoLabel.text = itemData.name
        infoLabel.textAlignment = .left
        infoLabel.sizeToFit()
        
        amountLabel.text = "\(itemData.amount ?? 0) ₽"
        
        serviceSwitch.isOn = itemData.activated ?? false && itemData.removeAfter == nil
    }
    @IBAction func optionSwitched(_ sender: UISwitch) {
        if let data = itemData {
            switchListener?.onChanged(value: sender.isOn, itemData: data)
        }
    }
    
}

protocol SwithChangedListener: class {
    func onChanged(value: Bool, itemData: RegularService)
}
