//
//  CostsVC.swift
//  ProDomofon
//
//  Created by Александр Новиков on 17/10/2019.
//  Copyright © 2019 Александр Новиков. All rights reserved.
//

import UIKit

class CostsVC: SimpleVCWithData, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet weak var dataTableView: UITableView!
    var refreshControl : UIRefreshControl?
    
    var contractID: String?    
    var sectionsList: [DetailItemType] = [.services, .single]
    
    var single: [SingleService] = []
    var regular: [RegularService] = []
    
    var сonnectionObserver: ConnectionObserver?
    
    enum DetailItemType: Int {
        case services = 0
        case single
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        self.dataTableView.estimatedRowHeight = 60
        self.dataTableView.rowHeight = UITableView.automaticDimension
        self.dataTableView.tableFooterView = UIView(frame: CGRect.zero)
        self.dataTableView.register(OptionalServiceViewCell.nib, forCellReuseIdentifier: OptionalServiceViewCell.className)
        triggerBackgroundView()
        setUpRefreshControl()
    }
    
    override func setUpRefreshControl() {
        refreshControl = UIRefreshControl()
        refreshControl!.addTarget(self, action: #selector(refreshControlTraget), for: UIControl.Event.valueChanged)
        refreshControl!.tintColor = Colors.RED_ACCENT
        
        self.dataTableView?.addSubview(self.refreshControl!)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        NotificationCenter.default.addObserver(self, selector: #selector(updateData), name: .updateData, object: nil)
        setUpNetworkObserving()
    }
    
    @objc
    func updateData(){
        readDataFromDatabase()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        //notification?.invalidate()
        NotificationCenter.default.removeObserver(self, name: .updateData, object: nil)
        сonnectionObserver?.endListening()
    }
    
    func setUpNetworkObserving(){
        сonnectionObserver = ConnectionObserver()
        сonnectionObserver!.observeOnConnection(reachable: { [weak self] in
            self?.loadDataFromServer(self?.refreshControl)
            }, notReachable: { [weak self] in
                
        })
    }

    @IBAction func footerButtonTapped(_ sender: Any) {
        guard let connected = self.сonnectionObserver?.isConnectedToInternet, connected else {
            AlertManager.shared.showAlertNoInternet(from: self)
            return
        }
        if let id = contractID {
            AlertManager.shared.showProgressDialog(from: self)
            RestClient.shared.requestTemporaryToken(for: id, success: { [weak self] (tokenObj) in
                AlertManager.shared.hideProgress()
                if let uid = tokenObj.uid {
                    var link = ApiRouter.getUser.baseApiUrl()
                    link += "/contracts/export-by-token/\(uid)"
                    if (!Utils.shared.openUrl(url: link)) {
                        Utils.shared.showToast(from: self, with: "Невалидная ссылка для доступа к договору.")
                    }
                }else {
                    Utils.shared.showToast(from: self, with: "Ошибка! Временный токен не получен.")
                }
                
            }) { [weak self] (err) in
                AlertManager.shared.hideProgress()
                Utils.shared.showToastHttp(from: self?.view, with: "Не удалось получить доступ к договору.", error: err)
            }
            
        }
    }
    
    @objc func refreshControlTraget(){
        loadDataFromServer(refreshControl)
    }
    
    override func loadDataFromServer(_ refreshControl: UIRefreshControl?) {
        refreshControl?.beginRefreshing()
        if let contractID = contractID {
            RestClient.shared.getContractInfo(for: contractID, success: { [weak self] (respData) in
                guard let self = self else { return }
                refreshControl?.endRefreshing()
                DataStorage.shared.singleServices = respData.single
                DataStorage.shared.regularServices = respData.regular
//                self?.updateTableView()
            }) { [weak self]  (err) in
                refreshControl?.endRefreshing()
                Utils.shared.showToastHttp(from: self?.view, with: "Не удалось загрузить договоры.", error: err)
            }
        }
        
    }
    
    override func readDataFromDatabase() {
        self.single = DataStorage.shared.singleServices
        self.regular = DataStorage.shared.regularServices
        self.updateTableView()
    }
    
    override func updateTableView() {
        dataTableView.reloadData()
        triggerBackgroundView()
    }
    
    // MARK: - Table view data source
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return sectionsList.count
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.width, height: 20))
        view.backgroundColor = .white

        let label = UILabel(frame: CGRect(x: 16, y: 0, width: view.frame.width - 32, height: view.frame.height))
        label.font = UIFont.boldSystemFont(ofSize: 12)
        label.textColor = .gray
        label.textAlignment = .left

        switch sectionsList[section]{
        case .single:
            label.text = single.count > 0 ? "Единовременно" : nil
        case .services:
            label.text = regular.count > 0 ? "Ежемесячно" : nil
        }

        view.addSubview(label)

        return view
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        switch sectionsList[section]{
        case .single:
            return single.count > 0 ? "Единовременно" : nil
        case .services:
            return regular.count > 0 ? "Ежемесячно" : nil
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let type = DetailItemType(rawValue: section){
            switch type{
            case .single:
                return single.count
            case .services:
                return regular.count
            }
        }else {
            return 0
        }
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        let type = sectionsList[indexPath.section]
        switch type{
        case .single:
            let cell = tableView.dequeueReusableCell(withIdentifier: CostViewCell.className, for: indexPath) as! CostViewCell
            cell.setupCell(with: single[indexPath.row])
            return cell
        case .services:
            let service = regular[indexPath.row]
            if let includable = service.includable, includable {
                let cell = tableView.dequeueReusableCell(withIdentifier: OptionalServiceViewCell.className, for: indexPath) as! OptionalServiceViewCell
                cell.setupCell(with: service)
                cell.switchListener = self
                return cell
            } else {
                let cell = tableView.dequeueReusableCell(withIdentifier: CostViewCell.className, for: indexPath) as! CostViewCell
                cell.setupCell(with: service)
                return cell
            }          
        }
    }
    
    func triggerBackgroundView() {
        if single.count == 0  && regular.count == 0{
            dataTableView.setBackgroundView(title: "Здесь будут отбражаться условия вашего договора", message: nil)
        } else {
            dataTableView.restoreBackgroundView()
        }
    }
    

}

extension CostsVC: SwithChangedListener {
    func onChanged(value: Bool, itemData: RegularService) {
        let action = value ? "включить" : "отключить"
        let btnAction = action.capitalizingFirstLetter()
        let message = "Вы действительно хотите \(action) услугу «\(itemData.name)»?"
        let dialog = AlertManager.shared.buildOkCancelAlert(title: "Внимание!", errorMessage: message, okText: btnAction, cancelText: "Отмена", okHandler: { [weak self] (act) in
            guard let self = self else { return }
            guard let connected = self.сonnectionObserver?.isConnectedToInternet, connected else {
                self.updateTableView()
                AlertManager.shared.showAlertNoInternet(from: self)
                return
            }
            
            if(value) {
                self.activateService(contractId: self.contractID!, serviceId: itemData.id, serviceName: itemData.name, groupId: itemData.groupId)
            }else {
                self.deactivateService(contractId: self.contractID!, serviceId: itemData.id, serviceName: itemData.name, groupId: itemData.groupId)
            }
            
        }) { [weak self] (act) in
            guard let self = self else { return }
            self.updateTableView()
        }
        
        present(dialog, animated: true, completion: nil)
    }
    
    func activateService(contractId: String, serviceId: String, serviceName: String, groupId: String?) {
        AlertManager.shared.showProgressDialog(from: self)
        RestClient.shared.activateService(for: contractId, serviceId: serviceId, groupId: groupId, activate: true, success: { [weak self] (resp) in
            AlertManager.shared.hideProgress()
            self?.regular = resp
            self?.updateTableView()
            Utils.shared.showToast(from: self?.view, with: "Услуга «\(serviceName)» включена.")
        }) { [weak self] (err) in
            self?.updateTableView()
            AlertManager.shared.hideProgress()
            Utils.shared.showToastHttp(from: self?.view, with: "Не удалось включить услугу.", error: err)
        }
    }
    
    func deactivateService(contractId: String, serviceId: String, serviceName: String, groupId: String?) {
        AlertManager.shared.showProgressDialog(from: self)
        RestClient.shared.activateService(for: contractId, serviceId: serviceId, groupId: groupId, activate: false, success: { [weak self] (resp) in
            AlertManager.shared.hideProgress()
            self?.regular = resp
            self?.updateTableView()
            Utils.shared.showToast(from: self?.view, with: "Услуга «\(serviceName)» отключена.")
        }) { [weak self] (err) in
            self?.updateTableView()
            AlertManager.shared.hideProgress()
            Utils.shared.showToastHttp(from: self?.view, with: "Не удалось отключить услугу.", error: err)
        }
    }
    
}
