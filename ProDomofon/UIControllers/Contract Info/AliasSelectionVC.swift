//
//  AliasSelectionVC.swift
//  ProDomofon
//
//  Created by Александр Новиков on 05/11/2019.
//  Copyright © 2019 Александр Новиков. All rights reserved.
//

import UIKit
import RxSwift

class AliasSelectionVC: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource {
    
    @IBOutlet weak var closeBtn: UIButton!
    @IBOutlet weak var changeBtn: UIButton!
    @IBOutlet weak var numberPickerView: UIPickerView!
    @IBOutlet weak var contentView: UIView!
    
    var pickerData: [Int] = [Int]()
    var selectedValue: Int?
    var contractId: String?
    var ranges: [Range]?
    
    var aliasesVm = AliasesVM()
    let disposeBag = DisposeBag()

    override func viewDidLoad() {
        super.viewDidLoad()
        contentView.setCornerRadius(radius: 10)
        
        ranges?.forEach({ (range) in
            for val in range.start!...range.end! {
                pickerData.append(val)
            }
        })
        
        // Connect data:
        self.numberPickerView.delegate = self
        self.numberPickerView.dataSource = self
    }
    
    
    @IBAction func closeTapped(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func changeTapped(_ sender: UIButton) {
        if let value = selectedValue {
            changeAlias(with: value)
        }else {
            changeAlias(with: pickerData[numberPickerView.selectedRow(inComponent: 0)])
        }
        
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return pickerData.count
    }

    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return String(pickerData[row])
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        selectedValue = pickerData[row]
    }
    
    func changeAlias(with newValue: Int) {
        guard ConnectionObserver().isConnectedToInternet else {
            AlertManager.shared.showAlertNoInternet(from: self)
            return
        }
        
        guard let contractId = contractId else {
            AlertManager.shared.showAlert(from: self, title: "Ошибка", message: "Не указан ID договора")
            return
        }
        
        AlertManager.shared.showProgressDialog(from: self)
        RestClient.shared.newAliasForContract(contractId: contractId, newAlias: newValue, success: { [weak self] (resp) in
            DataStorage.shared.updateAlias(contractId: contractId, alias: newValue)
            AlertManager.shared.hideProgress(completion: {
                self?.finishChanging(with: newValue)
            })
            
        }) { [weak self] (error) in
            AlertManager.shared.hideProgress()
            Utils.shared.showToastHttp(from: self?.view, with: "Не удалось изменить номера вызова.", error: error)
        }
        
    }
    
    func finishChanging(with newValue: Int) {
        AlertManager.shared.showAlert(from: self, title: "Номера вызова изменен", message: "Вы успешно изменили номер вызова квариры на \(newValue). Изменения вступят в силу в течение 40 минут. Вызов по номеру квартиры будет заблокирован!") { [weak self] (act) in
            self?.dismiss(animated: true, completion: nil)
        }
    }
}
