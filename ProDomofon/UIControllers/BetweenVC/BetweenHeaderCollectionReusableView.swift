//
//  BetweenHeaderCollectionReusableView.swift
//  ProDomofon
//
//  Created by Lexus on 07.02.2023.
//  Copyright © 2023 Алексей Агильдин. All rights reserved.
//

import UIKit

class BetweenHeaderCollectionReusableView: UICollectionReusableView {
        
    static var identifier = "BetweenHeaderCollectionReusableView"
    
    func setupHeader(address: String) {
        subviews.forEach({ $0.removeFromSuperview() })
        
        let view = UIView(frame: CGRect(x: 0, y: 0, width: frame.width, height: 1))
        view.backgroundColor = .lightGray
        self.addSubview(view)
        
        let titleLabel = UILabel(frame: CGRect(x: 16, y: 0, width: frame.width - 32, height: frame.height))
        titleLabel.textAlignment = .left
        titleLabel.textColor = UIColor.gray
        titleLabel.font = UIFont.boldSystemFont(ofSize: 14)
        titleLabel.lineBreakMode = .byWordWrapping
        titleLabel.numberOfLines = 0
        titleLabel.text = address
        addSubview(titleLabel)
    }
}
