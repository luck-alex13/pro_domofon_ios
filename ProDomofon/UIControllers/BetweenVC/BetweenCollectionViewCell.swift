//
//  BetweenCollectionViewCell.swift
//  ProDomofon
//
//  Created by Lexus on 07.02.2023.
//  Copyright © 2023 Алексей Агильдин. All rights reserved.
//

import UIKit

protocol BetweenCollectionViewCellDelegate: AnyObject {
    func openDevice(number: Int, devicePanel: DevicePanel?)
}

class BetweenCollectionViewCell: UICollectionViewCell {
    
    fileprivate var titleLabel: UILabel!
    fileprivate var imageView: UIImageView!
    fileprivate var overControl: UIControl!
    
    var number: Int = 0
    var devicePanel: DevicePanel?
    
    static var identificator = "BetweenCollectionViewCell"
    
    weak var delegate: BetweenCollectionViewCellDelegate?
    
    fileprivate var isActive: Bool {
        return DataStorage.shared.activeBetweenCellContractNumber != nil &&
                DataStorage.shared.activeBetweenCellTypeId != nil &&
                DataStorage.shared.activeBetweenCellContractNumber == number &&
                DataStorage.shared.activeBetweenCellTypeId == devicePanel?.panel.type?.id
    }
    
    func setupCell(contract: Contract, devicePanel: DevicePanel? = nil) {
        contentView.subviews.forEach({ $0.removeFromSuperview() })
        
        contentView.backgroundColor = .clear
        contentView.setRoundedBorder(color: .lightGray, borderWidth: 1, radius: 5)
        
        self.number = contract.number
        self.devicePanel = devicePanel
        
        titleLabel = UILabel(frame: CGRect(x: 8, y: 5, width: contentView.frame.width - 18 - (contentView.frame.height - 10), height: contentView.frame.height - 10))
        titleLabel.textAlignment = .left
        titleLabel.textColor = .darkGray
        titleLabel.font = UIFont.systemFont(ofSize: 14)
        titleLabel.lineBreakMode = .byWordWrapping
        titleLabel.numberOfLines = 0
        titleLabel.text = devicePanel?.name ?? "Все двери"
        contentView.addSubview(titleLabel)
        
        imageView = UIImageView(frame: CGRect(x: contentView.frame.width - contentView.frame.height, y: 0, width: contentView.frame.height, height: contentView.frame.height))
        imageView.contentMode = .scaleAspectFit
        imageView.tintColor = .white
        imageView.backgroundColor = Colors.GREEN_PRIMARY
        if let type = devicePanel?.panel.type, let imageName = type.imageName, let image = UIImage(named: imageName) {
            imageView.image = image.withRenderingMode(.alwaysTemplate)
            if let color = type.unactiveColor?.parseColor {
                imageView.backgroundColor = color
            }
        } else if let image = UIImage(named: "gate")  {
            imageView.image = image.withRenderingMode(.alwaysTemplate)
        } else {
            imageView.image = nil
            imageView.backgroundColor = .clear
        }
        contentView.addSubview(imageView)
        
        overControl = UIControl(frame: CGRect(x: 0, y: 0, width: contentView.frame.width, height: contentView.frame.height))
        overControl.backgroundColor = .clear
        overControl.addTarget(self, action: #selector(sendRequestOpen), for: .touchUpInside)
        contentView.addSubview(overControl)
        
        if isActive {
            betweenCellChanged()
        }
        
        NotificationCenter.default.addObserver(self, selector: #selector(betweenCellChanged), name: .betweenCellChanged, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(betweenCellOpened), name: .betweenCellOpened, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(betweenCellClosed), name: .betweenCellClosed, object: nil)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    fileprivate func setOpened() {
        if let color = devicePanel?.panel.type?.activeColor?.parseColor {
            imageView.backgroundColor = color
        } else {
            imageView.backgroundColor = Colors.RED_ACCENT
        }
    }
    
    fileprivate func setClosed() {
        if let color = devicePanel?.panel.type?.unactiveColor?.parseColor {
            imageView.backgroundColor = color
        } else {
            imageView.backgroundColor = Colors.GREEN_PRIMARY
        }
    }
    
    @objc
    func betweenCellChanged() {
        if isActive {
            var activeColor = Colors.RED_ACCENT
            if let color = devicePanel?.panel.type?.activeColor?.parseColor {
                activeColor = color
            }
            contentView.setRoundedBorder(color: activeColor, borderWidth: 2, radius: 5)
        } else {
            contentView.setRoundedBorder(color: .lightGray, borderWidth: 1, radius: 5)
        }
    }
    
    @objc
    func betweenCellOpened() {
        guard isActive else { return }
        setOpened()
    }
    
    @objc
    func betweenCellClosed() {
        guard isActive else { return }
        setClosed()
    }
    
    @objc
    func sendRequestOpen() {
        NotificationCenter.default.post(name: .betweenCellClosed, object: nil)
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5, execute: {
            DataStorage.shared.activeBetweenCellContractNumber = self.number
            DataStorage.shared.activeBetweenCellTypeId = self.devicePanel?.panel.type?.id
            self.delegate?.openDevice(number: self.number, devicePanel: self.devicePanel)
        })
    }
    
}
