//
//  BetweenVC.swift
//  ProDomofon
//
//  Created by Lexus on 07.02.2023.
//  Copyright © 2023 Алексей Агильдин. All rights reserved.
//

import UIKit
import AudioToolbox

class BetweenVC: UIViewController {

    fileprivate var controlView: UIView!
    fileprivate var titleLabel: UILabel!
    fileprivate var beaconButton: UIButton!
    fileprivate var collectionView: UICollectionView!
    fileprivate var infoLabel: UILabel!
    fileprivate var payButton: UIButton!
    fileprivate var bottomView: UIView!
    fileprivate var checkBox: Checkbox!
    fileprivate var checkCloseAfterOpenLabel: UILabel!
    fileprivate var closeButton: UIButton!
    fileprivate var openAppButton: UIButton!
    
    fileprivate var width: CGFloat = 0
    fileprivate var height: CGFloat = 50
    
    fileprivate var contracts: [Contract] = []
    fileprivate var devicePanels: [DevicePanel] = []
    
    fileprivate var secondsAfterOpen = 0
    fileprivate var savedText = ""
    
    fileprivate var isOpening = false
    fileprivate var paymentUrl: String = ""
    
    fileprivate var dataOpening: [String: Double] = [:]
    
    override func viewDidLoad() {
        super.viewDidLoad()

        view.backgroundColor = Colors.GREEN_PRIMARY
        
        controlView = UIView(frame: CGRect(x: 16, y: view.frame.height / 2 - 500 / 2, width: view.frame.width - 32, height: 550))
        controlView.backgroundColor = Colors.WHITE
        controlView.setCornerRadius(radius: 5)
        view.addSubview(controlView)
        
        width = (controlView.frame.width - 32 - 16) / 2
        
        titleLabel = UILabel(frame: CGRect(x: 0, y: 0, width: controlView.frame.width, height: 40))
        titleLabel.textAlignment = .center
        titleLabel.textColor = UIColor.darkGray
        titleLabel.font = UIFont.boldSystemFont(ofSize: 17)
        titleLabel.text = "Управление домофоном"
        controlView.addSubview(titleLabel)
        
        if #available(iOS 13.0, *) {
            beaconButton = UIButton(frame: CGRect(x: controlView.frame.width - 40, y: 0, width: 40, height: 40))
            beaconButton.setImage(UIImage(named: "beacon")?.withRenderingMode(.alwaysTemplate), for: .normal)
            beaconButton.addTarget(self, action: #selector(beaconTouchUpInside), for: .touchUpInside)
            beaconButton.tintColor = .lightGray
            beaconButton.imageEdgeInsets = UIEdgeInsets(top: 5, left: 5, bottom: 5, right: 5)
            controlView.addSubview(beaconButton)
        }
        
        
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.sectionInset = UIEdgeInsets(top: 10, left: 16, bottom: 10, right: 16)
        layout.minimumLineSpacing = 10
        layout.minimumInteritemSpacing = 0
        
        collectionView = UICollectionView(frame: CGRect(x: 0, y: titleLabel.frame.maxY, width: controlView.frame.width, height: controlView.frame.height - 50 - titleLabel.frame.maxY - 10 - 116), collectionViewLayout: layout)
        collectionView.register(BetweenCollectionViewCell.self, forCellWithReuseIdentifier: BetweenCollectionViewCell.identificator)
        collectionView.register(BetweenHeaderCollectionReusableView.self, forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: BetweenHeaderCollectionReusableView.identifier)
        collectionView.backgroundColor = .clear
        collectionView.delegate = self
        collectionView.dataSource = self
        controlView.addSubview(collectionView)
        
        infoLabel = UILabel(frame: CGRect(x: 16, y: collectionView.frame.maxY, width: controlView.frame.width - 32, height: 40))
        infoLabel.textAlignment = .center
        infoLabel.textColor = .darkGray
        infoLabel.font = UIFont.systemFont(ofSize: 14)
        infoLabel.lineBreakMode = .byWordWrapping
        infoLabel.numberOfLines = 0
        controlView.addSubview(infoLabel)
        
        payButton = UIButton()
        payButton.titleLabel?.font = UIFont.boldSystemFont(ofSize: 14)
        payButton.backgroundColor = .lightGray
        payButton.setTitleColor(.white, for: .normal)
        payButton.setTitle("Оплатить", for: .normal)
        payButton.addTarget(self, action: #selector(payTouch), for: .touchUpInside)
        payButton.sizeToFit()
        payButton.frame = CGRect(x: controlView.frame.width / 2 - (payButton.frame.width + 32) / 2, y: infoLabel.frame.maxY + 10, width: payButton.frame.width + 32, height: 40)
        payButton.setCornerRadius(radius: 5)
        controlView.addSubview(payButton)
        payButton.hideObject(0)
        
        bottomView = UIView(frame: CGRect(x: 0, y: infoLabel.frame.maxY, width: controlView.frame.width, height: 116))
        bottomView.backgroundColor = .clear
        controlView.addSubview(bottomView)
        
        checkBox = Checkbox(frame: CGRect(x: 16, y: 0, width: 30, height: 30))
        checkBox.cornerRadius = 5
        checkBox.checkedBorderColor = .gray
        checkBox.uncheckedBorderColor = .gray
        checkBox.checkmarkColor = .gray
        checkBox.checkmarkStyle = .tick
        checkBox.checkmarkSize = 0.6
        checkBox.isChecked = DataStorage.shared.closeBetweenAfterOpen
        checkBox.valueChanged = { (value) in
            DataStorage.shared.closeBetweenAfterOpen = value
        }
        bottomView.addSubview(checkBox)
        
        checkCloseAfterOpenLabel = UILabel(frame: CGRect(x: checkBox.frame.maxX + 8, y: checkBox.frame.origin.y, width: controlView.frame.width - checkBox.frame.maxX - 16, height: 30))
        checkCloseAfterOpenLabel.textAlignment = .left
        checkCloseAfterOpenLabel.textColor = UIColor.darkGray
        checkCloseAfterOpenLabel.font = UIFont.systemFont(ofSize: 12)
        checkCloseAfterOpenLabel.text = "Закрыть форму после открытия двери"
        bottomView.addSubview(checkCloseAfterOpenLabel)
        
        closeButton = UIButton()
        closeButton.titleLabel?.font = UIFont.boldSystemFont(ofSize: 14)
        closeButton.backgroundColor = .gray
        closeButton.setTitleColor(.white, for: .normal)
        closeButton.setTitle("Закрыть", for: .normal)
        closeButton.addTarget(self, action: #selector(closeTouch), for: .touchUpInside)
        closeButton.sizeToFit()
        closeButton.frame = CGRect(x: controlView.frame.width / 4 - (closeButton.frame.width + 32) / 2, y: checkCloseAfterOpenLabel.frame.maxY + 8, width: closeButton.frame.width + 32, height: 40)
        closeButton.setCornerRadius(radius: 5)
        bottomView.addSubview(closeButton)
        
        openAppButton = UIButton()
        openAppButton.titleLabel?.font = UIFont.boldSystemFont(ofSize: 14)
        openAppButton.backgroundColor = Colors.GREEN_PRIMARY
        openAppButton.setTitleColor(.white, for: .normal)
        openAppButton.setTitle("В приложение", for: .normal)
        openAppButton.addTarget(self, action: #selector(openAppTouch), for: .touchUpInside)
        openAppButton.sizeToFit()
        openAppButton.frame = CGRect(x: controlView.frame.width * 3 / 4 - (closeButton.frame.width + 32) / 2, y: closeButton.frame.origin.y, width: openAppButton.frame.width + 32, height: 40)
        openAppButton.setCornerRadius(radius: 5)
        bottomView.addSubview(openAppButton)
        
        bottomView.frame.origin.y = infoLabel.frame.maxY
        controlView.frame.size.height = 500
        
        readDataFromDatabase()
        
        DispatchQueue.main.async {
            DispatchQueue.global(qos: .background).async(execute: {
                print("Running beacon detection in the background queue")
                if #available(iOS 13.0, *) {
                    BLEManager.shared.delegate = self
                    BLEManager.shared.search(uuids: self.devicePanels.map({ $0.bleUuid! }))
                }
                while(true) {
                    Thread.sleep(forTimeInterval: 1)
                    print("running...")
                }
            })
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: true)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.setNavigationBarHidden(false, animated: true)
    }
    
    func readDataFromDatabase() {
        self.contracts = []
        self.devicePanels = []
        for c in DataStorage.shared.contracts {
            if let devices = c.devices, devices.count > 0 {
                self.contracts.append(c)
            }
            self.devicePanels += c.devicePanels.filter({ $0.bleUuid != nil })
        }
        self.collectionView.reloadData()
    }
    
    @objc
    func closeTouch() {
        UIButton.animate(withDuration: 0.1,
            animations: {
            self.closeButton.transform = CGAffineTransform(scaleX: 1.1, y: 1.1)
        },
            completion: { finish in
            UIButton.animate(withDuration: 0.2, animations: {
                self.closeButton.transform = CGAffineTransform.identity
            },  completion: { finish in
                self.closeTapped()
            })
        })
    }
    
    fileprivate func closeTapped() {
        DispatchQueue.main.asyncAfter(deadline: .now()) {
            UIApplication.shared.perform(#selector(NSXPCConnection.suspend))
        }
    }
    
    @objc
    func openAppTouch() {
        UIButton.animate(withDuration: 0.1,
            animations: {
            self.openAppButton.transform = CGAffineTransform(scaleX: 1.1, y: 1.1)
        },
            completion: { finish in
            UIButton.animate(withDuration: 0.2, animations: {
                self.openAppButton.transform = CGAffineTransform.identity
            },  completion: { finish in
                self.navigationController?.popViewController(animated: true)
            })
        })
    }
    
    @objc
    func payTouch() {
        guard paymentUrl != "", let url = URL(string: paymentUrl), UIApplication.shared.canOpenURL(url) else { return }
        UIApplication.shared.open(url)
    }
    
    fileprivate func closedTick() {
        show(message: "\(savedText)\(savedText.count > 0 ? " " : "")Приложение будет закрыто через \(secondsAfterOpen) секунд")
        
        if secondsAfterOpen <= 0 {
            show(message: savedText)
            closeTapped()
        } else {
            DispatchQueue.main.asyncAfter(deadline: .now() + 1, execute: {
                self.secondsAfterOpen -= 1
                self.closedTick()
            })
        }
    }
    
    func show(message: String, error: Bool = false) {
        infoLabel.text = message
        infoLabel.textColor = error ? Colors.RED_ACCENT : .darkGray
        if error {
            AudioServicesPlaySystemSound(SystemSoundID(1073))
        }
    }
    
    func successOpened() {
        if checkBox.isChecked {
            secondsAfterOpen = 5
            savedText = infoLabel.text ?? ""
            
            closedTick()
        }
    }
    
    func changePaymentUrl(url: String, duration: Double = 0.5) {
        self.paymentUrl = url
        if url.count > 0 {
            controlView.animateChangeHeight(550)
            bottomView.animateChangePositionY(payButton.frame.maxY)
            payButton.showObject(duration)
        } else {
            controlView.animateChangeHeight(500)
            bottomView.animateChangePositionY(infoLabel.frame.maxY)
            payButton.hideObject(duration)
        }
    }
    
    @objc
    func beaconTouchUpInside() {
        guard #available(iOS 13.0, *) else { return }
        if BLEManager.shared.isScanning {
            BLEManager.shared.stopScanning()
        } else {
            BLEManager.shared.startScanning()
        }
    }
    
    func readyOpenDoor(contractNumber: Int, uuid: String) {
        RestClient.shared.readyOpenDoor(contractNumber: contractNumber, uuid: uuid) { [weak self] (resp) in
            print(resp.isSuccess())
        } errorCall: { err in
            debugPrint(err)
        }

    }

}
extension BetweenVC: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: width, height: height)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        return CGSizeMake(collectionView.frame.width, 40)
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        let header = collectionView.dequeueReusableSupplementaryView(ofKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: BetweenHeaderCollectionReusableView.identifier, for: indexPath) as! BetweenHeaderCollectionReusableView
        let contract = contracts[indexPath.section]
        header.setupHeader(address: contract.getReadableAddress)
        
        return header
    }
}
extension BetweenVC: UICollectionViewDataSource {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return contracts.count
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        guard section < contracts.count else { return 0 }
//        return contracts[section].devicePanels.count > 1 ? contracts[section].devicePanels.count + 1 : contracts[section].devicePanels.count
        return contracts[section].devicePanels.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: BetweenCollectionViewCell.identificator, for: indexPath) as! BetweenCollectionViewCell
        if indexPath.section < contracts.count {
            if indexPath.row < contracts[indexPath.section].devicePanels.count {
                cell.setupCell(contract: contracts[indexPath.section], devicePanel: contracts[indexPath.section].devicePanels[indexPath.row])
            } else {
                cell.setupCell(contract: contracts[indexPath.section])
            }
            
            cell.delegate = self
        }
        
        return cell
    }
    
}
extension BetweenVC: BetweenCollectionViewCellDelegate {
    func openDevice(number: Int, devicePanel: DevicePanel?) {
        guard !isOpening else { return }
        isOpening = true
        
        show(message: "Открываем дверь...")
        changePaymentUrl(url: "", duration: 0)
        
        NotificationCenter.default.post(name: .betweenCellChanged, object: nil)
        
        RestClient.shared.openDoor(contractNumber: number, reason: .between, device: devicePanel?.deviceId ?? "", panel: devicePanel?.panel.field ?? "", success: { [weak self] (resp) in
            guard let self = self else { return }
            
            if(resp.isSuccess()) {
                NotificationCenter.default.post(name: .betweenCellOpened, object: nil)
                self.show(message: "Входите в подъезд. У вас есть 3 секунды.")
                
                self.successOpened()
                
                DispatchQueue.main.asyncAfter(deadline: .now() + 3, execute: {
                    self.isOpening = false
                    NotificationCenter.default.post(name: .betweenCellClosed, object: nil)
                })
            } else {
                self.isOpening = false
                self.show(message: resp.message ?? "", error: true)
                if let code = resp.code, code == 402, let url = resp.paymentUrl {
                    self.changePaymentUrl(url: url)
                }
            }
            
            }, errorCall: { [weak self] (err) in
                guard let self = self else { return }
                self.isOpening = false
                debugPrint(err)
                self.show(message: err.message ?? "", error: true)
                if let code = err.code, code == 402, let url = err.paymentUrl {
                    self.changePaymentUrl(url: url)
                }
        })
    }
}
extension BetweenVC: BLEManagerDelegate {
    func foundedTags(uuids: [String]) {
        devicePanels.forEach { dp in
            if let uuid = dp.bleUuid?.lowercased(), let _ = uuids.firstIndex(of: uuid), let number = dp.contractNumber {
                let timestamp = Date().timeIntervalSince1970
                print("timestamp: \(timestamp)")
                if dataOpening.keys.contains(uuid) {
                    if let lastDouble = dataOpening[uuid], timestamp > lastDouble + 15 {
                        dataOpening[uuid] = timestamp
                        readyOpenDoor(contractNumber: number, uuid: uuid)
                    }
                } else {
                    dataOpening[uuid] = timestamp
                    readyOpenDoor(contractNumber: number, uuid: uuid)
                }
                
            }
        }
    }
    
    func successStarted() {
        beaconButton.tintColor = Colors.GREEN_PRIMARY
    }
    
    func successFinished() {
        beaconButton.tintColor = .lightGray
    }
}
