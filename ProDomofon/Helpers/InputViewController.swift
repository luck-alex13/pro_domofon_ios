//
//  InputViewController.swift
//  ProDomofon
//
//  Created by Александр Новиков on 24/11/2019.
//  Copyright © 2019 Александр Новиков. All rights reserved.
//

import UIKit

class InputViewController: UIViewController {
    
    var rootScrollView: UIScrollView!
    // Constraints
    var constraintContentHeight: NSLayoutConstraint!
    
    var activeField: UIView?
    var lastOffset: CGPoint?
    var keyboardHeight: CGFloat!
    
    func viewDidLoad(scrollView: UIScrollView, constraintContentHeight: NSLayoutConstraint) {
        super.viewDidLoad()
        addKeyBoardObservers(scrollView: scrollView, constraintContentHeight: constraintContentHeight)
        
        // Add touch gesture for contentView
        let recognizer = UITapGestureRecognizer(target: self, action: #selector(returnTextView))
        self.view.addGestureRecognizer(recognizer)
    }
    
    func viewDidLoad(scrollView: UIScrollView, constraintContentHeight: NSLayoutConstraint, recognizer: UITapGestureRecognizer) {
        super.viewDidLoad()
        addKeyBoardObservers(scrollView: scrollView, constraintContentHeight: constraintContentHeight)
        
        self.view.addGestureRecognizer(recognizer)
    }
    
    func addKeyBoardObservers(scrollView: UIScrollView, constraintContentHeight: NSLayoutConstraint)  {
        self.rootScrollView = scrollView
        self.constraintContentHeight = constraintContentHeight
        
        // Добавляем наблюдателей за экшенами клавиатуры
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.keyboardWillShow),
                                               name: UIResponder.keyboardWillShowNotification,
                                               object: nil)
        
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(keyboardWillHide),
                                               name: UIResponder.keyboardWillHideNotification,
                                               object: nil)
    }
    
    @objc func returnTextView(gesture: UIGestureRecognizer) {
        guard activeField != nil else {
            return
        }
        
        hideKeyBoard()
    }
    
    func hideKeyBoard() {
        activeField?.resignFirstResponder()
        activeField = nil
    }
    
    func setActiveField(_ view: UIView) {
        activeField = view
    }
}


// MARK: UITextFieldDelegate
extension InputViewController: UITextFieldDelegate {
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        activeField = textField
        lastOffset = self.rootScrollView?.contentOffset
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        hideKeyBoard()
        return true
    }
}

extension InputViewController: UIScrollViewDelegate {
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        lastOffset = scrollView.contentOffset
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        lastOffset = scrollView.contentOffset
    }
}

// MARK: UITextViewDelegate
//extension InputViewController: UITextViewDelegate {
//    func textViewDidBeginEditing(_ textView: UITextView) {
//        activeField = textView
//        lastOffset = self.rootScrollView?.contentOffset
//    }
//
//    func textViewDidEndEditing(_ textView: UITextView) {
//        hideKeyBoard()
//    }
//}

// MARK: Keyboard Handling
extension InputViewController {
    @objc  func keyboardWillShow(notification: NSNotification) {
        if keyboardHeight != nil{
            return
        }
        
        if let lastOffset = self.lastOffset,
            let activeField = activeField,
            let keyboardInfo = notification.userInfo,
            let keyboardSize = (keyboardInfo[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            
            keyboardHeight = keyboardSize.height
            
            // so increase contentView's height by keyboard height
            
            
            // move if keyboard hide input field
            let scrHeigth = self.rootScrollView.frame.size.height
            let fieldY = activeField.globalPoint!.y
            let distanceToBottom = scrHeigth - fieldY - activeField.frame.size.height
            let collapseSpace = keyboardHeight - distanceToBottom
            
            if collapseSpace < 0 {
                UIView.animate(withDuration: 0.3, animations: {
                    self.constraintContentHeight.constant += self.keyboardHeight
                })
                // no collapse
                return
            }
            
            let durationValue : NSNumber = keyboardInfo[UIResponder.keyboardAnimationDurationUserInfoKey] as! NSNumber
            let animationDuration : TimeInterval = durationValue.doubleValue
            
            let options = UIView.AnimationOptions(rawValue: UInt((keyboardInfo[UIResponder.keyboardAnimationCurveUserInfoKey] as! NSNumber).intValue << 16))
            
            
            // set new offset for scroll view
            UIView.animate(withDuration: animationDuration, delay: 0, options:options, animations: {
                // scroll to the position above keyboard 10 points
                self.constraintContentHeight.constant += self.keyboardHeight
                self.rootScrollView.contentOffset = CGPoint(x: lastOffset.x, y: collapseSpace + 15)
            })
        }
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        guard let keyboardHeight = self.keyboardHeight, let lastOffset = self.lastOffset else { return }
        
        UIView.animate(withDuration: 0.3) {
            self.constraintContentHeight?.constant -= keyboardHeight
            
            self.rootScrollView?.contentOffset = lastOffset
        }
        
        self.keyboardHeight = nil
    }
}

