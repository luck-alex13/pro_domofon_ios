//
//  Utils.swift
//  ProDomofon
//
//  Created by Александр Новиков on 03/06/2019.
//  Copyright © 2019 Александр Новиков. All rights reserved.
//

import Foundation
import UIKit
import Toast_Swift

class Utils {
    
    static let shared = Utils()
    
    private init(){}
    
    var MilliSeconds: Int64 {
        return Int64(NSDate().timeIntervalSince1970 * 1000)
    }
    
    var TimeInSecond: Int {
        return Int(NSDate().timeIntervalSince1970)
    }
    
    enum Storyboards: String {
        case main = "Main"
        case additional = "Additional"
    }
    
    func mainStoryboard() -> UIStoryboard {
        return makeStoryboard(from: Storyboards.main)
    }
    
    func additionalStoryboard() -> UIStoryboard {
        return makeStoryboard(from: Storyboards.additional)
    }
    
    func makeStoryboard(from name: Storyboards) -> UIStoryboard {
        return UIStoryboard(name: name.rawValue, bundle: nil)
    }
    
    func getFullOsVersion() -> String {
        return UIDevice.current.systemVersion
    }
    
    func getFloatOsVersion() -> String {
        return String((UIDevice.current.systemVersion as NSString).floatValue)
    }
    
    func showToast(from view: UIView?, with message: String, duration: TimeInterval = ToastManager.shared.duration) {
        view?.makeToast(message.localized, duration: duration, position: .bottom)
    }
    
    func showToast(from controller: UIViewController?, with message: String, customPosition: Bool) {
        if customPosition {
            if let controller = controller {
                controller.view.makeToast(message.localized, point: CGPoint(x: controller.view.bounds.size.width / 2.0,
                                                                            y: ( controller.view.bounds.size.height - 60) - ToastManager.shared.style.verticalPadding), title: nil, image: nil, completion: nil)
            }
        }else {
            showToast(from: controller, with: message)
        }
    }
    
    func showToast(from controller: UIViewController?, with message: String) {
        showToast(from: controller?.view, with: message)
    }
    
    func showToastHttp(from controller: UIViewController?, with message: String, error: ErrorBody) {
        showToastHttp(from: controller?.view, with: message, error: error)
    }
    
    func showToastHttp(from view: UIView?, with message: String, error: ErrorBody) {
        let mess: String
        if(error.status == 200) {
            mess = "\(message.localized) \(error.message!)"
        } else {
            mess = makeHttpErrorMessage(from: message, with: error)
        }
        let duration: TimeInterval = 4.0
        showToast(from: view, with: mess, duration: duration)
    }
    
    func showToastHttp(from view: UIView?, with message: String, code: Int?) {
        let mess: String = makeHttpErrorMessage(from: message, with: code)
        
        showToast(from: view, with: mess)
    }
    
    func makeHttpErrorMessage(from message: String, with code: Int?) -> String {
        if let c = code {
            return  message.localized + String(format: "Код ошибки".localized, c)
        }else {
            return message.localized
        }
    }
    
    func makeHttpErrorMessage(from message: String, with error: ErrorBody) -> String {
        var finalMessage = message.localized
        if let code = error.status, error.status != 200 {
            finalMessage = "\(finalMessage) Код ошибки: \(code)."
        }
        if let mess = error.message{
            finalMessage = "\(finalMessage) Сообщение: \(mess)"
        }
        return finalMessage
    }
    
    func hideKeyboard(from view: UIView?) {
        view?.endEditing(true)
    }
    
    func hideKeyboard(from controller: UIViewController?) {
        controller?.view.endEditing(true)
    }
    
    @discardableResult
    func openUrl(url: String) -> Bool {
        if let Url = URL(string: url), UIApplication.shared.canOpenURL(Url) {
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(Url, options: [:]) { (res) in
                    
                }
                return true
            } else {
                return UIApplication.shared.openURL(Url)
            }
        }else {
            return false
        }
    }
    
}
