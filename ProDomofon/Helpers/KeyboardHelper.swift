//
//  KeyboardHelper.swift
//  ProDomofon
//
//  Created by Александр Новиков on 24/06/2019.
//  Copyright © 2019 Александр Новиков. All rights reserved.
//

import UIKit

class KeyboardHelper {
    
    var animating: Bool = false
    var kbShown: Bool = false
    weak var uiController: UIViewController?
    var tabBarHeight: CGFloat? = 0
    
    func subscribeOnKeyboardNotifications(observer: UIViewController) {
        uiController = observer
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.keyboardWillShow),
                                               name: UIResponder.keyboardWillShowNotification,
                                               object: nil)
        
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(keyboardWillHide),
                                               name: UIResponder.keyboardWillHideNotification,
                                               object: nil)
    }
    
    func removeObservers() {
        NotificationCenter.default.removeObserver(self)
    }
    
    @objc func keyboardWillShow(sender: NSNotification) {
        //print("keyboardWillShow")
        if let controller = uiController, !kbShown {
            kbShown = !kbShown
            animateFrame(from: controller, for: sender)
        }
    }
    
    @objc func keyboardWillHide(sender: NSNotification) {
        //print("keyboardWillHide")
        if let controller = uiController, kbShown {
            kbShown = !kbShown
            animateFrame(from: controller, for: sender)
        }
    }
    
    func animateFrame(from controller: UIViewController, for notification: NSNotification) {
        //print("animateFrame")
        if !animating {
            animating = true
        }else {
            return
        }
        
        let keyboardInfo = notification.userInfo!
        let kbHeigth = (keyboardInfo[UIResponder.keyboardFrameEndUserInfoKey] as! NSValue).cgRectValue.height
        let durationValue : NSNumber = keyboardInfo[UIResponder.keyboardAnimationDurationUserInfoKey] as! NSNumber
        let animationDuration : TimeInterval = durationValue.doubleValue
        
        let options = UIView.AnimationOptions(rawValue: UInt((keyboardInfo[UIResponder.keyboardAnimationCurveUserInfoKey] as! NSNumber).intValue << 16))
        var newY: CGFloat
        if notification.name == UIResponder.keyboardWillShowNotification || notification.name == UIResponder.keyboardWillChangeFrameNotification {
            newY = -kbHeigth
        } else {
            newY = -1 // хз но -tabBarHeight оставляет отступ пустой
            //newY = 0 // из-за махинаций стаббаром уезжает за пределы экрана
        }
        
        UIView.animate(withDuration: animationDuration, delay: 0, options:options , animations: { () -> Void in
            controller.view.frame.origin.y = newY            
        }, completion: { (Bool) -> Void in
            self.animating = false
        })
    }
    
    
    
    var tapGestureRecognizerThatHideKeyboard: UITapGestureRecognizer {
        get {
            let rec = UITapGestureRecognizer(target: self, action: #selector(clearKeyboard))
            rec.cancelsTouchesInView = false
            return rec
        }
    }
    
    var swipeGestureRecognizerThatHideKeyboard: UISwipeGestureRecognizer {
        get {
            let rec = UISwipeGestureRecognizer(target: self, action: #selector(clearKeyboard))
            rec.direction = .down
            return rec
        }
    }
    
    @objc func clearKeyboard() {
        uiController?.view.endEditing(true)
    }
    
    func hideKeyboard(from view: UIView?) {
        view?.endEditing(true)
    }
    
    func hideKeyboard(from controller: UIViewController?) {
        controller?.view.endEditing(true)
    }
    
    func showKeyboard(on textView: UITextView) {
        textView.becomeFirstResponder()
    }
    
    
}
