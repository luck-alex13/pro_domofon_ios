//
//  AlertManager.swift
//  ProDomofon
//
//  Created by Александр Новиков on 11/06/2019.
//  Copyright © 2019 Александр Новиков. All rights reserved.
//

import UIKit

class AlertManager {
    
    static let shared = AlertManager()
    
    private init(){}
    
    var progressAlert: UIAlertController?
    
    func buildProgressDialog(title: String) -> UIAlertController {
        let alert = UIAlertController(title: title, message: nil, preferredStyle: .alert)
        let indicator = UIActivityIndicatorView(frame: CGRect(x: 10, y: 6, width: 50, height: 50))
        indicator.hidesWhenStopped = true
        indicator.style = UIActivityIndicatorView.Style.whiteLarge
        indicator.color = Colors.RED_ACCENT
        indicator.startAnimating()
        alert.view.addSubview(indicator)
        return alert
    }
    
    func showProgressDialog(from viewController: UIViewController?) {
        guard progressAlert == nil else { return }
        if let alert = progressAlert {
            guard !alert.isBeingPresented else {
                print("alert is Being Presented")
                return }
        }
        progressAlert = buildProgressDialog(title: "Загрузка...")
        viewController?.present(progressAlert!, animated: false, completion: nil)
    }
    
    func showAlertNoInternet(from viewController: UIViewController?) {
        let alert = buildAlertNoInternet()
        let okAction = UIAlertAction(title: "Ok", style: .default, handler: nil)
        alert.addAction(okAction)
        viewController?.present(alert, animated: true, completion: nil)
    }
    
    func showAlertNoInternet(from viewController: UIViewController, retryHandler: ((UIAlertAction) -> Swift.Void)? = nil) {
        let alert = buildAlertNoInternet()
        let okAction = UIAlertAction(title: "Повторить", style: .default, handler: retryHandler)
        alert.addAction(okAction)
        viewController.present(alert, animated: true, completion: nil)
    }
    
    func buildAlertNoInternet() -> UIAlertController {
        return UIAlertController(title: "Ошибка", message: "Нет подключения к сети Интернет", preferredStyle: .alert)
    }
    
    func hideProgress(completion: (() -> Swift.Void)? = nil) {
        guard let progressAlert = self.progressAlert else { return }
        
        DispatchQueue.main.async {
            progressAlert.dismiss(animated: false) {
                self.progressAlert = nil
                completion?()
            }
        }
    }
    
    func buildMessageAlert(title: String, errorMessage:String) -> UIAlertController {
        return UIAlertController(title: title, message: errorMessage, preferredStyle: .alert)
    }
    
    func buildMessageAlert(title: String?, errorMessage:String?, handler: ((UIAlertAction) -> Swift.Void)? = nil) -> UIAlertController {
        let alert = UIAlertController(title: title, message: errorMessage, preferredStyle: .alert)
        
        let okAction = UIAlertAction(title: "Ok", style: .default, handler: handler)
        alert.addAction(okAction)
        
        return alert
    }
    
    func buildOkCancelAlert(title: String?, errorMessage:String?, okHandler: ((UIAlertAction) -> Swift.Void)? = nil, cancelHandler: ((UIAlertAction) -> Swift.Void)? = nil) -> UIAlertController {
        return buildOkCancelAlert(title: title, errorMessage: errorMessage, okText: "Ok", cancelText: "Отмена", okHandler: okHandler, cancelHandler: cancelHandler)
    }
    
    func buildOkCancelAlert(title: String?, errorMessage:String?, okText: String, cancelText: String, okHandler: ((UIAlertAction) -> Swift.Void)? = nil, cancelHandler: ((UIAlertAction) -> Swift.Void)? = nil) -> UIAlertController {
        let alert = UIAlertController(title: title, message: errorMessage, preferredStyle: .alert)
        
        let okAction = UIAlertAction(title: okText, style: .default, handler: okHandler)
        alert.addAction(okAction)
        
        let cancelAction = UIAlertAction(title: cancelText, style: .cancel, handler: cancelHandler)
        alert.addAction(cancelAction)
        
        return alert
    }
    
    func showAlert(from viewController: UIViewController?, title: String?, message: String?, handler: ((UIAlertAction) -> Swift.Void)? = nil) {
        viewController?.present(
            AlertManager.shared.buildMessageAlert(title: title, errorMessage: message, handler: handler),
            animated: true,
            completion: nil)
    }
}
