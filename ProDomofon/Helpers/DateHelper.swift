//
//  DateHelper.swift
//  ProDomofon
//
//  Created by Александр Новиков on 25/08/2019.
//  Copyright © 2019 Александр Новиков. All rights reserved.
//

import Foundation
import AFDateHelper

class DateHelper {
    
    let locale: Locale
    
    let russianDateKeys : [RelativeTimeStringType : String] = [
        .nowPast: "только что",
        .secondsPast: "%.f секунд назад",
        .oneMinutePast: "1 минуту назад",
        .minutesPast: "%.f минут назад",
        .oneHourPast: "час назад",
        .hoursPast: "%.f часов назад",
        .oneDayPast: "вчера"
    ]
    
    
    init(locale: Locale) {
//        self.locale = locale
        self.locale = Locale(identifier: "ru_RU")
    }
    
    func getRelativeDate(from timestamp: Int) -> String{
        let date = Date(timeIntervalSince1970: TimeInterval(timestamp))
        if locale.identifier.lowercased().contains("ru") {
            if date.compare(.isToday) {
                return date.toStringWithRelativeTime(strings: russianDateKeys)
            }else {
                return date.toString(dateStyle: .short, timeStyle: .short)
            }
        }
        
        return date.toStringWithRelativeTime()
    }
    
    func getOnlyDate(from timestamp: TimeInterval) -> String {
        let date = Date(timeIntervalSince1970: timestamp)
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "ru_RU")
        dateFormatter.dateStyle = .medium
        dateFormatter.timeStyle = .none
        return dateFormatter.string(from: date)
    }
    
    func getOnlyTime(from timestamp: TimeInterval) -> String {
        let date = Date(timeIntervalSince1970: timestamp)
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "ru_RU")
        dateFormatter.dateStyle = .none
        dateFormatter.timeStyle = .short
        return dateFormatter.string(from: date)
    }
    
    func parseDate(from stringDate: String, dateFormat: String = "yyyy-MM-dd'T'HH:mm:ss.SSSZ")  -> Date? {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = dateFormat
        dateFormatter.locale = Locale.current
        return dateFormatter.date(from: stringDate)
    }
    
     func getRelativeDate(from stringDate: String)  -> String {
        let date = parseDate(from: stringDate)
        if let date = date {
            if date.compare(.isToday) {
                return date.toString(dateStyle: .short, timeStyle: .short)
            }else {
                return date.toString(dateStyle: .short, timeStyle: .none)
            }
        }
        
        return "Parsing Error"
    }
    
    func getFullDate(from stringDate: String)  -> String {
        let date = parseDate(from: stringDate)
        if let date = date {
            return date.toString(dateStyle: .medium, timeStyle: .short)
        }
        
        return "Parsing Error"
    }
}
