//
//  ActionService.swift
//  ProDomofon
//
//  Created by Lexus on 18.01.2023.
//  Copyright © 2023 Алексей Агильдин. All rights reserved.
//

import UIKit

enum ActionType: String {
    case openDoor = "openDoor"
}

enum Action: Equatable {
    case openDoor(contract: String, device: String, panel: String)
    
    init?(shortcutItem: UIApplicationShortcutItem) {
        guard let type = ActionType(rawValue: shortcutItem.type) else {
            return nil
    }

    switch type {
        case .openDoor:
            if let contract = shortcutItem.userInfo?["contract"] as? String, let device = shortcutItem.userInfo?["device"] as? String, let panel = shortcutItem.userInfo?["panel"] as? String {
                self = .openDoor(contract: contract, device: device, panel: panel)
            } else {
                return nil
            }
        }
    }
}
