//
//  LinkManager.swift
//  ProDomofon
//
//  Created by Lexus on 26.01.2023.
//  Copyright © 2023 Алексей Агильдин. All rights reserved.
//

import Foundation

class LinkManager {
    
    enum LinkTypes: String {
        case contracts, calculations, services, phones, keys, videopanel, headsets, schedule, requests, pay, messages, widget, archive, settings
        
        var menuType: MenuVCType {
            switch self {
            case .contracts: return .contracts
            case .calculations: return .contractInfo
            case .services: return .costs
            case .phones: return .phones
            case .keys: return .keys
            case .videopanel: return .videoPanel
            case .headsets: return .headsets
            case .schedule: return .schedule
            case .requests: return .requests
            case .pay: return .pay
            case .messages: return .messages
            case .widget: return .widget
            case .archive: return .history
            case .settings: return .settings
            }
        }
    }
    
    static let shared = LinkManager()
    
    private init() {}
    
    func parseAndOpen(link: String) {
        if let type = LinkTypes(rawValue: link) {
            AppDelegate.shared.openVC(type: type.menuType)
        }
    }
    
}
