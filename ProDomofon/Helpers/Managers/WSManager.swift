//
//  WSManager.swift
//  ProDomofon
//
//  Created by Lexus on 29.07.2022.
//  Copyright © 2022 Алексей Агильдин. All rights reserved.
//

import Foundation
import SwiftyJSON

@objc
protocol WSManagerDelegate: class {
    func receiveInit(width: Int, height: Int)
    func receiveData(data: Data)
    @objc optional func receiveTs(ts: Double)
    func error()
}

@available(iOS 13.0, *)
class WSManager {
    
    public static let shared = WSManager()
    
    private init(){}
    
    fileprivate var webSocketTask: URLSessionWebSocketTask?
    
    weak var delegate: WSManagerDelegate?
    
    enum WSConnectionType: String {
        case stream, archive
    }
    
    fileprivate var type: WSConnectionType = .stream
    fileprivate var server: String = ""
    fileprivate var user: String = ""
    fileprivate var device: String = ""
    fileprivate var panel: String = ""
    
    public func setConnection(server: String = "video.domofon.dom38.ru:9999", user: String = DataStorage.shared.user?.id ?? "", device: String, panel: String, type: WSConnectionType = .stream) {
        self.type = type
        self.server = server
        self.user = user
        self.device = device
        self.panel = panel
        
        let params: [String: String] = [
            "device": "\(device)",
            "user": "\(user)",
            "panel": "\(panel)",
            "type": type.rawValue,
        ]
        
        guard let JSONData = try? JSONSerialization.data(withJSONObject: params,options: []) else {
            print("error for get JSONData")
            return
        }
        guard let JSONText = String(data: JSONData, encoding: .ascii) else {
            print("error for get JSONText")
            return
        }
        print("JSON string = \(JSONText)")
        let url = "wss://\(server)/\(JSONText.toBase64())"
        
        webSocketTask = URLSession(configuration: .default).webSocketTask(with: URL(string: url)!)
    }
    
    public func connect(type: WSConnectionType, withTimestart: Double? = nil) {
        guard let wstask = webSocketTask else {
            setConnection(server: server, user: user, device: device, panel: panel, type: type)
            connect(type: type, withTimestart: withTimestart)
            return
        }
        wstask.resume()
        receiveData()
        
        switch type {
        case .stream:
            if self.type == .stream {
                requestStream()
            } else {
                stopStream()
                setConnection(server: server, user: user, device: device, panel: panel, type: .stream)
            }
        case .archive:
            if self.type == .stream {
                stopStream()
                setConnection(server: server, user: user, device: device, panel: panel, type: .archive)
            }
            guard let withTimestart = withTimestart else { return }
            requestPlay(withTimestart: withTimestart)
        }
    }
    
    fileprivate func connectToArchive(withTimestart: Double?) {
        guard let wstask = webSocketTask else { return }
        wstask.resume()
        self.receiveData()
    }
    
    
    fileprivate func requestStream() {
        guard let wstask = webSocketTask else { return }
        let message = URLSessionWebSocketTask.Message.string("REQUESTSTREAM")
        wstask.send(message) { error in
            if let error = error {
                print("WebSocket couldn’t requestStream because: \(error)")
            }
        }
    }
    
    public func stopStream() {
        unSubscribeInit()
        guard let wstask = webSocketTask else { return }
        let message = URLSessionWebSocketTask.Message.string("STOPSTREAM")
        wstask.send(message) { error in
            if let error = error {
                print("WebSocket couldn’t stopStream because: \(error)")
            }
        }
        webSocketTask = nil
    }
    
    public func subscribeInit() {
        guard let wstask = webSocketTask else { return }
        let message = URLSessionWebSocketTask.Message.string("SUBSCRIBE: init")
        wstask.send(message) { error in
            if let error = error {
                print("WebSocket couldn’t subscribeInit because: \(error)")
            }
        }
    }

    public func unSubscribeInit() {
        guard let wstask = webSocketTask else { return }
        let message = URLSessionWebSocketTask.Message.string("UNSUBSCRIBE: init ")
        wstask.send(message) { error in
           if let error = error {
               print("WebSocket couldn’t unSubscribeInit because: \(error)")
           }
        }
    }
    
    fileprivate func requestPlay(withTimestart: Double) {
        guard let wstask = webSocketTask else { return }
        let message = URLSessionWebSocketTask.Message.string("{\"event\": \"play\", \"startTime\": \(withTimestart) }")
        wstask.send(message) { error in
            if let error = error {
                print("WebSocket couldn’t send requestPlay because: \(error)")
            }
        }
    }
    
    func requestSpeed(withValue: Int) {
        guard let wstask = webSocketTask else { return }
        let message = URLSessionWebSocketTask.Message.string("{\"event\": \"speed\", \"speed\": \(withValue) }")
        print(message)
        wstask.send(message) { error in
            if let error = error {
                print("WebSocket couldn’t send requestSpeed because: \(error)")
            }
        }
    }
    
    func receiveData() {
        guard let wstask = webSocketTask else { return }
        wstask.receive { result in
            switch result {
                case .failure(let error):
                    print("WebSocket Error in receiving message: \(error)")
                    self.delegate?.error()
                case .success(let message):
                    switch message {
                    case .string(let text):
//                        print("WebSocket get message: \(text)")
                        let json = JSON.init(parseJSON: text)
                        let action = json["action"].stringValue
                        if action == "init" {
                            let width = json["width"].intValue
                            let height = json["height"].intValue
                            self.delegate?.receiveInit(width: width, height: height)
                        } else if action == "ts" {
                            let ts = json["ts"].doubleValue
                            self.delegate?.receiveTs?(ts: ts)
                        }
                    case .data(let data):
//                        print("WebSocket get data: \(data)")
                        self.delegate?.receiveData(data: data)
                    @unknown default:
                        debugPrint("WebSocket Unknown message")
                    }
                    self.receiveData()
            }
        }
    }
}
