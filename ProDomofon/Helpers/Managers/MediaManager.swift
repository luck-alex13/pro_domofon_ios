//
//  MediaManager.swift
//  ProDomofon
//
//  Created by Алексей Агильдин on 11.06.2021.
//  Copyright © 2021 Алексей Агильдин. All rights reserved.
//

import Foundation
import UIKit
import Photos

class MediaManager: NSObject {
    
    static let shared = MediaManager()
    
    fileprivate let albumName = Bundle.main.appName

    var assetCollection: PHAssetCollection!
    
    private override init(){
        super.init()
        createFolder()
    }
    
    func createFolder() {
        func fetchAssetCollectionForAlbum() -> PHAssetCollection! {

            let fetchOptions = PHFetchOptions()
            fetchOptions.predicate = NSPredicate(format: "title = %@", albumName)
            let collection = PHAssetCollection.fetchAssetCollections(with: .album, subtype: .any, options: fetchOptions)

            if let firstObject = collection.firstObject {
                return firstObject
            }

            return nil
        }

        if let assetCollection = fetchAssetCollectionForAlbum() {
            self.assetCollection = assetCollection
            return
        }

        PHPhotoLibrary.shared().performChanges({ PHAssetCollectionChangeRequest.creationRequestForAssetCollection(withTitle: self.albumName) }) { success, error in
            if success {
                self.assetCollection = fetchAssetCollectionForAlbum()
            } else {
                print(error?.localizedDescription)
            }
        }
    }
    
    // function for save image to galery
    func saveImageToGalery(image: UIImage, callback: @escaping (Bool, String) -> Void) {
        guard assetCollection != nil else {
            callback(false, "Отсутствует доступ к галерее")
            return
        }
        
        guard PHPhotoLibrary.authorizationStatus() == .authorized else {
            callback(false, "Отсутствует доступ к галерее")
            return
        }
        
        PHPhotoLibrary.shared().performChanges {
            let assetChangeRequest = PHAssetChangeRequest.creationRequestForAsset(from: image)
            let assetPlaceholder = assetChangeRequest.placeholderForCreatedAsset
            if let albumChangeRequest = PHAssetCollectionChangeRequest(for: self.assetCollection) {
                albumChangeRequest.addAssets([assetPlaceholder] as NSFastEnumeration)
            }
        } completionHandler: { (success, error) in
            callback(success, error?.localizedDescription ?? "")
        }
    }
    
    func saveVideoByURL(url: URL, callback: @escaping (Bool, String) -> Void) {
        guard assetCollection != nil else {
            callback(false, "Отсутствует доступ к галерее")
            return
        }
        
        guard PHPhotoLibrary.authorizationStatus() == .authorized else {
            callback(false, "Отсутствует доступ к галерее")
            return
        }
        
        PHPhotoLibrary.shared().performChanges({
            guard let assetChangeRequest = PHAssetChangeRequest.creationRequestForAssetFromVideo(atFileURL: url) else {
                callback(false, "assetChangeRequest by video url == nil")
                return
            }
            let assetPlaceholder = assetChangeRequest.placeholderForCreatedAsset
            if let albumChangeRequest = PHAssetCollectionChangeRequest(for: self.assetCollection) {
                albumChangeRequest.addAssets([assetPlaceholder] as NSFastEnumeration)
            }
            
//            PHAssetChangeRequest.creationRequestForAssetFromVideo(atFileURL: url as URL)
//            let request = PHAssetCreationRequest.forAsset()

//            request.addResource(with: .video, fileURL: url, options: nil)

        }) { (success, error) in
            if !success {
                print("Error saving video: \(String(describing: error))")
            }
            callback(success, error?.localizedDescription ?? "")
        }
    }
    
    // function for save media to cache
    func saveMediaToCache(data: Data, withName: String) -> Bool {
        guard let directory = try? FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: true) as NSURL else {
            return false
        }
        do {
            guard let url = directory.appendingPathComponent(withName) else { return false }
            try data.write(to: url)
            return true
        } catch {
            print(error.localizedDescription)
            return false
        }
    }
    
    // function for save image to cache
    @discardableResult
    func saveImageToCache(image: UIImage, withName: String) -> Bool {
        guard let data = image.jpegData(compressionQuality: 1) ?? image.pngData() else {
            return false
        }
        return saveMediaToCache(data: data, withName: withName)
    }
    
    // function for get URL file from cache
    func getSavedURLFromCache(named: String) -> URL? {
        if let dir = try? FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: true) {
            let url = dir.appendingPathComponent(named)
            do {
                let data = try Data(contentsOf: url)
                print("There were \(data.count) bytes")
                return url
            } catch {
                return nil
            }
        }
        return nil
    }
    
    // function for get image from cache
    func getSavedImageFromCache(named: String) -> UIImage? {
        if let url = getSavedURLFromCache(named: named) {
            return UIImage(contentsOfFile: url.path)
        }
        return nil
    }
    
    // function for remove image from cache
    @discardableResult
    func removeImageFromCache(withName: String) -> Bool {
        if let dir = try? FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false) {
            let url = URL(fileURLWithPath: dir.absoluteString).appendingPathComponent(withName)
            do {
                try FileManager.default.removeItem(at: url)
                print("image with name \(withName) was deleted")
            } catch (let error) {
                print("removing image with name \(withName) error: \(error.localizedDescription)")
                return false
            }
        }
        return false
    }

    @objc func saveError(_ image: UIImage, didFinishSavingWithError error: Error?, contextInfo: UnsafeRawPointer) {
        print("Save finished!")
    }
}
