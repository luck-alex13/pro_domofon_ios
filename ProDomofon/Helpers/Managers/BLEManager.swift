//
//  BLEManager.swift
//  ProDomofon
//
//  Created by Lexus on 11.07.2023.
//  Copyright © 2023 Алексей Агильдин. All rights reserved.
//

import Foundation
import CoreLocation

protocol BLEManagerDelegate: AnyObject {
    func foundedTags(uuids: [String])
    func successStarted()
    func successFinished()
}

@available(iOS 13.0, *)
class BLEManager: NSObject {
    
    public static let shared = BLEManager()
    
    private override init(){}
    
    weak var delegate: BLEManagerDelegate?
    
    private var locationManager: CLLocationManager?
    private var uuids: [String] = []
    private var regions: [CLBeaconRegion] = []
    
    var isScanning: Bool {
        return regions.count > 0
    }
    
    func search(uuids: [String]) {
        stopScanning()
        self.uuids = uuids
        locationManager = CLLocationManager()
        locationManager?.delegate = self
        locationManager?.requestAlwaysAuthorization()
    }
    
    func stopScanning() {
        return
        guard let locationManager = locationManager else { return }
        delegate?.successFinished()
        regions.forEach { region in
            locationManager.stopMonitoring(for: region)
            locationManager.stopRangingBeacons(satisfying: CLBeaconIdentityConstraint(uuid: region.uuid))
        }
        regions = []
    }
    
    func startScanning() {
        guard let locationManager = locationManager else { return }
        guard uuids.count > 0 else { return }
        delegate?.successStarted()
        uuids.forEach { uuidStr in
            if let uuid = UUID(uuidString: uuidStr) {
                let beaconRegion = CLBeaconRegion(uuid: uuid, identifier: "MyBeacon")
                locationManager.startMonitoring(for: beaconRegion)
                locationManager.startRangingBeacons(satisfying: CLBeaconIdentityConstraint(uuid: beaconRegion.uuid))
                regions.append(beaconRegion)
            }
        }
    }
}
@available(iOS 13.0, *)
extension BLEManager: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if status == .authorizedAlways {
            if CLLocationManager.isMonitoringAvailable(for: CLBeaconRegion.self) {
                if CLLocationManager.isRangingAvailable() {
                    startScanning()
                }
            }
        }
    }
    
    @available(iOS 13.0, *)
    func locationManager(_ manager: CLLocationManager, didRange beacons: [CLBeacon], satisfying beaconConstraint: CLBeaconIdentityConstraint) {
        if beacons.count > 0 {
            //print(beacons)
            delegate?.foundedTags(uuids: beacons.map({ $0.uuid.uuidString.lowercased() }))
//            stopScanning()
        }
    }
}
