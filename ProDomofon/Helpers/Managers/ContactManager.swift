//
//  ContactManager.swift
//  ProDomofon
//
//  Created by Алексей Агильдин on 27.04.2021.
//  Copyright © 2021 Алексей Агильдин. All rights reserved.
//

import Foundation
import Contacts

protocol ContactManagerDelegate {
    func getName(text: String)
}

class ContactManager {
    
    static let shared = ContactManager()
    
    private init(){}
    
    func tryingGetContact(phone: String, delegate: ContactManagerDelegate) {
        CNContactStore().requestAccess(for: .contacts) { (access, error) in
            if let error = error {
                print(error.localizedDescription)
            } else {
                self.getContact(phone: phone, delegate: delegate)
            }
        }
    }
    
    fileprivate func getContact(phone: String, delegate: ContactManagerDelegate) {
        let keysToFetch = [CNContactGivenNameKey, CNContactFamilyNameKey, CNContactPhoneNumbersKey] as [CNKeyDescriptor]
        let fetchRequest = CNContactFetchRequest(keysToFetch: keysToFetch)
        CNContact.localizedString(forKey: CNLabelPhoneNumberiPhone)
        fetchRequest.mutableObjects = false
        fetchRequest.unifyResults = true
        fetchRequest.sortOrder = .userDefault
        
        do {
            try CNContactStore().enumerateContacts(with: fetchRequest) { (contact, stop) -> Void in
                if contact.phoneNumbers.count > 0 {
                    contact.phoneNumbers.forEach { (c) in
                        let foundedPhone = c.value.stringValue.onlyNumbers
                        
                        if [phone.onlyNumbers, "8\(phone.onlyNumbers)", "7\(phone.onlyNumbers)"].contains(foundedPhone) {
                            delegate.getName(text: "\(contact.givenName) \(contact.familyName)")
                            stop[0] = true
                        }
                    }
                }
            }
        } catch let e as NSError {
            print(e.localizedDescription)
        }
        
    }
}
