//
//  Const.swift
//  ProDomofon
//
//  Created by Александр Новиков on 29/05/2019.
//  Copyright © 2019 Александр Новиков. All rights reserved.
//

import Foundation
import UIKit

enum IntConst: Int {
    
    case PROMO_CODE_TIMEOUT = 28800
    case MAX_KEYS_COUNT = 10
}

enum StringConst: String {
    
    case MAIN_LINK,
        ADD_CONTRACT_TUTOTIAL,
        ALL_KEY_SHOPS,
        KEY_SHOP_PAGE,
        WIDGET_INFO,
        HELP,
        CONTACTS,
        DOMOFON_TO_TELEPHONE,
        POLICY,
        VIDEO_PL,
        APP_LINK
    
    var rawValue: String {
        var link = "https://domofon.dom38.ru"
//        if UIApplication.shared.isDebug {
//            link = "https://dev-domofon.dom38.ru"
//        }
        switch self {
        case .ADD_CONTRACT_TUTOTIAL: link += "/add-contract.html"
        case .ALL_KEY_SHOPS: link += "/key-retailers.html"
        case .KEY_SHOP_PAGE: link += "/key-retailers.html?store="
        case .WIDGET_INFO: link += "/statics/ios-widget.html"
        case .HELP: link += "/ios-instruction2.html"
        case .CONTACTS: link += "/contacts.html"
        case .DOMOFON_TO_TELEPHONE: link += "/domofon-to-telephone.html"
        case .POLICY: link += "/privacy_policy.html"
        case .VIDEO_PL : link += "/video-pl.html"
        case .APP_LINK: link += "/app/"
        default: break
        }
        return link
    }
}

enum HelpConst: String {
    case contracts,
         requests,
         phones,
         keys,
         apartment_intercoms,
         messages,
         widget,
         bluetooth
}

enum FireBase: String{
    
    case KEY_TYPE = "type"
    case KEY_TOKEN = "public_token"
    case ROOM_ID = "room_id"
    case NAME = "name"
    case KEY_PAYLOAD = "payload"
    case TYPE_SUPPORT = "support"
    case TYPE_BALANCE_INCOME = "balance"
    case FRIEND_REQUEST = "friend_request"
    case CONFIRMED_FRIEND_REQUEST = "confirmed_friend_request"
    case DIRECT_MESSAGE = "direct_message"
    case BANNED_FROM_CHAT = "banned_from_chat "
    case UNBANNED_FROM_CHAT = "unbanned_in_chat "
    case CHAT_REPLY = "chat_reply"
}

struct Colors {
    static let YELLOW = UIColor.init(rgb: 0xF7B733)
    static let GREEN_PRIMARY = UIColor.init(rgb: 0x029985)
    static let GREEN_DARK = UIColor.init(rgb: 0x00695C)
    static let WHITE = UIColor.init(rgb: 0xFFFFFF)
    static let RED_ACCENT = UIColor.init(rgb: 0xE65060)
    static let GREEN = UIColor.init(rgb: 0x01a861)
    static let BLACK = UIColor.init(rgb: 0x202020)
}

enum MessageType: Int {
    case TYPE_TEXT = 1
    case TYPE_IMAGE = 2
    case TYPE_GIFT = 3
}
