//
//  PushNotificationManager.swift
//  ProDomofon
//
//  Created by Александр Новиков on 13/09/2019.
//  Copyright © 2019 Александр Новиков. All rights reserved.
//

import Firebase
import UIKit
import UserNotifications
import SwiftyJSON
import AVFoundation

class PushNotificationManager: NSObject {
    
    static var shared = PushNotificationManager()
    fileprivate var wasInitFireBase = false
    
    var videoCall: VideoCall? {
        didSet {
            hasVideoCall()
            UNUserNotificationCenter.current().removeAllDeliveredNotifications()
            UIApplication.shared.applicationIconBadgeNumber = 0
        }
    }
    
//    var firebasePushSubscribed = false
    fileprivate var applePushSubscribed = false
    
    fileprivate var answeredCall: Bool = false
    var openedCall: Bool = false
    fileprivate var tryingAnswerCall: Bool = false
    
    var debugString: String?
    var info: [String: AnyObject]?
    var fromAppDelegate: Bool = false
    
    var openFromPush: Bool = false

    fileprivate var application: UIApplication!
    fileprivate var vc: UIViewController!
    
    func set(application: UIApplication){
        guard !wasInitFireBase else { return }
        wasInitFireBase = true
        
        if FirebaseApp.app() == nil {
            FirebaseApp.configure()
        }
        
        UNUserNotificationCenter.current().delegate = self
        
        self.application = application
//        NotificationCenter.default.addObserver(self, selector: #selector(endCall), name: .endCall, object: nil)
//        NotificationCenter.default.addObserver(self, selector: #selector(callWasAnsweredOther), name: .callWasAnsweredOther, object: nil)
    }
    
    func requestNotification(vc: UIViewController) {
        self.vc = vc
        if #available(iOS 10.0, *) {
            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(options: authOptions) { [weak self] (granted, _) in
                guard let self = self else { return }
                DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                    self.getNotificationSettings()
                }
            }
        } else {
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            UIApplication.shared.registerUserNotificationSettings(settings)
            DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                self.getNotificationSettings()
            }
        }
    }
    
    func getNotificationSettings() {
        UNUserNotificationCenter.current().getNotificationSettings { (settings) in
            if settings.authorizationStatus == .authorized {
                DispatchQueue.main.async {
                    UIApplication.shared.registerForRemoteNotifications()
                }
            } else if settings.authorizationStatus == .notDetermined {
                DispatchQueue.main.async {
                    UIApplication.shared.registerForRemoteNotifications()
                }
            } else {
                let currentDateTimeInterval = Date().timeIntervalSince1970
                let lastDateTimeInterval = DataStorage.shared.lastAskNotificationTimeInterval
                let difference = Double(7 * 24 * 60 * 60)
                if currentDateTimeInterval > lastDateTimeInterval + difference {
                    let alert = AlertManager.shared.buildOkCancelAlert(title: "Уведомления", errorMessage: "Включите уведомления в настройках телефона, чтобы получать пуш-сообщения по изменениям своего счета", okText: "Настройки", cancelText: "Позже") { (_) in
                        guard let settingsUrl = URL(string: UIApplication.openSettingsURLString) else {
                            return
                        }

                        if UIApplication.shared.canOpenURL(settingsUrl) {
                            UIApplication.shared.open(settingsUrl, options: [:], completionHandler: nil)
                        }
                    } cancelHandler: { (_) in
                        
                    }
                    print("present: \(self.vc.className)")
                    DispatchQueue.main.async {
                        self.vc.present(alert, animated: true) {
                            DataStorage.shared.lastAskNotificationTimeInterval = currentDateTimeInterval
                        }
                    }
                }
            }
        }
    }
    
    func parseVideoCallFrom(info: [AnyHashable: Any]) -> VideoCall? {
        let payload = "payload"
        let type = "type"
        
        NSLog("parseVideoCallFrom type: \(String(describing: info[type])) payload: \(String(describing: info[payload]))")
        
        if let typeValue = info[type], let payloadValue = info[payload] {
            let typeString = String(describing: typeValue)
            let payloadString = String(describing: payloadValue)
            if typeString == "sip" {
//                UIApplication.shared.showToast(text: "parseVideoCallFrom success")
                openFromPush = UIApplication.shared.applicationState != .active
                let payloadJson = JSON.init(parseJSON: payloadString)
                return VideoCall(json: payloadJson)
            }
        }
//        UIApplication.shared.showToast(text: "parseVideoCallFrom error")
        return nil
    }
    
    func subscribeIfNot() {
        if let appleToken = DataStorage.shared.appleToken, !applePushSubscribed  {
            subscribeOnPushes(appleToken: appleToken)
        }
    }
    
    func subscribeOnPushes(appleToken: String) {
        RestClient.shared.subscribeOnPush(token: appleToken, callType: false) { [weak self] in
            guard let self = self else { return }
            self.applePushSubscribed = true
        } errorCall: { (error) in
            NSLog("[PushNotificationManager] Error appleToken subscribeOnPushes() \(String(describing: error.message))")
        }
    }
    
    func unsubscribeOnPushes(success: @escaping () -> Void) {
        if let appleToken = DataStorage.shared.appleToken, applePushSubscribed  {
            RestClient.shared.unsubscribePush(token: appleToken) { [weak self] in
                guard let self = self else { return }
                self.applePushSubscribed = false
                success()
            } errorCall: { (error) in
                NSLog("[PushNotificationManager] Error appleToken unsubscribeOnPushes() \(String(describing: error.message))")
                success()
            }
        } else {
            success()
        }
    }
    
    func showNotification(title: String, subtitle: String? = nil, body: String?) {
        //creating the notification content
        
        if #available(iOS 10.0, *) {
            let content = UNMutableNotificationContent()
            //adding title, subtitle, body and badge
            content.title = title
            content.body = body ?? ""
            if let sub = subtitle {
                content.subtitle = sub
            }
            content.sound = UNNotificationSound.default
            
            //getting the notification request
            let request = UNNotificationRequest(identifier: "Prodomofon Notification", content: content, trigger: nil)
            
            //adding the notification to notification center
            UNUserNotificationCenter.current().add(request, withCompletionHandler: nil)
        } else {
            // Fallback on earlier versions
            let notification = UILocalNotification()
            notification.alertTitle = title
            notification.alertBody = body ?? ""
            notification.category = "PRO_DOMOFON_CATEGORY"
            // set the sound to be the default sound
            notification.soundName = UILocalNotificationDefaultSoundName
    
            // schedule it
            UIApplication.shared.presentLocalNotificationNow(notification)//scheduleLocalNotification(notification)//shared.ScheduleLocalNotification(notification);
        }
    }
    
    func showNotification(title: String, userInfo: [AnyHashable : Any]) {
        let content = UNMutableNotificationContent()
        //adding title, subtitle, body and badge
        content.title = title
        content.sound = UNNotificationSound(named: UNNotificationSoundName(rawValue: "sip_call_melody.mp3"))
        content.userInfo = userInfo
        
        //getting the notification request
        let request = UNNotificationRequest(identifier: "Prodomofon Notification", content: content, trigger: nil)
        
        //adding the notification to notification center
        UNUserNotificationCenter.current().add(request, withCompletionHandler: nil)
    }
    
    func hasVideoCall(){
        guard let videoCall = self.videoCall else { return }
        guard !answeredCall else { return }
        print("hasVideoCall: answeredCall=\(answeredCall)")
//        UIApplication.shared.showToast(text: "hasVideoCall: answeredCall=\(answeredCall)")
        
        RestClient.shared.hasCall(baseUrl: videoCall.socketUri, room: videoCall.room, user: videoCall.user) { (resp) in
            print("showVideoCall \(resp.messageText())")
//            UIApplication.shared.showToast(text: "showVideoCall resp: \(resp.messageText())")
            guard !self.answeredCall else { return }
            var debug = "\(resp.messageText())"
            if resp.isSuccess() {
                if !self.openedCall {
                    print("configuring audio session...")
                    let audioSession = AVAudioSession.sharedInstance();
                    do {
                        try audioSession.setCategory(.playAndRecord, mode: .voiceChat, options: AVAudioSession.CategoryOptions.mixWithOthers)
                    } catch (let error) {
                        print("Error setting audio category \(error.localizedDescription)")
//                        UIApplication.shared.showToast(text: "Error setting audio category \(error.localizedDescription)")
                        return
                    }
                    
                    let sampleRate: Double  = 44100.0
                    do {
                        try audioSession.setPreferredSampleRate(sampleRate)
                    } catch (let error) {
                        print("Error setPreferredSampleRate \(error.localizedDescription)")
//                        UIApplication.shared.showToast(text: "Error setPreferredSampleRate \(error.localizedDescription)")
                        return
                    }
                    let bufferDuration: TimeInterval = 0.005
                    do {
                        try audioSession.setPreferredIOBufferDuration(bufferDuration)
                    } catch (let error) {
                        print("Error setPreferredIOBufferDuration \(error.localizedDescription)")
//                        UIApplication.shared.showToast(text: "Error setPreferredIOBufferDuration \(error.localizedDescription)")
                        return
                    }
                    do {
                        try audioSession.overrideOutputAudioPort(.speaker)
                    } catch (let error) {
                        print("Error overrideOutputAudioPort \(error.localizedDescription)")
//                        UIApplication.shared.showToast(text: "Error overrideOutputAudioPort \(error.localizedDescription)")
                        return
                    }
                    do {
                        try audioSession.setActive(true, options: .notifyOthersOnDeactivation)
                    } catch (let error) {
                        print("Error setActive \(error.localizedDescription)")
//                        UIApplication.shared.showToast(text: "Error setActive \(error.localizedDescription)")
                        return
                    }
                    
//                    do {
//                        try audioSession.setActive(true, options: .notifyOthersOnDeactivation)
//                    } catch (let error) {
//                        print("Error setActive \(error.localizedDescription)")
//                        return
//                    }
                    
                    debug += "Trying open VideoCallVC"
                    
                    NotificationCenter.default.post(name: .showVideoCallVC, object: nil)
                }
                NotificationCenter.default.post(name: .hasCall, object: nil)
                self.startScheduleHasVideoCall()
            } else {
                self.endCall()
            }
            self.debugString = debug
            NotificationCenter.default.post(name: .showMainTabBarError, object: nil)
        } errorCall: { (error) in
            guard !self.answeredCall else { return }
            self.debugString = String(describing: error.message)
            NotificationCenter.default.post(name: .showMainTabBarError, object: nil)
            print("showVideoCall error \(String(describing: error.message))")
            self.endCall()
        }
    }
    
    func startScheduleHasVideoCall(){
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
            print("DispatchQueue.main.asyncAfter 1")
            self.hasVideoCall()
        }
    }
    
    func endCall() {
//        UIApplication.shared.showToast(text: "endCall tryingAnswerCall: \(tryingAnswerCall)")
        guard !tryingAnswerCall else {
            startScheduleHasVideoCall()
            return
        }
        answeredCall = false
        openedCall = false
        tryingAnswerCall = false
        NotificationCenter.default.post(name: .hasNotCall, object: nil)
    }
    
    func callAnswered(){
        answeredCall = true
        tryingAnswerCall = false
        print("answerVideoCall")
    }
    
    func tryAnswerCall(){
        tryingAnswerCall = true
    }
    
    @objc
    func callWasAnsweredOther(){
//        AlertManager.shared.hideProgress()
//        Utils.shared.showToast(from: self.view, with: "Звонок уже был отвечен")
    }
    
    func requestForExistCall(videoCall: VideoCall, success: @escaping () -> Void) {
        RestClient.shared.hasCall(baseUrl: videoCall.socketUri, room: videoCall.room, user: videoCall.user) { (resp) in
            if resp.isSuccess() {
                success()
            }
        } errorCall: { (_) in
            
        }
    }

}
extension PushNotificationManager: UNUserNotificationCenterDelegate {
    //MARK: - UNUserNotificationCenterDelegate methods
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        print("userNotificationCenter didReceive withCompletionHandler")
        
        let userInfo = response.notification.request.content.userInfo
       
        if let videoCall = parseVideoCallFrom(info: userInfo) {
            self.requestForExistCall(videoCall: videoCall) {
                self.videoCall = videoCall
                guard UIApplication.shared.applicationState != .active else { return }
                completionHandler()
            }
            return
        }
        
        completionHandler()
    }
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        print("userNotificationCenter willPresent")
        
        let userInfo = notification.request.content.userInfo
        
        if let videoCall = parseVideoCallFrom(info: userInfo) {
            self.requestForExistCall(videoCall: videoCall) {
                self.videoCall = videoCall
                guard UIApplication.shared.applicationState != .active else { return }
                completionHandler([.alert, .sound, .badge])
            }
            return
        }
        
        completionHandler([.alert, .sound, .badge])
    }
    
}
