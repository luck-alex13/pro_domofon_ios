//
//  MyHeadersAdapter.swift
//  ProDomofon
//
//  Created by Александр Новиков on 12/06/2019.
//  Copyright © 2019 Александр Новиков. All rights reserved.
//

import Foundation
import Alamofire

class MyHeadersAdapter: RequestAdapter {
    
    static let HEADER_TOKEN = "Authorization"
    static let REQUEST_SOURCE = "Request-Source"
    static let APP_VERSION = "app-version"
    static let CONTENT_TYPE = "Content-Type"
    
    private var accessToken: String?
    private var flagJson: Bool
    
    init(accessToken: String?, flagJson: Bool = false){
        if let token = accessToken {
            self.accessToken = "Bearer \(token)"
        }
        self.flagJson = flagJson
    }
    
    func adapt(_ urlRequest: URLRequest) throws -> URLRequest {
        
        var urlRequest = urlRequest
        
        urlRequest.setValue(self.accessToken, forHTTPHeaderField:  MyHeadersAdapter.HEADER_TOKEN)
        urlRequest.setValue("IOS", forHTTPHeaderField:  MyHeadersAdapter.REQUEST_SOURCE)
        urlRequest.setValue(Bundle.main.buildNumber, forHTTPHeaderField:  MyHeadersAdapter.APP_VERSION)
        if flagJson {
            urlRequest.setValue("application/json", forHTTPHeaderField:  MyHeadersAdapter.CONTENT_TYPE)
        }
        
        return urlRequest
    }
}
