//
//  ConnectionObserver.swift
//  ProDomofon
//
//  Created by Александр Новиков on 16/06/2019.
//  Copyright © 2019 Александр Новиков. All rights reserved.
//
import Alamofire

class ConnectionObserver {
    
    //    //shared instance
    //    static let shared = NetworkManager()
    
    var hostName: String!
    
    var reachabilityManager: NetworkReachabilityManager?
    
    init() {
        self.hostName = "www.apple.com"
        reachabilityManager = Alamofire.NetworkReachabilityManager(host: hostName)!
    }
    
    init(host: String){
        self.hostName = host
        reachabilityManager = Alamofire.NetworkReachabilityManager(host: hostName)!
    }
    
    var isConnectedToInternet: Bool {
        return reachabilityManager!.isReachable
    }
    
    func initWithOutLister() {
        reachabilityManager?.startListening()
    }
    
    
    
    func observeOnConnection(reachable: @escaping() -> Void, notReachable: @escaping() -> Void) {
        
        reachabilityManager?.listener = { status in
            switch status {
                
            case .notReachable:
                NSLog("The network is not reachable")
                notReachable()
            case .unknown :
                NSLog("It is unknown whether the network is reachable")
                reachable()
            case .reachable(.ethernetOrWiFi):
                NSLog("The network is reachable over the WiFi connection")
                reachable()
            case .reachable(.wwan):
                NSLog("The network is reachable over the WWAN connection")
                reachable()
            }
        }
        
        // start listening
        reachabilityManager?.startListening()
    }
    
    func endListening() {
        reachabilityManager?.stopListening()
    }
}

