//
//  ErrorBody.swift
//  ProDomofon
//
//  Created by Александр Новиков on 12/06/2019.
//  Copyright © 2019 Александр Новиков. All rights reserved.
//

import Foundation

class ErrorBody: Codable {
    
    var name, message, paymentUrl: String?
    var code, status: Int?
    
    init(name: String?, message: String?, code: Int?, status: Int?, paymentUrl: String? = nil) {
        self.name = name
        self.message = message
        self.paymentUrl = paymentUrl
        self.code = code
        self.status = status
    }
}

// MARK: Convenience initializers and mutators

extension ErrorBody {
    convenience init(data: Data) throws {
        let me = try JSONDecoder().decode(ErrorBody.self, from: data)
        self.init(name: me.name, message: me.message, code: me.code, status: me.status, paymentUrl: me.paymentUrl)
    }
    
    convenience init(_ json: String, using encoding: String.Encoding = .utf8) throws {
        guard let data = json.data(using: encoding) else {
            throw NSError(domain: "JSONDecoding", code: 0, userInfo: nil)
        }
        try self.init(data: data)
    }
    
    convenience init(fromURL url: URL) throws {
        try self.init(data: try Data(contentsOf: url))
    }
    
    
    func jsonData() throws -> Data {
        return try newJSONEncoder().encode(self)
    }
    
    func jsonString(encoding: String.Encoding = .utf8) throws -> String? {
        return String(data: try self.jsonData(), encoding: encoding)
    }
}

class ServerError: Error {
    var errorResponse: ErrorBody
    init(error: ErrorBody) {
        errorResponse = error
    }
}

class NoInternetError: Error {
    
}
