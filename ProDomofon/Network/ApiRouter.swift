//
//  ApiRouter.swift
//  ProDomofon
//
//  Created by Александр Новиков on 12/06/2019.
//  Copyright © 2019 Александр Новиков. All rights reserved.
//

import Foundation
import Alamofire

enum ApiRouter: URLRequestConvertible {
    
    case authRequest(parameters: Parameters)
    case authConfirm(parameters: Parameters)
    case getUser
    case newContract(parameters: Parameters)
    case getContractInfo(idContract: String)
    case getCalculations(idContract: String)
    case getServiceRequestsByUserId(parameters: Parameters)
    case newRequest(parameters: Parameters)
    case restoreRequest(parameters: Parameters)
    case getPhones
    case addNewPhone(parameters: Parameters)
    case removePhone(parameters: Parameters)
    case getContractsKeys
    case addKeyToContracts(parameters: Parameters)
    case deleteKey(parameters: Parameters)
    case getSupportMessages(parameters: Parameters)
    case postNewMessage(data: Data)
    case subscribeOnPush(parameters: Parameters)
    case unsubscribePush(parameters: Parameters)
    case openDoor(parameters: Parameters)
    case activateService(parameters: Parameters)
    case temporaryToken(idContract: String)
    case availableRanges(idContract: String)
    case newAliasForContract(idContract: String, parameters: Parameters)
    case combineKeys
    case unlockKeys(parameters: Parameters)
    case videoArchiveRange(contractId: String, deviceId: String, panel: String)
    case contractConfirm(parameters: Parameters)
    case editComment(keyId: String, parameters: Parameters)
    case getKeyShops(parameters: Parameters)
    case hasCall(baseUrl: String, room: String, user: String)
    case acceptCall(baseUrl: String, room: String, user: String, callId: String)
    case endCall(baseUrl: String, room: String, user: String, callId: String)
    case openDoorCall(baseUrl: String, room: String, user: String, callId: String)
    case emulateCall(baseUrl: String, room: String, user: String)
    case getFacesCall(baseUrl: String, room: String, user: String)
    case getHeadsetDevices
    case cancelRequest(parameters: Parameters)
    case editContract(contractId: String, data: Data)
    case editPhonesSip(data: Data)
    case editSipConfiguration(data: Data)
    case sipHistory
    case getMediaSip(urlStr: String)
    case getCamLastPhoto(idContract: String, devicePanel: DevicePanel)
    case camRaiseMotion(parameters: Parameters)
    case authAdmin(parameters: Parameters)
    
    func baseApiUrl() -> String {
        switch self {
        case .hasCall, .acceptCall, .endCall, .openDoorCall, .emulateCall, .getFacesCall: return getBaseUrl()
        case .getMediaSip(let urlStr): return urlStr
        default: return getBaseUrl()+"/api"
        }
//        return getBaseUrl()+"/api"
        //return "https://dev-domofon.dom38.ru/api"
    }
    
    func getBaseUrl() -> String {
        switch self {
        case .hasCall(let baseUrl,_,_): return baseUrl
        case .acceptCall(let baseUrl,_,_,_): return baseUrl
        case .endCall(let baseUrl,_,_,_): return baseUrl
        case .openDoorCall(let baseUrl,_,_,_): return baseUrl
        case .emulateCall(let baseUrl,_,_): return baseUrl
        case .getFacesCall(let baseUrl, _, _): return baseUrl
        default: return StringConst.MAIN_LINK.rawValue
        }
    }
    
    func fullApiUrl() -> String {
        return self.baseApiUrl() + self.path
    }
    
    func method() -> HTTPMethod {
        switch self {
        case .getUser, .getContractInfo, .getCalculations, .getServiceRequestsByUserId, .getPhones, .getContractsKeys, .getSupportMessages, .temporaryToken, .availableRanges, .hasCall, .acceptCall, .endCall, .openDoorCall, .emulateCall, .getFacesCall, .getHeadsetDevices, .sipHistory, .getMediaSip, .getCamLastPhoto, .videoArchiveRange:
            return .get
        case .authRequest, .authConfirm, .newContract, .newRequest, .restoreRequest, .addNewPhone, .removePhone, .addKeyToContracts, .deleteKey, .postNewMessage, .subscribeOnPush, .unsubscribePush, .openDoor, .activateService, .newAliasForContract, .combineKeys, .unlockKeys, .getKeyShops, .cancelRequest, .editPhonesSip, .editSipConfiguration, .camRaiseMotion, .authAdmin, .contractConfirm:
            return .post
        case .editComment, .editContract:
            return .put
        }
    }
    
    var path: String {
        switch self {
        case .authRequest:
            return "users/request"
        case .authConfirm:
            return "users/confirm"
        case .getUser:
            return "users/me"
        case .newContract:
            return "passwords/register"
        case .getContractInfo(let idContract):
            return "contracts/information/\(idContract)"
        case .getCalculations(let idContract):
            return "contracts/calculations/\(idContract)"
        case .getServiceRequestsByUserId:
            return "calls"
        case .newRequest:
            return "calls/from-app"
        case .restoreRequest:
            return "calls/restore"
        case .getPhones:
            return "phones/allowed"
        case .addNewPhone:
            return "phones/allow"
        case .removePhone:
            return "phones/disallow"
        case .getContractsKeys:
            return "contracts/mykeys"
        case .addKeyToContracts:
            return "contracts/addkey"
        case .deleteKey:
            return "contracts/delkey"
        case .getSupportMessages:
            return "sms/my"
        case .postNewMessage:
            return "sms/incoming"
        case .subscribeOnPush:
            return "phones/subscribe"
        case .unsubscribePush:
            return "phones/unsubscribe"
        case .openDoor:
            return "contracts/opendoor"
        case .activateService:
            return "contracts/service"
        case .temporaryToken(let idContract):
            return "contracts/temporary-token/\(idContract)"
        case .availableRanges(let idContract):
            return "contracts/aliases/\(idContract)"
        case .newAliasForContract(let idContract, _):
            return "contracts/aliases/\(idContract)"
        case .combineKeys:
            return "contracts/concat-keys"
        case .unlockKeys:
            return "contracts/unlock"
        case .videoArchiveRange(let contractId, let deviceId, let panel):
            return "contracts/video-archive-range/\(contractId)/\(deviceId)/\(panel)"
        case .contractConfirm(_):
            return "contracts/confirm"
        case .editComment(let keyId, _):
            return "buttons/comment/\(keyId)"
        case .getKeyShops:
            return "keystores/closests"
        case .hasCall(_, let room, let user):
            return "call/has/\(room)/\(user)"
        case .acceptCall(_, let room, let user, let callId):
            return "call/accept/\(room)/\(user)/\(callId)"
        case .endCall(_, let room, let user, let callId):
            return "call/endcall/\(room)/\(user)/\(callId)"
        case .openDoorCall(_, let room, let user, let callId):
            return "call/opendoor/\(room)/\(user)/\(callId)"
        case .emulateCall(_, let room, let user):
            return "call/emulate/\(room)/\(user)"
        case .getFacesCall(_, let room, let user):
            return "call/faces/wait/\(room)/\(user)"
        case .getHeadsetDevices:
            return "users/get-headset-devices"
        case .cancelRequest:
            return "calls/cancel"
        case .editContract(let contractId, _):
            return "contracts/\(contractId)"
        case .editPhonesSip:
            return "phones/sip"
        case .editSipConfiguration(_):
            return "phones/sip-configuration"
        case .sipHistory:
            return "phones/sip/history"
        case .getMediaSip:
            return ""
        case .getCamLastPhoto(let idContract, let devicePanel):
            return "contracts/cam-last-photo/\(idContract)/\(devicePanel.deviceId)/\(devicePanel.panel.field)"
        case .camRaiseMotion:
            return "contracts/cam-raise-motion"
        case .authAdmin:
            return "users/login-by-phone"
        }

    }
    
    func asURLRequest() throws -> URLRequest {
        let url = try self.baseApiUrl().asURL()
        
        var urlRequest = URLRequest(url: url.appendingPathComponent(path))
        urlRequest.httpMethod = self.method().rawValue
        
        switch self {
        case .authRequest(let parameters):
            urlRequest = try URLEncoding.default.encode(urlRequest, with: parameters)
        case .authConfirm(let parameters):
            urlRequest = try URLEncoding.default.encode(urlRequest, with: parameters)
        case .newContract(let parameters):
            urlRequest = try URLEncoding.default.encode(urlRequest, with: parameters)
        case .newRequest(let parameters):
            urlRequest = try URLEncoding.default.encode(urlRequest, with: parameters)
        case .restoreRequest(let parameters):
            urlRequest = try URLEncoding.default.encode(urlRequest, with: parameters)
        case .addNewPhone(let parameters):
            urlRequest = try JSONEncoding.default.encode(urlRequest, with: parameters)
        case .removePhone(let parameters):
            urlRequest = try URLEncoding.default.encode(urlRequest, with: parameters)
        case .addKeyToContracts(let parameters):
            urlRequest = try JSONEncoding.default.encode(urlRequest, with: parameters)
        case .deleteKey(let parameters):
            urlRequest = try URLEncoding.default.encode(urlRequest, with: parameters)
        case .postNewMessage(let data):
            urlRequest.httpBody = data
        case .getServiceRequestsByUserId(let parameters):
            urlRequest = try URLEncoding.default.encode(urlRequest, with: parameters)
        case .getSupportMessages(let parameters):
            urlRequest = try URLEncoding.default.encode(urlRequest, with: parameters)
        case .subscribeOnPush(let parameters):
            urlRequest = try URLEncoding.default.encode(urlRequest, with: parameters)
        case .unsubscribePush(let parameters):
            urlRequest = try URLEncoding.default.encode(urlRequest, with: parameters)
        case .openDoor(let parameters):
            urlRequest = try URLEncoding.default.encode(urlRequest, with: parameters)
        case .activateService(let parameters):
            urlRequest = try URLEncoding.default.encode(urlRequest, with: parameters)
        case .newAliasForContract(_, let parameters):
            urlRequest = try URLEncoding.default.encode(urlRequest, with: parameters)
        case .editComment(_, let parameters):
            urlRequest = try URLEncoding.default.encode(urlRequest, with: parameters)
        case .getKeyShops(let parameters):
            urlRequest = try URLEncoding.default.encode(urlRequest, with: parameters)
        case .cancelRequest(let parameters):
            urlRequest = try URLEncoding.default.encode(urlRequest, with: parameters)
        case .editContract(_, let data):
            urlRequest.httpBody = data
        case .editPhonesSip(let data):
            urlRequest.httpBody = data
        case .editSipConfiguration(let data):
            urlRequest.httpBody = data
        case .camRaiseMotion(let parameters):
            urlRequest = try URLEncoding.default.encode(urlRequest, with: parameters)
        case .authAdmin(let parameters):
            urlRequest = try URLEncoding.default.encode(urlRequest, with: parameters)
        case .contractConfirm(let parameters):
            urlRequest = try URLEncoding.default.encode(urlRequest, with: parameters)
        case .unlockKeys(let parameters):
            urlRequest = try URLEncoding.default.encode(urlRequest, with: parameters)
        default:
            break
        }
        
        return urlRequest
    }
    
}
