//
//  RestClient.swift
//  ProDomofon
//
//  Created by Александр Новиков on 12/06/2019.
//  Copyright © 2019 Александр Новиков. All rights reserved.
//

import Foundation
import Alamofire
import Reqres
import RxSwift
import SwiftyJSON

class RestClient {
    
    static let shared = RestClient()
    var sessionManager: SessionManager
    var sessionManagerWithJson: SessionManager
    
    private init() {
        let configuration: URLSessionConfiguration
        #if DEBUG
        Reqres.logger.logLevel = .verbose
        configuration = Reqres.defaultSessionConfiguration()
        #else
        configuration = URLSessionConfiguration.default
        #endif
        configuration.httpAdditionalHeaders = SessionManager.defaultHTTPHeaders
        configuration.requestCachePolicy = .reloadIgnoringLocalCacheData
        
        self.sessionManager = SessionManager(configuration: configuration)
        self.sessionManager.adapter = MyHeadersAdapter(accessToken: nil)
        
        self.sessionManagerWithJson = SessionManager(configuration: configuration)
        self.sessionManagerWithJson.adapter = MyHeadersAdapter(accessToken: nil, flagJson: true)
    }
    
    func updateToken(newToken: String) {
        self.sessionManager.adapter = MyHeadersAdapter(accessToken: newToken)
        self.sessionManagerWithJson.adapter = MyHeadersAdapter(accessToken: newToken, flagJson: true)
    }
    
    func makeRequest(to url: URLRequestConvertible) -> DataRequest {
        return self.sessionManager.request(url)
    }
    
    func makeRequestWithJson(to url: URLRequestConvertible) -> DataRequest {
        return self.sessionManagerWithJson.request(url)
    }
    
    func makeRequestAndValidate(to url: URLRequestConvertible) -> DataRequest {
        return makeRequest(to: url).validate()
    }
    
    func makeRequestAndValidateWithJson(to url: URLRequestConvertible) -> DataRequest {
        return makeRequestWithJson(to: url).validate()
    }
    
    func authRequestWithCall(phoneNumber: String, success: @escaping () -> Void, errorCall: @escaping (ErrorBody) -> Void) {
        let params = [
            "phone" : phoneNumber,
            "call": true
        ] as Parameters
        makeRequestAndValidate(to: ApiRouter.authRequest(parameters: params)).responseJSON { (resp) in
            resp.handleResponse(onSuccess: success, onError: errorCall)
        }
    }
    
    func authRequest(phoneNumber: String, success: @escaping (AuthResponse) -> Void, errorCall: @escaping (ErrorBody) -> Void) {
        let params = [
            "phone" : phoneNumber
        ] as Parameters
        makeRequestAndValidate(to: ApiRouter.authRequest(parameters: params)).responseDecodable { resp in
            resp.handleResponse(onSuccess: success, onError: errorCall)
        }
    }
    
    func confirmAuthBySms(phoneNumber: String, call: Bool = false, code: String, success: @escaping (TokenResponse) -> Void, errorCall: @escaping (ErrorBody) -> Void) {
        var params = [
            "phone" : phoneNumber,
            "code" : code
        ] as Parameters
        if call {
            params["call"] = true
        }
        makeRequestAndValidate(to: ApiRouter.authConfirm(parameters: params)).responseDecodable { (resp) in
            resp.handleResponse(onSuccess: success, onError: errorCall)
        }
    }
    
    func getUserAndContracts(success: @escaping (User) -> Void, errorCall: @escaping (ErrorBody) -> Void) {
        makeRequestAndValidate(to: ApiRouter.getUser).responseDecodable { (resp) in
            resp.handleResponse(onSuccess: success, onError: errorCall)
        }
    }
    
    func newContract(number: String, flat: String, success: @escaping (User) -> Void, errorCall: @escaping (ErrorBody) -> Void)  {
        let params = [
            "number" : number,
            "flat" : flat
        ]
        makeRequestAndValidate(to: ApiRouter.newContract(parameters: params)).responseDecodable { (resp) in
            resp.handleResponse(onSuccess: success, onError: errorCall)
        }
    }
    
    func getPhones(success: @escaping (PhonesResponse) -> Void, errorCall: @escaping (ErrorBody) -> Void) {
        makeRequestAndValidate(to: ApiRouter.getPhones).responseDecodable { (resp) in
            resp.handleResponse(onSuccess: success, onError: errorCall)
        }
    }
    
    func addNewPhone(phoneNumber: String, contacts: [String], success: @escaping (PhonesResponse) -> Void, errorCall: @escaping (ErrorBody) -> Void) {
        let params = [
            "phone" : phoneNumber,
            "contracts" : contacts
            ] as Parameters
        makeRequestAndValidate(to: ApiRouter.addNewPhone(parameters: params)).responseDecodable { (resp) in
            resp.handleResponse(onSuccess: success, onError: errorCall)
        }
    }
    
    func removePhone(phoneNumber: String, contactID: String, success: @escaping (PhonesResponse) -> Void, errorCall: @escaping (ErrorBody) -> Void) {
        let params: Parameters = [
            "phone" : phoneNumber,
            "contractId" : contactID
            ]
        makeRequestAndValidate(to: ApiRouter.removePhone(parameters: params)).responseDecodable { (resp) in
            resp.handleResponse(onSuccess: success, onError: errorCall)
        }
    }
    
    func removePhone(phoneNumber: String, contactID: String) -> Observable<PhonesResponse> {
        return Observable<PhonesResponse>.create({ (observer) -> Disposable in
            let params: Parameters = [
                "phone" : phoneNumber,
                "contractId" : contactID
            ]
            let task =  self.makeRequestAndValidate(to: ApiRouter.removePhone(parameters: params))
            task.responseDecodable(completionHandler: { (resp: DataResponse<PhonesResponse>) in
                if let error = resp.error {
                    observer.onError(error)
                }else {
                    observer.onNext(resp.value!)
                }
                observer.onCompleted()
            })
            return Disposables.create {
                task.cancel()
            }
        })
        
    }
    
    func getKeys(success: @escaping ([ContractsKeys]) -> Void, errorCall: @escaping (ErrorBody) -> Void) {
        makeRequestAndValidate(to: ApiRouter.getContractsKeys).responseDecodable { (resp) in
            resp.handleResponse(onSuccess: success, onError: errorCall)
        }
    }
    
    func addNewKey(number: String, contacts: [String], success: @escaping () -> Void, errorCall: @escaping (ErrorBody) -> Void) {
        let params = [
            "number" : number,
            "contracts" : contacts
            ] as Parameters
        makeRequestAndValidate(to: ApiRouter.addKeyToContracts(parameters: params)).responseJSON { (resp) in
            resp.handleResponse(onSuccess: success, onError: errorCall)
        }
    }
    
    func removeKey(keyId: String, contactID: String) -> Observable<Key> {
        return Observable<Key>.create({ (observer) -> Disposable in
            let params: Parameters = [
                "buttonId" : keyId,
                "contractId" : contactID
            ]
            let task =  self.makeRequestAndValidate(to: ApiRouter.deleteKey(parameters: params))
            task.responseDecodable(completionHandler: { (resp: DataResponse<Key>) in
                if let error = resp.error {
                    observer.onError(error)
                }else {
                    observer.onNext(resp.value!)
                }
                observer.onCompleted()
            })
            return Disposables.create {
                task.cancel()
            }
        })        
    }
    
    func getRequests(userId: String, success: @escaping ([ServiceRequest]) -> Void, errorCall: @escaping (ErrorBody) -> Void) {
        let params: Parameters = [
            "userId" : userId
        ]
        makeRequestAndValidate(to: ApiRouter.getServiceRequestsByUserId(parameters: params)).responseDecodable { (resp) in
            resp.handleResponse(onSuccess: success, onError: errorCall)
        }
    }
    
    func createRequest(contractId: String, comment: String, success: @escaping (ServiceRequest) -> Void, errorCall: @escaping (ErrorBody) -> Void) {
        let params: Parameters = [
            "contractId" : contractId,
            "comment" : comment
        ]
        makeRequestAndValidate(to: ApiRouter.newRequest(parameters: params)).responseDecodable { (resp) in
            resp.handleResponse(onSuccess: success, onError: errorCall)
        }
    }
    
    func restoreRequest(requestId: String, comment: String, success: @escaping (ServiceRequest) -> Void, errorCall: @escaping (ErrorBody) -> Void) {
        let params: Parameters = [
            "_id" : requestId,
            "comment" : comment
        ]
        makeRequestAndValidate(to: ApiRouter.restoreRequest(parameters: params)).responseDecodable { (resp) in
            resp.handleResponse(onSuccess: success, onError: errorCall)
        }
    }
    
    func getSupportMessages(offset: Int, limit: Int, success: @escaping (SupportMessageResponse) -> Void, errorCall: @escaping (ErrorBody) -> Void)  {
        let params: Parameters = [
            "offset" : offset,
            "limit" : limit
        ]
        makeRequestAndValidate(to: ApiRouter.getSupportMessages(parameters: params)).responseDecodable { (resp) in
            resp.handleResponse(onSuccess: success, onError: errorCall)
        }
    }
    
    func postNewMessage(_ newMessage: NewMessage, success: @escaping (SupportMessage) -> Void, errorCall: @escaping (ErrorBody) -> Void) {
        do {
            let data = try newMessage.jsonData()
            makeRequestAndValidateWithJson(to: ApiRouter.postNewMessage(data: data)).responseDecodable(completionHandler: { (resp) in
                resp.handleResponse(onSuccess: success, onError: errorCall)
            })
        } catch  {
            NSLog(error.localizedDescription)
        }
    }
    
    func subscribeOnPush(token: String, callType: Bool, success: @escaping () -> Void, errorCall: @escaping (ErrorBody) -> Void) {
        var params: Parameters = [
            "token" : token
        ]
        if UIApplication.shared.appConfiguration == .Debug {
            params["dev"] = true
        }
        if callType {
            params["type"] = "voip"
        }
        makeRequestAndValidate(to: ApiRouter.subscribeOnPush(parameters: params)).responseJSON { (resp) in
            resp.handleResponse(onSuccess: success, onError: errorCall)
        }
    }
    
    func unsubscribePush(token: String, success: @escaping () -> Void, errorCall: @escaping (ErrorBody) -> Void) {
        var params: Parameters = [
            "token" : token
        ]
        makeRequestAndValidate(to: ApiRouter.unsubscribePush(parameters: params)).responseJSON { (resp) in
            resp.handleResponse(onSuccess: success, onError: errorCall)
        }
    }
    
    enum ReasonOpenDoor: String {
        case widget, contracts, device, between, ble
    }
    
    func openDoor(contractNumber: Int, reason: ReasonOpenDoor, device: String = "", panel: String = "", success: @escaping (OpenDoorResponse) -> Void, errorCall: @escaping (ErrorBody) -> Void) {
        var params = [
            "contract" : contractNumber,
            "reason" : reason.rawValue
            ] as Parameters
        if device != "" {
            params["device"] = device
        }
        if panel != "" {
            params["panel"] = panel
        }
        
        makeRequestAndValidate(to: ApiRouter.openDoor(parameters: params)).responseDecodable { (resp) in
            resp.handleResponse(onSuccess: success, onError: errorCall)
        }
    }
    
    func readyOpenDoor(contractNumber: Int, uuid: String, success: @escaping (OpenDoorResponse) -> Void, errorCall: @escaping (ErrorBody) -> Void) {
        var params = [
            "contract" : contractNumber,
            "entrance" : uuid
            ] as Parameters
        print(params)
        makeRequestAndValidate(to: ApiRouter.openDoor(parameters: params)).responseDecodable { (resp) in
            resp.handleResponse(onSuccess: success, onError: errorCall)
        }
    }
    
    func getCalculations(for contractID: String, success: @escaping (CalculationsObject) -> Void, errorCall: @escaping (ErrorBody) -> Void) {
        makeRequestAndValidate(to: ApiRouter.getCalculations(idContract: contractID)).responseDecodable { (resp) in
            resp.handleResponse(onSuccess: success, onError: errorCall)
        }
    }
    
    func getContractInfo(for contractID: String, success: @escaping (InfoResponse) -> Void, errorCall: @escaping (ErrorBody) -> Void) {
        makeRequestAndValidate(to: ApiRouter.getContractInfo(idContract: contractID)).responseDecodable { (resp) in
            resp.handleResponse(onSuccess: success, onError: errorCall)
        }
    }
    
    func activateService(for contractId: String, serviceId: String, groupId: String?, activate: Bool, success: @escaping ([RegularService]) -> Void, errorCall: @escaping (ErrorBody) -> Void) {
        let INCLUDE = "include"
        let EXCLUDE = "exclude"
        var params: Parameters = [
            "contractId" : contractId,
            "serviceId" : serviceId,
            "type" : activate ? INCLUDE : EXCLUDE
        ]
        if let groupId = groupId {
            params["groupId"] = groupId
        }
        makeRequestAndValidate(to: ApiRouter.activateService(parameters: params)).responseDecodable { (resp) in
            resp.handleResponse(onSuccess: success, onError: errorCall)
        }
    }
    
    func requestTemporaryToken(for contractId: String, success: @escaping (TemporaryToken) -> Void, errorCall: @escaping (ErrorBody) -> Void) {
        makeRequestAndValidate(to: ApiRouter.temporaryToken(idContract: contractId)).responseDecodable { (resp) in
            resp.handleResponse(onSuccess: success, onError: errorCall)
        }
    }
    
    func requestAvailableRanges(for contractId: String, success: @escaping (RangesResponse) -> Void, errorCall: @escaping (ErrorBody) -> Void) {
        makeRequestAndValidate(to: ApiRouter.availableRanges(idContract: contractId)).responseDecodable { (resp) in
            resp.handleResponse(onSuccess: success, onError: errorCall)
        }
    }
    
    func newAliasForContract(contractId: String, newAlias: Int, success: @escaping (Contract) -> Void, errorCall: @escaping (ErrorBody) -> Void) {
        let params: Parameters = [
            "alias" : newAlias
        ]
        makeRequestAndValidate(to: ApiRouter.newAliasForContract(idContract: contractId, parameters: params)).responseDecodable { (resp) in
            resp.handleResponse(onSuccess: success, onError: errorCall)
        }
    }
    
    func combineKeys(success: @escaping () -> Void, errorCall: @escaping (ErrorBody) -> Void) {
        makeRequestAndValidate(to: ApiRouter.combineKeys).responseJSON { (resp) in
            resp.handleResponse(onSuccess: success, onError: errorCall)
        }
    }
    
    func videoArchiveRange(contractId: String, deviceId: String, panel: String,success: @escaping ([VideoHistory]) -> Void, errorCall: @escaping (ErrorBody) -> Void ) {
        makeRequestAndValidate(to: ApiRouter.videoArchiveRange(contractId: contractId, deviceId: deviceId, panel: panel)).responseJSON { (response) in
            switch response.result {
            case .failure(let error):
                errorCall(ErrorBody(name: "Unknown error", message: error.localizedDescription, code: nil, status: nil))
                break
            case .success(let data):
                let json = JSON(data)
                if let array = json.array{
                    let videoHistories = array.map { VideoHistory.init(json: $0) }
                    success(videoHistories)
                } else {
                    errorCall(ErrorBody(name: "Ошибка парсинга", message: "Ошибка парсинга объектов VideoHistory", code: nil, status: nil))
                }
                break
            }
        }
    }
    
    func contractConfirm(contractId: String, code: String,success: @escaping () -> Void, errorCall: @escaping (ErrorBody) -> Void ) {
        let params: Parameters = [
            "device" : code,
            "id": contractId
        ]
        makeRequestAndValidate(to: ApiRouter.contractConfirm(parameters: params)).responseJSON { (resp) in
            resp.handleResponse(onSuccess: success, onError: errorCall)
        }
    }
    
    func editKeyComment(keyId: String, comment: String, success: @escaping (Key) -> Void, errorCall: @escaping (ErrorBody) -> Void) {
        let params: Parameters = [
            "comment" : comment
        ]
        makeRequestAndValidate(to: ApiRouter.editComment(keyId: keyId, parameters: params)).responseDecodable { (resp) in
            resp.handleResponse(onSuccess: success, onError: errorCall)
        }
    }
    
    func unlockKeys(number: Int, keys: [String], success: @escaping () -> Void, errorCall: @escaping (ErrorBody) -> Void) {
        let params: Parameters = [
            "number" : number,
            "buttons" : keys.joined(separator: " ")
        ]
        makeRequestAndValidate(to: ApiRouter.unlockKeys(parameters: params)).responseJSON { (resp) in
            resp.handleResponse(onSuccess: success, onError: errorCall)
        }
    }
    
    func getKeyShops(houseId: Int, success: @escaping ([KeyShop]) -> Void, errorCall: @escaping (ErrorBody) -> Void) {
        let params: Parameters = [
            "houseId" : houseId
        ]
        makeRequestAndValidate(to: ApiRouter.getKeyShops(parameters: params)).responseDecodable { (resp) in
            resp.handleResponse(onSuccess: success, onError: errorCall)
        }
    }
    
    func hasCall(baseUrl: String, room: String, user: String, success: @escaping (CallResponse) -> Void, errorCall: @escaping (ErrorBody) -> Void) {
        makeRequestAndValidate(to: ApiRouter.hasCall(baseUrl: baseUrl, room: room, user: user)).responseDecodable { (resp) in
            resp.handleResponse(onSuccess: success, onError: errorCall)
        }
    }
    
    func acceptCall(baseUrl: String, room: String, user: String, callId: String, success: @escaping (CallResponse) -> Void, errorCall: @escaping (ErrorBody) -> Void) {
        makeRequestAndValidate(to: ApiRouter.acceptCall(baseUrl: baseUrl, room: room, user: user, callId: callId)).responseDecodable { (resp) in
            resp.handleResponse(onSuccess: success, onError: errorCall)
        }
    }
    
    func endCall(baseUrl: String, room: String, user: String, callId: String, success: @escaping (CallResponse) -> Void, errorCall: @escaping (ErrorBody) -> Void) {
        makeRequestAndValidate(to: ApiRouter.endCall(baseUrl: baseUrl, room: room, user: user, callId: callId)).responseDecodable { (resp) in
            resp.handleResponse(onSuccess: success, onError: errorCall)
        }
    }
    
    func openDoorCall(baseUrl: String, room: String, user: String, callId: String, success: @escaping (CallResponse) -> Void, errorCall: @escaping (ErrorBody) -> Void) {
        makeRequestAndValidate(to: ApiRouter.openDoorCall(baseUrl: baseUrl, room: room, user: user, callId: callId)).responseDecodable { (resp) in
            resp.handleResponse(onSuccess: success, onError: errorCall)
        }
    }
    
    func emulateCall(baseUrl: String, room: String, user: String, success: @escaping (CallResponse) -> Void, errorCall: @escaping (ErrorBody) -> Void) {
        makeRequestAndValidate(to: ApiRouter.emulateCall(baseUrl: baseUrl, room: room, user: user)).responseDecodable { (resp) in
            resp.handleResponse(onSuccess: success, onError: errorCall)
        }
    }
    
    func getFacesCall(baseUrl: String, room: String, user: String, success: @escaping (Data) -> Void, errorCall: @escaping (ErrorBody) -> Void) -> DataRequest {
        let req = makeRequestAndValidate(to: ApiRouter.getFacesCall(baseUrl: baseUrl, room: room, user: user))
        req.responseJSON { (response) in
            if let _ = response.error {
                response.handleError(response, callback: errorCall)
            } else {
                success(response.data!)
            }
        }
        return req
    }
    
    func getHeadsetDevices(success: @escaping ([Install]) -> Void, errorCall: @escaping (ErrorBody) -> Void) {
        makeRequestAndValidate(to: ApiRouter.getHeadsetDevices).responseDecodable { (resp) in
            resp.handleResponse(onSuccess: success, onError: errorCall)
        }
    }
    
    func createRequest(contractId: String, deviceId: Int, success: @escaping (ServiceRequest) -> Void, errorCall: @escaping (ErrorBody) -> Void) {
        let params: Parameters = [
            "contractId" : contractId,
            "typeId": 2,
            "deviceId" : deviceId
        ]
        makeRequestAndValidate(to: ApiRouter.newRequest(parameters: params)).responseDecodable { (resp) in
            resp.handleResponse(onSuccess: success, onError: errorCall)
        }
    }
    
    func cancelRequest(callId: Int, success: @escaping (ServiceRequest) -> Void, errorCall: @escaping (ErrorBody) -> Void) {
        let params: Parameters = [
            "id" : callId
        ]
        makeRequestAndValidate(to: ApiRouter.cancelRequest(parameters: params)).responseDecodable { (resp) in
            resp.handleResponse(onSuccess: success, onError: errorCall)
        }
    }
    
    func editContract(contractId: String, data: Data, success: @escaping (Contract) -> Void, errorCall: @escaping (ErrorBody) -> Void) {
        makeRequestAndValidateWithJson(to: ApiRouter.editContract(contractId: contractId, data: data)).responseDecodable { (resp) in
            resp.handleResponse(onSuccess: success, onError: errorCall)
        }
    }
    
    func editPhoneSip(data: Data, success: @escaping () -> Void, errorCall: @escaping (ErrorBody) -> Void) {
        makeRequestAndValidateWithJson(to: ApiRouter.editPhonesSip(data: data)).responseJSON { (resp) in
            resp.handleResponse(onSuccess: success, onError: errorCall)
        }
    }
    
    func editSipConfiguration(phone: Phone, success: @escaping (Phone) -> Void, errorCall: @escaping (ErrorBody) -> Void) {
        if let data = phone.sipConfigurationData {
            makeRequestAndValidateWithJson(to: ApiRouter.editSipConfiguration(data: data)).responseDecodable { (resp) in
                resp.handleResponse(onSuccess: success, onError: errorCall)
            }
        } else {
            errorCall(ErrorBody(name: "Ошибка", message: "Ошибка при преобразовании объекта Phone в json", code: nil, status: nil))
        }
    }
    
    func sipHistory(success: @escaping ([SipHistory]) -> Void, errorCall: @escaping (ErrorBody) -> Void) {
        makeRequestAndValidate(to: ApiRouter.sipHistory).responseJSON { (response) in
            switch response.result {
            case .failure(let error):
                errorCall(ErrorBody(name: "Unknown error", message: error.localizedDescription, code: nil, status: nil))
                print("Failed Request \(error.localizedDescription))")
                break
            case .success(let data):
                let json = JSON(data)
                if let array = json.array{
                    let sipHistories = array.map { SipHistory.init(json: $0) }
                    success(sipHistories)
                } else {
                    errorCall(ErrorBody(name: "Ошибка парсинга", message: "Ошибка парсинга объектов SipHistory", code: nil, status: nil))
                    print("Ошибка парсинга объектов SipHistory")
                }
                break
            }
        }
    }
    
    func getMediaSip(url: URL, success: @escaping (Data) -> Void, errorCall: @escaping (ErrorBody) -> Void) {
        makeRequestAndValidate(to: ApiRouter.getMediaSip(urlStr: url.absoluteString)).responseData { (response) in
            switch response.result {
            case .failure(let error):
                errorCall(ErrorBody(name: "Unknown error", message: error.localizedDescription, code: nil, status: nil))
                print("Failed Request \(error.localizedDescription))")
                break
            case .success(let data):
                print("image downloaded: \(data)")
                success(data)
                break
            }
        }
    }
    
    
    func getCamLastPhoto(idContract: String, devicePanel: DevicePanel, success: @escaping (Data) -> Void, errorCall: @escaping (ErrorBody) -> Void) {
        makeRequestAndValidate(to: ApiRouter.getCamLastPhoto(idContract: idContract, devicePanel: devicePanel)).responseData { (response) in
            switch response.result {
            case .failure(let error):
                errorCall(ErrorBody(name: "Unknown error", message: error.localizedDescription, code: nil, status: nil))
                print("Failed Request \(error.localizedDescription))")
                break
            case .success(let data):
                print("image downloaded: \(data)")
                success(data)
                break
            }
        }
    }
    
    func camRaiseMotion(idContract: String, deviceId: String, panel: String, success: @escaping () -> Void, errorCall: @escaping (ErrorBody) -> Void) {
        let params = [
            "id" : idContract,
            "deviceId" : deviceId,
            "panel": panel
        ] as Parameters
        makeRequestAndValidate(to: ApiRouter.camRaiseMotion(parameters: params) ).responseJSON { (resp) in
            resp.handleResponse(onSuccess: success, onError: errorCall)
        }
    }
    
    
    func authAdmin(phoneNumber: String, success: @escaping (TokenResponse) -> Void, errorCall: @escaping (ErrorBody) -> Void) {
        let params = [
            "phone" : phoneNumber
        ] as Parameters
        makeRequestAndValidate(to: ApiRouter.authAdmin(parameters: params)).responseDecodable { resp in
            resp.handleResponse(onSuccess: success, onError: errorCall)
        }
    }
}

extension DataResponse {
    
    func handleResponse(onSuccess: @escaping (Value) -> Void, onError: @escaping (ErrorBody) -> Void) {
        if let _ = self.error {
            handleError(self, callback: onError)
        }else {
            onSuccess(self.value!)
        }
    }
    
    func handleResponse(onSuccess: @escaping () -> Void, onError: @escaping (ErrorBody) -> Void) {
        if let _ = self.error {
            handleError(self, callback: onError)
        }else {
            onSuccess()
        }
    }
    
    func handleError<T>(_ data: DataResponse<T>, callback: @escaping (ErrorBody) -> Void) {
        guard let response = data.response else {
            callback(ErrorBody(name: "Unknown error", message: data.error!.localizedDescription, code: nil, status: nil))
            print("Failed Request \(data.error!.localizedDescription))");
            return
        }
        NSLog("Failed Request to: \(String(describing: response.url!))")
        let err = data.error!
        do {
            let error = try ErrorBody(data: data.data!)
            if error.status == nil {
                error.status = response.statusCode
            }
            if let typeMismatch = err as? DecodingError {
                error.message = typeMismatch.localizedDescription
            }
            NSLog((try error.jsonString())!)
            debugPrint(err)
            callback(error)
        }catch {
            guard let _ = String(data: data.data!, encoding: .utf8) else {
                NSLog(err.localizedDescription)
                callback(ErrorBody(name: "Unknown error", message: err.localizedDescription, code: nil, status: response.statusCode))
                return
            }
            debugPrint(error)
            callback(ErrorBody(name: "Unknown error", message: err.localizedDescription, code: nil, status: response.statusCode))
        }
    }
}

extension DefaultDataResponse {
    
    
    func handleResponse(onSuccess: @escaping () -> Void, onError: @escaping (ErrorBody) -> Void) {
        if let _ = self.error {
            handleError(self, callback: onError)
        }else {
            onSuccess()
        }
    }
    
    func handleError(_ response: DefaultDataResponse, callback: @escaping (ErrorBody) -> Void) {
        guard let responseData = response.data else {
            callback(ErrorBody(name: "Unknown error", message: response.error!.localizedDescription, code: nil, status: nil))
            print("Failed Request \(response.error!.localizedDescription))");
            return
        }
        do {
            NSLog("Failed Request to: \(String(describing: response.request?.url))")
            if let errorText = String(data: responseData, encoding: String.Encoding.utf8) {
                NSLog("CAUSE -> \(errorText)")
                let error = try ErrorBody(errorText, using: String.Encoding.utf8)
                if error.status == nil {
                    error.status = response.response?.statusCode
                }
                callback(error)
            }else {
                callback(ErrorBody(name: "Unknown error", message: response.error!.localizedDescription, code: nil, status: response.response?.statusCode))
            }
            
        }catch {
            debugPrint(error)
            callback(ErrorBody(name: "Unknown error", message: response.error!.localizedDescription, code: nil, status: response.response?.statusCode))
        }
        
        
    }
}

// MARK: - Alamofire response handlers

extension DataRequest {
    func decodableResponseSerializer<T: Decodable>() -> DataResponseSerializer<T> {
        return DataResponseSerializer { _, response, data, error in
            guard error == nil else { return .failure(error!) }
            
            guard let data = data else {
                return .failure(AFError.responseSerializationFailed(reason: .inputDataNil))
            }
            
            return Result { try JSONDecoder().decode(T.self, from: data) }
        }
    }
    
    @discardableResult
    func responseDecodable<T: Decodable>(queue: DispatchQueue? = nil, completionHandler: @escaping (DataResponse<T>) -> Void) -> Self {
        return response(queue: queue, responseSerializer: decodableResponseSerializer(), completionHandler: completionHandler)
    }
}
