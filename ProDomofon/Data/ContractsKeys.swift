//
//  ContractsKeys.swift
//  ProDomofon
//
//  Created by Александр Новиков on 16/08/2019.
//  Copyright © 2019 Александр Новиков. All rights reserved.
//

import Foundation

class ContractsKeys: Codable {
    
    var contractId: String
    var contractNumber: Int
    var address: String?
    var flat: String
    var keys: [Key]
    
    enum CodingKeys: String, CodingKey {
        case contractId = "_id"
        case keys
        case contractNumber = "number"
        case address, flat
    }
    
    init(contractId: String, contractNumber: Int, address: String?, flat: String, keys: [Key]) {
        self.contractId = contractId
        self.contractNumber = contractNumber
        self.address = address
        self.flat = flat
        self.keys = keys
    }
    
    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(contractId, forKey: .contractId)
        try container.encode(contractNumber, forKey: .contractNumber)
        if let address = address {
            try container.encode(address, forKey: .address)
        }
        try container.encode(flat, forKey: .flat)
        try container.encode(keys, forKey: .keys)
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        contractId = try container.decode(String.self, forKey: .contractId)
        contractNumber = try container.decode(Int.self, forKey: .contractNumber)
        do {
            address = try container.decode(String.self, forKey: .address)
        } catch {
            address = nil
        }
        flat = try container.decode(String.self, forKey: .flat)
        keys = try container.decode([Key].self, forKey: .keys)
    }
}


class Key: Codable {
    
    var id: String
    var contractId: String?
    var number: String
    var comment: String?
    var attached: Bool
    var removed: Bool
    
    enum CodingKeys: String, CodingKey {
        case id = "_id"
        case number, comment, attached, removed, contractId
    }
    
    init(id: String, number: String, comment: String?, attached: Bool, removed: Bool, contractId: String?) {
        self.id = id
        self.number = number
        self.comment = comment
        self.attached = attached
        self.removed = removed
        self.contractId = contractId
    }
    
    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(id, forKey: .id)
        try container.encode(number, forKey: .number)
        try container.encode(comment, forKey: .comment)
        try container.encode(attached, forKey: .attached)
        try container.encode(removed, forKey: .removed)
        try container.encode(contractId, forKey: .contractId)
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        id = try container.decode(String.self, forKey: .id)
        number = try container.decode(String.self, forKey: .number)
        comment = try container.decodeIfPresent(String.self, forKey: .comment)
        attached = try container.decode(Bool.self, forKey: .attached)
        removed = try container.decode(Bool.self, forKey: .removed)
        contractId = try container.decodeIfPresent(String.self, forKey: .contractId)
    }
}
