//
//  OpenDoorResponse.swift
//  ProDomofon
//
//  Created by Александр Новиков on 29/09/2019.
//  Copyright © 2019 Александр Новиков. All rights reserved.
//

import Foundation

class OpenDoorResponse: Codable {
    
    var type, message, paymentUrl: String?
    var code: Int?
    
    init(type: String?, message: String?, code: Int?, paymentUrl: String?) {
        self.type = type
        self.message = message
        self.code = code
        self.paymentUrl = paymentUrl
    }
    
    func isSuccess() -> Bool {
        return type?.elementsEqual("success") ?? false
    }
}

// MARK: Convenience initializers and mutators

extension OpenDoorResponse {
    convenience init(data: Data) throws {
        let me = try JSONDecoder().decode(OpenDoorResponse.self, from: data)
        self.init(type: me.type, message: me.message, code: me.code, paymentUrl: me.paymentUrl)
    }
    
    convenience init(_ json: String, using encoding: String.Encoding = .utf8) throws {
        guard let data = json.data(using: encoding) else {
            throw NSError(domain: "JSONDecoding", code: 0, userInfo: nil)
        }
        try self.init(data: data)
    }
    
    convenience init(fromURL url: URL) throws {
        try self.init(data: try Data(contentsOf: url))
    }
    
    
    func jsonData() throws -> Data {
        return try newJSONEncoder().encode(self)
    }
    
    func jsonString(encoding: String.Encoding = .utf8) throws -> String? {
        return String(data: try self.jsonData(), encoding: encoding)
    }
}
