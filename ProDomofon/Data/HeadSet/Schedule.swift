//
//  Schedule.swift
//  ProDomofon
//
//  Created by Алексей Агильдин on 18.02.2021.
//  Copyright © 2021 Александр Новиков. All rights reserved.
//

import Foundation
import UIKit

class Schedule: Codable {
    
    var id: String?
    var start: Int
    var end: Int
    var enabled: Bool
    var dow: [Int]
    
    enum CodingKeys: String, CodingKey {
        case id = "_id"
        case start, end, enabled, dow
    }
    
    init() {
        self.start = 0
        self.end = 24 * 60 * 60 - 1
        self.enabled = true
        self.dow = []
    }
    
    init(id: String?, start: Int, end: Int, enabled: Bool, dow: [Int]) {
        self.id = id
        self.start = start
        self.end = end
        self.enabled = enabled
        self.dow = dow
    }
}
extension Schedule {
    // return count of minutes by value of seconds
    fileprivate func getMinutes(value: Int) -> Int {
        return (value / 60) % 60
    }
    
    // return count of hours by value of seconds
    fileprivate func getHours(value: Int) -> Int {
        return value / (60 * 60)
    }
    
    // return time text, like "Время: 11:00 - 19:00"
    var timeText: String {
        return "Время: \(startText) - \(endText)"
    }
    
    // return string value by adding 0 in begining for format "00", "01", "10"
    fileprivate func getString(from intValue: Int) -> String {
        if intValue >= 10 {
            return "\(intValue)"
        } else {
            return "0\(intValue)"
        }
    }
    
    // return start time text, like "11:00"
    var startText: String {
        let startHours: Int = getHours(value: start)
        let startMinutes: Int = getMinutes(value: start)
        return "\(getString(from: startHours)):\(getString(from: startMinutes))"
    }
    
    // return end time text, like "19:00"
    var endText: String {
        let endHours: Int = getHours(value: end)
        let endMinutes: Int = getMinutes(value: end)
        return "\(getString(from: endHours)):\(getString(from: endMinutes))"
    }
    
    // return date from int value by value of seconds
    fileprivate func date(value: Int) -> Date {
        return Calendar.current.date(bySettingHour: getHours(value: value), minute: getMinutes(value: value), second: 0, of: Date())!
    }
    
    // return date from int value of seconds start
    var startDate: Date {
        return date(value: start)
    }
    
    // return date from int value of seconds end
    var endDate: Date {
        return date(value: end)
    }
    
    // arrays of days
    static var arrayDays: [String] = ["Пн", "Вт", "Ср", "Чт", "Пт", "Сб", "Вс"]
    
    // attributed string for showing
    var daysAttributedText: NSMutableAttributedString {
        let daysString = Schedule.arrayDays.joined(separator: " ")
        let attributedText = NSMutableAttributedString(string: daysString)
        let fullRange = NSMakeRange(0, attributedText.length)
        
        attributedText.addAttribute(NSAttributedString.Key.foregroundColor, value: Colors.BLACK, range: fullRange)
        attributedText.addAttribute(NSAttributedString.Key.font, value: UIFont.systemFont(ofSize: 16), range: fullRange)
        
        dow.forEach { (d) in
            let range = attributedText.mutableString.range(of: Schedule.arrayDays[d])
            attributedText.addAttribute(NSAttributedString.Key.foregroundColor, value: Colors.GREEN_PRIMARY, range: range)
            attributedText.addAttribute(NSAttributedString.Key.font, value: UIFont.boldSystemFont(ofSize: 16), range: range)
        }
        
        return attributedText
    }
    
    var maximumStartDate: Date {
        let secondsVal = 24 * 60 * 60 - 60 - 1
        return date(value: secondsVal)
    }
    
    var minimumEndDate: Date {
        return date(value: 60)
    }
}
extension Schedule {
    func editDow(value: Int) {
        if let index = dow.firstIndex(where: { $0 == value }) {
            dow.remove(at: index)
        } else {
            dow.append(value)
        }
    }
    
    func editStart(value: Int) {
        start = value
    }
    
    func editEnd(value: Int) {
        end = value
    }
    
    func enableChanged(state: Bool) {
        enabled = state
    }
}
