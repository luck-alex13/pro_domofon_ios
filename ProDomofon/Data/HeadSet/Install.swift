//
//  Install.swift
//  ProDomofon
//
//  Created by Алексей Агильдин on 18.02.2021.
//  Copyright © 2021 Александр Новиков. All rights reserved.
//

import Foundation

class Install: Codable {
    
    var id: Int
    var publish: Int
    var type: Int
    var amount: Int
    var replaceAmount: Int
    var name: String
    var desc: String
    var image: String
    var url: String
    var useDefault: Bool
    var ordering: Int
    var contracts: [InstallContract]
    
    enum CodingKeys: String, CodingKey {
        case id = "_id"
        case publish, type, amount, replaceAmount, name, desc, image, url, useDefault, ordering, contracts
    }
    
    init(id: Int, publish: Int, type: Int, amount: Int, replaceAmount: Int, name: String, desc: String, image: String, url: String, useDefault: Bool, ordering: Int, contracts: [InstallContract]) {
        self.id = id
        self.publish = publish
        self.type = type
        self.amount = amount
        self.replaceAmount = replaceAmount
        self.name = name
        self.desc = desc
        self.image = image
        self.url = url
        self.useDefault = useDefault
        self.ordering = ordering
        self.contracts = contracts
    }
}

class InstallContract: Codable {
    
    var amount: Int
    var number: Int
    var contractId: String
    var enabled: Bool
    var call: InstallCall?
    
    enum CodingKeys: String, CodingKey {
        case amount, number, contractId, enabled, call
    }
    
    init(amount: Int, number: Int, contractId: String, enabled: Bool, call: InstallCall?) {
        self.amount = amount
        self.number = number
        self.contractId = contractId
        self.enabled = enabled
        self.call = call
    }
}

class InstallCall: Codable {
    
    var amount: Int
    var callId: Int
    var created: String
    var deviceId: Int
    var paid: Int
    
    enum CodingKeys: String, CodingKey {
        case amount, callId, created, deviceId, paid
    }
    
    init(amount: Int, callId: Int, created: String, deviceId: Int, paid: Int) {
        self.amount = amount
        self.callId = callId
        self.created = created
        self.deviceId = deviceId
        self.paid = paid
    }
}
