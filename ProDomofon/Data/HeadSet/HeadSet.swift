//
//  HeadSet.swift
//  ProDomofon
//
//  Created by Алексей Агильдин on 11.03.2021.
//  Copyright © 2021 Александр Новиков. All rights reserved.
//

import Foundation

class HeadSet: Codable {
    
    var always: Bool = false
    var schedules: [Schedule]
    
    enum CodingKeys: String, CodingKey {
        case always, schedules
    }
    
    init(always: Bool, schedules: [Schedule]) {
        self.always = always
        self.schedules = schedules
    }
}
extension HeadSet {
    
    var data: Data? {
        var sc: [Any] = []
        schedules.forEach { (s) in
            var dow: [Int] = []
            s.dow.forEach { (d) in
                dow.append(d)
            }
            let sData: [String: Any] = [
                "_id": s.id ?? "",
                "start": s.start,
                "end": s.end,
                "enabled": s.enabled,
                "dow": dow
            ]
            sc.append(sData)
        }
        
        let dict: [String: Any] = [
            "headset": [
                "always": always,
                "schedules": sc
            ]
        ]
        
        do {
            let jsonData = try JSONSerialization.data(withJSONObject: dict, options: [])
            let jsonString = String(data: jsonData, encoding: String.Encoding.ascii)!
            print("headSet: \(jsonString)")
            return jsonData
        } catch {
            NSLog(error.localizedDescription)
            return nil
        }
    }
    
}
