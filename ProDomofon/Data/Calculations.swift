//
//  Calculations.swift
//  ProDomofon
//
//  Created by Александр Новиков on 16/10/2019.
//  Copyright © 2019 Александр Новиков. All rights reserved.
//

import Foundation

class Calculation: Codable {
    
    var id: String
    var comment: String
    var dtime: String
    var amount: Float
    var contractId: String
    
    enum CodingKeys: String, CodingKey {
        case id, comment, dtime, amount, contractId
    }
    
    init(id: String, comment: String, dtime: String, amount: Float, contractId: String) {
        self.id = id
        self.comment = comment
        self.dtime = dtime
        self.amount = amount
        self.contractId = contractId
    }
    
    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(id, forKey: .id)
        try container.encode(comment, forKey: .comment)
        try container.encode(dtime, forKey: .dtime)
        try container.encode(amount, forKey: .amount)
        try container.encode(contractId, forKey: .contractId)
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        id = try container.decode(String.self, forKey: .id)
        comment = try container.decode(String.self, forKey: .comment)
        dtime = try container.decode(String.self, forKey: .dtime)
        amount = try container.decode(Float.self, forKey: .amount)
        contractId = try container.decode(String.self, forKey: .contractId)
    }
}

class CalculationsObject: Codable {
    
    var calculations: [Calculation]
    var invoices: [Calculation]
}
