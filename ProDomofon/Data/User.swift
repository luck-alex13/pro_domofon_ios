//
//  User.swift
//  ProDomofon
//
//  Created by Александр Новиков on 16/06/2019.
//  Copyright © 2019 Александр Новиков. All rights reserved.
//

import Foundation

class User: Codable {
    
    var id: String
    var phone: String
    var clientId: Int
    var confirmed: Bool
    var role: String
    var contracts: [Contract]
    
    enum CodingKeys: String, CodingKey {
        case id = "_id"
        case phone, clientId, confirmed, role, contracts
    }
    
    init(id: String, phone: String, clientId: Int, confirmed: Bool, role: String, contracts: [Contract]) {
        self.id = id
        self.phone = phone
        self.clientId = clientId
        self.confirmed = confirmed
        self.role = role
        self.contracts = contracts
    }
    
    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(id, forKey: .id)
        try container.encode(phone, forKey: .phone)
        try container.encode(clientId, forKey: .clientId)
        try container.encode(confirmed, forKey: .confirmed)
        try container.encode(role, forKey: .role)
        try container.encode(contracts, forKey: .contracts)
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        id = try container.decode(String.self, forKey: .id)
        phone = try container.decode(String.self, forKey: .phone)
        clientId = try container.decode(Int.self, forKey: .clientId)
        confirmed = try container.decode(Bool.self, forKey: .confirmed)
        role = try container.decode(String.self, forKey: .role)
        contracts = try container.decode([Contract].self, forKey: .contracts)
    }
    
    
}
extension User {
    var isAdmin: Bool {
        return role == "admin"
    }
}
