//
//  VideoCall.swift
//  ProDomofon
//
//  Created by Алексей Агильдин on 08.12.2020.
//  Copyright © 2020 Александр Новиков. All rights reserved.
//

import Foundation
import SwiftyJSON

class Video {
    let server: String
    let device: String
    let panel: String
    let user: String
    
    init(json: JSON) {
        server = json["server"].stringValue
        device = json["device"].stringValue
        panel = json["panel"].stringValue
        user = json["user"].stringValue
    }
    
    init(server: String, device: String, panel: String, user: String){
        self.server = server
        self.device = device
        self.panel = panel
        self.user = user
    }
}

class VideoCall {
    let user: String
    let password: String
    let host: String
    let socketUri: String
    let address: String
    let callId: String
    let room: String
    let port: Int
    let hash: String
    let bannerImageURL: URL?
    let backgroundImageUrl: URL?
    var images: [URL]
    let videoStreamUrl: URL?
    let video: Video?
    
    init(json: JSON) {
        user = json["user"].stringValue
        password = json["password"].stringValue
        host = json["host"].stringValue
        socketUri = json["socketUri"].stringValue
        address = json["address"].stringValue
        callId = json["call_id"].stringValue
        room = json["room"].stringValue
        port = json["port"].intValue
        hash = json["hash"].stringValue
        bannerImageURL = json["bannerImageUrl"].url
        backgroundImageUrl = json["backgroundImageUrl"].url
        images = []
        if let imagesJson = json["images"].array {
            for im in imagesJson {
                if let url = im.url {
                    images.append(url)
                }
            }
        }
        videoStreamUrl = json["videoStreamUrl"].url
        video = Video.init(json: json["video"])
    }
    
    init(user: String, password: String, host: String, socketUri: String, address: String, callId: String, room: String, port: Int, hash: String, bannerImageURL: URL, backgroundImageUrl: URL, images: [URL], videoStreamUrl: URL, video: Video) {
        self.user = user
        self.password = password
        self.host = host
        self.socketUri = socketUri
        self.address = address
        self.callId = callId
        self.room = room
        self.port = port
        self.hash = hash
        self.bannerImageURL = bannerImageURL
        self.backgroundImageUrl = backgroundImageUrl
        self.images = images
        self.videoStreamUrl = videoStreamUrl
        self.video = video
    }
    
}

class CallResponse: Codable {
    
    fileprivate var success: Bool
    fileprivate var message: String?
    
    init(success: Bool, message: String?) {
        self.success = success
        self.message = message
    }
    
    func isSuccess() -> Bool {
        return success
    }
    
    func messageText() -> String {
        return message ?? ""
    }
}

// MARK: Convenience initializers and mutators

extension CallResponse {
    convenience init(data: Data) throws {
        let me = try JSONDecoder().decode(CallResponse.self, from: data)
        self.init(success: me.success, message: me.message)
    }
    
    convenience init(_ json: String, using encoding: String.Encoding = .utf8) throws {
        guard let data = json.data(using: encoding) else {
            throw NSError(domain: "JSONDecoding", code: 0, userInfo: nil)
        }
        try self.init(data: data)
    }
    
    convenience init(fromURL url: URL) throws {
        try self.init(data: try Data(contentsOf: url))
    }
    
    
    func jsonData() throws -> Data {
        return try newJSONEncoder().encode(self)
    }
    
    func jsonString(encoding: String.Encoding = .utf8) throws -> String? {
        return String(data: try self.jsonData(), encoding: encoding)
    }
}
