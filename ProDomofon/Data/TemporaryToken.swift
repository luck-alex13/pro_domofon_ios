//
//  TemporaryToken.swift
//  ProDomofon
//
//  Created by Александр Новиков on 22/10/2019.
//  Copyright © 2019 Александр Новиков. All rights reserved.
//

import Foundation

class TemporaryToken: Codable {
    let id, uid, contractID, lifeTime: String?
    
    enum CodingKeys: String, CodingKey {
        case id = "_id"
        case uid
        case contractID = "contractId"
        case lifeTime
    }
    
    init(id: String?, uid: String?, contractID: String?, lifeTime: String?) {
        self.id = id
        self.uid = uid
        self.contractID = contractID
        self.lifeTime = lifeTime
    }
}
