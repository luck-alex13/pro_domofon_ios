//
//  Contract.swift
//  ProDomofon
//
//  Created by Александр Новиков on 16/06/2019.
//  Copyright © 2019 Александр Новиков. All rights reserved.
//

import Foundation
import UIKit

class Contract: Codable {
    
    var id: String
    var number: Int
    var address: String?
    var flat: String
    var alias: Int
    var deviceId: String?
    var houseId: Int?
    var house: String?
    var street: String?
    var toPay: Int
    var balance: Int?
    var userId: String
    var companyId: String
    var noCaptchaConfirmHash: String
    var allowMoreKeys: Int
    var closed: Bool
    var pipe: Bool
    var owners: Int
    var juridical: Bool
    var confirmed: Bool
    var headset: HeadSet?
    var devices: [Device]?
    var customNamesOfPanel: [ContractPanelName]
    
    enum CodingKeys: String, CodingKey {
        case id = "_id"
        case number, address, flat, alias, deviceId, houseId, house, street, toPay, balance, userId, companyId, noCaptchaConfirmHash, allowMoreKeys, closed, pipe, owners, juridical, confirmed, headset, devices, customNamesOfPanel
    }
    
    init(id: String, number: Int, address: String?, flat: String, alias: Int, deviceId: String?, houseId: Int?, house: String?, street: String?, toPay: Int, balance: Int?, userId: String, companyId: String, noCaptchaConfirmHash: String, allowMoreKeys: Int, closed: Bool, pipe: Bool, owners: Int, juridical: Bool, confirmed: Bool, headset: HeadSet?, devices: [Device]?, customNamesOfPanel: [ContractPanelName]) {
        self.id = id
        self.number = number
        self.address = address
        self.flat = flat
        self.alias = alias
        self.deviceId = deviceId
        self.houseId = houseId
        self.house = house
        self.street = street
        self.toPay = toPay
        self.balance = balance
        self.userId = userId
        self.companyId = companyId
        self.noCaptchaConfirmHash = noCaptchaConfirmHash
        self.allowMoreKeys = allowMoreKeys
        self.closed = closed
        self.pipe = pipe
        self.owners = owners
        self.juridical = juridical
        self.confirmed = confirmed
        self.headset = headset
        self.devices = devices
        self.customNamesOfPanel = customNamesOfPanel
    }
    
    var getReadableAddress: String {
        var addr = ""
        if let street = street {
            addr = "\(street), "
        }
        if let house = house {
            addr += "\(house) "
        }
        
        addr += "кв. \(flat) ";
        if(alias != 0 && alias != Int(flat)) {
            addr = addr + "(\(alias))"
        }
        return addr
    }
    
    func getLinkToPay(baseUrl: String) -> String {
        var link = baseUrl + "/pay/\(number)"
        if noCaptchaConfirmHash != "" {
            link += "?hash=\(noCaptchaConfirmHash)"
        }
        return link
    }
    
    var isOwner: Bool {
        if let user = DataStorage.shared.user, userId == user.id {
            return true
        } else {
            return false
        }
    }
    
    var contractColor: UIColor {
        if isOwner {
            if confirmed {
                return  Colors.GREEN_PRIMARY
            } else {
                return Colors.RED_ACCENT
            }
        } else {
            return Colors.BLACK
        }
    }
    
}
extension Contract {
    var menuContractName: String {
        return "#\(number). \(getReadableAddress)"
    }
}

extension Contract {
    var hasCamera: Bool {
        guard let devices = devices else { return false }
        return devices.filter({ device in
            return device.panels.filter({ panel in
                return panel.camera
            }).count > 0
        }).count > 0
    }
    
    var hasBarrier: Bool {
        guard let devices = devices else { return false }
        return devices.filter({ device in
            return device.panels.filter({ panel in
                return panel.type?.icon == "barrier"
            }).count > 0
        }).count > 0
    }
    
    var deviceCameraPanels: [DevicePanel] {
        return devicePanels.filter({ $0.panel.camera })
    }
    
    var devicePanels: [DevicePanel] {
        var dp: [DevicePanel] = []
        guard let devices = devices else { return [] }
        for d in devices {
            for pp in d.panels {
                let names = customNamesOfPanel.filter({ $0.deviceId == d.deviceId && $0.panel == pp.field })
                let name = names.count > 0 ? names[0].name : pp.type?.title ?? ""
                let newDP = DevicePanel.init(sip: d.sip, deviceId: d.deviceId, panel: pp, name: name, bleUuid: pp.bleUuid, contractNumber: number)
                dp.append(newDP)
            }
        }
        return dp
    }
}
extension Contract {
    var customNamesOfPanelData: Data? {
        let dict: [String: Any] = [
            "customNamesOfPanel": customNamesOfPanel.map({ $0.dict })
        ]
        
        do {
            let jsonData = try JSONSerialization.data(withJSONObject: dict, options: [])
            let jsonString = String(data: jsonData, encoding: String.Encoding.ascii)!
            print("customNamesOfPanel: \(jsonString)")
            return jsonData
        } catch {
            NSLog(error.localizedDescription)
            return nil
        }
    }
}
extension Contract {
    var bleUuids: [String] {
        var arr: [String] = []
        devices?.forEach({ device in
            if !device.bleUuids.isEmpty {
                arr += device.bleUuids
            }
        })
        return arr
    }
}
