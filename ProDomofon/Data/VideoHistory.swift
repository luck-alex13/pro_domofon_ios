//
//  VideoHistory.swift
//  ProDomofon
//
//  Created by Lexus on 08.12.2022.
//  Copyright © 2022 Алексей Агильдин. All rights reserved.
//

import Foundation
import SwiftyJSON

class VideoHistory: Codable {
    
    var st: String
    var et: String?
    var start: Double
    var end: Double?
    
    enum CodingKeys: String, CodingKey {
        case st, et, start, end
    }
    
    init(st: String, et: String, start: Double, end: Double) {
        self.st = st
        self.et = et
        self.start = start
        self.end = end
    }
    
    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(st, forKey: .st)
        try container.encode(et, forKey: .et)
        try container.encode(start, forKey: .start)
        try container.encode(end, forKey: .end)
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        st = try container.decode(String.self, forKey: .st)
        et = try container.decode(String.self, forKey: .et)
        start = try container.decode(Double.self, forKey: .start)
        end = try container.decode(Double.self, forKey: .end)
    }
    
    init(json: JSON) {
        st = json["st"].stringValue
        et = json["et"].string
        start = json["start"].doubleValue
        end = json["end"].double
    }
}
