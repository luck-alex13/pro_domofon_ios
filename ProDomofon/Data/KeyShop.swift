//
//  KeyShop.swift
//  ProDomofon
//
//  Created by Александр Новиков on 04/12/2019.
//  Copyright © 2019 Александр Новиков. All rights reserved.
//

import Foundation

// MARK: - KeyShop
class KeyShop: Codable {
    
    var id: String
    var distance: Float
    var name: String
    var address: String
    var worktime: String
    var point: Point
    
    enum CodingKeys: String, CodingKey {
        case id = "_id"
        case distance, name, address, worktime, point
    }
    
    init(id: String, distance: Float, name: String, address: String, worktime: String, point: Point) {
        self.id = id
        self.distance = distance
        self.name = name
        self.address = address
        self.worktime = worktime
        self.point = point
    }
    
    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(id, forKey: .id)
        try container.encode(distance, forKey: .distance)
        try container.encode(name, forKey: .name)
        try container.encode(address, forKey: .address)
        try container.encode(worktime, forKey: .worktime)
        try container.encode(point, forKey: .point)
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        id = try container.decode(String.self, forKey: .id)
        distance = try container.decode(Float.self, forKey: .distance)
        name = try container.decode(String.self, forKey: .name)
        address = try container.decode(String.self, forKey: .address)
        worktime = try container.decode(String.self, forKey: .worktime)
        point = try container.decode(Point.self, forKey: .point)
    }
}

// MARK: - Point
class Point: Codable {
    
    var id: String
    var address: String
    var city: String
    var name: String
    var worktime: String
    var lon: Float
    var lat: Float
    var isOff: Bool
    
    enum CodingKeys: String, CodingKey {
        case id, address, city, name, worktime, lon, lat, isOff
    }
    
//    let owners = LinkingObjects(fromType: KeyShop.self, property: "point")
    
    init(id: String, address: String, city: String, name: String, worktime: String, lon: Float, lat: Float, isOff: Bool) {
        self.id = id
        self.address = address
        self.city = city
        self.name = name
        self.worktime = worktime
        self.lon = lon
        self.lat = lat
        self.isOff = isOff
    }
    
    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(id, forKey: .id)
        try container.encode(address, forKey: .address)
        try container.encode(city, forKey: .city)
        try container.encode(name, forKey: .name)
        try container.encode(worktime, forKey: .worktime)
        try container.encode(lon, forKey: .lon)
        try container.encode(lat, forKey: .lat)
        try container.encode(isOff, forKey: .isOff)
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        id = try container.decode(String.self, forKey: .id)
        address = try container.decode(String.self, forKey: .address)
        city = try container.decode(String.self, forKey: .city)
        name = try container.decode(String.self, forKey: .name)
        worktime = try container.decode(String.self, forKey: .worktime)
        lon = try container.decode(Float.self, forKey: .lon)
        lat = try container.decode(Float.self, forKey: .lat)
        isOff = try container.decode(Bool.self, forKey: .isOff)
    }
}
