//
//  SupportMessageResponse.swift
//  ProDomofon
//
//  Created by Александр Новиков on 05/09/2019.
//  Copyright © 2019 Александр Новиков. All rights reserved.
//

import Foundation

class SupportMessageResponse: Codable {
    
    let total: Int
    let offset: Int
    let limit: Int
    let rows: [SupportMessage]
    
    init(total: Int, offset: Int, limit: Int, rows: [SupportMessage]) {
        self.total = total
        self.offset = offset
        self.limit = limit
        self.rows = rows
    }
}

// MARK: - Row
class SupportMessage: Codable {
    
    let OUTGOING = "import"
    let INCOMING = "export"
    static let SERVER_DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
    
    var id: String
    var text: String
    var direction: String
    var dtime: String
    
    enum CodingKeys: String, CodingKey {
        case id = "_id"
        case text, direction, dtime
    }
    
    init(id: String, text: String, direction: String, dtime: String) {
        self.id = id
        self.text = text
        self.direction = direction
        self.dtime = dtime
    }
    
    func isFromUser() -> Bool {
        return direction.elementsEqual(OUTGOING)
    }
}

class NewMessage: Codable {
    
    var text: String
    var dtime: String
    
    init(text: String, dtime: String) {
        self.text = text
        self.dtime = dtime
    }
}
