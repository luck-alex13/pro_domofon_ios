//
//  RegularService.swift
//  ProDomofon
//
//  Created by Александр Новиков on 18/10/2019.
//  Copyright © 2019 Александр Новиков. All rights reserved.
//

class InfoResponse: Codable {
    
    var single: [SingleService]
    var regular: [RegularService]
    
    enum CodingKeys: String, CodingKey {
        case single
        case regular = "services"
    }
}

class RegularService: Codable {
    let id: String
    let name: String
    let groupId: String?
    let amount: Int
    let editable: Bool
    let hidden: Bool
    let activated: Bool
    let includable: Bool?
    var removeAfter: String?
    
    enum CodingKeys: String, CodingKey {
        case id = "_id"
        case name, groupId, amount, editable, hidden, activated, includable
    }
    
    init(id: String, name: String, amount: Int, editable: Bool, hidden: Bool, activated: Bool, includable: Bool?, groupId: String?, removeAfter: String) {
        self.id = id
        self.name = name
        self.amount = amount
        self.editable = editable
        self.hidden = hidden
        self.activated = activated
        self.includable = includable
        self.groupId = groupId
        self.removeAfter = groupId
    }
}

class SingleService: Codable {
    let id: Int
    let typeName: String
    let amount, primeCost: Int
    let comment: String
    let status: Int
    let dtime: String
    let type: Int
    
    enum CodingKeys: String, CodingKey {
        case id, typeName, amount
        case primeCost = "prime_cost"
        case comment, status, dtime, type
    }
    
    init(id: Int, typeName: String, amount: Int, primeCost: Int, comment: String, status: Int, dtime: String, type: Int) {
        self.id = id
        self.typeName = typeName
        self.amount = amount
        self.primeCost = primeCost
        self.comment = comment
        self.status = status
        self.dtime = dtime
        self.type = type
    }
}
