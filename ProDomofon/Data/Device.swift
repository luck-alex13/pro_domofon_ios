//
//  Device.swift
//  ProDomofon
//
//  Created by Lexus on 22.07.2022.
//  Copyright © 2022 Алексей Агильдин. All rights reserved.
//

import Foundation

class ContractPanelName: Codable {
    var deviceId: String
    var panel: String
    var name: String
    
    enum CodingKeys: String, CodingKey {
        case deviceId, panel, name
    }
    
    init(deviceId: String, panel: String, name: String) {
        self.deviceId = deviceId
        self.panel = panel
        self.name = name
    }
    
    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(deviceId, forKey: .deviceId)
        try container.encode(panel, forKey: .panel)
        try container.encode(name, forKey: .name)
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        deviceId = try container.decode(String.self, forKey: .deviceId)
        panel = try container.decode(String.self, forKey: .panel)
        name = try container.decode(String.self, forKey: .name)
    }
    
    var dict: [String: String] {
        return [
            "deviceId": deviceId,
            "panel": panel,
            "name": name
        ]
    }
}

class DevicePanel {
    
    var sip: String
    var deviceId: String
    var panel: Panel
    var name: String
    var bleUuid: String?
    var contractNumber: Int?
    
    init(sip: String, deviceId: String, panel: Panel, name: String, bleUuid: String?, contractNumber: Int?) {
        self.sip = sip
        self.deviceId = deviceId
        self.panel = panel
        self.name = name
        self.bleUuid = bleUuid
        self.contractNumber = contractNumber
    }
}

class Device: Codable {
    
    var sip: String
    var deviceId: String
    var panels: [Panel]
    
    enum CodingKeys: String, CodingKey {
        case sip, deviceId, panels
    }
    
    var bleUuids: [String] {
        return panels.filter({ $0.bleUuid != nil }).map({ $0.bleUuid! })
    }
    
    init(sip: String, deviceId: String, panels: [Panel]) {
        self.sip = sip
        self.deviceId = deviceId
        self.panels = panels
    }
    
    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(sip, forKey: .sip)
        try container.encode(deviceId, forKey: .deviceId)
        try container.encode(panels, forKey: .panels)
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        sip = try container.decode(String.self, forKey: .sip)
        deviceId = try container.decode(String.self, forKey: .deviceId)
        panels = try container.decode([Panel].self, forKey: .panels)
    }
}

class Panel: Codable {
    
    var number: Int
    var camera: Bool
    var bleUuid: String?
    var field: String
    var type: PanelType?
    
    enum CodingKeys: String, CodingKey {
        case number, camera, field, type
        case bleUuid = "ble_uuid"
    }
    
    init(number: Int, camera: Bool, bleUuid: String?, field: String, type: PanelType?) {
        self.number = number
        self.camera = camera
        self.bleUuid = bleUuid
        self.field = field
        self.type = type
    }
    
    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(number, forKey: .number)
        try container.encode(camera, forKey: .camera)
        try container.encode(bleUuid, forKey: .bleUuid)
        try container.encode(field, forKey: .field)
        try container.encode(type, forKey: .type)
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        number = try container.decode(Int.self, forKey: .number)
        camera = try container.decode(Bool.self, forKey: .camera)
        do {
            bleUuid = try container.decode(String.self, forKey: .bleUuid)
        } catch {
            bleUuid = nil
        }
        field = try container.decode(String.self, forKey: .field)
        type = try container.decode(PanelType?.self, forKey: .type)
    }
}

class PanelType: Codable {
    
    var id: String
    var title: String
    var icon: String
    var activeColor: String?
    var unactiveColor: String?
    
    enum CodingKeys: String, CodingKey {
        case id = "_id"
        case title, icon, activeColor, unactiveColor
    }
    
    init(id: String, title: String, icon: String, activeColor: String, unactiveColor: String) {
        self.id = id
        self.title = title
        self.icon = icon
        self.activeColor = activeColor
        self.unactiveColor = unactiveColor
    }
    
    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(id, forKey: .id)
        try container.encode(title, forKey: .title)
        try container.encode(icon, forKey: .icon)
        try container.encode(activeColor, forKey: .activeColor)
        try container.encode(unactiveColor, forKey: .unactiveColor)
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        id = try container.decode(String.self, forKey: .id)
        title = try container.decode(String.self, forKey: .title)
        icon = try container.decode(String.self, forKey: .icon)
        activeColor = try container.decode(String.self, forKey: .activeColor)
        unactiveColor = try container.decode(String.self, forKey: .unactiveColor)
    }
    
    var imageName: String? {
        switch icon {
        case "reader": return "reader"
        case "panel": return "panel"
        case "fire-exit": return "fire_exit"
        case "gate": return "gate"
        case "barrier": return "barrier"
        default: return nil
        }
    }
    
}
