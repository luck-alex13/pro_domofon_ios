//
//  RangesResponse.swift
//  ProDomofon
//
//  Created by Александр Новиков on 05/11/2019.
//  Copyright © 2019 Александр Новиков. All rights reserved.
//

import Foundation

// MARK: - RangesResponse
class RangesResponse: Codable {
    let ranges: [Range]?
    
    init(ranges: [Range]?) {
        self.ranges = ranges
    }
}

// MARK: - Range
class Range: Codable {
    let start, end: Int?
    
    init(start: Int?, end: Int?) {
        self.start = start
        self.end = end
    }
}
