//
//  ServiceRequest.swift
//  ProDomofon
//
//  Created by Александр Новиков on 23/08/2019.
//  Copyright © 2019 Александр Новиков. All rights reserved.
//

import Foundation

class ServiceRequest: Codable {
    let id: String
    let callId: Int
    let type: Int
    let typeName: String
    let status: Int
    let statusName: String
    var workComplete: Bool? = nil
    let desc: String
    let created: String
    let amount: Int
    let contract: String
    var contractFlat: String? = nil
    var contractAddress: String? = nil
    var contractNumber: Int? = nil
    let phone: String
    let paid: Int
    
    enum CodingKeys: String, CodingKey {
        case id = "_id"
        case callId, type, typeName, status, statusName, workComplete, desc, created, amount, contract, contractFlat, contractAddress, contractNumber, phone, paid
    }
    
    init(id: String, callId: Int, type: Int, typeName: String, status: Int, statusName: String, workComplete: Bool?, desc: String, created: String, amount: Int, contract: String, contractFlat: String?, contractAddress: String?, contractNumber: Int?, phone: String, paid: Int) {
        self.id = id
        self.callId = callId
        self.type = type
        self.typeName = typeName
        self.status = status
        self.statusName = statusName
        self.desc = desc
        self.created = created
        self.amount = amount
        self.contract = contract
        self.contractFlat = contractFlat
        self.contractAddress = contractAddress
        self.contractNumber = contractNumber
        self.phone = phone
        self.paid = paid
    }
    
    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(id, forKey: .id)
        try container.encode(callId, forKey: .callId)
        try container.encode(type, forKey: .type)
        try container.encode(typeName, forKey: .typeName)
        try container.encode(status, forKey: .status)
        try container.encode(statusName, forKey: .statusName)
        try container.encode(workComplete, forKey: .workComplete)
        try container.encode(desc, forKey: .desc)
        try container.encode(created, forKey: .created)
        try container.encode(amount, forKey: .amount)
        try container.encode(contract, forKey: .contract)
        try container.encode(contractFlat, forKey: .contractFlat)
        try container.encode(contractAddress, forKey: .contractAddress)
        try container.encode(contractNumber, forKey: .contractNumber)
        try container.encode(phone, forKey: .phone)
        try container.encode(paid, forKey: .paid)
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        id = try container.decode(String.self, forKey: .id)
        callId = try container.decode(Int.self, forKey: .callId)
        type = try container.decode(Int.self, forKey: .type)
        typeName = try container.decode(String.self, forKey: .typeName)
        status = try container.decode(Int.self, forKey: .status)
        statusName = try container.decode(String.self, forKey: .statusName)
        workComplete = try container.decodeIfPresent(Bool.self, forKey: .workComplete)
        desc = try container.decode(String.self, forKey: .desc)
        created = try container.decode(String.self, forKey: .created)
        amount = try container.decode(Int.self, forKey: .amount)
        contract = try container.decode(String.self, forKey: .contract)
        contractFlat = try container.decodeIfPresent(String.self, forKey: .contractFlat)
        contractAddress = try container.decodeIfPresent(String.self, forKey: .contractAddress)
        contractNumber = try container.decodeIfPresent(Int.self, forKey: .contractNumber)
        phone = try container.decode(String.self, forKey: .phone)
        paid = try container.decode(Int.self, forKey: .paid)
    }
    
    enum Status: Int {
        case NOT_COMPLETED = 1
        case COMPLETED = 2
        case WORK_DONE = 3
        case CALL_BACK = 4
        case BAD_WORK = 5
        case INCORRECT_REQUEST = 6
        case WRONG_PHONE = 7
        case ACT_DONE = 8
        case CANCELLED = 9
        case EDITING = 10
        case WAITING_FOR_DEVICE_SELECTION = 11
        case NOT_PAID = 12
        case CLOSED_TO_REPAIR = 13
    }
}
