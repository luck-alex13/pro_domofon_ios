//
//  DataStorage.swift
//  ProDomofon
//
//  Created by Алексей Агильдин on 30.03.2021.
//  Copyright © 2021 Алексей Агильдин. All rights reserved.
//

import Foundation
import UIKit
//import SwiftyJSON

class DataStorage {
    
    static let shared = DataStorage()
    
    private init() {}
    
    private let userDefaults = UserDefaults(suiteName: "group.ProDomofonSharing")!
    private let encoder = JSONEncoder()
    private let decoder = JSONDecoder()

    
    
    func removeObjectsBeforeLogginingOut() {
        self.removeAllMedia()
        let keys = ["user", "choosenContract", "choosenContractInfoIndex", "contracts", "contractKeys", "requests", "contractPhones", "messages", "calculations", "regularServices", "singleServices", "keyShops", "installs", "sipHistories"]
        keys.forEach { userDefaults.removeObject(forKey: $0) }
        self.clearCash()
    }
    
    func removeAdminObjectsBeforeLogginingOut() {
        adminToken = accessToken
        lastAdminPhone = lastPhone
        lastAdminPhoneWithMask = lastPhoneWithMask
        removeObjectsBeforeLogginingOut()
    }
    
    func backAdminLogginingIn() {
        removeObjectsBeforeLogginingOut()
        accessToken = adminToken
        adminToken = nil
        lastPhone = lastAdminPhone
        lastAdminPhone = nil
        lastPhoneWithMask = lastAdminPhoneWithMask
        lastAdminPhoneWithMask = nil
    }
    
    fileprivate func removeAllMedia() {
        guard let documentsUrl = try? FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: true) else { return }
        
//        guard let documentsUrl =  FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first else { return }

        do {
            let fileURLs = try FileManager.default.contentsOfDirectory(at: documentsUrl,
                                                                       includingPropertiesForKeys: nil,
                                                                       options: .skipsHiddenFiles)
            for fileURL in fileURLs {
                try FileManager.default.removeItem(at: fileURL)
            }
        } catch  {
            print(error)
        }
    }
    
    fileprivate func clearCash(){
        let keys = ["accessToken", "accsessToken", "lastAuthTime", "appleToken", "lastAskNotificationTimeInterval", "closeBetweenAfterOpen"]
        keys.forEach { userDefaults.removeObject(forKey: $0) }
    }
    
    var activeBetweenCellContractNumber: Int?
    var activeBetweenCellTypeId: String?
    var selectedType: MenuVCType?
    
    var settingsURLString: String {
        get {
            return userDefaults.string(forKey: #function) ?? UIApplication.openSettingsURLString
        }
        set {
            userDefaults.set(newValue, forKey: #function)
            userDefaults.synchronize()
        }
    }
    
    var accessToken: String? {
        get {
            var str = userDefaults.string(forKey: #function)
            if str == nil {
                str = accsessToken
            }
            return str
        }
        set {
            userDefaults.set(newValue, forKey: #function)
            userDefaults.synchronize()
        }
    }
    
    fileprivate var accsessToken: String? {
        get {
            let str = userDefaults.string(forKey: #function)
            return str
        }
        set {
            userDefaults.set(newValue, forKey: #function)
            userDefaults.synchronize()
        }
    }

    
    var adminToken: String? {
        get {
            let str = userDefaults.string(forKey: #function)
            return str
        }
        set {
            userDefaults.set(newValue, forKey: #function)
            userDefaults.synchronize()
        }
    }
    
    var lastAuthTime: Int {
        get {
            let val = userDefaults.integer(forKey: #function)
            return val
        }
        set {
            userDefaults.set(newValue, forKey: #function)
        }
    }
    
    var appleToken: String? {
        get {
            let str = userDefaults.string(forKey: #function)
            return str
        }
        set {
            userDefaults.set(newValue, forKey: #function)
        }
    }
    
    var lastAskNotificationTimeInterval: TimeInterval {
        get {
            let val = userDefaults.double(forKey: #function)
            return val
        }
        set {
            userDefaults.set(newValue, forKey: #function)
        }
    }
    
    var user: User? {
        get {
            guard let savedData = userDefaults.object(forKey: #function) as? Data else { return nil }
            let user = try? decoder.decode(User.self, from: savedData)
            return user
        }
        set {
            let encoded = try! encoder.encode(newValue)
            userDefaults.set(encoded, forKey: #function)
        }
    }
    
    var choosenContractString: String? {
        get {
            let str = userDefaults.string(forKey: #function)
            return str
        }
        set {
            userDefaults.set(newValue, forKey: #function)
        }
    }

    var choosenContract: Contract? {
        if let cId = choosenContractString, let c = contracts.first( where: { $0.id == cId } ) {
            return c
        } else if contracts.count == 1, let c = contracts.first {
            return c
        } else {
            return nil
        }
    }
    
    var choosenContractInfoIndex: Int? {
        get {
            let value = userDefaults.integer(forKey: #function)
            return value
        }
        set {
            userDefaults.set(newValue, forKey: #function)
        }
    }
    
    var contracts: [Contract] {
        get {
            guard let savedData = userDefaults.object(forKey: #function) as? Data else { return [] }
            do {
                let array = try decoder.decode([Contract].self, from: savedData)
                return array.sorted(by: { $0.id < $1.id })
            } catch {
                return []
            }
        }
        set {
            let encodedData = try? encoder.encode(newValue)
            userDefaults.set(encodedData, forKey: #function)
            userDefaults.synchronize()
            if choosenContractString == nil && !newValue.isEmpty {
                choosenContractString = newValue[0].id
            }
            
            NotificationCenter.default.post(name: .updateData, object: nil)
        }
    }
    
    var contractKeys: [ContractsKeys] {
        get {
            guard let savedData = userDefaults.object(forKey: #function) as? Data else { return [] }
            do {
                let array = try decoder.decode([ContractsKeys].self, from: savedData)
                return array
            } catch {
                return []
            }
        }
        set {
            var array = newValue
            array.forEach { (contractKey) in
                contractKey.keys.forEach { (key) in
                    key.contractId = contractKey.contractId
                }
            }
            let encodedData = try? encoder.encode(array)
            userDefaults.set(encodedData, forKey: #function)
            NotificationCenter.default.post(name: .updateData, object: nil)
        }
    }
    
    var scheduleContracts: [Contract] {
        get {
            guard let savedData = userDefaults.object(forKey: #function) as? Data else { return [] }
            do {
                let array = try decoder.decode([Contract].self, from: savedData)
                return array
            } catch {
                return []
            }
        }
        set {
            let encodedData = try? encoder.encode(newValue)
            userDefaults.set(encodedData, forKey: #function)
            NotificationCenter.default.post(name: .updateData, object: nil)
        }
    }
    
    var requests: [ServiceRequest] {
        get {
            guard let savedData = userDefaults.object(forKey: #function) as? Data else { return [] }
            do {
                let array = try decoder.decode([ServiceRequest].self, from: savedData)
                return array.sorted(by: { $0.callId < $1.callId })
            } catch {
                return []
            }
        }
        set {
            newValue.forEach { (request) in
                if let contract = contracts.first(where: { $0.id == request.contract }) {
                    if request.contractFlat != nil {
                        request.contractFlat = contract.flat
                    }
                    if request.contractAddress != nil {
                        request.contractAddress = contract.address
                    }
                    if request.contractNumber != nil {
                        request.contractNumber = contract.number
                    }
                }
            }
            let encodedData = try? encoder.encode(newValue)
            userDefaults.set(encodedData, forKey: #function)
            NotificationCenter.default.post(name: .updateData, object: nil)
        }
    }
    
    var contractPhones: [ContractsPhones] {
        get {
            guard let savedData = userDefaults.object(forKey: #function) as? Data else { return [] }
            do {
                let array = try decoder.decode([ContractsPhones].self, from: savedData)
                return array
            } catch {
                return []
            }
        }
        set {
            let encodedData = try? encoder.encode(newValue)
            userDefaults.set(encodedData, forKey: #function)
            NotificationCenter.default.post(name: .updateData, object: nil)
        }
    }
    
    var messages: [SupportMessage] {
        get {
            guard let savedData = userDefaults.object(forKey: #function) as? Data else { return [] }
            do {
                let array = try decoder.decode([SupportMessage].self, from: savedData)
                return array.sorted(by: { $0.dtime > $1.dtime })
            } catch {
                return []
            }
        }
        set {
            let encodedData = try? encoder.encode(newValue)
            userDefaults.set(encodedData, forKey: #function)
            NotificationCenter.default.post(name: .updateData, object: nil)
        }
    }
    
    var calculations: [Calculation] {
        get {
            guard let savedData = userDefaults.object(forKey: #function) as? Data else { return [] }
            do {
                let array = try decoder.decode([Calculation].self, from: savedData)
                return array.sorted(by: { $0.dtime > $1.dtime })
            } catch {
                return []
            }
        }
        set {
            let encodedData = try? encoder.encode(newValue)
            userDefaults.set(encodedData, forKey: #function)
            NotificationCenter.default.post(name: .updateData, object: nil)
        }
    }
    
    var regularServices: [RegularService] {
        get {
            guard let savedData = userDefaults.object(forKey: #function) as? Data else { return [] }
            do {
                let array = try decoder.decode([RegularService].self, from: savedData)
                return array
            } catch {
                return []
            }
        }
        set {
            let encodedData = try? encoder.encode(newValue)
            userDefaults.set(encodedData, forKey: #function)
            NotificationCenter.default.post(name: .updateData, object: nil)
        }
    }
    
    var singleServices: [SingleService] {
        get {
            guard let savedData = userDefaults.object(forKey: #function) as? Data else { return [] }
            do {
                let array = try decoder.decode([SingleService].self, from: savedData)
                return array.sorted(by: { $0.dtime > $1.dtime })
            } catch {
                return []
            }
        }
        set {
            let encodedData = try? encoder.encode(newValue)
            userDefaults.set(encodedData, forKey: #function)
            NotificationCenter.default.post(name: .updateData, object: nil)
        }
    }
    
    var keyShops: [KeyShop] {
        get {
            guard let savedData = userDefaults.object(forKey: #function) as? Data else { return [] }
            do {
                let array = try decoder.decode([KeyShop].self, from: savedData)
                return array
            } catch {
                return []
            }
        }
        set {
            let encodedData = try? encoder.encode(newValue)
            userDefaults.set(encodedData, forKey: #function)
            NotificationCenter.default.post(name: .updateData, object: nil)
        }
    }
    
    var installs: [Install] {
        get {
            guard let savedData = userDefaults.object(forKey: #function) as? Data else { return [] }
            do {
                let array = try decoder.decode([Install].self, from: savedData)
                return array
            } catch {
                return []
            }
        }
        set {
            let encodedData = try? encoder.encode(newValue)
            userDefaults.set(encodedData, forKey: #function)
            NotificationCenter.default.post(name: .updateData, object: nil)
        }
    }
    
    var sipHistories: [SipHistory] {
        get {
            guard let savedData = userDefaults.object(forKey: #function) as? Data else { return [] }
            do {
                let array = try decoder.decode([SipHistory].self, from: savedData)
                return array
            } catch (let error) {
                return []
            }
        }
        set {
            do {
                let encodedData = try encoder.encode(newValue)
                userDefaults.set(encodedData, forKey: #function)
                NotificationCenter.default.post(name: .updateData, object: nil)
            } catch (let error) {
            }
        }
    }
    
    var lastPhone: String? {
        get {
            let str = userDefaults.string(forKey: #function)
            return str
        }
        set {
            userDefaults.set(newValue, forKey: #function)
        }
    }
    
    var lastPhoneWithMask: String? {
        get {
            let str = userDefaults.string(forKey: #function)
            return str
        }
        set {
            userDefaults.set(newValue, forKey: #function)
        }
    }
    
    var lastAdminPhone: String? {
        get {
            let str = userDefaults.string(forKey: #function)
            return str
        }
        set {
            userDefaults.set(newValue, forKey: #function)
        }
    }
    
    var lastAdminPhoneWithMask: String? {
        get {
            let str = userDefaults.string(forKey: #function)
            return str
        }
        set {
            userDefaults.set(newValue, forKey: #function)
        }
    }
    
    var closeBetweenAfterOpen: Bool {
        get {
            let str = userDefaults.bool(forKey: #function)
            return str
        }
        set {
            userDefaults.set(newValue, forKey: #function)
        }
    }
    
    var timestampGetSipPush: Double? {
        get {
            return userDefaults.double(forKey: #function)
        }
        set {
            userDefaults.set(newValue, forKey: #function)
            userDefaults.synchronize()
        }
    }
}
extension DataStorage {
    func updateAlias(contractId: String, alias: Int) {
        let newContracts = contracts
        newContracts.forEach { (contract) in
            if contract.id == contractId {
                contract.alias = alias
            }
        }
        contracts = newContracts
    }
    
    func updateRequest(request: ServiceRequest) {
        var newRequests = requests
        if let index = newRequests.firstIndex(where: { $0.id == request.id }) {
            newRequests[index] = request
        } else {
            newRequests.append(request)
        }
        requests = newRequests
    }
    
    func updateMessage(message: SupportMessage){
        var newMessages = messages
        if let index = newMessages.firstIndex(where: { $0.id == message.id }) {
            newMessages[index] = message
        } else {
            newMessages.append(message)
        }
        messages = newMessages
    }
    
    func updateMessages(mess: [SupportMessage]){
        let newMessages = DataStorage.shared.messages + mess
        messages = newMessages
    }
}
