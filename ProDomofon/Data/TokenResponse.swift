//
//  TokenResponse.swift
//  ProDomofon
//
//  Created by Александр Новиков on 14/06/2019.
//  Copyright © 2019 Александр Новиков. All rights reserved.
//

import Foundation

class TokenResponse: Codable {
    
    var token: String?
    
    init(token: String?) {
        self.token = token
    }
    
    enum CodingKeys: String, CodingKey {
        case token
    }
}
