//
//  PhonesResponse.swift
//  ProDomofon
//
//  Created by Александр Новиков on 01/07/2019.
//  Copyright © 2019 Александр Новиков. All rights reserved.
//

import Foundation

// MARK: - PhonesResponse
class PhonesResponse: Codable {
    let maxPhonesPerContract: Int?
    let contracts: [ContractsPhones]?
    
    init(maxPhonesPerContract: Int?, contracts: [ContractsPhones]?) {
        self.maxPhonesPerContract = maxPhonesPerContract
        self.contracts = contracts
    }
}


// MARK: - Contract
class ContractsPhones: Codable {
    
    var contractId: String
    var contractNumber: Int
    var address: String?
    var flat: String
    
    var phones: [Phone]
    
    enum CodingKeys: String, CodingKey {
        case contractNumber = "number"
        case contractId, phones, address, flat
    }
    
    init(contractId: String, contractNumber: Int, address: String?, flat: String, phones: [Phone]) {
        self.contractId = contractId
        self.contractNumber = contractNumber
        self.address = address
        self.flat = flat
        self.phones = phones
    }
    
    
    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(contractId, forKey: .contractId)
        try container.encode(contractNumber, forKey: .contractNumber)
        if let address = address {
            try container.encode(address, forKey: .address)
        }
        try container.encode(flat, forKey: .flat)
        try container.encode(phones, forKey: .phones)
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        contractId = try container.decode(String.self, forKey: .contractId)
        contractNumber = try container.decode(Int.self, forKey: .contractNumber)
        do {
            address = try container.decode(String?.self, forKey: .address)
        } catch {
            address = nil
        }
        flat = try container.decode(String.self, forKey: .flat)
        phones = try container.decode([Phone].self, forKey: .phones)
    }
    
}
extension ContractsPhones {
    
    fileprivate var recipients: [String] {
        var arrayPhones: [String] = []
        phones.forEach { (p) in
            if p.sipEnabled, p.number.count > 0 {
                arrayPhones.append(p.number)
            }
        }
        return arrayPhones
    }
    
    var dataForEditPhoneSip: Data? {
        let dict: [String: Any] = [
            "contractId": contractId,
            "recipients": recipients
        ]
        
        do {
            let jsonData = try JSONSerialization.data(withJSONObject: dict, options: [])
            let jsonString = String(data: jsonData, encoding: String.Encoding.ascii)!
            print("ContractsPhones: \(jsonString)")
            return jsonData
        } catch {
            NSLog(error.localizedDescription)
            return nil
        }
    }
}


// MARK: - Phone
class Phone: Codable {
    
    var id: String
    var number: String
    var contractId: String
    var isActive: Bool
    var position: Int
    var sipEnabled: Bool
    var always: Bool
    var useGSM: Bool
    var schedules: [Schedule]
    var carNumber: String?
    
//    let owners: [ContractsPhones]
    
    enum CodingKeys: String, CodingKey {
        case id = "_id"
        case contractId, number, isActive, position, sipEnabled, always, useGSM, schedules, carNumber
    }
    
    init(id: String, number: String, contractId: String, isActive: Bool, position: Int, sipEnabled: Bool, always: Bool, useGSM: Bool, schedules: [Schedule], carNumber: String?) {
        self.id = id
        self.number = number
        self.contractId = contractId
        self.isActive = isActive
        self.position = position
        self.sipEnabled = sipEnabled
        self.always = always
        self.useGSM = useGSM
        self.schedules = schedules
        self.carNumber = carNumber
    }
    
    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(id, forKey: .id)
        try container.encode(number, forKey: .number)
        try container.encode(contractId, forKey: .contractId)
        try container.encode(isActive, forKey: .isActive)
        try container.encode(position, forKey: .position)
        try container.encode(sipEnabled, forKey: .sipEnabled)
        try container.encode(always, forKey: .always)
        try container.encode(useGSM, forKey: .useGSM)
        try container.encode(schedules, forKey: .schedules)
        try container.encode(carNumber, forKey: .carNumber)
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        id = try container.decode(String.self, forKey: .id)
        number = try container.decode(String.self, forKey: .number)
        contractId = try container.decode(String.self, forKey: .contractId)
        isActive = try container.decode(Bool.self, forKey: .isActive)
        position = try container.decode(Int.self, forKey: .position)
        sipEnabled = try container.decode(Bool.self, forKey: .sipEnabled)
        always = try container.decode(Bool.self, forKey: .always)
        useGSM = try container.decode(Bool.self, forKey: .useGSM)
        schedules = try container.decode([Schedule].self, forKey: .schedules)
        do {
            carNumber = try container.decode(String.self, forKey: .carNumber)
        } catch {
            carNumber = nil
        }
    }
}
extension Phone {
    var sipConfigurationData: Data? {
        var sc: [Any] = []
        schedules.forEach { (s) in
            var dow: [Int] = []
            s.dow.forEach { (d) in
                dow.append(d)
            }
            let sData: [String: Any] = [
                "_id": s.id ?? "",
                "start": s.start,
                "end": s.end,
                "enabled": s.enabled,
                "dow": dow
            ]
            sc.append(sData)
        }
        
        let dict: [String: Any] = [
            "contractId": contractId,
            "phoneId": id,
            "configuration": [
                "always": always,
                "useGSM": useGSM,
                "schedules": sc
            ]
        ]
        
        do {
            let jsonData = try JSONSerialization.data(withJSONObject: dict, options: [])
            let jsonString = String(data: jsonData, encoding: String.Encoding.ascii)!
            print("phone sipConfigurationData: \(jsonString)")
            return jsonData
        } catch {
            NSLog(error.localizedDescription)
            return nil
        }
    }
}
