//
//  AuthResponse.swift
//  ProDomofon
//
//  Created by Lexus on 21.01.2022.
//  Copyright © 2022 Алексей Агильдин. All rights reserved.
//

import Foundation

class AuthResponse: Codable {
    
    var messanger: String
    
    init(messanger: String) {
        self.messanger = messanger
    }
    
    enum CodingKeys: String, CodingKey {
        case messanger
    }
    
    var infoString: String {
        return messanger != "" ? "из \(messanger)" : ""
    }
}
