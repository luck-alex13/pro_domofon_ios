//
//  SipHistory.swift
//  ProDomofon
//
//  Created by Алексей Агильдин on 01.06.2021.
//  Copyright © 2021 Алексей Агильдин. All rights reserved.
//

import Foundation
import SwiftyJSON

class SipHistory: Codable {
    let callId: String
    let contract: Int
    let address: String
    var photo: SipHistoryMedia?
    var video: SipHistoryMedia?
    var audio: SipHistoryMedia?
    var faces: [SipHistoryMedia]
    let accepted: String
    let removed: Bool
    let created: Double
    let panel: String
    let sip: String
    let device: String
    
    enum CodingKeys: String, CodingKey {
        case callId = "call_id"
        case contract, address, photo, video, audio, faces, accepted, removed, created, panel, sip, device
    }
    
    init(json: JSON) {
        callId = json["call_id"].stringValue
        contract = json["contract"].intValue
        address = json["address"].stringValue
        photo = SipHistoryMedia.init(json: json["photo"])
        video = SipHistoryMedia.init(json: json["video"])
        audio = SipHistoryMedia.init(json: json["audio"])
        faces = []
        if let array = json["faces"].array {
            faces = array.map({ SipHistoryMedia.init(json: $0) })
        }
        accepted = json["accepted"].stringValue
        removed = json["removed"].boolValue
        created = json["created"].doubleValue
        panel = json["panel"].stringValue
        sip = json["sip"].stringValue
        device = json["device"].stringValue
    }
    
    init(callId: String, contract: Int, address: String, photo: SipHistoryMedia?, video: SipHistoryMedia?, audio: SipHistoryMedia?, faces: [SipHistoryMedia], accepted: String, removed: Bool, created: Double, panel: String, sip: String, device: String) {
        self.callId = callId
        self.contract = contract
        self.address = address
        self.photo = photo
        self.video = video
        self.audio = audio
        self.faces = faces
        self.accepted = accepted
        self.removed = removed
        self.created = created
        self.panel = panel
        self.sip = sip
        self.device = device
    }
    
    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(callId, forKey: .callId)
        try container.encode(contract, forKey: .contract)
        try container.encode(address, forKey: .address)
        try container.encode(photo, forKey: .photo)
        try container.encode(video, forKey: .video)
        try container.encode(audio, forKey: .audio)
        try container.encode(faces, forKey: .faces)
        try container.encode(accepted, forKey: .accepted)
        try container.encode(removed, forKey: .removed)
        try container.encode(created, forKey: .created)
        try container.encode(panel, forKey: .panel)
        try container.encode(sip, forKey: .sip)
        try container.encode(device, forKey: .device)
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        callId = try container.decode(String.self, forKey: .callId)
        contract = try container.decode(Int.self, forKey: .contract)
        address = try container.decode(String.self, forKey: .address)
        do {
            photo = try container.decode(SipHistoryMedia.self, forKey: .photo)
        } catch {
            photo = nil
        }
        do {
            video = try container.decode(SipHistoryMedia.self, forKey: .video)
        } catch {
            video = nil
        }
        do {
            audio = try container.decode(SipHistoryMedia.self, forKey: .audio)
        } catch {
            audio = nil
        }
        faces = try container.decode([SipHistoryMedia].self, forKey: .faces)
        accepted = try container.decode(String.self, forKey: .accepted)
        removed = try container.decode(Bool.self, forKey: .removed)
        created = try container.decode(Double.self, forKey: .created)
        panel = try container.decode(String.self, forKey: .panel)
        sip = try container.decode(String.self, forKey: .sip)
        device = try container.decode(String.self, forKey: .device)
    }
}
extension SipHistory {
    var dateString: String {
        let dateHelper = DateHelper(locale: Locale.current)
        return dateHelper.getOnlyDate(from: created / 1000)
    }
    
    var timeString: String {
        let dateHelper = DateHelper(locale: Locale.current)
        return dateHelper.getOnlyTime(from: created / 1000)
    }
}

class SipHistoryMedia: Codable {
    let name: String
    let url: URL?
    let thumb: URL?
    
    var originalName: String {
        return "original_\(name)"
    }
    
    var thumbName: String {
        return "thumb_\(name)"
    }
    
    enum CodingKeys: String, CodingKey {
        case name, url, thumb
    }
    
    init(name: String, url: URL?, thumb: URL?) {
        self.name = name
        self.url = url
        self.thumb = thumb
    }
    
    init(json: JSON) {
        name = json["name"].stringValue
        url = json["url"].url
        thumb = json["thumb"].url
    }
    
    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(name, forKey: .name)
        try container.encode(url, forKey: .url)
        try container.encode(thumb, forKey: .thumb)
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        name = try container.decode(String.self, forKey: .name)
        do {
            url = try container.decode(URL.self, forKey: .url)
        } catch {
            url = nil
        }
        do {
            thumb = try container.decode(URL.self, forKey: .thumb)
        } catch {
            thumb = nil
        }
    }
}
