//
//  UIExtesions.swift
//  ProDomofon
//
//  Created by Александр Новиков on 26/07/2019.
//  Copyright © 2019 Александр Новиков. All rights reserved.
//

import UIKit
import SideMenu

public extension UIViewController {
    
    func setBarWithBack(title: String, rightTitle: String? = nil, rightSelector: Selector? = nil) {
        navigationItem.setTitle(title: title)
        
        let backButton = UIBarButtonItem(title: "", style: .plain, target: navigationController, action: nil)
        navigationItem.backBarButtonItem = backButton
        
        if let rTitle = rightTitle, let selector = rightSelector {
            let rightButton = UIButton()
            rightButton.titleLabel?.font = UIFont.boldSystemFont(ofSize: 17)
            rightButton.setTitleColor(.white, for: .normal)
            rightButton.setTitle(rTitle, for: .normal)
            rightButton.addTarget(self, action: selector, for: .touchUpInside)
            rightButton.sizeToFit()
            let rightBarButton = UIBarButtonItem.init(customView: rightButton)
            navigationItem.rightBarButtonItem = rightBarButton
        } else {
            navigationItem.rightBarButtonItem = nil
        }
    }
    
    func setBar(title: String, rightTitle: String? = nil, rightSelector: Selector? = nil) {
        navigationItem.setTitle(title: title)
        
        let leftButton = UIButton()
        leftButton.titleLabel?.font = UIFont.boldSystemFont(ofSize: 17)
        leftButton.setTitleColor(.white, for: .normal)
        leftButton.setTitle("Меню", for: .normal)
        leftButton.addTarget(self, action: #selector(menuTouch), for: .touchUpInside)
        leftButton.sizeToFit()
        let leftBarButton = UIBarButtonItem.init(customView: leftButton)
        
        navigationItem.leftBarButtonItem = leftBarButton
        
        if let rTitle = rightTitle, let selector = rightSelector {
            let rightButton = UIButton()
            rightButton.titleLabel?.font = UIFont.boldSystemFont(ofSize: 17)
            rightButton.setTitleColor(.white, for: .normal)
            rightButton.setTitle(rTitle, for: .normal)
            rightButton.addTarget(self, action: selector, for: .touchUpInside)
            rightButton.sizeToFit()
            
            navigationItem.rightBarButtonItem = UIBarButtonItem.init(customView: rightButton)
        } else {
            navigationItem.rightBarButtonItem = nil
        }
    }
    
    @objc
    func menuTouch(){
        if let menuVC = Utils.shared.mainStoryboard().instantiateViewController(withIdentifier: "SideMenu") as? SideMenuNavigationController {
            menuVC.menuWidth = view.frame.width * 0.8
            menuVC.presentationStyle = .menuSlideIn
            menuVC.presentationStyle.backgroundColor = .black
            menuVC.presentationStyle.presentingEndAlpha = 0.25
            present(menuVC, animated: true, completion: nil)
        }
    }
    
    func setTitle(title:String, fontSize: CGFloat = 17) {
        let label = UILabel()
        label.text = title
        label.textColor = .white
        label.font = UIFont.boldSystemFont(ofSize: fontSize)
        label.sizeToFit()
        
        self.navigationItem.titleView = label
    }
    
    func setEmptyBackArrow(){
        // удаляет текст возле кнопки назад
//        let backButton = UIBarButtonItem(title: "", style: .plain, target: navigationController, action: nil)
//        self.navigationItem.backBarButtonItem = backButton
        self.navigationController?.navigationBar.topItem?.title = ""
    }
    
    func setTitleForBackArrow(title: String) {
        // устнавливает текст возле кнопки назад
//        let backButton = UIBarButtonItem(title: title, style: .plain, target: navigationController, action: nil)
//        self.navigationItem.backBarButtonItem = backButton
        self.navigationController?.navigationBar.topItem?.title = title
    }
}


extension UINavigationItem {
    
    func setTitle(title:String) {
        
        let label = UILabel()
        label.text = title
        label.textColor = .white
        label.font = UIFont.boldSystemFont(ofSize: 17)
//        label.backgroundColor = .green
        label.sizeToFit()
        
        self.titleView = label
    }
    
    func setTitle(title:String, subtitle:String) {
        
        let one = UILabel()
        one.text = title
        one.font = UIFont.systemFont(ofSize: 17)
        one.sizeToFit()
        
        let two = UILabel()
        two.text = subtitle
        two.font = UIFont.systemFont(ofSize: 12)
        two.textAlignment = .center
        two.sizeToFit()
        
        let stackView = UIStackView(arrangedSubviews: [one, two])
        stackView.distribution = .equalCentering
        stackView.axis = .vertical
        
        let width = max(one.frame.size.width, two.frame.size.width)
        stackView.frame = CGRect(x: 0, y: 0, width: width, height: 35)
        
        one.sizeToFit()
        two.sizeToFit()
        
        self.titleView = stackView
    }
}

extension UITableView {
    
    func setBackgroundView(title: String?, message: String?) {
        let emptyView = UIView(frame: CGRect(x: self.center.x, y: self.center.y, width: self.bounds.size.width, height: self.bounds.size.height))
        let titleLabel = UILabel()
        let messageLabel = UILabel()
        titleLabel.translatesAutoresizingMaskIntoConstraints = false
        messageLabel.translatesAutoresizingMaskIntoConstraints = false
        titleLabel.textColor = UIColor.black
        titleLabel.font = UIFont(name: "HelveticaNeue-Bold", size: 18)
        messageLabel.textColor = UIColor.lightGray
        messageLabel.font = UIFont(name: "HelveticaNeue-Regular", size: 17)
        emptyView.addSubview(titleLabel)
        emptyView.addSubview(messageLabel)
        titleLabel.centerYAnchor.constraint(equalTo: emptyView.centerYAnchor).isActive = true
        titleLabel.centerXAnchor.constraint(equalTo: emptyView.centerXAnchor).isActive = true
        titleLabel.leftAnchor.constraint(equalTo: emptyView.leftAnchor, constant: 10).isActive = true
        titleLabel.rightAnchor.constraint(equalTo: emptyView.rightAnchor, constant: -10).isActive = true
        messageLabel.topAnchor.constraint(equalTo: titleLabel.bottomAnchor, constant: 20).isActive = true
        messageLabel.leftAnchor.constraint(equalTo: emptyView.leftAnchor, constant: 20).isActive = true
        messageLabel.rightAnchor.constraint(equalTo: emptyView.rightAnchor, constant: -20).isActive = true
        titleLabel.text = title
        titleLabel.numberOfLines = 0
        titleLabel.textAlignment = .center
        messageLabel.text = message
        messageLabel.numberOfLines = 0
        messageLabel.textAlignment = .center
        // The only tricky part is here:
        self.backgroundView = emptyView
        self.separatorStyle = .none
    }
    
    func restoreBackgroundView() {
        self.backgroundView = nil
        self.separatorStyle = .singleLine
    }
    
    
}

extension UITableViewCell{
    
    class var nib:UINib {
        return UINib(nibName: self.className, bundle: nil)
    }
    
    class var identifier: String {
        return String(describing: self)
    }
}

extension UIImageView {
    
    func setRounded() {
        self.layer.cornerRadius = self.frame.width / 2
        self.layer.masksToBounds = true
    }
}

extension UIImage {
    
    func imageWithColor(color1: UIColor) -> UIImage {
        UIGraphicsBeginImageContextWithOptions(self.size, false, self.scale)
        color1.setFill()
        
        let context = UIGraphicsGetCurrentContext()
        context?.translateBy(x: 0, y: self.size.height)
        context?.scaleBy(x: 1.0, y: -1.0)
        context?.setBlendMode(CGBlendMode.normal)
        
        let rect = CGRect(origin: .zero, size: CGSize(width: self.size.width, height: self.size.height))
        context?.clip(to: rect, mask: self.cgImage!)
        context?.fill(rect)
        
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage!
    }
    
    func imageWithSize(_ size:CGSize) -> UIImage {
        var scaledImageRect = CGRect.zero
        
        let aspectWidth:CGFloat = size.width / self.size.width
        let aspectHeight:CGFloat = size.height / self.size.height
        let aspectRatio:CGFloat = min(aspectWidth, aspectHeight)
        
        scaledImageRect.size.width = self.size.width * aspectRatio
        scaledImageRect.size.height = self.size.height * aspectRatio
        scaledImageRect.origin.x = (size.width - scaledImageRect.size.width) / 2.0
        scaledImageRect.origin.y = (size.height - scaledImageRect.size.height) / 2.0
        
        UIGraphicsBeginImageContextWithOptions(size, false, 0)
        
        self.draw(in: scaledImageRect)
        
        let scaledImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return scaledImage!
    }
    
    static func getImageWithColor(color: UIColor, size: CGSize) -> UIImage {
        let rect = CGRect(x: 0, y: 0, width: size.width, height: size.height)
        UIGraphicsBeginImageContextWithOptions(size, false, 0)
        color.setFill()
        UIRectFill(rect)
        let image: UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        return image
    }

}

extension UIAlertController {
    
    func addImage(image: UIImage) {
        let maxSize = CGSize(width: 245, height: 300)
        let imgSize = image.size
        
        var ratio: CGFloat!
        
        if(imgSize.width > imgSize.height){
            ratio = maxSize.width / imgSize.width
        }else {
            ratio = maxSize.height / imgSize.height
        }
        let scaledSize = CGSize(width: imgSize.width * ratio, height: imgSize.height * ratio)
        let resizedImage = image.imageWithSize(scaledSize)
        
        let imgAction = UIAlertAction(title: "", style: .default, handler: nil)
        imgAction.isEnabled = false
        imgAction.setValue(resizedImage.withRenderingMode(.alwaysOriginal), forKey: "image")
        self.addAction(imgAction)
    }
}

extension UIButton {

    
}


extension UIRefreshControl {
    func beginRefreshingManually() {
        self.tintColor = Colors.RED_ACCENT
        tintColorDidChange()
        if let scrollView = superview as? UIScrollView {
            scrollView.setContentOffset(CGPoint(x: 0, y:scrollView.contentOffset.y - frame.height), animated: true)
        }
        beginRefreshing()
    }
}


extension UITabBarController {
    func setTabBarHidden(_ isHidden: Bool, animated: Bool, completion: (() -> Void)? = nil ) {
        if (tabBar.isHidden == isHidden) {
            completion?()
        }

        if !isHidden {
            tabBar.isHidden = false
        }

        let height = tabBar.frame.size.height
        let screenOffset = isHidden ? tabBar.frame.size.height : -tabBar.frame.size.height
        let offsetY = view.frame.height - (isHidden ? 0 : height)
        let duration = (animated ? 0.25 : 0.0)
        
        

        let frame = CGRect(origin: CGPoint(x: tabBar.frame.minX, y: offsetY), size: tabBar.frame.size)
        UIView.animate(withDuration: duration, animations: {
            self.tabBar.frame = frame
            
            self.view.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height + screenOffset)
        }) { _ in
            self.tabBar.isHidden = isHidden
            completion?()
        }
    }
}
extension NSNotification.Name {
    static let showVideoCallVC = NSNotification.Name("showVideoCallVC")
    static let showMainTabBarError = NSNotification.Name("showMainTabBarError")
    static let openCallVC = NSNotification.Name("openCallVC")
    
    static let endCall = NSNotification.Name("endCall")
    static let callAnswered = NSNotification.Name("callAnswered")
    static let callEnded = NSNotification.Name("callEnded")
    static let hasCall = NSNotification.Name("hasCall")
    static let hasNotCall = NSNotification.Name("hasNotCall")
    static let callWasAnsweredOther = NSNotification.Name("callWasAnsweredOther")
    
    static let longPressPhones = NSNotification.Name("longPressPhones")
    static let cancelLongPressPhones = NSNotification.Name("cancelLongPressPhones")
    
    static let updateData = NSNotification.Name("updateData")
    
    static let betweenCellChanged = NSNotification.Name("betweenCellChanged")
    static let betweenCellOpened = NSNotification.Name("betweenCellOpened")
    static let betweenCellClosed = NSNotification.Name("betweenCellClosed")
    
    static let showBetweenVC = NSNotification.Name("showBetweenVC")
}

extension CGFloat {
    static var bottomOffset: CGFloat {
        var offset: CGFloat = 0
        if #available(iOS 11.0, *), UIApplication.shared.windows.count > 0  {
            let window = UIApplication.shared.windows[0]
            offset = window.safeAreaInsets.bottom
        }
        return offset
    }
    
    static var topOffset: CGFloat {
        var offset: CGFloat = 20
        if #available(iOS 11.0, *), UIApplication.shared.windows.count > 0 {
            let window = UIApplication.shared.windows[0]
            if window.safeAreaInsets.top > 0 {
                offset = window.safeAreaInsets.top
            }
            if UIDevice.current.userInterfaceIdiom == .pad {
                 offset += 5
            }
        }
        return offset
    }
}


public protocol NibLoadable {
    static var nibName: String { get }
}

extension NibLoadable where Self: UIView {

    static var nibName: String {
        return String(describing: Self.self)
    }

    static var nib: UINib {
        let bundle = Bundle(for: Self.self)
        return UINib(nibName: Self.nibName, bundle: bundle)
    }

    func setupFromNib() {
        guard let view = Self.nib.instantiate(withOwner: self, options: nil).first as? UIView else { fatalError("Error loading \(self) from nib") }
        addSubview(view)
        view.translatesAutoresizingMaskIntoConstraints = false
        view.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 0).isActive = true
        view.topAnchor.constraint(equalTo: self.topAnchor, constant: 0).isActive = true
        view.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: 0).isActive = true
        view.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: 0).isActive = true
    }
}

extension UILabel {
    static func estimatedHeight(width: CGFloat, text: String, font: UIFont) -> CGFloat {
        let size = CGSize(width: width, height: 1000)
        let options = NSStringDrawingOptions.usesFontLeading.union(.usesLineFragmentOrigin)
        let estimatedFrame = NSString(string: text).boundingRect(with: size, options: options, attributes: [NSAttributedString.Key.font: font], context: nil)
        return estimatedFrame.height
    }
}
extension UITextField {
    var selectedRangeStart: Int? {
        guard let range = selectedTextRange else { return nil }
        return offset(from: beginningOfDocument, to: range.start)
    }
    
    var selectedRangeEnd: Int? {
        guard let range = selectedTextRange else { return nil }
        return offset(from: beginningOfDocument, to: range.end)
    }
}
extension UIApplication {
    func showToast(text: String) {
        if let rootWindow = UIApplication.shared.windows.first, let rootVC = rootWindow.rootViewController, let vc = rootVC as? MainNavigationVC {
            Utils.shared.showToast(from: vc, with: text)
        }
    }
    
    enum AppConfiguration {
        case Debug
        case TestFlight
        case AppStore
    }
    
    private var isTestFlight: Bool {
        return Bundle.main.appStoreReceiptURL?.lastPathComponent == "sandboxReceipt"
    }
    
    var isDebug: Bool {
        #if DEBUG
            return true
        #else
            return false
        #endif
    }
    
    var appConfiguration: AppConfiguration {
        if isDebug {
            return .Debug
        } else if isTestFlight {
            return .TestFlight
        } else {
            return .AppStore
        }
    }
}
