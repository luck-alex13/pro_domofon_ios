//
//  String.swift
//  ProDomofon
//
//  Created by Александр Новиков on 31/05/2019.
//  Copyright © 2019 Александр Новиков. All rights reserved.
//


import Foundation
import UIKit

extension String {
    
    func truncate(to index: Int) -> String {
        if self.count > index {
            return String(self.prefix(index))
        }else {
            return self
        }
    }
    
    func truncatePretty(to index: Int) -> String {
        if self.count > index {
            return String(self.prefix(index)) + "..."
        }else {
            return self
        }
    }
    
    
    var htmlToAttributedString: NSAttributedString? {
        guard let data = data(using: .utf8) else { return NSAttributedString() }
        do {
            return try NSAttributedString(data: data, options: [.documentType: NSAttributedString.DocumentType.html, .characterEncoding:String.Encoding.utf8.rawValue], documentAttributes: nil)
        } catch {
            return NSAttributedString()
        }
    }
    var htmlToString: String {
        return htmlToAttributedString?.string ?? ""
    }
    
    func htmlAttributed(using font: UIFont, color: UIColor) -> NSAttributedString? {
        do {
            let htmlCSSString = "<style>" +
                "html *" +
                "{" +
                "font-size: \(font.pointSize)pt !important;" +
                "color: #\(color.hexString!) !important;" +
                "font-family: \(font.familyName), Helvetica !important;" +
            "}</style> \(self)"
            
            guard let data = htmlCSSString.data(using: String.Encoding.utf8) else {
                return nil
            }
            
            return try NSAttributedString(data: data,
                                          options: [.documentType: NSAttributedString.DocumentType.html,
                                                    .characterEncoding: String.Encoding.utf8.rawValue],
                                          documentAttributes: nil)
        } catch {
            print("error: ", error)
            return nil
        }
    }
    
    func htmlAttributed(using font: UIFont) -> NSAttributedString? {
        do {
            let htmlCSSString = "<style>" +
                "html *" +
                "{" +
                //"font-size: \(font.pointSize)pt !important;" +
                "font-family: \(font.familyName), Helvetica !important;" +
            "}</style> \(self)"
            
            guard let data = htmlCSSString.data(using: String.Encoding.utf8) else {
                return nil
            }
            
            return try NSAttributedString(data: data,
                                          options: [.documentType: NSAttributedString.DocumentType.html,
                                                    .characterEncoding: String.Encoding.utf8.rawValue],
                                          documentAttributes: nil)
        } catch {
            print("error: ", error)
            return nil
        }
    }
    
    func htmlAttributed(using fontFamily: String, fontSize: CGFloat? = UIFont.systemFont(ofSize: 12).pointSize) -> NSAttributedString? {
        do {
            let htmlCSSString = "<style>" +
                "html *" +
                "{" +
                "font-size: \(fontSize!)pt ;" +
                "font-family: \(fontFamily), Helvetica !important;" +
            "}</style> \(self)"
            
            guard let data = htmlCSSString.data(using: String.Encoding.utf8) else {
                return nil
            }
            
            return try NSAttributedString(data: data,
                                          options: [.documentType: NSAttributedString.DocumentType.html,
                                                    .characterEncoding: String.Encoding.utf8.rawValue],
                                          documentAttributes: nil)
        } catch {
            print("error: ", error)
            return nil
        }
    }
    
    mutating func removingRegexMatches(pattern: String, replaceWith: String = "") {
        do {
            let regex = try NSRegularExpression(pattern: pattern, options: NSRegularExpression.Options.caseInsensitive)
            let range = NSMakeRange(0, self.count)
            self = regex.stringByReplacingMatches(in: self, options: [], range: range, withTemplate: replaceWith)
        } catch {
            return
        }
    }
    
    func applyPatternOnNumbers(pattern: String, replacmentCharacter: Character) -> String {
        var pureNumber = self.replacingOccurrences( of: "[^0-9]", with: "", options: .regularExpression)
        for index in 0 ..< pattern.count {
            guard index < pureNumber.count else { return pureNumber }
            let stringIndex = String.Index(utf16Offset: index, in: pureNumber)
            let patternCharacter = pattern[stringIndex]
            guard patternCharacter != replacmentCharacter else { continue }
            pureNumber.insert(patternCharacter, at: stringIndex)
        }
        return pureNumber
    }
    
    func capitalizingFirstLetter() -> String {
        return prefix(1).uppercased() + self.lowercased().dropFirst()
    }
    
    mutating func capitalizeFirstLetter() {
        self = self.capitalizingFirstLetter()
    }
    
    var onlyNumbers: String {
        return components(separatedBy: CharacterSet.decimalDigits.inverted).joined()
    }
    
    var getPhoneWithMask: String {
        return applyPatternOnNumbers(pattern: "+7(###)###-##-##", replacmentCharacter: "#")
    }
    
    func fromBase64() -> String? {
        guard let data = Data(base64Encoded: self) else {
            return nil
        }

        return String(data: data, encoding: .utf8)
    }

    func toBase64() -> String {
        return Data(self.utf8).base64EncodedString()
    }
    
    func foundUrlArray() -> [String] {
        var arrayString: [String] = []
        
        let separateStr = "http"
        let arr = self.components(separatedBy: separateStr)
        
        if arr.count > 1 {
            let arr1 = arr[1].components(separatedBy: " ")
            if arr1.count > 0 {
                arrayString.append(separateStr+arr1[0])
            } else if arr1.count == 0 {
                arrayString.append(separateStr+arr[1])
            }
        }
        
        return arrayString
    }
    
    var replacingByURL: String {
        return self.replacingOccurrences(of: "&#x2F;", with: "/")
    }
    
    static func randomString(length: Int) -> String {
        let letters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
        return String((0..<length).map{ _ in letters.randomElement()! })
    }
    
    var parseColor: UIColor? {
        guard prefix(2) == "0x" && count == 8 else { return nil }
        let redHex = String(suffix(count-2).prefix(2))
        let greenHex = String(suffix(count-4).prefix(2))
        let blueHex = String(suffix(count-6).prefix(2))
        let redInt = redHex.convertFromHex
        let greenInt = greenHex.convertFromHex
        let blueInt = blueHex.convertFromHex
        return UIColor.init(red: redInt, green: greenInt, blue: blueInt)
    }
    
    var convertFromHex: Int {
        guard count == 2 else { return 0 }
        let firstCharacter = String(prefix(1))
        let secondCharacter = String(suffix(1).prefix(1))
        return firstCharacter.intFromHex * 16 + secondCharacter.intFromHex
    }
    
    var intFromHex: Int {
        guard count == 1 else { return 0 }
        switch lowercased() {
        case "a": return 10
        case "b": return 11
        case "c": return 12
        case "d": return 13
        case "e": return 14
        case "f": return 15
        default:
            if let intValue = Int(self) {
                return intValue
            } else {
                return 0
            }
        }
        
    }
}
