//
//  FoundationExtension.swift
//  ProDomofon
//
//  Created by Алексей Агильдин on 03.02.2021.
//  Copyright © 2021 Александр Новиков. All rights reserved.
//

import Foundation

extension Data {
    var hexString: String {
        let hexString = map { String(format: "%02.2hhx", $0) }.joined()
        return hexString
    }
}
extension Double {
    func rounded(toPlaces places:Int) -> Double {
        let divisor = pow(10.0, Double(places))
        return (self * divisor).rounded() / divisor
    }
}
extension Sequence where Iterator.Element: Hashable {
    func unique() -> [Iterator.Element] {
        var seen: [Iterator.Element: Bool] = [:]
        return self.filter { seen.updateValue(true, forKey: $0) == nil }
    }
}
extension Int {
    var twoSymbolsString: String {
        return "\(self < 10 ? "0" : "")\(self)"
    }
}
