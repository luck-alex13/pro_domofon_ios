//
//  UIView+Extension.swift
//  ProDomofon
//
//  Created by Lexus on 10.02.2023.
//  Copyright © 2023 Алексей Агильдин. All rights reserved.
//

import UIKit

extension UIView {
    
    func hideKeyboard() {
        self.endEditing(true)
    }
    
    func rounded() {
        let halfHeightRadius = frame.height / 2
        setCornerRadius(radius: halfHeightRadius)
    }
    
    func setRoundedBorder(color: UIColor, borderWidth: CGFloat = 1, radius: CGFloat? = nil){
        if let r = radius {
            setCornerRadius(radius: r)
        } else {
            rounded()
        }
        self.layer.borderColor = color.cgColor
        self.layer.borderWidth = borderWidth
    }
    
    func setCornerRadius(radius: CGFloat) {
        self.clipsToBounds = true
        self.layer.cornerRadius = radius
    }
        
    func setCornerRadiusWithRect(radius: Int, needRect: UIRectCorner = .allCorners){
        self.layoutIfNeeded()
        let rectShape = CAShapeLayer()
        rectShape.bounds = self.frame
        rectShape.position = self.center
        rectShape.path = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: needRect, cornerRadii: CGSize(width: radius, height: radius)).cgPath
        self.layer.mask = rectShape
    }
    
    var globalPoint :CGPoint? {
        return self.superview?.convert(self.frame.origin, to: nil)
    }
    
    var globalFrame :CGRect? {
        return self.superview?.convert(self.frame, to: nil)
    }
    
    func hideObject(_ duration: Double = 0.5){
        guard !self.isHidden else { return }
        if duration > 0 {
            UIView.animate(withDuration: duration, animations: {
                self.alpha = 0
            }) { (_) in
                self.isHidden = true
            }
        } else {
            self.alpha = 0
            self.isHidden = true
        }
    }
    
    @objc
    func showObject(_ duration: Double = 0.5){
        guard self.isHidden else { return }
        UIView.animate(withDuration: duration, animations: {
            self.alpha = 1
            self.isHidden = false
        }) { (_) in
            
        }
    }
    
    func animateChangePositionX(_ newPosition: CGFloat,_ delay: TimeInterval = 0,_ completion: ((Bool) -> Void)? = nil){
        self.animateChangePosition(newPosition, nil, delay, completion)
    }

    func animateChangePositionY(_ newPosition: CGFloat,_ delay: TimeInterval = 0,_ completion: ((Bool) -> Void)? = nil){
        self.animateChangePosition(nil, newPosition, delay, completion)
    }

    func animateChangePosition(_ newPosition: CGPoint,_ delay: TimeInterval = 0,_ completion: ((Bool) -> Void)? = nil){
        self.animateChangePosition(newPosition.x, newPosition.y, delay, completion)
    }

    fileprivate func animateChangePosition(_ newPositionX: CGFloat? = nil,_ newPositionY: CGFloat? = nil,_ delay: TimeInterval = 0,_ completion: ((Bool) -> Void)? = nil){
        UIView.animate(withDuration: 0.5, delay: delay, options: [.curveEaseOut, .allowUserInteraction], animations: {
            if let newPosX = newPositionX {
                self.frame.origin.x = newPosX
            }
            if let newPosY = newPositionY {
                self.frame.origin.y = newPosY
            }
        }, completion: completion)
    }
    
    func animateChangeHeight(_ newHeight: CGFloat,_ delay: TimeInterval = 0,_ completion: ((Bool) -> Void)? = nil){
        self.animateChangeSize(newHeight, nil, delay, completion)
    }

    func animateChangeWidth(_ newWidth: CGFloat,_ delay: TimeInterval = 0,_ completion: ((Bool) -> Void)? = nil){
        self.animateChangeSize(nil, newWidth, delay, completion)
    }

    func animateChangeSize(_ newSize: CGSize,_ delay: TimeInterval = 0,_ completion: ((Bool) -> Void)? = nil){
        self.animateChangePosition(newSize.height, newSize.width, delay, completion)
    }
    
    fileprivate func animateChangeSize(_ newHeight: CGFloat? = nil,_ newWidth: CGFloat? = nil,_ delay: TimeInterval = 0,_ completion: ((Bool) -> Void)? = nil){
        UIView.animate(withDuration: 0.5, delay: delay, options: [.curveEaseOut, .allowUserInteraction], animations: {
            if let newH = newHeight {
                self.frame.size.height = newH
            }
            if let newW = newWidth {
                self.frame.size.width = newW
            }
        }, completion: completion)
    }
}
