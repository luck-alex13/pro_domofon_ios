//
//  Date+Extension.swift
//  ProDomofon
//
//  Created by Lexus on 26.11.2022.
//  Copyright © 2022 Алексей Агильдин. All rights reserved.
//

import Foundation

extension Calendar {
    static var calendarWithoutTimezone: Calendar {
        var calendar = Calendar.current
        calendar.timeZone = TimeZone(secondsFromGMT: 0)!
        return calendar
    }
}

extension Date {
    static var currentDateWithoutTimezone: Date {
        var components = DateComponents()
        components.hour = Foundation.NSTimeZone.default.secondsFromGMT() / 3600
        return Calendar.calendarWithoutTimezone.date(byAdding: components, to: Date())!
    }
    
    var startOfDayWithoutTimezone: Date {
        return Calendar.calendarWithoutTimezone.startOfDay(for: self)
    }
    var endOfDayWithoutTimezone: Date {
        var components = DateComponents()
        components.day = 1
        components.second = -1
        return Calendar.calendarWithoutTimezone.date(byAdding: components, to: startOfDayWithoutTimezone)!
    }
    
    var startOfDay: Date {
        return Calendar.current.startOfDay(for: self)
    }

    var endOfDay: Date {
        var components = DateComponents()
        components.day = 1
        components.second = -1
        return Calendar.current.date(byAdding: components, to: startOfDay)!
    }
    
    var prevDate: Date {
        var components = DateComponents()
        components.day = -1
        return Calendar.current.date(byAdding: components, to: startOfDay)!
    }
    
    var startNextDate: Date {
        var components = DateComponents()
        components.day = 1
        return Calendar.current.date(byAdding: components, to: startOfDay)!
    }
    
    var startOfWeek: Date {
        Calendar.current.dateComponents([.calendar, .yearForWeekOfYear, .weekOfYear], from: self).date!
    }
    
    var endOfWeek: Date {
        var components = DateComponents()
        components.weekOfYear = 1
        components.second = -1
        return Calendar.current.date(byAdding: components, to: startOfWeek)!
    }
    
    var startOfMonth: Date {
        let components = Calendar.current.dateComponents([.year, .month], from: startOfDay)
        return Calendar.current.date(from: components)!
    }

    var endOfMonth: Date {
        var components = DateComponents()
        components.month = 1
        components.second = -1
        return Calendar.current.date(byAdding: components, to: startOfMonth)!
    }
    
    var time: String {
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "ru_RU")
//        dateFormatter.timeZone = TimeZone(secondsFromGMT: 0)!
        dateFormatter.dateStyle = .none
        dateFormatter.timeStyle = .short
        return dateFormatter.string(from: self)
    }
    
    var dateShort: String {
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "ru_RU")
//        dateFormatter.timeZone = TimeZone(secondsFromGMT: 0)!
        dateFormatter.dateFormat = "E, dd/MM"
        return dateFormatter.string(from: self)
    }
}
