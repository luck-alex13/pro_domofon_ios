//
//  NSObject.swift
//  ProDomofon
//
//  Created by Александр Новиков on 31/05/2019.
//  Copyright © 2019 Александр Новиков. All rights reserved.
//

import Foundation

extension NSObject {
    
    var className: String {
        return String(describing: type(of: self))
    }
    
    class var className: String {
        return String(describing: self)
    }
}
