//
//  SelectButton.swift
//  ProDomofon
//
//  Created by Александр Новиков on 27/08/2019.
//  Copyright © 2019 Александр Новиков. All rights reserved.
//

import UIKit

class SelectButton: UIButton {
    
    static let BOTTOM_VIEW_TAG = 100
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        let view = UIView()
        view.backgroundColor = UIColor.init(rgb: 0x029985)
        view.tag = SelectButton.BOTTOM_VIEW_TAG
        view.translatesAutoresizingMaskIntoConstraints = false
        addSubview(view)
        
        view.addConstraint(NSLayoutConstraint(
            item: view,
            attribute: .height,
            relatedBy: .equal,
            toItem: nil,
            attribute: .height,
            multiplier: 1,
            constant: 1
            )
        )
        
        addConstraint(NSLayoutConstraint(
            item: view,
            attribute: .left,
            relatedBy: .equal,
            toItem: self,
            attribute: .left,
            multiplier: 1,
            constant: 0
            )
        )
        
        addConstraint(NSLayoutConstraint(
            item: view,
            attribute: .right,
            relatedBy: .equal,
            toItem: self,
            attribute: .right,
            multiplier: 1,
            constant: 0
            )
        )
        
        addConstraint(NSLayoutConstraint(
            item: view,
            attribute: .bottom,
            relatedBy: .equal,
            toItem: self,
            attribute: .bottom,
            multiplier: 1,
            constant: 8
            )
        )
    }
    
    func removeBottomStripe(){
        if let bottomStripe = viewWithTag(SelectButton.BOTTOM_VIEW_TAG) {
            bottomStripe.removeFromSuperview()
        }
    }
    
}
