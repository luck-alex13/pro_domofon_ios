//
//  CardView.swift
//  ProDomofon
//
//  Created by Александр Новиков on 31/05/2019.
//  Copyright © 2019 Александр Новиков. All rights reserved.
//

import UIKit


class CardView: UIView {
    
    var cornerRadius: CGFloat = 6
    
    var shadowOffsetWidth: Int = 0
    var shadowOffsetHeight: Int = 1
    var shadowColor: UIColor? = UIColor.darkGray
    var shadowOpacity: Float = 0.5
    
    override func layoutSubviews() {
        layer.cornerRadius = cornerRadius
        let shadowPath = UIBezierPath(roundedRect: bounds, cornerRadius: cornerRadius)
        
        layer.masksToBounds = false
        layer.shadowColor = shadowColor?.cgColor
        layer.shadowOffset = CGSize(width: shadowOffsetWidth, height: shadowOffsetHeight);
        layer.shadowOpacity = shadowOpacity
        layer.shadowPath = shadowPath.cgPath
    }
    
}

