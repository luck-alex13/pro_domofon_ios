//
//  DatePickerView.swift
//  ProDomofon
//
//  Created by Lexus on 28.12.2022.
//  Copyright © 2022 Алексей Агильдин. All rights reserved.
//

import UIKit

protocol DatePickerViewDelegate: AnyObject {
    func dateSelected(date: Date)
}

class DatePickerView: UIView {
    
    fileprivate var datePicker: UIDatePicker!
    
    weak var delegate: DatePickerViewDelegate?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        backgroundColor = .clear
        
        let control = UIControl(frame: CGRect(x: 0, y: 0, width: frame.width, height: frame.height))
        control.backgroundColor = .clear
        control.addTarget(self, action: #selector(cancelTouch), for: .touchDown)
        addSubview(control)
        
        let width: CGFloat = frame.width - 2 * 16
        var height: CGFloat {
            if #available(iOS 14.0, *) {
                return 350
            } else if #available(iOS 13.4, *) {
                return 250
            } else {
                return 90
            }
        }
        
        let view = UIView(frame: CGRect(x: frame.width / 2 - width / 2, y: frame.height / 2 - height / 2, width: width, height: height))
        view.backgroundColor = .white
        view.setCornerRadius(radius: 5)
        addSubview(view)
        
        let okButton = UIButton()
        okButton.titleLabel?.font = UIFont.boldSystemFont(ofSize: 20)
        okButton.setTitleColor(Colors.GREEN_PRIMARY, for: .normal)
        okButton.setTitle("ОК", for: .normal)
        okButton.addTarget(self, action: #selector(okTouch), for: .touchUpInside)
        okButton.sizeToFit()
        okButton.frame = CGRect(x: width - okButton.frame.width - 60, y: view.frame.height - 40, width: okButton.frame.width + 60, height: 40)
        view.addSubview(okButton)
        
        let cancelButton = UIButton()
        cancelButton.titleLabel?.font = UIFont.boldSystemFont(ofSize: 20)
        cancelButton.setTitleColor(Colors.GREEN_PRIMARY, for: .normal)
        cancelButton.setTitle("Отмена", for: .normal)
        cancelButton.addTarget(self, action: #selector(cancelTouch), for: .touchUpInside)
        cancelButton.sizeToFit()
        cancelButton.frame = CGRect(x: okButton.frame.origin.x - cancelButton.frame.width - 20, y: okButton.frame.origin.y, width: cancelButton.frame.width + 20, height: okButton.frame.height)
        view.addSubview(cancelButton)
        
        datePicker = UIDatePicker(frame: CGRect(x: width / 2 - 320 / 2, y: 0, width: 320, height: okButton.frame.origin.y))
        datePicker.locale = Locale(identifier: "ru_RU")
        datePicker.contentHorizontalAlignment = .center
        datePicker.datePickerMode = .date
        if #available(iOS 14.0, *) {
            datePicker.preferredDatePickerStyle = .inline
        } else
        if #available(iOS 13.4, *) {
            datePicker.preferredDatePickerStyle = .wheels
        }
        view.addSubview(datePicker)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    @objc
    func cancelTouch() {
        hideObject()
    }
    
    @objc
    func okTouch() {
        delegate?.dateSelected(date: datePicker.date)
        hideObject()
    }
    
    func setDate(date: Date) {
        datePicker.setDate(date, animated: true)
    }
    
    func setEnabledDate(videoHistories: [VideoHistory]) {
        let vhs = videoHistories.sorted(by: { $0.start < $1.start })
        if vhs.count > 0 {
            let dateStart = Date(timeIntervalSince1970: vhs[0].start)
            datePicker.minimumDate = dateStart
        } else {
            datePicker.minimumDate = Date()
        }
        datePicker.maximumDate = Date()
    }

}
