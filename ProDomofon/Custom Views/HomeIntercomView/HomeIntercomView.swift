//
//  HomeIntercomView.swift
//  ProDomofon
//
//  Created by Алексей Агильдин on 02.03.2021.
//  Copyright © 2021 Александр Новиков. All rights reserved.
//

import UIKit

protocol HomeIntercomViewDelegate: UITextViewDelegate {
    func switchChanged(state: Bool)
}

class HomeIntercomView: UIView, NibLoadable {

    weak var delegate: HomeIntercomViewDelegate?
    
    @IBOutlet weak var mySwitch: UISwitch! {
        didSet {
            mySwitch.isEnabled = false
        }
    }
    
    var textAlign = NSTextAlignment.center {
        didSet {
            setTextView()
        }
    }
    
    @IBOutlet weak var textView: UITextView! {
        didSet {
            setTextView()
        }
    }
    
    func setTextView(){
        guard textView != nil else { return }
        textView.delegate = self
        textView.backgroundColor = .clear
//            textView.isSelectable = false
        textView.isEditable = false
        textView.isScrollEnabled = false
        
        let text = "Дополнительная услуга, позволяющая направлять звонки звонки с подъездной панели на смартфон. "
        let link = "ПОДРОБНЕЕ"
        let url = URL(string: StringConst.DOMOFON_TO_TELEPHONE.rawValue)!
        
        let style = NSMutableParagraphStyle()
        style.alignment = textAlign

        let attributedOriginalText = NSMutableAttributedString(string: text + link)
        let textRange = attributedOriginalText.mutableString.range(of: text)
        let linkRange = attributedOriginalText.mutableString.range(of: link)
        let fullRange = NSMakeRange(0, attributedOriginalText.length)
        
        attributedOriginalText.addAttribute(NSAttributedString.Key.link, value: link, range: linkRange)
        attributedOriginalText.setAttributes([.link: url], range: linkRange)
        attributedOriginalText.addAttribute(NSAttributedString.Key.paragraphStyle, value: style, range: fullRange)
        attributedOriginalText.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.black, range: textRange)
        attributedOriginalText.addAttribute(NSAttributedString.Key.foregroundColor, value: Colors.RED_ACCENT, range: linkRange)
        attributedOriginalText.addAttribute(NSAttributedString.Key.font, value: UIFont.systemFont(ofSize: 16), range: fullRange)

        textView.attributedText = attributedOriginalText
        textView.linkTextAttributes = [
            kCTForegroundColorAttributeName: Colors.RED_ACCENT,
            kCTUnderlineStyleAttributeName: NSUnderlineStyle.single.rawValue,
        ] as [NSAttributedString.Key : Any]
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupFromNib()
    }

    required init?(coder: NSCoder) {
        super.init(coder: coder)
        setupFromNib()
    }
    
    
    @IBAction func switchChanged() {
        
    }

    
}
extension HomeIntercomView: UITextViewDelegate {
    func textView(_ textView: UITextView, shouldInteractWith URL: URL, in characterRange: NSRange, interaction: UITextItemInteraction) -> Bool {
        Utils.shared.openUrl(url: URL.absoluteString)
        return false
    }
    
    func textView(_ textView: UITextView, shouldInteractWith textAttachment: NSTextAttachment, in characterRange: NSRange, interaction: UITextItemInteraction) -> Bool {
        print("textView shouldInteractWith textAttachment characterRange: \(characterRange)")
        return false
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        print("textView shouldChangeTextIn ")
        return true
    }
    
    
}
