//
//  EdgeInsetLabel.swift
//  ProDomofon
//
//  Created by Александр Новиков on 27/08/2019.
//  Copyright © 2019 Александр Новиков. All rights reserved.
//
import UIKit

class EdgeInsetLabel: UILabel {
    
    @IBInspectable var topInset: CGFloat = 5.0
    @IBInspectable var bottomInset: CGFloat = 5.0
    @IBInspectable var leftInset: CGFloat = 7.0
    @IBInspectable var rightInset: CGFloat = 7.0
    
    override func drawText(in rect: CGRect) {
        let insets = UIEdgeInsets.init(top: topInset, left: leftInset, bottom: bottomInset, right: rightInset)
        //super.drawText(in: UIEdgeInsetsInsetRect(rect, insets))
        super.drawText(in: bounds.inset(by: insets))
    }
    
    override var intrinsicContentSize: CGSize {
        let size = super.intrinsicContentSize
        return CGSize(width: size.width + leftInset + rightInset,
                      height: size.height + topInset + bottomInset)
    }
}
