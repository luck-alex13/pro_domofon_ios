//
//  ControlVideoFlowView.swift
//  ProDomofon
//
//  Created by Lexus on 26.11.2022.
//  Copyright © 2022 Алексей Агильдин. All rights reserved.
//

import UIKit

protocol ControlVideoFlowViewDelegate: AnyObject {
    func speedChanged(speedValue: Int)
    func play(timeInterval: Double)
    func pause()
    func showSpeedAlert(speedFactor: Double)
    func showDateAlert(date: Date)
    func openDoor()
}

enum ZoomState: Int {
    case sixHours, hour, halfHour, quartHour, fiveMinutes, minute
    
    var seconds: Double {
        switch self {
        case .sixHours: return 6 * 60 * 60
        case .hour: return 60 * 60
        case .halfHour: return 30 * 60
        case .quartHour: return 15 * 60
        case .fiveMinutes: return 5 * 60
        case .minute: return 60
        }
    }
    
    var factor: Int {
        return Int(24 * 60 * 60 / self.seconds)
    }
    
    var factorInside: Int {
        switch self {
        case .sixHours: return 6
        case .hour: return 6
        case .halfHour: return 3
        case .quartHour: return 3
        case .fiveMinutes: return 5
        case .minute: return 6
        }
    }
    
    func getComponents(value: Int) -> DateComponents {
        var comp = DateComponents()
        switch self {
        case .sixHours: comp.hour = 6 * value
        case .hour: comp.hour = value
        case .halfHour: comp.minute = 30 * value
        case .quartHour: comp.minute = 15 * value
        case .fiveMinutes: comp.minute = 5 * value
        case .minute: comp.minute = value
        }
        
        return comp
    }
    
    var stepText: String {
        switch self {
        case .sixHours: return "6 ч."
        case .hour: return "1 ч."
        case .halfHour: return "30 мин."
        case .quartHour: return "15 мин."
        case .fiveMinutes: return "5 мин."
        case .minute: return "1 мин."
        }
    }
}

class ControlVideoFlowView: UIView {
    
    fileprivate var backgroundView: UIView!
    fileprivate var stepLabel: UILabel!
    
    fileprivate var timeLineView: UIControl!
    fileprivate var disableView: UIView!
    
    fileprivate var zoomOutButton: UIButton!
    fileprivate var zoomInButton: UIButton!
    
    fileprivate var videoHistoryViews: [UIView] = []
    fileprivate var currentTimeView: UIView?

    fileprivate var speedButton: UIButton!
    fileprivate var dateButton: UIButton!
    fileprivate var nowButton: UIButton!
    
    fileprivate var openDoorView: UIView!
    fileprivate var stateDoorImageView: UIImageView!
    fileprivate var stateOpeningActivityIndicatorView: UIActivityIndicatorView!
    
    fileprivate var verticalLine: UIView!
    fileprivate var timeLabel: UILabel!
    
    fileprivate var initialPosX: CGFloat = 0
    fileprivate var visibleStep: CGFloat = 100
    
    fileprivate var zoomState = ZoomState.hour {
        didSet {
            if zoomState == .sixHours {
                zoomOutButton.tintColor = .gray
            } else {
                zoomOutButton.tintColor = .white
            }
            if zoomState == .minute {
                zoomInButton.tintColor = .gray
            } else {
                zoomInButton.tintColor = .white
            }
            stepLabel.text = zoomState.stepText
            redrawTimeLineView()
        }
    }
    
    fileprivate var date = Date() {
        didSet {
            dateButton.setTitle(date.dateShort, for: .normal)
        }
    }
    
    fileprivate var timeStampSecondDay: TimeInterval = 86400
    fileprivate var timeStampSeconds: TimeInterval = 0 {
        didSet {
            let hours = Int(timeStampSeconds / (60 * 60))
            let minutes = Int(timeStampSeconds / 60) - 60 * hours
            let seconds = Int(timeStampSeconds) - 60 * 60 * hours - 60 * minutes
            
            timeLabel.text = "\(hours.twoSymbolsString):\(minutes.twoSymbolsString):\(seconds.twoSymbolsString)"
            
            if timeLabel.isHidden && !backgroundView.isHidden {
                timeLabel.showObject()
                verticalLine.showObject()
            }
        }
    }
    fileprivate var currentTimeInterval: TimeInterval {
        let choosenDateTime = Date(timeIntervalSince1970: date.startOfDay.timeIntervalSince1970 + timeStampSeconds)
        return choosenDateTime.timeIntervalSince1970
    }
    func getTimeInterval() -> TimeInterval {
        return currentTimeInterval
    }
    
    fileprivate var videoHistories: [VideoHistory] = []
    
    fileprivate var played = false
    
    fileprivate var speedValue: Int = 100
    fileprivate var speedFactor: Double {
        let value = Double(speedValue) / 100
        return value.rounded(toPlaces: 1)
    }
    
    fileprivate var nowDateTimeInterval: TimeInterval? {
        let currentDate = Date()
        if currentDate.dateShort == date.dateShort {
            let dif = currentDate.timeIntervalSince1970 - currentDate.startOfDay.timeIntervalSince1970
            return dif
        } else {
            return nil
        }
    }
    
    fileprivate var opening = false
    fileprivate var timeIntervalPlay: TimeInterval?
    fileprivate var timeIntervalUpdated: TimeInterval?
    
    weak var delegate: ControlVideoFlowViewDelegate?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        backgroundColor = .clear
        
        backgroundView = UIView(frame: CGRect(x: 0, y: 0, width: frame.width, height: frame.height))
        backgroundView.backgroundColor = .black.withAlphaComponent(0.5)
        addSubview(backgroundView)
        
        stepLabel = UILabel(frame: CGRect(x: 5, y: 5, width: 150, height: 20))
        stepLabel.textColor = .white
        stepLabel.font = UIFont.systemFont(ofSize: 15)
        stepLabel.textAlignment = .left
        stepLabel.text = zoomState.stepText
        addSubview(stepLabel)
        
        redrawTimeLineView()
        
        zoomOutButton = UIButton(frame: CGRect(x: 0, y: timeLineView.frame.origin.y + 10, width: timeLineView.frame.height - 30, height: timeLineView.frame.height - 30))
        zoomOutButton.backgroundColor = .black
        zoomOutButton.setImage(UIImage(named: "zoom_out")?.withRenderingMode(.alwaysTemplate), for: .normal)
        zoomOutButton.tintColor = .white
        zoomOutButton.setCornerRadiusWithRect(radius: 5, needRect: [.topRight, .bottomRight])
        zoomOutButton.addTarget(self, action: #selector(zoomOutTouch), for: .touchUpInside)
        addSubview(zoomOutButton)
        
        zoomInButton = UIButton(frame: CGRect(x: frame.width - zoomOutButton.frame.height, y: zoomOutButton.frame.origin.y, width: zoomOutButton.frame.height, height: zoomOutButton.frame.height))
        zoomInButton.backgroundColor = .black
        zoomInButton.setImage(UIImage(named: "zoom_in")?.withRenderingMode(.alwaysTemplate), for: .normal)
        zoomInButton.tintColor = .white
        zoomInButton.setCornerRadiusWithRect(radius: 5, needRect: [.topLeft, .bottomLeft])
        zoomInButton.addTarget(self, action: #selector(zoomInTouch), for: .touchUpInside)
        addSubview(zoomInButton)
        
        verticalLine = UIView(frame: CGRect(x: frame.width / 2, y: 15, width: 1, height: timeLineView.frame.height-15))
        verticalLine.backgroundColor = Colors.RED_ACCENT
        verticalLine.hideObject(0)
        addSubview(verticalLine)
        
        timeLabel = UILabel(frame: CGRect(x: frame.width / 2 - 80 / 2, y: 5, width: 80, height: 20))
        timeLabel.backgroundColor = Colors.RED_ACCENT
        timeLabel.font = UIFont.systemFont(ofSize: 15)
        timeLabel.textAlignment = .center
        timeLabel.textColor = .white
        timeLabel.text = ""
        timeLabel.setCornerRadius(radius: 5)
        timeLabel.hideObject(0)
        addSubview(timeLabel)
        
        speedButton = UIButton(frame: CGRect(x: 16, y: timeLineView.frame.maxY + 4, width: 40, height: 50))
        speedButton.backgroundColor = Colors.GREEN_PRIMARY
        speedButton.setTitle("x1", for: .normal)
        speedButton.setTitleColor(.white, for: .normal)
        speedButton.titleLabel?.font = UIFont.systemFont(ofSize: 15)
        speedButton.setCornerRadius(radius: 5)
        speedButton.addTarget(self, action: #selector(speedTouch), for: .touchUpInside)
        addSubview(speedButton)
        speedButton.hideObject(0)
        
        nowButton = UIButton(frame: CGRect(x: frame.width - 16 - 50, y: timeLineView.frame.maxY + 4, width: 50, height: 50))
        nowButton.backgroundColor = Colors.GREEN_PRIMARY
        nowButton.setCornerRadius(radius: 5)
        nowButton.setImage(UIImage(named: "camera")?.withRenderingMode(.alwaysTemplate), for: .normal)
        nowButton.tintColor = .white
        nowButton.addTarget(self, action: #selector(nowTouch), for: .touchUpInside)
        addSubview(nowButton)
        
        dateButton = UIButton(frame: CGRect(x: nowButton.frame.origin.x - 8 - 100, y: timeLineView.frame.maxY + 4, width: 100, height: 50))
        dateButton.backgroundColor = Colors.GREEN_PRIMARY
        dateButton.setTitle(date.dateShort, for: .normal)
        dateButton.setTitleColor(.white, for: .normal)
        dateButton.titleLabel?.font = UIFont.systemFont(ofSize: 15)
        dateButton.setCornerRadius(radius: 5)
        dateButton.addTarget(self, action: #selector(dateTouch), for: .touchUpInside)
        addSubview(dateButton)
        
        openDoorView = UIView(frame: CGRect(x: dateButton.frame.origin.x - 8 - 50, y: dateButton.frame.origin.y, width: 50, height: 50))
        openDoorView.backgroundColor = Colors.GREEN_PRIMARY
        openDoorView.setCornerRadius(radius: 5)
        let tapOpenDoorView = UITapGestureRecognizer(target: self, action: #selector(openDoor))
        tapOpenDoorView.cancelsTouchesInView = true
        openDoorView.addGestureRecognizer(tapOpenDoorView)
        openDoorView.isUserInteractionEnabled = true
        addSubview(openDoorView)
        
        stateDoorImageView = UIImageView(frame: CGRect(x: 5, y: 5, width: 40, height: 40))
        stateDoorImageView.image = UIImage(named: "closed_lock_red")?.withRenderingMode(.alwaysTemplate)
        stateDoorImageView.tintColor = .white
        openDoorView.addSubview(stateDoorImageView)
        
        stateOpeningActivityIndicatorView = UIActivityIndicatorView(frame: CGRect(x: 15, y: 15, width: 20, height: 20))
        stateOpeningActivityIndicatorView.color = .black
        openDoorView.addSubview(stateOpeningActivityIndicatorView)
        stateOpeningActivityIndicatorView.hideObject()
        
        timerTick()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    @objc
    func panGestureAction(_ gestureRecognizer : UIPanGestureRecognizer) {
        guard gestureRecognizer.view != nil else {return}
        let piece = gestureRecognizer.view!

        let translation = gestureRecognizer.translation(in: piece.superview)
        
        if gestureRecognizer.state == .began {
            initialPosX = piece.frame.origin.x
            delegate?.pause()
            played = false
            timeIntervalPlay = nil
        }
        
        
        if gestureRecognizer.state != .cancelled {
            var newPosX = initialPosX + translation.x
            if newPosX > 0 {
                newPosX = 0
            } else if newPosX + piece.frame.width < frame.width {
                newPosX = frame.width - piece.frame.width
            }
            
            let disablePosX = disableView.frame.origin.x - frame.width / 2
            if let _ = nowDateTimeInterval, abs(newPosX) > disablePosX {
                piece.frame.origin.x = -disablePosX
            } else {
                piece.frame.origin.x = newPosX
            }
            calculateTimeInterval(posX: piece.frame.origin.x)
        } else {
            piece.frame.origin.x = initialPosX
        }
        
        if gestureRecognizer.state == .ended {
            timeIntervalPlay = Date().timeIntervalSince1970 + 2
//            timerTick()
        }
    }
    
    @objc
    func speedTouch() {
//        delegate?.showSpeedAlert(speedFactor: speedFactor)
    }
    
    @objc
    func dateTouch() {
        delegate?.showDateAlert(date: date)
    }
    
    @objc
    func nowTouch() {
        setDate(date: Date())
    }
    
    fileprivate func timerTick() {
        if played {
            timeStampSeconds += self.speedFactor
        }
        
        if timeStampSeconds > 24 * 60 * 60 {
            setDate(date: date.startNextDate, accuracy: true)
        }
        
        var posX = -Double(timeStampSeconds) * visibleStep / zoomState.seconds
        if played {
            timeLineView.frame.origin.x = posX
        }
        
        
        
        if let _ = nowDateTimeInterval {
            let todayTimeStampSeconds = Date().timeIntervalSince1970 - Date().startOfDay.timeIntervalSince1970
            posX = Double(todayTimeStampSeconds) * visibleStep / zoomState.seconds
            let widthHalf: CGFloat = frame.size.width / 2
            disableView.frame.origin.x = posX + widthHalf
            disableView.showObject()
            
            if let currentTimeView = currentTimeView {
                currentTimeView.frame.size.width = widthHalf + abs(posX) - currentTimeView.frame.origin.x
            }
        }
        
        
        let timeIntervalNow = Date().timeIntervalSince1970
        if let timeInterval = timeIntervalPlay, timeIntervalNow >= timeInterval {
            timeIntervalPlay = nil
            delegate?.play(timeInterval: currentTimeInterval)
        }
        if let timeInterval = timeIntervalUpdated, timeIntervalNow >= timeInterval+2 {
            played = false
        }
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 1, execute: {
            self.timerTick()
        })
    }
    
    func setDate(date: Date, accuracy: Bool = false) {
        delegate?.pause()
        self.played = false
        self.date = date
        
        if accuracy {
            timeStampSeconds = date.timeIntervalSince1970 - date.startOfDay.timeIntervalSince1970
        }
        
        if let _ = nowDateTimeInterval {
            disableView.showObject()
            if !accuracy {
                timeStampSeconds = Date().timeIntervalSince1970 - Date().startOfDay.timeIntervalSince1970
            }
        } else {
            disableView.hideObject()
            disableView.frame.origin.x = timeLineView.frame.width
            if !accuracy {
                timeStampSeconds = timeStampSecondDay / 2
            }
        }
        
        updateVideoHistoryViews()
        
        delegate?.play(timeInterval: currentTimeInterval)
    }
    
    func setVideoHistories(videoHistories: [VideoHistory]) {
        self.videoHistories = videoHistories
        
        if videoHistories.count == 0 {
            backgroundView.hideObject(0)
            stepLabel.hideObject(0)
            timeLineView.hideObject(0)
            zoomOutButton.hideObject(0)
            zoomInButton.hideObject(0)
            dateButton.hideObject(0)
            verticalLine.hideObject(0)
            timeLabel.hideObject(0)
            nowButton.hideObject(0)
            openDoorView.frame.origin.x = frame.width - 16 - 50
        }
        
        updateVideoHistoryViews()
    }
    
    func checkPlayed() {
        timeIntervalUpdated = Date().timeIntervalSince1970
        guard !played else { return }
        played = true
    }
    
    func stopPlaying() {
        guard played else { return }
        played = false
    }
    
    fileprivate func updateVideoHistoryViews() {
        videoHistoryViews.forEach({ $0.removeFromSuperview() })
        currentTimeView = nil
        
        for i in 0..<videoHistories.count {
            let view = UIView()
            view.backgroundColor = UIColor.init(rgb: 0xFFC475)
            
            var posX: CGFloat = frame.size.width / 2
            var width: CGFloat = 0
            
            let vh = videoHistories[i]
            let startDate = Date(timeIntervalSince1970: vh.start)
            var timeinterval: TimeInterval = 0
            var startinterval: TimeInterval = 0
            var currentTimeInterval = false
            
            
            //print("start: \(vh.st) end: \(vh.et ?? "")")
            
            if startDate.dateShort == date.dateShort {
                if let end = vh.end {
                    let finishDate = Date(timeIntervalSince1970: end)
                    if finishDate.dateShort == date.dateShort {
                        timeinterval = end - videoHistories[i].start
                    } else {
                        timeinterval = date.endOfDayWithoutTimezone.timeIntervalSince1970 - vh.start
                    }
                } else {
                    timeinterval = date.endOfDayWithoutTimezone.timeIntervalSince1970 - vh.start
                    currentTimeInterval = startDate.dateShort == Date().dateShort
                }
                startinterval = vh.start - date.startOfDayWithoutTimezone.timeIntervalSince1970
            } else if startDate.dateShort == date.prevDate.dateShort {
                if let end = vh.end {
                    let finishDate = Date(timeIntervalSince1970: end)
                    if finishDate.dateShort == date.dateShort {
                        timeinterval = end - date.startOfDayWithoutTimezone.timeIntervalSince1970
                    }
                } else {
                    if date.dateShort == Date().dateShort {
                        currentTimeInterval = true
                        timeinterval = Date.currentDateWithoutTimezone.timeIntervalSince1970 - date.startOfDayWithoutTimezone.timeIntervalSince1970
                    } else {
                        timeinterval = date.endOfDayWithoutTimezone.timeIntervalSince1970 - vh.start
                    }
                }
            }
            
            //print("startinterval: \(startinterval) timeinterval: \(timeinterval)")
            
            if timeinterval > 0 {
                width = timeinterval * visibleStep / zoomState.seconds
                posX += startinterval * visibleStep / zoomState.seconds
                
                //print("posX: \(posX) width: \(width)")
                
                view.frame = CGRect(x: posX, y: disableView.frame.origin.y, width: width, height: disableView.frame.height)
                
                timeLineView.addSubview(view)
                timeLineView.sendSubviewToBack(view)
                videoHistoryViews.append(view)
                if currentTimeInterval {
                    currentTimeView = view
                }
            }
            
        }
    }
    
    func setSpeed(value: Double) {
        guard value >= 0.3 && value <= 3 else { return }
        speedValue = Int(value * 100)
        delegate?.speedChanged(speedValue: speedValue)
        speedButton.setTitle("\(speedFactor)x", for: .normal)
    }
    
    fileprivate func calculateTimeInterval(posX: CGFloat) {
        let delta = abs(Int(posX))
        let bigStepCount = Int(delta / Int(visibleStep))
        let smallStepPercent = Double(delta - bigStepCount * Int(visibleStep)) / Double(visibleStep)
        let smallStepCount = Int(smallStepPercent * zoomState.seconds)
        timeStampSeconds = TimeInterval(Double(bigStepCount) * zoomState.seconds + Double(smallStepCount))
    }
    
    @objc
    func openDoor() {
        guard !opening else { return }
        opening = true
        
        runProgress()
        delegate?.openDoor()
    }
    
    func runProgress() {
        stateOpeningActivityIndicatorView.showObject()
        stateOpeningActivityIndicatorView.startAnimating()
        openDoorView.isUserInteractionEnabled = false
    }
    
    func stopProgress(success: Bool) {
        stateOpeningActivityIndicatorView.hideObject()
        stateOpeningActivityIndicatorView.stopAnimating()
        openDoorView.isUserInteractionEnabled = true
        stateDoorImageView.image = success ? UIImage(named: "opened_lock_green")?.withRenderingMode(.alwaysTemplate) : UIImage(named: "closed_lock_red")?.withRenderingMode(.alwaysTemplate)
        opening = success
    }
    
    @objc
    func zoomOutTouch() {
        let newStateInt = zoomState.rawValue-1
        if let newState = ZoomState.init(rawValue: newStateInt) {
            zoomState = newState
        }
    }
    
    @objc
    func zoomInTouch() {
        let newStateInt = zoomState.rawValue+1
        if let newState = ZoomState.init(rawValue: newStateInt) {
            zoomState = newState
        }
    }
    
    fileprivate func redrawTimeLineView() {
//        delegate?.pause()
//        played = false
        if timeLineView != nil {
            timeLineView.removeFromSuperview()
        }
        
        initialPosX = frame.width / 2 - CGFloat(zoomState.factor) * visibleStep / 2
        timeLineView = UIControl(frame: CGRect(x: initialPosX, y: 20, width: CGFloat(zoomState.factor) * visibleStep + frame.width, height: 70))
        timeLineView.backgroundColor = .clear
        timeLineView.hideObject(0)
        addSubview(timeLineView)
        
        if zoomInButton != nil {
            bringSubviewToFront(zoomInButton)
        }
        if zoomOutButton != nil {
            bringSubviewToFront(zoomOutButton)
        }
        if verticalLine != nil {
            bringSubviewToFront(verticalLine)
        }
        if timeLabel != nil {
            bringSubviewToFront(timeLabel)
        }
        
        disableView = UIView(frame: CGRect(x: timeLineView.frame.width, y: 10, width: timeLineView.frame.width - frame.width, height: timeLineView.frame.height - 30))
        disableView.backgroundColor = .lightGray
        timeLineView.addSubview(disableView)
        disableView.hideObject(0)
        
        let leftView = UIView(frame: CGRect(x: 0, y: 10, width: frame.width / 2, height: timeLineView.frame.height - 30))
        leftView.backgroundColor = .lightGray
        timeLineView.addSubview(leftView)
        
        let rightView = UIView(frame: CGRect(x: timeLineView.frame.width - leftView.frame.width, y: leftView.frame.origin.y, width: leftView.frame.width, height: leftView.frame.height))
        rightView.backgroundColor = .lightGray
        timeLineView.addSubview(rightView)
        
        let panGesture = UIPanGestureRecognizer()
        panGesture.addTarget(self, action: #selector(panGestureAction))
        timeLineView.addGestureRecognizer(panGesture)
        
        for i in 0...zoomState.factor{
            let view = UIView(frame: CGRect(x: frame.width / 2 + CGFloat(i) * visibleStep, y: leftView.frame.origin.y, width: 1, height: leftView.frame.height))
            view.backgroundColor = .white
            timeLineView.addSubview(view)
            
            if i != zoomState.factor {
                let height: CGFloat = view.frame.height - 12
                let posY: CGFloat = view.frame.origin.y + 6
                
                for i in 1..<zoomState.factorInside{
                    
                    let lessView = UIView(frame: CGRect(x: view.frame.origin.x + CGFloat(i) * visibleStep / CGFloat(zoomState.factorInside), y: posY, width: 1, height: height))
                    lessView.backgroundColor = .white
                    timeLineView.addSubview(lessView)
                }
            }
            
            var components = DateComponents()
            components.second = i * Int(zoomState.seconds)
            let date = Calendar.current.date(byAdding: components, to: Date().startOfDay)!
            
            let label = UILabel()
            label.font = UIFont.systemFont(ofSize: 10)
            label.textColor = .white
            label.text = date.time
            label.sizeToFit()
            
            let posX = frame.width / 2 + CGFloat(i) * visibleStep - label.frame.width / 2
            label.frame = CGRect(x: posX, y: view.frame.maxY + 4, width: label.frame.width, height: 20)
            timeLineView.addSubview(label)
        }
        
        timeLineView.showObject()
        
        let posX = -Double(timeStampSeconds) * visibleStep / zoomState.seconds
        timeLineView.frame.origin.x = posX
        
        updateVideoHistoryViews()
        
//        delegate?.play(timeInterval: currentTimeInterval)
    }
    
    func setTimestamp(ts: Double) {
//        print("old timestamp: \(timeStampSeconds)")
//        timeStampSeconds = ts - date.startOfDay.timeIntervalSince1970
//        print("new timeStampSeconds: \(timeStampSeconds)")
//        timerTick()
    }
}
