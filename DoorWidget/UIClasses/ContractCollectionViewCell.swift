//
//  ContractCollectionViewCell.swift
//  DoorWidget
//
//  Created by Александр Новиков on 18/09/2019.
//  Copyright © 2019 Александр Новиков. All rights reserved.
//

import UIKit
import UserNotifications

class ContractCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var contractIcon: UIImageView!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var numberLab: UILabel!
    @IBOutlet weak var itemProgress: UIActivityIndicatorView!
    
    var restClient: RestClient?
    var opening = false
    let REASON_WIDGET = "widget";
    
    var contract: Contract? {
        didSet {
            if let house = contract?.house {
                addressLabel.text = "д. \(house)"
            }
            
            if let apt = contract?.flat {
                numberLab.text = "кв. \(apt)"
            }
        }
    }
    
    func openDoor() {
        guard let token = DataStorage.shared.accessToken else {
            return
        }
        
        guard let contract = contract else {
            // проебал привязку к данным
            return
        }
        
        guard !opening else {
            return
        }
        
        opening = true
        
        if restClient == nil {
            restClient = RestClient()
            restClient!.updateToken(newToken: token)
        }
        
        runProgress()
        restClient?.openDoor(contractNumber: contract.number, reason: REASON_WIDGET, success: { [weak self] (resp) in
            guard let self = self else { return }
            self.stopProgress(finishImage: #imageLiteral(resourceName: "opened_lock_green"))
            if #available(iOSApplicationExtension 10.0, *) {
                self.showNotification(title: "Дверь успешно открыта!", body: "Входите в подъезд. У вас есть 3 секунды.")
            }
            DispatchQueue.main.asyncAfter(deadline: .now() + 3.5, execute: {
                self.opening = false
                self.contractIcon.image = #imageLiteral(resourceName: "closed_lock_red")
            })

        }, errorCall: { [weak self] (err) in
            guard let self = self else { return }
            self.opening = false
            self.stopProgress(finishImage: #imageLiteral(resourceName: "closed_lock_red"))
            debugPrint(err)
            if #available(iOSApplicationExtension 10.0, *) {
                self.showNotification(title: "Не удалось открыть дверь", subtitle: nil, body: err.message)
            }
        })
    }
    
    func runProgress() {
        UIView.animate(withDuration: 0.3,   animations: { [weak self] in
            guard let self = self else { return }
            self.itemProgress.isHidden = false
            self.contractIcon.isHidden = true
        },  completion: { [weak self] finish in
            guard let self = self else { return }
            self.itemProgress.startAnimating()
        })       
        
        
    }
    
    func stopProgress(finishImage: UIImage) {
        UIView.animate(withDuration: 0.3,   animations: { [weak self] in
            guard let self = self else { return }
            self.itemProgress.isHidden = true
            self.contractIcon.isHidden = false
            self.contractIcon.image = finishImage
            },  completion: { [weak self] finish in
                guard let self = self else { return }
                self.itemProgress.stopAnimating()
        })
    }
    
    @available(iOS 10.0, *)
    func showNotification(title: String, subtitle: Int? = nil, body: String?) {
        //creating the notification content
        let content = UNMutableNotificationContent()
        
        //adding title, subtitle, body and badge
        content.title = title
        content.body = body ?? ""
        if let sub = subtitle {
            content.subtitle = "Код ошибки \(sub)"
        }
        content.sound = UNNotificationSound.default
        
        //getting the notification request
        let request = UNNotificationRequest(identifier: "SimplifiedIOSNotification", content: content, trigger: nil)
        
        //adding the notification to notification center
        UNUserNotificationCenter.current().add(request, withCompletionHandler: nil)
    }
    
}
