//
//  TodayViewController.swift
//  DoorWidget
//
//  Created by Александр Новиков on 15/09/2019.
//  Copyright © 2019 Александр Новиков. All rights reserved.
//

import UIKit
import NotificationCenter

class TodayViewController: UIViewController, NCWidgetProviding {
    
    @IBOutlet weak var contactsList: UICollectionView!
    @IBOutlet weak var infoLabel: UILabel!
    @IBOutlet weak var settingsButton: UIButton! {
        didSet {
            settingsButton.setCornerRadius(radius: 5)
            settingsButton.addTarget(self, action: #selector(settingsTouch), for: .touchUpInside)
        }
    }
    
    var dataArray: [Contract] = []
//    var notification: NotificationToken? = nil

    override func viewDidLoad() {
        super.viewDidLoad()
        contactsList.dataSource = self
        contactsList.delegate = self
        
        readDataFromDatabase()
        setupDataChangeObserver()
        
        if #available(iOSApplicationExtension 10.0, *) {
            self.extensionContext?.widgetLargestAvailableDisplayMode = .expanded
        } else {
            // Fallback on earlier versions
        }
        
        UNUserNotificationCenter.current().getNotificationSettings { (settings) in
            if settings.authorizationStatus == .authorized {
                self.infoLabel.hideObject()
                self.settingsButton.hideObject()
                self.contactsList.showObject()
            } else {
                self.infoLabel.showObject()
                self.settingsButton.showObject()
                self.contactsList.hideObject()
            }
        }
    }
    
    @objc
    func settingsTouch() {
        guard let settingsUrl = URL(string: DataStorage.shared.settingsURLString) else {
            return
        }
        
        extensionContext?.open(settingsUrl, completionHandler: { (success) in
            if !success {
                var responder = self as UIResponder?

                while (responder != nil){
                    let selectorOpenURL = NSSelectorFromString("openURL:")
                    if responder?.responds(to: selectorOpenURL) == true {
                        _ = responder?.perform(selectorOpenURL, with: settingsUrl)
                    }
                    responder = responder?.next
                }
            }
        })
    }
        
    func widgetPerformUpdate(completionHandler: (@escaping (NCUpdateResult) -> Void)) {
        completionHandler(NCUpdateResult.newData)
    }
    
    func readDataFromDatabase() {
        dataArray = DataStorage.shared.contracts
    }
    
    func setupDataChangeObserver() {
//        notification = dataList?._observe({ [weak self] (changes) in
//            self?.contactsList?.reloadData()
//        })
    }
    
    @available(iOSApplicationExtension 10.0, *)
    func widgetActiveDisplayModeDidChange(_ activeDisplayMode: NCWidgetDisplayMode, withMaximumSize maxSize: CGSize) {
        if activeDisplayMode == .compact {
            self.preferredContentSize = maxSize
        } else if activeDisplayMode == .expanded {
            self.preferredContentSize = CGSize(width: maxSize.width, height: 180)
        }
    }
}
extension TodayViewController: UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return dataArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: ContractCollectionViewCell.className, for: indexPath) as! ContractCollectionViewCell
        cell.contract = dataArray[indexPath.row]
        return cell
    }
    
}
extension TodayViewController: UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if let cell = collectionView.cellForItem(at:indexPath) as? ContractCollectionViewCell {
            cell.openDoor()
        }
    }
}
