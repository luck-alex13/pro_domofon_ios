//
//  SomeExtensions.swift
//  DoorWidget
//
//  Created by Александр Новиков on 18/09/2019.
//  Copyright © 2019 Александр Новиков. All rights reserved.
//

import Foundation
import UIKit


extension NSObject {
    
    var className: String {
        return String(describing: type(of: self))
    }
    
    class var className: String {
        return String(describing: self)
    }
}


extension Bundle {
    
    var appName: String {
        return infoDictionary?["CFBundleName"] as! String
    }
    
    var bundleId: String {
        return bundleIdentifier!
    }
    
    var versionNumber: String {
        return infoDictionary?["CFBundleShortVersionString"] as! String
    }
    
    var buildNumber: String {
        return infoDictionary?["CFBundleVersion"] as! String
    }
    
}

extension Encodable {
    
    func jsonData() throws -> Data {
        return try newJSONEncoder().encode(self)
    }
    
    func jsonString(encoding: String.Encoding = .utf8) throws -> String? {
        return String(data: try self.jsonData(), encoding: encoding)
    }
    
    
    
    func newJSONEncoder() -> JSONEncoder {
        let encoder = JSONEncoder()
        if #available(iOS 10.0, OSX 10.12, tvOS 10.0, watchOS 3.0, *) {
            encoder.dateEncodingStrategy = .iso8601
        }
        return encoder
    }
}

extension Decodable {
    
    func newJSONDecoder() -> JSONDecoder {
        let decoder = JSONDecoder()
        if #available(iOS 10.0, OSX 10.12, tvOS 10.0, watchOS 3.0, *) {
            decoder.dateDecodingStrategy = .iso8601
        }
        return decoder
    }
}

extension UIView {
    
    func rounded() {
        let halfHeightRadius = frame.height / 2
        setCornerRadius(radius: halfHeightRadius)
    }
    
    func setCornerRadius(radius: CGFloat) {
        self.clipsToBounds = true
        self.layer.cornerRadius = radius
    }
    
    func hideObject(_ duration: Double = 0.5){
        if duration > 0 {
            UIView.animate(withDuration: duration, animations: {
                self.alpha = 0
            }) { (_) in
                self.isHidden = true
            }
        } else {
            self.alpha = 0
            self.isHidden = true
        }
    }
    
    @objc
    func showObject(_ duration: Double = 0.5){
        UIView.animate(withDuration: duration, animations: {
            self.alpha = 1
            self.isHidden = false
        }) { (_) in
            
        }
    }
}
