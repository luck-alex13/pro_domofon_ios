//
//  DataStorage.swift
//  DoorWidget
//
//  Created by Алексей Агильдин on 31.03.2021.
//  Copyright © 2021 Алексей Агильдин. All rights reserved.
//

import Foundation
import UIKit

class DataStorage {
    
    static let shared = DataStorage()
    
    private init() {}
    
    private let userDefaults = UserDefaults(suiteName: "group.ProDomofonSharing")!
    private let encoder = JSONEncoder()
    private let decoder = JSONDecoder()
    
    var accessToken: String? {
        get {
            var str = userDefaults.string(forKey: #function)
            if str == nil {
                str = accsessToken
            }
            return str
        }
    }
    
    fileprivate var accsessToken: String? {
        let str = userDefaults.string(forKey: #function)
        return str
    }
    
    var contracts: [Contract] {
        guard let savedData = userDefaults.object(forKey: #function) as? Data else { return [] }
        do {
            let array = try decoder.decode([Contract].self, from: savedData)
            return array
        } catch {
            return []
        }
    }
    
    var settingsURLString: String {
        get {
            return userDefaults.string(forKey: #function) ?? UIApplication.openSettingsURLString
        }
        set {
            userDefaults.set(newValue, forKey: #function)
            userDefaults.synchronize()
        }
    }
}
