//
//  Contract.swift
//  ProDomofon
//
//  Created by Александр Новиков on 16/06/2019.
//  Copyright © 2019 Александр Новиков. All rights reserved.
//

import Foundation
import UIKit

class Contract: Codable {
    
    var id: String
    var number: Int
    var address: String?
    var flat: String
    var alias: Int
    var deviceId: String?
    var houseId: Int?
    var house: String?
    var street: String?
    var toPay: Int
    var balance: Int?
    var userId: String
    var companyId: String
    var noCaptchaConfirmHash: String
    var allowMoreKeys: Int
    var closed: Bool
    var pipe: Bool
    var owners: Int
    var juridical: Bool
    var confirmed: Bool
    
    //var customNamesOfPanel: [ContractPanelName]
    
    enum CodingKeys: String, CodingKey {
        case id = "_id"
        case number, address, flat, alias, deviceId, houseId, house, street, toPay, balance, userId, companyId, noCaptchaConfirmHash, allowMoreKeys, closed, pipe, owners, juridical, confirmed
    }
    
    init(id: String, number: Int, address: String?, flat: String, alias: Int, deviceId: String?, houseId: Int?, house: String?, street: String?, toPay: Int, balance: Int?, created: String, userId: String, companyId: String, noCaptchaConfirmHash: String, allowMoreKeys: Int, closed: Bool, pipe: Bool, owners: Int, juridical: Bool, confirmed: Bool) {
        self.id = id
        self.number = number
        self.address = address
        self.flat = flat
        self.alias = alias
        self.deviceId = deviceId
        self.houseId = houseId
        self.house = house
        self.street = street
        self.toPay = toPay
        self.balance = balance
        self.userId = userId
        self.companyId = companyId
        self.noCaptchaConfirmHash = noCaptchaConfirmHash
        self.allowMoreKeys = allowMoreKeys
        self.closed = closed
        self.pipe = pipe
        self.owners = owners
        self.juridical = juridical
        self.confirmed = confirmed
    }
    
    var getReadableAddress: String {
        var addr = ""
        if let street = street {
            addr = "\(street), "
        }
        if let house = house {
            addr += "\(house) "
        }
        
        addr += "кв. \(flat) ";
        if(alias != 0 && alias != Int(flat)) {
            addr = addr + "(\(alias))"
        }
        return addr
    }
    
}
