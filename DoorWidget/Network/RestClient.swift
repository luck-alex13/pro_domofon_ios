//
//  RestClient.swift
//  DoorWidget
//
//  Created by Александр Новиков on 23/09/2019.
//  Copyright © 2019 Александр Новиков. All rights reserved.
//

import Foundation
import Alamofire
import Reqres
import RxSwift

class RestClient {
    
    var sessionManager: SessionManager
    
    init() {
        let configuration: URLSessionConfiguration
        
        Reqres.logger.logLevel = .verbose
        configuration = Reqres.defaultSessionConfiguration()
        
        configuration.httpAdditionalHeaders = SessionManager.defaultHTTPHeaders
        configuration.requestCachePolicy = .reloadIgnoringLocalCacheData
        self.sessionManager = SessionManager(configuration: configuration)
        self.sessionManager.adapter = MyHeadersAdapter(accessToken: nil)
    }
    
    func updateToken(newToken: String) {
        self.sessionManager.adapter = MyHeadersAdapter(accessToken: newToken)
    }
    
    func makeRequest(to url: URLRequestConvertible) -> DataRequest {
        return self.sessionManager.request(url)
    }
    
    func makeRequestAndValidate(to url: URLRequestConvertible) -> DataRequest {
        return makeRequest(to: url).validate()
    }
    
    func openDoor(contractNumber: Int, reason: String, success: @escaping (OpenDoorResponse) -> Void, errorCall: @escaping (ErrorBody) -> Void) {
        let params = [
            "contract" : contractNumber,
            "reason" : reason
            ] as Parameters
        
        makeRequestAndValidate(to: ApiRouter.openDoor(parameters: params)).responseDecodable { (resp) in
            resp.handleResponse(onSuccess: success, onError: errorCall)
        }
    }
}

// MARK: - Alamofire response handlers

extension DataRequest {
    
    func decodableResponseSerializer<T: Decodable>() -> DataResponseSerializer<T> {
        return DataResponseSerializer { _, response, data, error in
            guard error == nil else { return .failure(error!) }
            
            guard let data = data else {
                return .failure(AFError.responseSerializationFailed(reason: .inputDataNil))
            }
            
            return Result { try JSONDecoder().decode(T.self, from: data) }
        }
    }
    
    @discardableResult
    func responseDecodable<T: Decodable>(queue: DispatchQueue? = nil, completionHandler: @escaping (DataResponse<T>) -> Void) -> Self {
        return response(queue: queue, responseSerializer: decodableResponseSerializer(), completionHandler: completionHandler)
    }
}

extension DataResponse {
    
    func handleResponse(onSuccess: @escaping (Value) -> Void, onError: @escaping (ErrorBody) -> Void) {
        if let _ = self.error {
            handleError(self, callback: onError)
        }else {
            onSuccess(self.value!)
        }
    }
    
    func handleResponse(onSuccess: @escaping () -> Void, onError: @escaping (ErrorBody) -> Void) {
        if let _ = self.error {
            handleError(self, callback: onError)
        }else {
            onSuccess()
        }
    }
    
    func handleError<T>(_ data: DataResponse<T>, callback: @escaping (ErrorBody) -> Void) {
        guard let response = data.response else {
            callback(ErrorBody(name: "Unknown error", message: data.error!.localizedDescription, code: nil, status: nil))
            print("Failed Request \(data.error!.localizedDescription))");
            return
        }
        NSLog("Failed Request to: \(String(describing: response.url!))")
        let err = data.error!
        do {
            let error = try ErrorBody(data: data.data!)
            if error.status == nil {
                error.status = response.statusCode
            }
            if let typeMismatch = err as? DecodingError {
                error.message = typeMismatch.localizedDescription
            }
            NSLog((try error.jsonString())!)
            debugPrint(err)
            callback(error)
        }catch {
            guard let string = String(data: data.data!, encoding: .utf8) else {
                NSLog(err.localizedDescription)
                callback(ErrorBody(name: "Unknown error", message: err.localizedDescription, code: nil, status: response.statusCode))
                return
            }
            debugPrint(error)
            callback(ErrorBody(name: "Unknown error", message: err.localizedDescription, code: nil, status: response.statusCode))
        }
    }
}


