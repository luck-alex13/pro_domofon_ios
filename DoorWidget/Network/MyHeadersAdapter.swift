//
//  MyHeadersAdapter.swift
//  DoorWidget
//
//  Created by Александр Новиков on 23/09/2019.
//  Copyright © 2019 Александр Новиков. All rights reserved.
//

import Foundation
import Alamofire

class MyHeadersAdapter: RequestAdapter {
    
    static let HEADER_TOKEN = "Authorization"
    static let REQUEST_SOURCE = "Request-Source"
    static let APP_VERSION = "app-version";
    
    private var accessToken: String?
    
    init(accessToken: String?){
        if let token = accessToken {
            self.accessToken = "Bearer \(token)"
        }
    }
    
    func adapt(_ urlRequest: URLRequest) throws -> URLRequest {
        
        var urlRequest = urlRequest
        
        urlRequest.setValue(self.accessToken, forHTTPHeaderField:  MyHeadersAdapter.HEADER_TOKEN)
        urlRequest.setValue("IOS", forHTTPHeaderField:  MyHeadersAdapter.REQUEST_SOURCE)
        urlRequest.setValue(Bundle.main.buildNumber, forHTTPHeaderField:  MyHeadersAdapter.APP_VERSION)
        
        return urlRequest
    }
}
