//
//  ApiRouter.swift
//  DoorWidget
//
//  Created by Александр Новиков on 23/09/2019.
//  Copyright © 2019 Александр Новиков. All rights reserved.
//

import Alamofire

enum ApiRouter: URLRequestConvertible {
    
    case openDoor(parameters: Parameters?)
    
    func baseUrl() -> String {
        return "https://domofon.dom38.ru/api"
        //return "https://dev-domofon.dom38.ru/api"
    }
    
    func fullApiUrl() -> String {
        return self.baseUrl() + self.path
    }
    
    func method() -> HTTPMethod {
        switch self {
        case .openDoor:
            return .post
        }
    }
    
    var path: String {
        switch self {
        case .openDoor:
            return "/contracts/opendoor"
        }
    }
    
    func asURLRequest() throws -> URLRequest {
        let url = try self.baseUrl().asURL()
        
        var urlRequest = URLRequest(url: url.appendingPathComponent(path))
        urlRequest.httpMethod = self.method().rawValue
        
        switch self {
        case .openDoor(let parameters):
            urlRequest = try URLEncoding.default.encode(urlRequest, with: parameters)
        default:
            break
        }
        
        return urlRequest
    }
    
}
