//
//  OpenDoorResponse.swift
//  DoorWidget
//
//  Created by Александр Новиков on 23/09/2019.
//  Copyright © 2019 Александр Новиков. All rights reserved.
//

import Foundation

class OpenDoorResponse: Codable {
    
    var type, message: String?
    
    init(type: String?, message: String?) {
        self.type = type
        self.message = message
    }
}

// MARK: Convenience initializers and mutators

extension OpenDoorResponse {
    convenience init(data: Data) throws {
        let me = try JSONDecoder().decode(OpenDoorResponse.self, from: data)
        self.init(type: me.type, message: me.message)
    }
    
    convenience init(_ json: String, using encoding: String.Encoding = .utf8) throws {
        guard let data = json.data(using: encoding) else {
            throw NSError(domain: "JSONDecoding", code: 0, userInfo: nil)
        }
        try self.init(data: data)
    }
    
    convenience init(fromURL url: URL) throws {
        try self.init(data: try Data(contentsOf: url))
    }
    
    
    func jsonData() throws -> Data {
        return try newJSONEncoder().encode(self)
    }
    
    func jsonString(encoding: String.Encoding = .utf8) throws -> String? {
        return String(data: try self.jsonData(), encoding: encoding)
    }
}
